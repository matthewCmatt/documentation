---
layout: default
title: Camera IMU Coordinate Frames
search_exclude: true
parent: Deprecated
nav_order: 22
permalink: /camera-imu-coordinate-frames/
---

# Camera and IMU Coordinate Frames
{: .no_toc }

This page is deprecated, please see [configure-extrinsics](/configure-extrinsics/).
