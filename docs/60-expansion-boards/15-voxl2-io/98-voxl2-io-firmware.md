---
layout: default
title: VOXL 2 IO Firmware
parent: VOXL 2 IO
nav_order: 98
has_children: false
permalink: /voxl2-io-firmware/
---

# VOXL 2 IO Firmware / Bootloader
{: .no_toc }

## How to Tell What FW Version You Have

The **current** version which uses a VOXL ESC based firmware has LED pattern like this upon power up:

<iframe width="560" height="315" src="https://www.youtube.com/embed/MFhsogbSmZQ?si=M86Kfquvm2Q2yplh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

The **legacy** version which uses PX4IO has LED pattern like this upon power up:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nG__wp8szCA?si=HUvaExB5r_YztgSR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>

To update from the legacy version to the new version, see the section below.

If you would like to get a replacement by sending your hardware in, please contact us.

## Release Notes

### VOXL SDK 1.1.1

- shipping VOXL 2 IO (`M0065`) firmware version: TODO
- Support for 4 channel PWM output
- Support for SBUS radio in

## How to Update Firmware

### Using VOXL 2 Command Line Tools

TODO

### Using STLink

## How to Update Bootloader

### Using STLink

TODO

## How to Update Legacy Firmware to New Firmware

### Send into ModalAI

We strive to enable in field updates, but when a bootloader update is required, sometimes lower level tools are needed.  If you are do not want to deal with in field update, you can send in your existing hardware to be updated without charge.

### Using STLink

TODO