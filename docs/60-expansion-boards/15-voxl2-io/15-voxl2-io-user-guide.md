---
layout: default
title: VOXL 2 IO User Guide
parent: VOXL 2 IO
nav_order: 15
has_children: false
permalink: /voxl2-io-user-guide/
---

# VOXL 2 IO User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

<img src="/images/voxl2-io/user-guide/M0065-hero.png" width="240"/>

## Summary

### VOXL SDK Support

- This documentation requires VOXL SDK 1.1.1 or newer.
- For documentation on the legacy PX4IO/VOXL SDK 1.1.0 and older usage, see [here](/voxl2-io-legacy-guide/)

### Important Update

Starting in VOXL SDK 1.1.1, the VOXL 2 IO system is being updated, with changes including:

- a new bootloader based on same bootloader as VOXL ESC
- a new firmware based on same firmware as VOXL ESC
- a new `voxl-px4` driver `voxl2-io` to support new firmware

Existing VOXL 2 IO may be updated but requires a bootloader update over the debug header.  This can be done in the field, or contact ModalAI to arrange a swap out if needed.

For updating the legacy firmware, please see the [firmware guide](/voxl2-io-firmware/).

## System Overview

In general, VOXL 2 IO provides PWM outputs along with additional RC inputs for the VOXL 2 / VOXL 2 Mini ecosystem.

### Block Diagram

<img src="/images/voxl2-io/user-guide/M0065-system.jpg" width="640"/>

### Components

The following components describe the VOXL 2 IO system:

- VOXL 2 IO Hardware (M0065)
  - voxl2-io firmware - application running on hardware that communicates with voxl-px4
  - voxl2-io bootloader - used for firmware updates over UART

- Hardware connection for power and 2-wire UART

- VOXL 2 (M0054) or VOXL 2 Mini (M0104)
  - VOXL SDK 1.1.1 or newer
    - voxl-px4 package
      - voxl2-io PX4 driver that communicates with VOXL 2 IO application
  - Command line tools for VOXL 2 IO (e.g. FW update)

## Wiring Guides

**NOTE** Connector colors may vary due to supply chain variations.

### Host Connection

#### VOXL 2 Host (M0054)

Using the default DSP UART QUP2 on J18:

<img src="/images/voxl2-io/user-guide/M0054-J18-M0065.jpg" width="640"/>

|           |           |
|-----------|-----------|
| Connectors | VOXL 2 `J18` to VOXL 2 IO `J4` |
| Cable     | [MCBL-00015](/cable-datasheets/#mcbl-00015) - 4pin-JST-GH-to-4pin-JST-GH cable, [Buy](https://www.modalai.com/products/mcbl-00015) |
| PX4 Param | `VOXL2_IO_PORT` = `2`         |

Note: the UART on J19 pins 10/11 may be used (QUP7) instead if required.  Note PX4 configuration is required.


#### VOXL 2 Mini Host (M0104)

Using the default DSP UART QUP2 on J19 pins 7/8:

<img src="/images/voxl2-io/user-guide/M0104-J19-M0065.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 `J19` to VOXL 2 IO `J4` |
| Cable      | TODO |
| PX4 Param  | `VOXL2_IO_PORT` = `2`         |

Note: the UART on J19 pins 10/11 may be used (QUP7) instead if required.  Note PX4 configuration is required.

### RC Connections

#### SBUS RC

##### FrSky R-XSR

<img src="/images/voxl2-io/user-guide/M0065-rc-sbus-r-xsr.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J3` to R-XSR |
| Cable      | [MCBL-00065](/cable-datasheets/#mcbl-00065) |

##### Graupner GR-16

<img src="/images/voxl2-io/user-guide/M0065-rc-sbus-gr-16.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J3` to GR-16 |
| Cable      | [MCBL-00064](/cable-datasheets/#mcbl-00064) |

### PWM Out Connection

<img src="/images/voxl2-io/user-guide/M0065-pwm.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J1` to [M0022](https://modalai.com/collections/accessories/products/flight-core-pwm-output-cable-and-break-out-board) PWM Breakout Board |
| Cable      | [MCBL-00004](/cable-datasheets/#mcbl-00004) |


## VOXL PX4 Configuration

### Actuator Setup using QGC

The **Actuators** tab in QGroundControl will display a **VOXL 2 IO** tab that can be used to configure and test the outputs.

<img src="/images/voxl2-io/user-guide/M0065-qgc-actuator.png">

### Actuator Testing Using Command Line

If you have an adb connection to VOXL2, you can, for example, spin motor 1 at 10% for 1 second using the following command:

```
px4-actuator_test -m 1 -v 0.1 -t 1
```

### PX4 Parameters

TODO: update

| Name               | Description                                                                                                  | Min > Max (Incr.) | Default      | Units    |
|--------------------|--------------------------------------------------------------------------------------------------------------|-:-:---------------|-:-:----------|-:-:------|
| `VOXL2_IO_CONFIG`  | VOXL 2 IO Configuration <br>**Values:** <br> - 0: Disabled <br> - 1: TODO <br><br> **Reboot Required**       |                   | 0 (Disabled) |          |

### How to Perform PWM Calibration

TODO

## Supported Use Cases

VOXL SDK 1.1.1 or newer required for the following use cases.

### VOXL 2 standard RC input and PWM Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-ELRS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage       | PX4 Driver |
|------------------|-------------|------------|
| J18              | PWM Out     | `voxl2_io` |
| J19              | RC, ELRS    | `crsf_rc`  |

#### VOXL 2 Mini (M0104)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0104-M0065-ELRS-PWM.jpg" width="640"/>

| VOXL 2 Mini Connector | Usage       | PX4 Driver |
|------------------|-------------|------------|
| J19              | PWM Out     | `voxl2_io` |
| J19              | RC, ELRS    | `crsf_rc`  |

### VOXL 2 IO S.BUS input and PWM Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-SBUS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|------------------|---------------------|------------|
| J18              | PWM Out, SBUS RC in | `voxl2_io` |

#### VOXL 2 Mini (M0104)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0104-M0065-SBUS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|------------------|---------------------|------------|
| J19              | PWM Out, SBUS RC in | `voxl2_io` |

### VOXL 2 IO S.BUS input and 4-in-1 UART ESC Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-M0134.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|------------------|---------------------|------------|
| J18              | 4-in-1 ESC          | `voxl_esc` |
| J19              | SBUS RC in          | `voxl2_io` |
