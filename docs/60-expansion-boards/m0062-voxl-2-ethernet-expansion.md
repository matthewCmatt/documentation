---
layout: default3
title: M0062 VOXL 2 Ethernet Expansion and USB Hub
nav_order: 17
parent: Expansion Boards
has_children: true
permalink: /m0062-2/
buylink: https://www.modalai.com/products/m0062-2
summary: VOXL 2 Developer Test Board (M0144)
---

# VOXL 2 Ethernet Expansion and USB Hub

{: .no_toc }

