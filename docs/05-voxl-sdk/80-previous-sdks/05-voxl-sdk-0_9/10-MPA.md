---
layout: default
title: Modal Pipe Architecture 0.9
search_exclude: true
nav_order: 5
parent: VOXL SDK 0.9
has_children: true
permalink: /mpa-0_9/
---

# Modal Pipe Architecture (MPA)
{: .no_toc }



![voxl-sdk-map](/images/voxl-sdk/voxl-sdk-mpa.png)

The vast majority of VOXL [services](/mpa-services/), [tools](/inspect-tools/), and [utilities](/sdk-utilities/) require some inter-process communication to function. For example, [voxl-qvio-server](/voxl-qvio-server/) consumes camera data from [voxl-camera-server](/voxl-camera-server/) and IMU data from [voxl-imu-server](/voxl-imu-server/) to provide [Visual Inertial Odometery VIO](/vio/). We use POSIX pipes as the underlying transport mechanism for this inter-process communication due to their robustness, efficiency, and portability. This structure is called Modal Pipe Architecture, or simply MPA.

To provide standardization and ease of use, all [MPA services](/mpa-services/) use the C/C++ library [libmodal_pipe](/libmodal-pipe/) to create, publish, and subscribe to MPA data.

