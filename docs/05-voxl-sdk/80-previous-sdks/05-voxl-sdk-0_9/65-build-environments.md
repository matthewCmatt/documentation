---
layout: default
title: Build Environments 0.9
search_exclude: true
parent: VOXL SDK 0.9
nav_order: 65
has_children: true
has_toc: true
permalink: /build-environments-0_9/
---


# Build Applications for VOXL, VOXL 2 and RB5 Flight

This page describes how to build applications for VOXL using the same tools and techniques ModalAI builds the voxl-sdk with.

The [voxl-docker](https://gitlab.com/voxl-public/voxl-docker) project provides setup instructions for two docker images which provide build environments for the VOXL's ARM applications processor and Hexagon SDSP. It also provides the "voxl-docker" script for easily launching these docker images. These two docker images are prerequisites for building the majority of open-source projects on https://gitlab.com/voxl-public

See [Installing voxl-docker](/install-voxl-docker/) for instructions on installing and using these docker images.


## voxl-cross for cross-compiling applications

### VOXL and VOXL Flight building ARMv7 (32-bit) or ARMv8 (64-bit) applications-processor projects

The voxl-cross Docker is configured with the proper cross-compiler to compile ARMv7 (32-bit) and ARMv8 (64-bit) code for the VOXL platform. The majority of MPA projects are built in voxl-cross except where Qualcomm-specific 32-bit libraries are required in which case voxl-emulator is used.

### VOXL 2 and RB5 Flight building ARMv8 (64-bit) applications-processor projects

The voxl-cross Docker is configured with the proper cross-compiler to compile ARMv8 (64-bit) code for the VOXL 2 and RB5 Flight platforms.

## voxl-emulator for building applications-processor projects

We provide a Docker image in .tar format which contains the root file system that runs on VOXL itself. Through QEMU emulation and Docker, the ARM binaries in the VOXL rootFS can run on a desktop computer aiding development and speeding up compilation time. Anything that can be compiled onboard VOXL should be able to be compiled in voxl-emulator.

## rb5-flight-px4-build-docker for VOXL 2 PX4 Development

Please see instructions for building PX4 for the VOXL 2 Flight DSP in the [VOXL PX4 Developer Guide](/voxl-px4-developer-guide/)


## voxl-hexagon for building hexagon DSP projects

### VOXL and VOXL Flight

The voxl-hexagon docker image is based on the x86_64 Ubuntu Bionic docker image but additionally contains the Qualcomm Hexagon SDK 3.1 and an ARM cross compiler. For legal reasons these components must be downloaded from their respective sources by the user before building the docker image. However, we provide instructions and an install script here in this project.

### VOXL 2

Pending

# How to develop for different components of the SoC

## How to build for CPU Applications Processor

VOXL uses a Snapdragon 821 with an ARMv8-A 64-bit Qualcomm® Kyro Quad Core CPU.

RB5 Flight and M0054 use a QRB5165 with an ARMv8 8-core mix of Cortex-A77 and Cortex-A55.

### Background

VOXL uses a 64-bit linux kernel, however most of the core libraries on VOXL are 32-bit and are built using GCC4.9. Most applications running natively on VOXL have to be built with that same version of compiler. GCC4.9 toolchain that comes with VOXL is only capable of building 32-bit applications, however building and running 64-bit applications is still possible and recommended for maximum performance benefit. Most VOXL services have been migrated to 64-bit.

RB5 Flight and M0054 are 64-bit native, voxl-cross should be used for that development and only 64-bit binaries are supported. (*MORE DETAILS NEEDED*)

Follow the instructions to install voxl-docker and the [voxl-cross](https://docs.modalai.com/voxl-cross/) docker image [here](https://docs.modalai.com/install-voxl-docker/).

### Build 64-bit applications

The [voxl-cross](https://docs.modalai.com/voxl-cross/) Docker can be used to cross-compile 64-bit applications using GCC 4.9

Alternatively, a 64-bit docker image, such as, `arm64v8/ubuntu` or `arm64v8/ros` can also build and run 64-bit application in a Docker container directly on VOXL.

Our recommended compiler flags for 64-bit are as follows - specifying fpu flags is optional :

```
-march=armv8-a
```

Examples:

| Target   | Name          | Description                                                                                                                                               | Link                                                                                                  |
|----------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| VOXL CPU | libmodal_pipe | Core MPA library for data passing over posix pipes contains both 32-bit and 64-bit build scripts using [voxl-cross](https://docs.modalai.com/voxl-cross/) | [Source](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe/-/blob/master/build.sh) |


### Build 32-bit applications on VOXL or using voxl-emulator docker

The [https://docs.modalai.com/install-voxl-docker/](voxl-emulator) builds 32-bit applications and is easy to use for quick ports of existing applications. 32-bit applications are only needed if direct access to 32-bit hardware drivers are required (camera and IMU).

Our recommended compiler flags for 32-bit architecture are as follows. Note that it is *very important* to specify correct fpu flags, otherwise the NEON engine will not be used for floating-point computations, significantly slowing down floating-point math.

```
-march=armv7-a -mfloat-abi=softfp -mfpu=neon-vfpv4
```


## How to build for DSP

There are two DSPs available for programming on VOXL (Snapdragon 821). The Applications DSP (aDSP) and the Sensors DSP (sDSP or SLPI)

The aDSP is a powerful digital signal processor to offload signal processing tasks from the CPU. It is most capable performing fixed-point computer vision compute offload.

The sDSP is well suited to work with I/O and has a real-time operating system QuRT.

Follow the instructions to install voxl-docker and the [voxl-hexagon](https://docs.modalai.com/voxl-hexagon/) docker image [here](https://docs.modalai.com/install-voxl-docker/).

### VOXL Examples

Examples of how to write code for the Applications DSP, Qualcomm® Hexagon™ 680 with HVX:

| Target    | Name         | Description                                                                | Link                                                                                 |
|-----------|--------------|----------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| VOXL aDSP | version-test | Simple application to grab information from the aDSP                       | [GitLab](https://gitlab.com/voxl-public/adsp-proc-examples/tree/master/version-test) |
| VOXL sDSP | Hello World  | Simple hello world application to build and run code on the sDSP processor | [GitLab](https://gitlab.com/voxl-public/sdsp-proc-examples/tree/master/helloworld)   |
| VOXL sDSP | libvoxl-io   | Talk to serial ports through the sDSP                                      | [GitLab](https://gitlab.com/voxl-public/libvoxl_io)                                  |


### RB5 Flight / M0054 Examples

Examples of how to write code for the Qualcomm® Hexagon™ Sensors Low Power Island:

| Target          | Name | Description           | Link                                                                     |
|-----------------|-:----|-:---------------------|-:------------------------------------------------------------------------|
| RB5 Flight sDSP | PX4  | PX4 Flight Controller | [Github](https://github.com/modalai/px4-firmware/tree/eric-m0051-master) |


## DSP Development Tools

### mini-dm

mini-dm is a diag log tool for debugging the Hexagon DSP that comes with [Hexagon SDK](https://developer.qualcomm.com/software/hexagon-dsp-sdk).

To configure on Ubuntu:

```lsusb``` and find the Qualcomm entry.
VOXL's device ID is
```05c6:901d Qualcomm, Inc.```

Edit /etc/udev/rules.d/70-android.rules and add the following line:
```SUBSYSTEM=="usb",ATTRS{idVendor}=="05c6",ATTRS{idProduct}=="901d",MODE=="0666"```

To run:

```$ ./Qualcomm/Hexagon_SDK/3.3.3/tools/debug/mini-dm/Linux_Debug/mini-dm```


## How to build for GPU

### Overview

VOXL contains an embedded [Adreno 530](https://en.wikipedia.org/wiki/Adreno) GPU with 256 ALUs. This GPU is exposed through both OpenCL 1.1 and OpenGLES 3.1. The GPU can be exploited for significant algorithmic acceleration for use cases like computer vision and deep learning.

RB5 Flight and M0054 contain an embedded [Adreno 650](https://en.wikipedia.org/wiki/Adreno) with 1024 ALUs at 600MHz. 

### Examples

Examples of how to write code for the Qualcomm® Adreno™ 530 GPU

| Target   | Name                            | Description                                                          | Link                                                                                |
|----------|---------------------------------|----------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| VOXL GPU | Hello CL                        | Simple application showing how to interact with the GPU using OpenCL | [GitLab](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocl)     |
| VOXL GPU | TensorFlow Lite                 | Use TensorFlow Lite with OpenCL on VOXL                              | [Docs](https://docs.modalai.com/voxl-tflite-server/)                                |
| VOXL GPU | OpenCV Docker ARMv8 with OpenCL |                                                                      | [Code](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-opencv-opencl) |

