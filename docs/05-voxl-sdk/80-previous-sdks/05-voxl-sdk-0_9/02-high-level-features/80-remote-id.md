---
layout: default
title: Remote ID 0.9
parent: High Level Features 0.9
search_exclude: true
nav_order: 80
permalink:  /voxl-remote-id-0_9/
---

# Remote ID
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Introduction

## Video Quickstart

{% include youtubePlayer.html id="7a5b3oV4hT4" %}

## Summary

The [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) is a VOXL SDK based service that be used to facilitate remote ID compliance on VOXL2 only.

There are various use cases and radio types depending one end user requirements, thus there are multiple use cases captured below.

![voxl-remote-id-use-case-1](/images/voxl-sdk/voxl-px4/voxl-remote-id-use-case-1.png)

## Supported PX4 Versions

### QRB5165 (VOXL 2 / RB5 Flight)

`voxl-remote-id` requires `voxl-px4` 1.12.30 or newer to function on the QRB5165 based platforms.

### AQP8096 (VOXL 1 / VOXL-Flight)

`voxl-remote-id` requires ModalAI built [PX4 1.13.1-0.0.6](https://github.com/modalai/px4-firmware/tree/modalai-1.13) or newer to function on the APQ8096 based platforms.

## How to Enable/Disable

The PX4 parameter `COM_PREARM_ODID` can be used to enable or disable the remote ID pre-arm check by developers.

- When `COM_PREARM_ODID` is `0`, the remote ID pre-arm check is disabled and the pilot can arm the vehicle regardless of the remote ID status.
- When `COM_PREARM_ODID` is `1`, the remote ID pre-arm check is enabled and the pilot can not arm the vehicle unless there's a valid remote ID status.

For final product requiring remote ID, you would want to remove the parameter and hardcode the check on.

## Support Use Cases

| ID | Flight Controller       | Radio        | Reference | Status    |
|--- |-------------------      |--------------|---------- |---------  |
| 1  | VOXL 2                  | WiFi, SoftAP | Sentinel  | in beta   |
| 2  | VOXL-Flight, VOXL + FC  | WiFi, SoftAP | M500      | in beta   |

Please send use case requests [here](https://forum.modalai.com/category/29/feature-requests)!

# User Guide

## Beta Release Limitation - Install Package

### Online Install

The `voxl-remote-id` is currently available for beta testing.

Locate the latest `voxl-remote-id` package from:

- QRB5165: http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/
- AQP8096: http://voxl-packages.modalai.com/dists/apq8096/dev/binary-arm64/

While connected to the internet, run the following, as an example to install `voxl-remote-id_0.0.5-202210140144_arm64.deb`

```
adb push voxl-remote-id_0.0.5-202210140144_arm64.deb /home
adb shell
cd /home 
dpkg -i voxl-remote-id_0.0.5-202210140144_arm64.deb
```

See: 

## Use Case 1 - VOXL Wi-Fi SoftAp

### Summary

- Vehicle advertises remote ID data via Wi-Fi beacons using vehicle in SoftAP mode
- PX4 runs on VOXL 2 or an external flight controller (VOXL-Flight)
- `voxl-remote-id` runs on the applications processor of VOXL/VOXL2 and a hostapd based service facilitate beacon comms
- a GPS fix is provided from PX4 to `voxl-remote-id` to send out in 

![voxl-remote-id-use-case-1](/images/voxl-sdk/voxl-px4/voxl-remote-id-use-case-1.png)

### Requirements

Hardware:
  - VOXL 2 (Sentinel reference drone)
    - Add on board supporting USB (like [M0125](/voxl2-usb3-uart-add-on-user-guide/)), with MCBL-00009
    - Wi-Fi dongle (Alpha Networks AWUS036ACS used for test)
  - or VOXL-Flight (M500 reference drone, built in WiFi)

- GPS unit (Holybro M9N GPS used for test) with a GPS fix (fix_type > 2)

Software:
- Platform Release 1.0 or newer (available for beta test in 0.9 with limitation noted below)
- `voxl-mavlink-server`, `voxl-px4` configured and running

Visualization Tools:
- QGroundControl v4.2 or newer
- Android device with [OpenDroneID OSM](https://play.google.com/store/apps/details?id=org.opendroneid.android_osm&hl=en_US&gl=US) application installed

### Network Setup

#### Enter SoftAP Mode for Beacon Broadcasting

To create a SoftAP network to enable broadcasting with SSID name `remoteId` and password `1234567890`, run the following:

```
voxl-wifi softap remoteId 1234567890
```

#### Connect Ground Station to SoftAP

Connect you groundstation machine to the network above.  Obtain the IP address from your machine.

#### Update conf file

Update `/etc/modalai/voxl-vision-px4.conf` with the IP address above in `qgc_ip` in the file.

#### Open QGroundControl

Open QGroundControl and confirm you have a telemetry connection.

### voxl-px4 Configuration

#### Beta Release Limitation - Enable using PX4 Parameter

Set the `COM_PREARM_ODID` value to `1` to enable the check.

```
voxl2:/$ px4-param set COM_PREARM_ODID 1
  COM_PREARM_ODID: curr: 0 -> new: 1
```

If you have a QGC connection open, you will see this if attempting to arm now without completing the remainder of the guide.

![voxl-px4](/images/voxl-sdk/voxl-px4/voxl-remote-id-prearm-error-1.jpg)

### Enable voxl-remote-id

Run the `voxl-configure-remote-id` command to open the configuration wizard.

*Note: see installation note above if command not found*

*Note: beta Release Limitation, the `operator registration ID` is not used.*

```
voxl2:/$ voxl-configure-remote-id
Starting Wizard

Do you want to reset the config file to factory defaults?
1) yes
2) no
#? 1
wiping old config file
INFO:    Writing new configuration file to /etc/modalai/voxl-remote-id.conf

Do you want to enable the voxl-remote-id service
1) yes
2) no
#? 1

Now we are going to do a preliminary configuration of /etc/modalai/voxl-remote-id.conf

Please enter your operator registration ID
Some regulations require operators to include their operator id in the
remote ID packets. If the rule in your area doesn't require this,
you can press enter to leave this option empty

enabling  voxl-remote-id systemd service
Created symlink /etc/systemd/system/multi-user.target.wants/voxl-remote-id.service → /etc/systemd/system/voxl-remote-id.service.
starting  voxl-remote-id systemd service
Done configuring voxl-remote-id
```

### Check voxl-remote-id Status

Run the following `systemctl status voxl-remote-id`

```
voxl2:/$ systemctl status voxl-remote-id
● voxl-remote-id.service - voxl-remote-id
   Loaded: loaded (/usr/bin/voxl-remote-id; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2022-09-06 03:22:58 UTC; 18s ago
 Main PID: 2541 (voxl-remote-id)
    Tasks: 5 (limit: 4915)
   CGroup: /system.slice/voxl-remote-id.service
           └─2541 /usr/bin/voxl-remote-id

Sep 06 03:22:58 m0054 systemd[1]: Started voxl-remote-id.
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    SN: 1814C819434028
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    Attempting to Connect to hostapd: /data/misc/wifi/hostapd/wlan0
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    Connected to hostapd: /data/misc/wifi/hostapd/wlan0
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    Connected to voxl-mavlink-server
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    Detected PX4 Mavlink SYSID 1
Sep 06 03:22:58 m0054 voxl-remote-id[2541]: INFO:    Remote ID is active
```

### Validate GPS

To get GPS status and confirm `fix_type > 2`, run the following:

- VOXL2, run `px4-listener sensor_gps`
- VOXL1, run `voxl-px4-shell listener sensor_gps`

For example:

```
px4-listener sensor_gps

TOPIC: sensor_gps
 sensor_gps_s
	timestamp: 171195446  (0.087693 seconds ago)
	time_utc_usec: 0
	lat: 327783759
	lon: -1170410696
	alt: 159712
	alt_ellipsoid: 125966
	s_variance_m_s: 0.3480
	c_variance_rad: 2.8493
	eph: 1.8870
	epv: 2.6060
	hdop: 0.9900
	vdop: 1.6100
	noise_per_ms: 105
	jamming_indicator: 167
	vel_m_s: 0.0600
	vel_n_m_s: -0.0180
	vel_e_m_s: -0.0570
	vel_d_m_s: 0.0780
	cog_rad: 0.0000
	timestamp_time_relative: 0
	heading: nan
	heading_offset: 0.0000
	fix_type: 4
	jamming_state: 0
	vel_ned_valid: True
	satellites_used: 10
```

### Ready To Arm

At this point, the vehicle is broadcasting Wi-Fi Beacons advertising remote ID data, and the vehicle is ready to arm.

# Troubleshooting

## Not in SoftAP Mode

If you run `systemctl status voxl-remote-id` and see something like this:

```
voxl:/$ systemctl status voxl-remote-id
● voxl-remote-id.service - voxl-remote-id
   Loaded: loaded (/usr/bin/voxl-remote-id; enabled; vendor preset: enabled)
   Active: active (running) since Thu 1970-01-01 02:19:30 UTC; 52 years 9 months ago
 Main PID: 2447 (voxl-remote-id)
   CGroup: /system.slice/voxl-remote-id.service
           └─2447 /usr/bin/voxl-remote-id

Jan 01 02:19:30 apq8096 systemd[1]: Started voxl-remote-id.
Jan 01 02:19:31 apq8096 voxl-remote-id[2447]: INFO:    Remote ID SN: 1814C4056553343
Jan 01 02:19:31 apq8096 voxl-remote-id[2447]: INFO:    Attempting to Connect to hostapd: /data/misc/wifi/hostapd/wlan0
```

It means that you need to put the device into SoftAP mode.  You can do that with SSID of `remoteId` and password `1234567890`:

```
voxl-wifi softap remoteId 1234567890
```

# Developer Guide

## Wireshark

### Soft AP - WiFi Beacons

In order to decode Opendrone ID packets in Wireshark, the following was done (on OSX host as example):

- Open Wireshark > About > Folders
- Locate "Global Configuration" and "Personal LUA Plugins"
- Using [this](https://github.com/opendroneid/wireshark-dissector) project, clone into those directories above
- On OSX, you need to "disconnect" from any networks to use monitor mode
- Now, monitor the WLAN interface to sniff for ODID packets:

![voxl-px4](/images/voxl-sdk/voxl-px4/voxl-remote-id-wireshark.jpg)
