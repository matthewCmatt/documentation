---
layout: default
title: Core Libs 0.9
search_exclude: true
parent: VOXL SDK 0.9
nav_order: 55
has_children: true
has_toc: true
permalink: /core-libs-0_9/
---

# Core Libs

This is the collection of core libraries that the voxl software bundle is built on. 


