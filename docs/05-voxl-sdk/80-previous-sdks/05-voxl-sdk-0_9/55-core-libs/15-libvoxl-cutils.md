---
layout: default
title: libvoxl_cutils 0.9
parent: Core Libs 0.9
search_exclude: true
nav_order: 15
permalink: /libvoxl-cutils-0_9/
---

# VOXL-CUtils
{: .no_toc }

VOXL C Utils is a small package of C/C++ Utilities for use in various packages.

## Table of Contents
{: .no_toc }

1. TOC
{:toc}

---

## Silencing Macros

The package contains the macros ```VCU_silent``` and ```VCU_silentErrors``` which can be used to silence the stdout and stderr outputs of their contents, respectively. For example:
```
VCU_silent(
	some_noisy_libaray_call();
)
```
will only print the output of stderr to the screen. You can also combine these to completely silence terminal output such as:
```
VCU_silent(
	VCU_silentErrors(
		some_very_noisy_libaray_call();
	)
)
```

## Timing functions

The package contains the functions ```VCU_time_realtime_ns``` and ```VCU_time_monotonic_ns``` to get the real and monotonic time of the system, respectively. These can be used to time the execution of code or provide timestamps of sensor readings.

## Terminal Font Manipulation

These definitions allow you to manipulate the terminal output in various ways. You can change the face/color of print statements like such: ```printf("This sentence has both " FONT_BOLD "bold" RESET_FONT " and " COLOR_RED "red" RESET_COLOR "text in it!");``` reulting in: "This sentence has both __bold__ and <span style="color:red">red</span> text in it!"

### Space Manipulation

|Name                      |Definition         |Code          |
|---                       |---                |---           |
|Clear Screen              |CLEAR_TERMINAL     | \033c        |
|Disable Line Wrap         |DISABLE_WRAP       | \033[?7l     |
|Enable Line Wrap          |ENABLE_WRAP        | \033[?7h     |
|Clear Current Line        |CLEAR_LINE         | \033[2K      |
|Move Cursor To Top Left   |GOTO_TOP_LEFT      | \033[f       |

### Face

|Name                      |Definition         |Code          |
|---                       |---                |---           |
|Reset All Font Items      |RESET_FONT         | \e[0m        |
|Bold                      |FONT_BOLD          | \e[1m        |
|Dim                       |FONT_DIM           | \e[2m        |
|Underline                 |FONT_UNDERLINE     | \e[4m        |
|Flashing Font             |FONT_BLINK         | \e[5m        |
|Invert Fore/Back Colors   |FONT_INVERT        | \e[7m        |
|Hidden Text               |FONT_HIDDEN        | \e[8m        |
|Reset Bold                |RESET_BOLD         | \e[21m       |
|Reset Dim                 |RESET_DIM          | \e[22m       |
|Reset Underline           |RESET_UNDERLINE    | \e[24m       |
|Reset Blink               |RESET_BLINK        | \e[25m       |
|Reset Invery              |RESET_INVERT       | \e[27m       |
|Reset Hidden              |RESET_HIDDEN       | \e[28m       |

### Foreground Color

|Name                      |Definition         |Code          |
|---                       |---                |---           |
|Reset Color               |RESET_COLOR        | \e[39m       |
|Black                     |COLOR_BLK          | \e[30m       |
|Red                       |COLOR_RED          | \e[31m       |
|Green                     |COLOR_GRN          | \e[32m       |
|Yellow                    |COLOR_YLW          | \e[33m       |
|Blue                      |COLOR_BLU          | \e[34m       |
|Magenta                   |COLOR_MAG          | \e[35m       |
|Cyan                      |COLOR_CYN          | \e[36m       |
|Light Gray                |COLOR_LIT_GRY      | \e[37m       |
|Dark Gray                 |COLOR_DRK_GRY      | \e[90m       |
|Light Red                 |COLOR_LIT_RED      | \e[91m       |
|Light Green               |COLOR_LIT_GRN      | \e[92m       |
|Light Yellow              |COLOR_LIT_YLW      | \e[93m       |
|Light Blue                |COLOR_LIT_BLU      | \e[94m       |
|Light Magenta             |COLOR_LIT_MAG      | \e[95m       |
|Light Cyan                |COLOR_LIT_CYN      | \e[96m       |
|White                     |COLOR_WHT          | \e[97m       |

### Background Color

|Name                      |Definition         |Code          |
|---                       |---                |---           |
|Reset Background Color    |RESET_COLOR_BKG    | \e[49m       |
|Background Black          |COLOR_BKG_BLK      | \e[40m       |
|Background Red            |COLOR_BKG_RED      | \e[41m       |
|Background Green          |COLOR_BKG_GRN      | \e[42m       |
|Background Yellow         |COLOR_BKG_YLW      | \e[43m       |
|Background Blue           |COLOR_BKG_BLU      | \e[44m       |
|Background Magenta        |COLOR_BKG_MAG      | \e[45m       |
|Background Cyan           |COLOR_BKG_CYN      | \e[46m       |
|Background Light Gray     |COLOR_BKG_LIT_GRY  | \e[47m       |
|Background Dark Gray      |COLOR_BKG_DRK_GRY  | \e[100m      |
|Background Light Red      |COLOR_BKG_LIT_RED  | \e[101m      |
|Background Light Green    |COLOR_BKG_LIT_GRN  | \e[102m      |
|Background Light Yellow   |COLOR_BKG_LIT_YLW  | \e[103m      |
|Background Light Blue     |COLOR_BKG_LIT_BLU  | \e[104m      |
|Background Light Magenta  |COLOR_BKG_LIT_MAG  | \e[105m      |
|Background Light Cyan     |COLOR_BKG_LIT_CYN  | \e[106m      |
|Background White          |COLOR_BKG_WHT      | \e[107m      |
