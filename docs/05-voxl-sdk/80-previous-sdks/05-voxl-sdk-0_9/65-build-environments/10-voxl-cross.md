---
layout: default
title: VOXL Cross Docker Image
parent: Build Environments
nav_order: 15
permalink: /voxl-cross/
---

# VOXL Cross Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

This tool lets you use the docker build environments described in the [build environments page](/build-environments/).


## Prerequisite

* [Install docker-ce](https://docs.docker.com/engine/install/ubuntu/)

## Install pre-built voxl-cross

Download the pre-built voxl-cross [voxl-cross_V2.5.tar.gz](https://developer.modalai.com/asset/download/120) from https://developer.modalai.com/

```
$ docker load -i voxl-cross_V2.5.tar.gz
$ docker tag voxl-cross:V2.5 voxl-cross:latest
$ voxl-docker -i voxl-cross
```

To build an example project for VOXL

```
$ mkdir cross-workspace
$ cd cross-workspace
$ voxl-docker -i voxl-cross
voxl-cross:~$ git clone https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json.git
voxl-cross:~$ cd libmodal-json/
voxl-cross:~$ ./build.sh qrb5165

```

## Build the voxl-cross Docker Image

If, for some reason, you would like to build the voxl-cross Docker Image yourself, you can find instructions for this at the project's README file [here](https://gitlab.com/voxl-public/support/voxl-docker)

