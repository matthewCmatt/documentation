---
layout: default
title: Setup ROS on Host PC 0.9
parent: ROS 0.9
search_exclude: true
nav_order: 05
permalink: /setup-ros-on-host-pc-0_9/
---

# Setup ROS on Host PC

*ModalAI Top Tip:* ROS is not required to use or develop with VOXL! If you don't like ROS or don't want to use ROS, then you are done with the VOXL quickstarts! You can look through the more in-depth [Developing with VOXL](/developing-with-voxl/) and [Flying with VOXL](/flying-with-voxl/) guides.

If you like ROS, let's proceed:

---

## Install

VOXL ships with ROS Indigo pre-installed. However you can use whichever distribution of ROS 1 on your host PC. This will probably be ROS Melodic on Ubuntu 18.04. Follow the ROS wiki instructions to install `ros-melodic-desktop-full`  here: <http://wiki.ros.org/melodic/Installation/Ubuntu>

## Environment Setup Script

To maintain consistency with the instructions for setting up ROS on the VOXL we will set up a similar `my_ros_env.sh` script on the host pc:

```bash
touch ~/my_ros_env.sh
echo ". ~/my_ros_env.sh" >> ~/.bashrc
```

Then paste the following into your `my_ros_env.sh` script. This is where you will edit IP addresses as necessary.

``` bash
#!/bin/bash
#
# Script loads ROS envrionment variables and sets IP addresses

# load main ros environment
if [ -f /opt/ros/melodic/setup.bash ]; then
    source /opt/ros/melodic/setup.bash
elif [ -f /opt/ros/kinetic/setup.bash ]; then
    source /opt/ros/kinetic/setup.bash
elif [ -f /opt/ros/indiego/setup.bash ]; then
    source /opt/ros/indigo/setup.bash
fi

# if a catkin workspace is setup then make sure the launch
# files and run files are available in the ROS PATH
if [ -f ~/catkin_ws/devel/setup.bash ]; then
    source ~/catkin_ws/devel/setup.bash
fi
if [ -f ~/catkin_ws/install/setup.bash ]; then
    source ~/catkin_ws/install/setup.bash
fi

# ignore ROS_HOSTNAME, it only causes problems and overrides ROS_IP
unset ROS_HOSTNAME

# configure ROS IPs HERE
export ROS_MASTER_IP=192.168.8.1
export ROS_IP=192.168.8.91

# ROS_MASTER_URI is derived from ROS_MASTER_IP
export ROS_MASTER_URI=http://${ROS_MASTER_IP}:11311/

## bonus alias to ssh into ros master
alias sshh='sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${ROS_MASTER_IP}'
```

The script above provides an alias 'sshh' which allows you to ssh remotely into your VOXL using default credentials. Update `ROS_MASTER_IP` and `ROS_IP` to match your network configuration. On your host machine, also be sure to update `ROS_MASTER_IP` and `ROS_IP` to match this VOXL IP address.

You will need to install sshpass to use the aforementioned alias.

```bash
# ON HOST PC
$ sudo apt install sshpass
```

## Troubleshooting

`Note:` Due to SSH environment variable issues using the default terminal, we recommend that users with host Ubuntu machine versions newer than Ubuntu 14.04 use XTerm (not the default terminal) to connect to VOXL via SSH while using ROS.


## Next Steps

Now you can [setup ROS on [VOXL](/setup-ros-on-voxl/) or [VOXL 2](/setup-ros-on-voxl-2/).
