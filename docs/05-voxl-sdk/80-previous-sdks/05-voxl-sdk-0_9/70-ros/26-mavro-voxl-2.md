---
layout: default
title: How to Run MAVROS on VOXL 2 with PX4 0.9
parent: ROS 0.9
search_exclude: true
nav_order: 25
has_children: false
permalink: /mavros-voxl-2-0_9/
youtubeId: zTeWbBnCPuo
---

# How to Run MAVROS on VOXL 2 with PX4
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Tutorial Video 

{% include youtubePlayer.html id=page.youtubeId %}

## Overview

This tutorial demonstrates how to fly a VOXL2 / RB5 Flight based drone autonomously in a figure 8, using MAVROS.

For this setup, MAVROS is installed and runs on VOXL 2 / RB5 Flight. Additionally, the `voxl-px4`, `voxl-vision-px4` and `voxl-mavlink-server` background services will be running.  This provides the PX4 flight controller, Visual Inertial Odometry (VIO) for PX4, and manages the mavlink telemetry between the ground control station remotely and ROS locally.

{: .alert .danger-alert}
**WARNING:** At the end of this tutorial, your drone will be flying autonomously. Please only run this in a **safe location** away from people and **wear protective eyewear!!**

## Block Diagram

![sw-mavros](/images/voxl-sdk/ros/sw-mavros.png)

## Prerequisites

### Install ROS Melodic

- To install software using `apt`, please have vehicle [connected to the internet](/voxl2-networking-user-guide/)
- Install ROS Melodic on VOXL 2 following [this guide](/setup-ros-on-voxl-2/)

### Install MAVROS Dependencies

- Install the following packages MAVROS packages:

```bash
apt-get install ros-melodic-mavros ros-melodic-mavros-extras ros-melodic-control-toolbox
```

- Install `GeographicLib dataset`:

```
cd /opt/ros/melodic/lib/mavros 
./install_geographiclib_datasets.sh
```

### Configure voxl-vision-px4 localhost

Use `voxl-inspect-services` to validate `voxl-vision-px4` is enabled.

Edit the configuration file:

```bash
vi /etc/modalai/voxl-vision-px4.conf
```

We will check the following
- `qgc_ip` - should be your host PCs IP address where the Ground Control Station is
- `en_localhost_mavlink_udp` - set to true to allow UDP MAVLink comms on localhost
- `offboard_mode` - set to "off" since we will not use VVPX4 for offboard mode, but use MAVROS instead

Validate your configuration file

```bash
  "qgc_ip":                     "X.X.X.X",
  "en_localhost_mavlink_udp":   true,
  ...
  "offboard_mode":              "off"
```

### Configure voxl-mavlink-server

Use `voxl-inspect-services` to validate `voxl-mavlink-server` is enabled.

## mavros_test Project (simple-example)

### Checklist

- Make sure your drone can arm
- Have RC transmitter bound to vehicle with 'offboard' and 'manual' mode
- This `simple-example` can be done on the bench **WITHOUT PROPS**

### Overview

This `simple-example` branch demo project:

- waits for `offboard` mode
- arms the vehicle
- disarms the vehicle

### Build

On VOXL 2 / RB5 Flight, clone the `simple-example` branch of the `mavros_test` project.  In this example, we're cloning to `/home`:

```bash
cd /home
git clone -b simple-example https://gitlab.com/voxl-public/support/mavros_test.git
```

Build the project:


```bash
cd /home/mavros_test
./build.sh

Base path: /home/mavros_test/catkin_ws
Source space: /home/mavros_test/catkin_ws/src
Build space: /home/mavros_test/catkin_ws/build
Devel space: /home/mavros_test/catkin_ws/devel
Install space: /home/mavros_test/catkin_ws/install
Creating symlink "/home/mavros_test/catkin_ws/src/CMakeLists.txt" pointing to "/opt/ros/melodic/share/catkin/cmake/toplevel.cmake"
...
```

### Configure ROS ENV

The `ros_environment.sh` file contains environment variables used by ROS.

Update the `ROS_IP` value to use your IP address on target (you can use `ifconfig`):

```
more /home/mavros_test/ros_environment.sh

#!/bin/bash
#

source /opt/ros/kinetic/setup.bash
source catkin_ws/install/setup.bash
unset ROS_HOSTNAME

# configure ROS IPs here
export ROS_MASTER_IP=127.0.0.1
export ROS_IP=192.168.8.1
export ROS_MASTER_URI=http://${ROS_MASTER_IP}:11311/
# mavros needs to know what PX4's system id is
export PX4_SYS_ID=1
```

### Launch the simple-example Demo

You can do this on bench top if you'd like, but **REMOVE THE PROPELLORS** as this will arm the drone!

- Open an adb shell
- Run the following to launch the program

```
cd /home/mavros_test

./run_mavros_test.sh
```

You should get an output that ends similar to this:

```
...
 INFO] [1663886683.264138601]: MAVROS started. MY ID 1.240, TARGET ID 1.1
[ INFO] [1663886683.264246359]: udp0: Remote address: 127.0.0.1:38711
[ INFO] [1663886683.264606406]: IMU: Attitude quaternion IMU detected!
[ INFO] [1663886683.265673318]: IMU: High resolution IMU detected!
[ WARN] [1663886683.267312670]: GP: No GPS fix
[ INFO] [1663886683.313571850]: CON: Got HEARTBEAT, connected. FCU: PX4 Autopilot
[ INFO] [1663886683.315295002]: IMU: High resolution IMU detected!
[ INFO] [1663886683.378609250]: IMU: Attitude quaternion IMU detected!
[ INFO] [1663886684.337418988]: GF: Using MISSION_ITEM_INT
[ INFO] [1663886684.337929605]: RP: Using MISSION_ITEM_INT
[ INFO] [1663886684.338302621]: WP: Using MISSION_ITEM_INT
[ INFO] [1663886684.338667459]: VER: 1.1: Capabilities         0x000000000000e4ef
[ INFO] [1663886684.339006205]: VER: 1.1: Flight software:     01041040 (33795a0a30000000)
[ INFO] [1663886684.339548645]: VER: 1.1: Middleware software: 01041040 (33795a0a30000000)
[ INFO] [1663886684.339894838]: VER: 1.1: OS software:         04137dff (e7701b9e792e3101)
[ INFO] [1663886684.340202177]: VER: 1.1: Board hardware:      00000001
[ INFO] [1663886684.340527172]: VER: 1.1: VID/PID:             0000:0000
[ INFO] [1663886684.340860813]: VER: 1.1: UID:                 3044494530303030
[ WARN] [1663886684.341231173]: CMD: Unexpected command 520, result 0
[ INFO] [1663886687.929310850]: waiting for offboard mode
[ INFO] [1663886693.315015302]: HP: requesting home position
[ INFO] [1663886696.836791903]: RC_CHANNELS message detected!
[ INFO] [1663886698.315995312]: RP: mission received
[ INFO] [1663886698.317024880]: GF: mission received
[ INFO] [1663886698.317390396]: WP: mission received
[ INFO] [1663886703.314904662]: HP: requesting home position
[ INFO] [1663886707.538530390]: VER: 1.1: Capabilities         0x000000000000e4ef
[ INFO] [1663886707.538779396]: VER: 1.1: Flight software:     01041040 (33795a0a30000000)
[ INFO] [1663886707.538893405]: VER: 1.1: Middleware software: 01041040 (33795a0a30000000)
[ INFO] [1663886707.539031580]: VER: 1.1: OS software:         04137dff (e7701b9e792e3101)
[ INFO] [1663886707.539153506]: VER: 1.1: Board hardware:      00000001
[ INFO] [1663886707.539264337]: VER: 1.1: VID/PID:             0000:0000
[ INFO] [1663886707.539952557]: VER: 1.1: UID:                 3044494530303030
[ WARN] [1663886707.540114325]: CMD: Unexpected command 512, result 0
[ INFO] [1663886707.708296390]: VER: 1.1: Capabilities         0x000000000000e4ef
[ INFO] [1663886707.708538834]: VER: 1.1: Flight software:     01041040 (33795a0a30000000)
[ INFO] [1663886707.708672791]: VER: 1.1: Middleware software: 01041040 (33795a0a30000000)
[ INFO] [1663886707.708817060]: VER: 1.1: OS software:         04137dff (e7701b9e792e3101)
[ INFO] [1663886707.708943881]: VER: 1.1: Board hardware:      00000001
[ INFO] [1663886707.709068618]: VER: 1.1: VID/PID:             0000:0000
[ INFO] [1663886707.709161221]: VER: 1.1: UID:                 3044494530303030
[ INFO] [1663886708.451312390]: WP: seems GCS requesting mission
[ INFO] [1663886708.452393312]: WP: scheduling pull after GCS is done
[ INFO] [1663886708.566541986]: GF: seems GCS requesting mission
[ INFO] [1663886708.566864534]: GF: scheduling pull after GCS is done
[ INFO] [1663886708.610489220]: RP: seems GCS requesting mission
[ INFO] [1663886708.611010150]: RP: scheduling pull after GCS is done
[ INFO] [1663886713.314919596]: HP: requesting home position
[ WARN] [1663886713.449405269]: GP: No GPS fix
[ INFO] [1663886713.456587822]: WP: mission received
[ INFO] [1663886713.568093669]: GF: mission received
[ INFO] [1663886713.612369544]: RP: mission received
[ INFO] [1663886723.314870316]: HP: requesting home position
[ INFO] [1663886726.379315832]: arming
[ INFO] [1663886726.427174936]: FCU: Armed by external command
[ INFO] [1663886726.428385127]: Vehicle armed
[ INFO] [1663886726.476409542]: FCU: [logger] /data/px4/log/sess005/log001.ulg
[ INFO] [1663886731.379187081]: disarming
[ INFO] [1663886731.395421067]: FCU: Disarmed by external command
[ INFO] [1663886731.396092620]: Vehicle disarmed
[ INFO] [1663886731.396233503]: done
[mavros_test_node-3] process has finished cleanly
log file: /home/root/.ros/log/25b80dbe-3ac8-11ed-b22c-00c0caafc7f0/mavros_test_node-3*.log
...
```
