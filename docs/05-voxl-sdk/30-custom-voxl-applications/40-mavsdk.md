---
title: MAVSDK
layout: default
parent: Custom VOXL Applications
has_children: true
nav_order: 40
permalink: /mavsdk/
---

# MAVSDK on VOXL2

## Overview
[Mavsdk](https://github.com/mavlink/MAVSDK) is a C++ based API that is capable of interfacing with PX4. It leverages mavlink alongside plugins written for each specific function (Action, System, Mission, etc.) in PX4 to allow for seemless communication from a programmer written code base. There also exists a Python version of the API which can be researched [here](https://github.com/mavlink/MAVSDK-Python)

## Communication from MAVSDK to VOXL2

In order to communicate with the VOXL2 from MAVSDK, the user must have the two following voxl2 services actively configured and running: 

1. [voxl-vision-hub](/voxl-vision-hub/)
2. [voxl-mavlink-server](/voxl-mavlink-server/)

For `voxl-vision-hub`, the user must edit the following two parameters to:
1. `en_localhost_mavlink_udp`: `true`<br\ >
2. `localhost_udp_port_number`: `14551`<br\ >

For `voxl-mavlink-server`, the user must edit the following parameter to:
1. `secondary_static_gcs_ip`: `127.0.0.1`<br\ >

Once these have been services have been configured properly, enabled, and are running in the background, the user now has the ability to interface with PX4 via MAVSDK. 

In order to run MAVSDK on target, you must build the voxl-docker-mavsdk docker container that encompasses all the necessary libraries - you can follow the build steps [here](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-mavsdk-python)

This link above is how we recommend running MAVSDK on target (in this case a VOXL2). We run mavsdk inside a bionic docker container that has the ability to interface with the PX4 instance running outside docker. Once you have finished building the docker instance, proceed to `adb shell` into the target/voxl2 and spin up the docker instance. This can be done by running the `run-docker.sh` executable in the `/data/docker/mavsdk` directory of your voxl2.

Once you are in the Docker environment, you can now leverage the pre-written python and C++ examples of MAVSDK to control the drone. The two examples leveraged for this are the `takeoff_and_land` code written both in python and C++. If you wish to inspect the actual MAVSDK code that drives the drone to takeoff and land, Python examples can be found [here](https://github.com/mavlink/MAVSDK-Python/blob/main/examples/takeoff_and_land.py) and the C++ examples can be found [here](https://github.com/mavlink/MAVSDK/blob/main/examples/takeoff_and_land/takeoff_and_land.cpp).

It is recommended when starting out with MAVSDK to leverage HITL to test out the communications. In order to setup HITL, follow the documentation [here](/voxl2-PX4-hitl/).

## Video Demonstration

Below is a video showing how to setup `voxl-docker-mavsdk` in tandem with HITL to get a drone armed and flying in the simulation:

{% include youtubePlayer.html id="Q6pph-VqAEM" %}
