---
title: VOXL Emulator
layout: default
parent: Custom VOXL Applications
has_children: true
nav_order: 10
permalink: /voxl-emulator/
---


# VOXL Emulator Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Install the voxl-emulator Docker Image

### VOXL and VOXL Flight

voxl-emulator enables one to develop CPU-based applications for VOXL and VOXL Flight, then test CPU-only in an emulated environment.

Download archived docker image from ModalAI Developer portal (login required). Currently it is on version 1.7 [voxl-emulator_v1.7.tgz](https://developer.modalai.com/asset/download/75) from [developer.modalai.com](https://developer.modalai.com)

### VOXL 2

qrb5165-emulator enables one to develop CPU-based applications for VOXL 2, then test CPU-only in an emulated environment. It is an ARM 64 emulator with the VOXL 2 sysroot mounted. It has very limited application to end developers. It is for low-level service development for cross-compiling applications not yet supported by cross-compiling in [voxl-cross](/voxl-cross/)


### Installation Process
The installation process is the same for both VOXL and VOXL 2.

1. Download archived docker image from [developer.modalai.com](https://developer.modalai.com) (login required). 
- For VOXL: [voxl-emulator_v1.7.tgz](https://developer.modalai.com/asset/download/75)
- For VOXL 2: [qrb5165-emulator_V1.5.tgz](https://developer.modalai.com/asset/download/125)


2. Install Dependencies:
```
# install packages
$ sudo apt-get install qemu binfmt-support qemu-user-static

# Execute the registering scripts, see $https://www.stereolabs.com/docs/docker/building-arm-container-on-x86/
$ docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

3. Load and Tag the Docker Image:
```
$ docker load -i qrb5165-emulator_V1.5.tgz 
$ docker tag voxl-cross:V2.5 voxl-cross:latest
```

### Using the Installed Docker Image
To enter the docker image's build environment, run the command:
```
$ voxl-docker -i qrb5165-emulator_V1.5.tgz
```
