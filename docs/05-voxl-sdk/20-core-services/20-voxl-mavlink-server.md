---
title: voxl-mavlink-server
layout: default
parent: Core Services
has_children: true
nav_order: 20
permalink: /voxl-mavlink-server/
---

# Overview

voxl-mavlink-server manages MAVLink routing between network interfaces, MPA applications, and the flight controller such as PX4.

# Configuration

## udp_mtu

For radios, such as Doodle Labs, that utilize larger packet sizes with increased latency, you can enable mavlink aggregation feature with the udp_mtu field in /etc/modalai/voxl-mavlink-server.conf, the JSON file contains the following description of the field:

```
 * udp_mtu - maximum transfer unit for UDP packets back to GCS. voxl-mavlink-server
 *           will bundle up packets for the GCS into a single UDP packet with 
 *           a maximum size of this. This might help with some radios.
 *           This feature is off by default to let the network stack handle aggregation.
 *           Set to 0 to disable this feature and send one UDP packet per msg.
 *           Set to something like 500 to bundle a handful of packets together.
```

Try a few different values such as 100, 200, 300 etc to find an optimal value for your setup.