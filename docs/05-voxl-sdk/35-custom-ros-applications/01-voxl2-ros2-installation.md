---
title: ROS2 Installation VOXL 2
layout: default
parent: Custom ROS Applications
nav_order: 05
permalink: /ros2-installation-voxl2/
---


# ROS2 Installation on VOXL 2

VOXL2 SDK1.1 and image will ship out with ROS2 Foxy base installed on target in the `/opt` directory. If it is not installed, you can leverage the link [`here`](https://github.com/jetsonhacks/installROS2) to install on target ROS2 foxy on Ubuntu 18.04.

### Environment Setup Script

Once the ROS2 environment has been installed, the user can source the required bash script in order to have all the proper paths setup for ros2 foxy to run. This can be done by running: `source /opt/ros/foxy/setup.bash`

It is also worth adding this to the `~/.bashrc` so you do not need to source the paths every time you adb shell into the VOXL2.


## MPA-TO-ROS2

Once the installation of ros2 has been done, the user can then install [`voxl-mpa-to-ros2`](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros2) - which is a ros2 node that will convert the following from MPA to ROS2 messages:

1. IMU data
2. Camera Images (Hires, tracking, and stereo)
3. Point Cloud Information
4. 6DOF VIO data

In order to install it, follow the steps below: 

1. Clone the voxl-mpa-to-ros2 repository
2. Run on a parent computer the qrb5165-emulator:1.5 docker image once inside the voxl-mpa-to-ros2 repo
3. Run the install_deps script
4. Run the build script
5. Run the make package script
6. Once done running, push the deb file to the voxl2 via adb push
7. adb-shell onto the voxl2, and dpkg install the deb file
8. source the right directory and add it to bashrc: `source /opt/ros/foxy/mpa-to-ros2/install/setup.bash`

Once this is done, the user can then run the mpa-to-ros2 node!

`ros2 run voxl_mpa_to_ros2 voxl_mpa_to_ros2`

```
MPA to ROS app is now running

Found new interface: hires_small_color
Found new interface: stereo_front
Found new interface: stereo_rear
Found new interface: tracking
Found new interface: imu_apps
Found new interface: voa_pc_out
```

### List Topics

In a new shell, run the following:

```
voxl2:~$ ros2 topic list
/hires_small_color
/imu_apps
/stereo_front/left
/stereo_front/right
/tracking
/voa_pc_out
```

### All topic outputs and ROS2 msgs formats

Topics:

```
/hires_large_color - sensors_msg/msg/Image
/hires_large_encoded - sensors_msg/msg/Image
/hires_large_grey - sensors_msg/msg/Image
/hires_small_color - sensors_msg/msg/Image
/hires_small_encoded - sensors_msg/msg/Image
/hires_small_snapshot - sensors_msg/msg/Image
/stereo - sensors_msg/msg/Image
/tracking - sensors_msg/msg/Image
/imu_apps - sensor_msgs/msg/Imu
/qvio - geometry_msgs/msg/PoseStamped / nav_msgs/msg/Odometry
/qvio_overlay - sensors_msg/msg/Image
/tof_conf - sensors_msg/msg/Image
/tof_depth - sensors_msg/msg/Image
/tof_ir - sensors_msg/msg/Image
/tof_pc - sensors_msg/msg/PointCloud2
/voa_pc_out - sensors_msg/msg/PointCloud2
/vvhub_body_wrt_fixed - geometry_msgs/msg/PoseStamped / nav_msgs/msg/Odometry
/vvhub_boxy_wrt_local - geometry_msgs/msg/PoseStamped / nav_msgs/msg/Odometry
/tflite_data - voxl_msgs/msg/Aidetection
```

The way mpa_to_ros2 is programmed, topics only get published when something is subscribed to the topic. Otherwise it will just be advertised!

## Voxl2 MicroDDS Communication with PX4

Users on SDK 1.1 and higher will have the ability to download voxl-microdds-agent which runs a dds agent middle ware that can communicate with px4 running onboard. This can be installed via:

1. `apt install -y voxl-microdds-agent`
2. `voxl-configure-microdds`

Once this is setup and running (can be checked with a `voxl-inspect-services`), the user now has the ability to probe px4 via ros2. The topics that you can interface with are the following:

```
/fmu/in/obstacle_distance
/fmu/in/offboard_control_mode
/fmu/in/onboard_computer_status
/fmu/in/sensor_optical_flow
/fmu/in/telemetry_status
/fmu/in/trajectory_setpoint
/fmu/in/vehicle_attitude_setpoint
/fmu/in/vehicle_command
/fmu/in/vehicle_mocap_odometry
/fmu/in/vehicla_rates_setpoint
/fmu/in/vehicle_trajectory_bezier
/fmu/in/vehicle_trajectory_waypoint
/fmu/in/vehicle_visual_odometry
/fmu/out/failsafe_flags
/fmu/out/position_setpoint_triplet
/fmu/out/sensor_combined
/fmu/out/timesync_status
/fmu/out/vehicle_attitude
/fmu/out/vehicle_control_mode
/fmu/out/vehicle_gps_position
/fmu/out/vehicle_local_position
/fmu/out/vehicle_odometry
/fmu/out/vehicle_status
```

### Running a figure 8 via ros2, microdds, and px4

With microdds agent running and the client running on px4, we now have the ability to send setpoints in via the dds brige and ros2. This feature is already implemented with the voxl-mpa-to-ros2 code base. In order to run this, the user must run:

`ros2 run voxl_offboard_figure8 voxl_offboard_figure8`

Using a Bernoulli's Lemniscate, we calculate the position, velocity, and acceleration along the path, and package those up into a trajectory_setpoint ros2 message, when then gets published to px4 to execute the path!

In order to run this demo, ensure that you have the parameter `offboard_mode` set to `off` in the voxl-vision-hub.conf file. Then restart voxl-vision-hub via `systemctl restart voxl-vision-hub`