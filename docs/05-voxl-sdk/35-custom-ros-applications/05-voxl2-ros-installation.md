---
title: ROS Installation VOXL 2
layout: default
parent: Custom ROS Applications
nav_order: 05
permalink: /ros-installation-voxl2/
---


# ROS Installation on VOXL 2

ROS must be installed on both the Host computer and on VOXL. This page guides you through this process.


## 1. Set up ROS on Host PC

### Install

You can use whichever distribution of ROS 1 on your host PC (i.e. ROS Melodic on Ubuntu 18.04). Follow the ROS wiki instructions to install `ros-melodic-desktop-full`: <http://wiki.ros.org/melodic/Installation/Ubuntu>

### Environment Setup Script

We will set up a `my_ros_env.sh` script on the host pc:

```bash
me@mylaptop:~/$ touch ~/my_ros_env.sh
me@mylaptop:~/$ echo ". ~/my_ros_env.sh" >> ~/.bashrc
```

Then paste the following into your `my_ros_env.sh` script. This is where you will edit IP addresses as necessary.

``` bash
#!/bin/bash

# Script loads ROS envrionment variables and sets IP addresses

# load main ros environment
if [ -f /opt/ros/melodic/setup.bash ]; then
    source /opt/ros/melodic/setup.bash
elif [ -f /opt/ros/kinetic/setup.bash ]; then
    source /opt/ros/kinetic/setup.bash
elif [ -f /opt/ros/indiego/setup.bash ]; then
    source /opt/ros/indigo/setup.bash
fi

# if a catkin workspace is setup then make sure the launch
# files and run files are available in the ROS PATH
if [ -f ~/catkin_ws/devel/setup.bash ]; then
    source ~/catkin_ws/devel/setup.bash
fi
if [ -f ~/catkin_ws/install/setup.bash ]; then
    source ~/catkin_ws/install/setup.bash
fi

# ignore ROS_HOSTNAME, it only causes problems and overrides ROS_IP
unset ROS_HOSTNAME

# configure ROS IPs HERE
export ROS_MASTER_IP=192.168.8.1
export ROS_IP=192.168.8.91

# ROS_MASTER_URI is derived from ROS_MASTER_IP
export ROS_MASTER_URI=http://${ROS_MASTER_IP}:11311/

## bonus alias to ssh into ros master
alias sshh='sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${ROS_MASTER_IP}'
```

The script above provides an alias 'sshh' which allows you to ssh remotely into your VOXL using default credentials. Update `ROS_MASTER_IP` and `ROS_IP` to match your network configuration. On your host machine, also be sure to update `ROS_MASTER_IP` and `ROS_IP` to match this VOXL IP address.

You will need to install sshpass to use the aforementioned alias.

```bash
# ON HOST PC
me@mylaptop:~/$ sudo apt install sshpass
```

### Troubleshooting

Note: Due to SSH environment variable issues using the default terminal, we recommend that users with host Ubuntu machine versions newer than Ubuntu 14.04 use XTerm (not the default terminal) to connect to VOXL via SSH while using ROS.



## 2. Set up ROS on VOXL 2

### Tutorial Video 

{% include youtubePlayer.html id="zEGe-5EdXC0" %}

### Install ROS

VOXL 2 can use apt package management, which allows flexibility on what ROS version to install, and allows us to keep the core system image smaller. For these reasons, we leave it up to the end user to select what to install.

To install ROS Melodic (as an example), connect VOXL 2 to the internet (see the [WiFi guide](/voxl-2-wifi-guide/) for help on this) and run the following:

```
voxl2:/$ apt-get update
voxl2:/$ apt install -y ros-melodic-ros-base ros-melodic-image-transport
```


### Launch 

Source ROS setup script:

```
voxl2:~/$ . /opt/ros/melodic/setup.sh
```

Run the [voxl_mpa_to_ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) node, which uses this launch file `/opt/ros/melodic/share/voxl_mpa_to_ros/launch/voxl_mpa_to_ros.launch`

```
voxl2:~/$ roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
```

You should get similar output:

```
... logging to /home/root/.ros/log/81ccbc5c-34ac-11ed-80c6-00c0caafca29/roslaunch-m0054-2535.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://m0054:45955/

SUMMARY
========

PARAMETERS
 * /rosdistro: melodic
 * /rosversion: 1.14.13

NODES
  /
    voxl_mpa_to_ros_node (voxl_mpa_to_ros/voxl_mpa_to_ros_node)

auto-starting new master
process[master]: started with pid [2557]
ROS_MASTER_URI=http://localhost:11311

setting /run_id to 81ccbc5c-34ac-11ed-80c6-00c0caafca29
process[rosout-1]: started with pid [2568]
started core service [/rosout]
process[voxl_mpa_to_ros_node-2]: started with pid [2571]


MPA to ROS app is now running

Found new interface: hires
Found new interface: qvio_overlay
Found new interface: stereo_front
Found new interface: stereo_rear
Found new interface: tracking
Found new interface: imu_apps
Found new interface: voa_pc_out
Found new interface: qvio
```

### List Topics

In a new shell, run the following:

```
voxl2:~$ rostopic list
/hires
/imu_apps
/qvio/odometry
/qvio/pose
/qvio_overlay
/rosout
/rosout_agg
/stereo_front/left
/stereo_front/right
/stereo_rear/left
/stereo_rear/right
/tracking
/voa_pc_out
```

## Next Steps

See the [MAVROS test example for VOXL 2](/mavros-voxl-2/).

See the [building ROS nodes for VOXL](/build-ros-nodes-for-voxl/) for information on making your own ROS nodes.

