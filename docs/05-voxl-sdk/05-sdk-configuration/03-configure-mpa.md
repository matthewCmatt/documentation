---
title: voxl-configure-mpa
layout: default
parent: SDK Configuration
has_children: false
nav_order: 03
permalink: /voxl-configure-mpa/
---

# voxl-configure-mpa


voxl-configure-mpa serves to define the set of config files and [MPA](/mpa/) services that should be enabled for a given [SKU](/sku/) number. When reflashing a VOXL with an [SDK installer](/upgrading-sdk-version/), the last step of the installation process is an automatic call to `voxl-configure-mpa` to set up all the necessary services.

## When to use voxl-configure-mpa?

You should run `voxl-configure-mpa` if you wish to reset all config files and systemd service states to default conditions. This is useful if you've been experimenting with different services and configurations, but want to return to a known default state without reflashing VOXL.

You should also run `voxl-configure-mpa` after changing the [SKU](/sku/) number on your VOXL to update all services to the respective defaults for the new [SKU](/sku/).

## What does voxl-configure-mpa do?

### 1. Reads and Validates SKU

First, `voxl-configure-mpa` reads the `/data/modalai/sku.txt` file, validates it, and asks the user if it is correct before continuing:

```bash
voxl2:/$ voxl-configure-mpa
------------------------------------------------------------------
               Welcome to voxl-configure-mpa!

VOXL is currently thinks it is in the following hardware:

family code:   MRB-D0005 (starling)
compute board: 4 (voxl2)
hw version:    2
cam config:    6
SKU:           MRB-D0005-4-V2-C6

If this doesn't look right, quit and run voxl-configure-sku to
set it correctly. Then run voxl-configure-mpa again.


Continue? y/n:
```


### 2. Constructs list of Config Steps

Then, it internally makes a list of the necessary `voxl-configure-xyz` commands necessary to configure [MPA](/mpa/). This list is printed out during the configuration process but so is a lot of other stuff. If you want to do a dry-run jsut to see what would be executed, run it in debug mode with `voxl-configure-mpa --debug` which will pause the program before exectuing, letting you see what is about to be run.

```
voxl2:/$ voxl-configure-mpa --debug

About to Execute:
voxl-configure-extrinsics starling_v2_voxl2
voxl-configure-cameras 6
voxl-configure-cpu-monitor factory_enable
voxl-configure-qvio factory_enable_imu_apps
voxl-configure-dfs factory_disable
voxl-configure-tag-detector factory_disable
voxl-configure-tflite factory_disable
voxl-configure-vision-hub factory_enable
voxl-configure-mavlink-server factory_enable
voxl-configure-portal enable
voxl-configure-lepton disable
voxl-configure-uvc disable
voxl-configure-streamer factory_enable
voxl-configure-modem disable
voxl-configure-mavcam factory_enable
voxl-configure-px4-imu-server factory_disable
voxl-configure-imu factory_enable
voxl-configure-flow-server disable
voxl-configure-feature-tracker disable
voxl-esc setup_starling_v2
voxl-elrs --scan
voxl-configure-px4 starling_v2
voxl-configure-px4-params -np MRB-D0005-V2


Continue? y/n:
```


### 3. Executes these steps

All steps are executed, whether they pass or fail. At the end, a summary is presented showing which steps failed, if any.

Note: If your SKU has a voxl-esc and is based on the QRB5165 Platform (VOXL2 and VOXL2 Mini) then `voxl-configure-mpa` will upgrade the voxl-esc firmware if necessary and also set ESC params for the motors and propellers on your particular SKU. If your SKU has an ELRS radio, `voxl-configure-mpa` will check and update the ELRS firmware if necessary. Both of these steps requires the `voxl-px4` service to be stopped and a different SDSP binary to be loaded before starting voxl-px4 again. Occationally this will fail and voxl-configure-px4-params will fail as a result. If this happenens, simply try voxl-configure-mpa again.

If this happens, you will see this:

```bash
[ERROR] Failed to set px4-parameters. Check voxl-px4 status for details.
FAILED TO EXECUTE: voxl-configure-px4-params -np MRB-D0005-V2
This is likely because voxl-px4 on the SDSP failed to restart.
Please try running voxl-configure-mpa again.
If that doesn't work, power cycle and try again.
```

### 4. Print summary

If you've flashed VOXL with the [SDK installer](/upgrading-sdk-version/) then this summary print will look familiar as it's the last step of the installer. Note that the list of required calibration files can be accessed separately with the [voxl-check-calibration](/check-calibration/) tool.


```bash
------------------------------------------------------------------
        SUCCESSFULLY CONFIGURED MPA SERVICES!
        Services will start up on next reboot

Calibration File Status:
Present /data/modalai/voxl-imu-server.cal
Present /data/modalai/opencv_tracking_intrinsics.yml
Present /data/px4/param/parameters_gyro.cal
Present /data/px4/param/parameters_acc.cal
Present /data/px4/param/parameters_mag.cal
Present /data/px4/param/parameters_level.cal

                PLEASE POWER CYCLE YOUR VOXL
------------------------------------------------------------------
```

## Next Steps

Now, `voxl-configure-mpa` has reset your configuration to defaults for a particular SKU. The remaining pages in the [Configuration](/configuration/) section are about customizing particular configuration files for your custom applicaiton.

[Next: Configuring Extrinsics](/configure-extrinsics/){: .btn .btn-green }
