---
title: VOXL 2 PX4 Developer and Build Guide
layout: default
parent: VOXL PX4
has_children: true
nav_order: 20
permalink: /voxl-px4-dev-build-guide/
---

# Introduction

Since not all support for QRB5165 has been integrated into the mainline PX4
source code ModalAI maintains a fork of the project where our custom additions
are kept. The intention is to eventually merge all of these customizations
into the mainline PX4 source repository so that our fork will no longer be necessary
other than for research and development purposes.

## Main components

- The PX4 firmware lives in a fork of the main PX4 firmware repository
- The [voxl-px4](/voxl-px4/) project is a wrapper that includes the PX4 firmware as a git submodule. The main objectives are to provide packaging and installation support.
- The [build environment](https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker) for QRB5165 is encapsulated in a docker image. This docker includes
an ARM64 toolchain from Linaro to build the code running on the applications processor and a
Hexagon DSP toolchain to build the code running on the SDSP. The Hexagon SDK is only
available directly from Qualcomm so instructions are provided to obtain the SDK and
integrate it into the build environment docker image.

## Links to source code

- [voxl-px4 wrapper project](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) which runs the main service across Linux and sDSP.
- [Development Branch on ModalAI's VOXL 2 PX4 Fork](https://github.com/modalai/px4-firmware/tree/voxl-dev) which has the latest source code for the PX4 build for VOXL 2.
- [Docker image to build PX4 for VOXL 2 and RB5 Flight sDSP and CPU](https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker)

