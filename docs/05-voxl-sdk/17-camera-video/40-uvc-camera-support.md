---
title: UVC Camera Support
layout: default
parent: Cameras & Video
has_children: true
nav_order: 40
permalink: /voxl-uvc-server/
---

# voxl-uvc-server

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

## Overview

<i class="fab fa-gitlab" style="color: #e66100;"></i>
[voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server)

`voxl-uvc-server` is an application which grabs video from [UVC cameras](https://en.wikipedia.org/wiki/USB_video_device_class) and serves up the video as an MPA (Modal Pipe Architecture) pipe for other applications to pull from. This server is often used in conjunction with [voxl-streamer](/voxl-streamer/).

There are a large array of UVC cameras on the market and we haven't tested them all, but below is a list of some that we know `voxl-uvc-server` supports.

In general, `voxl-uvc-server` supports UVC cameras that have the ability to provide a raw frame output.

- Logitech C270
- Logitech C930e
- Logitech Brio
- Videology 5MP Micro USB 2.0 Board Camera
- [PureThermal Mini with Flir Lepton](https://groupgets.com/manufacturers/getlab/products/purethermal-mini-flir-lepton-smart-i-o-module)
- Flir Boson 640
- Blue Robotics h264 camera

## Usage

As with all MPA services, `voxl-uvc-server` can be configured and enabled to run on boot using it's configuration script. Usage is as follows:

```
voxl2:/$ voxl-configure-uvc -h

Start wizard with prompts:
voxl-configure-uvc

Shortcut configuration arguments for scripted setup.
factory_enable will reset the config file to factory defaults
before enabling the service.


voxl-configure-uvc disable
voxl-configure-uvc factory_enable
voxl-configure-uvc enable

show this help message:
voxl-configure-uvc help
```

Configuring `voxl-uvc-server` will set up the following default configuration file:

```
voxl2:/$ cat /etc/modalai/voxl-uvc-server.conf 
/**
 * This file contains configuration that's specific to voxl-uvc-server.
 * parameter descriptions:
 * 
 * width: Desired image width. (e.g. 640)
 * Default is 640.
 *
 * height: Desired image height. (e.g. 480)
 * Default is 480.
 *
 * height: Desired frame rate in fps (e.g. 15).
 * Default is 30.
 *
 * pipe-name: Desired MPA pipe name in /run/mpa (e.g. webcam).
 * Default is uvc.
 *
 */
{
	"pipe_name":	"uvc",
	"width":	640,
	"height":	480,
	"fps":	30
}
```

If a camera doesn't support the default configuration, it can be manually tweaked. For example, using the PureThermal Mini with Flir Lepton requires the following configuration options:

```
{
	"pipe_name":	"uvc",
	"width":	160,
	"height":	120,
	"fps":	9
}
```

After modifying the configuration file, `voxl-uvc-server` can be restarted by power cycling or running the following:

```
systemctl restart voxl-uvc-server
```


## Tips

`voxl-uvc-server` also provides a few helpful debugging tools in order to troubleshoot issues or determine the required confiuration for a new camera.

The Linux command line utility, `lsusb`, can be very helpful to see what devices have enumerated on the USB bus. One can run the command with a USB camera attached to see if the camera has enumerated.

```
voxl2:/$ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 002: ID 046d:0825 Logitech, Inc. Webcam C270
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

One can look at the above output and determine the vendor ID and Product ID of their camera, in this example the vendor ID is `046d`, while the product ID is `0825`

`lsusb -v -d $VENDOR_ID:$PRODUCT_ID` can also provide a lot more information about a specific device, for example:

```
voxl2:/$ lsusb -v -d 046d:0825

Bus 001 Device 002: ID 046d:0825 Logitech, Inc. Webcam C270
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               2.00
  bDeviceClass          239 Miscellaneous Device
  bDeviceSubClass         2
  bDeviceProtocol         1 Interface Association
  bMaxPacketSize0        64
  idVendor           0x046d Logitech, Inc.
  idProduct          0x0825 Webcam C270
  bcdDevice            0.12
  iManufacturer           0
  iProduct                0
  iSerial                 2 9C06CEC0
  bNumConfigurations      1
  ...
```

`voxl-uvc-server` also provides a script called `show-video-device-info.sh` which can be used to get useful information about any attached UVC cameras:

```
voxl2:/$ show-video-device-info.sh
/dev/video2: UVC Camera (046d:0825) 046d:0825
```

For debugging purposes, `voxl-uvc-server` can be ran manually from the command line and has the ability to pass in a few arguments for customization:

```
voxl2:/$ voxl-uvc-server -h
loading config file
Usage: voxl-uvc-server <options>
Options:
-d                Show extra debug messages
-v <vendor-id>    USB vendor id of the desired UVC device in hexadecimal (e.g. 090c)
                  Default is search for any vendor id
-p <product-id>   USB product id of the desired UVC device in hexadecimal (e.g. 337b)
                  Default is search for any product id
-r <resolution>   Desired image resolution (e.g. 320x240)
                  Default is 640x480
-f <fps>          Desired frame rate in fps (e.g. 15)
                  Default is 30
-l                List all devices found then exit
-o <name>         Custom output name
                  Default is uvc
-h                Show help
```

These arguments will override the config file at `/etc/modalai/voxl-uvc-server.conf`

## Simultaneous UVC cameras

Due to a bug in [libuvc](https://github.com/libuvc/libuvc) in order to run two USB cameras simultaneously, you need to make one camera have different formatting options than the other. The least intrusive way to do this is to set the frame rate of one camera different than the other.

Here we set the frame rate of the first camera to `25` fps using the `-f` option:

```
voxl2:/$ voxl-uvc-server -f 25
```

Assuming the other camera defaults to 30 fps, you do not need to set its frame rate:

```
voxl2:/$ voxl-uvc-server
```

