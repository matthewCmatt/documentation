---
title: RTSP Video Stream
layout: default
parent: Cameras & Video
has_children: true
nav_order: 45
permalink: /voxl-streamer/
---

# How to Stream Video with RTSP on VOXL

voxl-streamer is a background service that subscribes to [MPA image streams](/mpa-camera-interface/), compresses them if necessary, and makes them available as an RTSP Stream. This is enabled by default on most systems starting in SDK-1.0 to enable automatic streaming of video to QGroundControl by [voxl-mavcam-manager](/voxl-mavcam-manager/).

The following video [here](https://www.youtube.com/watch?v=DFbJE9w4PGI&ab_channel=ModalAI) shows how to setup 720p encoded video stream to voxl-streamer using a VOXL2 and IMX412.


## General use as a Service

voxl-streamer is meant to be run in the background with it's pre-installed systemd service that shows up in running [voxl-inspect-services](/mpa/#using-services). It's configured by a config file that lets the user pick which MPA pipe it should subscribe to for image data.

If no RTSP clients are connected, it will disconnect from the MPA pipe to save power. Then reconnect when an RTSP client requests a connection.


## Running Multiple Instances

You can start additional instances of the voxl-streamer binary by passing in command line arguments to configure it to override what is in the config file. Most importantly, the `-s` or `--standalone` argument will tell it not to interfere with the background service, and the `-p` or `--port` argument must be used to set an output port that's different than that of the background service or any other running instances.

# TODO paste command line help text and config file



## Two primary modes

voxl-streamer cam subscribe to pre-encoded H264 or H265 MPA pipes. This is most efficient and is the default behavior since it subscribes to the `hires_small_encoded` pipe out of the box. In this case, the `bitrate` and `rotation` fields in the config file are IGNORED since the video is already compressed.

When configured through the config file or command line arguments to subscribe to a raw8 or nv12 uncompressed pipe, voxl-streamer will set up a gstreamer pipeline that allows the video to be rotated and then compressed to a desired fixed bitrate. This uses the same hardware accelerated OMX encoder as voxl-camera-server but is less efficient as it requires the overhead of gstreamer and additional memcopies.





