---
title: Configure Camera 
layout: default
parent: Camera Video
has_children: false
nav_order: 10
permalink: /configure-cameras/
---

# Configure Cameras

When setting up a custom VOXL for the first time or when changing camera configurations, you will need to configure VOXL's software to make it aware of the physical camera configuration. This can easily be done by running the `voxl-configure-cameras` script that's included with the voxl-utils package.

```
voxl2:~$ voxl-configure-cameras
```

By default, without arguments given, this will prompt you for the configuration you are using from the list below:

```
Tool to configure camera server

Usage:
voxl-configure-cameras <config-id>  - set up a specific cam config
voxl-configure-cameras sentinel_v1  - setup for Sentinel drone
voxl-configure-cameras fpv_revB     - setup for fpv_revB
voxl-configure-cameras starling_v2  - setup for Starling V2
voxl-configure-cameras disable      - disables voxl-camera-server
voxl-configure-cameras help         - show this help text

If no config-id or arguments are given, the user will be
prompted with questions to help complete configuration

available camera config IDs are as follows:

0  None
1  tracking(ov7251) + Stereo(ov7251)
2  tracking(ov7251) Only
3  Hires(imx214) + Stereo(ov7251) + tracking(ov7251)
4  Hires(imx214) + tracking(ov7251)
5  TOF + tracking(ov7251)
6 Hires(imx214) + TOF + tracking(ov7251)
7  TOF + Stereo(ov7251) + tracking(ov7251)
8  Hires(imx214) Only
9  TOF Only
10 Stereo(ov7251) only
11 tracking(ov7251) + Hires(imx214) + Dual Stereo
12 Stereo(OV9782) only
13 Hires(imx214) + Stereo(ov7251)
14 Stereo(OV9782) + TOF + tracking(ov7251)
15 Dual OV9782

If you are using optional imx412 or imx678 units in place of the
default imx214 hires cameras, please use the following args to swap:
--imx412 or --imx678

```


To actually configure the cameras, add the correct integer argument like below:

```
voxl2:~$ voxl-configure-cameras 1
```

A configuration file will be generated in VOXL's file system at `/etc/modalai/voxl-camera-server.conf`. 

Due to the fact that the script messes with drivers and other low-level camera options, it is stongly recommended that you restart the board after running the script.

You can further customize the configuration (i.e. image resolutions) by editing the config file directly. We recommend reading the page on [`voxl-camera-server`](/voxl-camera-server/) for more information also.


