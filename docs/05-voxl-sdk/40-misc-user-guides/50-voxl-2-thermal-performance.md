---
title: VOXL 2 Thermal Performance
layout: default
parent: Misc. User Guides
has_children: true
nav_order: 05
permalink: /voxl2-thermal-performance/
---


# VOXL 2 Thermal Performance and EMC
{: .no_toc }

Note: this is written with a soft tongue-in-cheek demeanor to keep the topic light and fun. This page will focus on some brief information and guidance to help hardware and system designers meet the thermal and EMC/EMI challenges most likely to occur.

This is not a comprehensive list, and of course, we encourage feedback and other questions on our [Forum](https://forum.modalai.com/). We hope to update this page often, and it's applicability will not be limited to just Voxl2, but rather, all hardware designs. It may move locations in the future as we author more and more application notes.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Thermal Performance

Thermal is a huge challenge, especially in tight spaces and with enormous compute power.
We have decades of experience with thermal management, and the summary that best explains how to approach thermal design is this 4-step process:

### 1.	Do not solve a problem you do not have.
-	This is the biggest mistake we see designers do. Prior experiences and peer led discussions and legacy designs lead many designers into instinctively wanting to add heat-sinks and thermal mass to a device that generates heat. This is a mistake.
- Yes, there are times when heatsinks are needed, however do not forget what they do.
   - All a heatsink is trying to do is provide a good thermally conductive path for heat energy to be removed from the heat source and put someplace else benign.
   - Good heatsinks have a ton of surface area to aid in dissipation into the ambient/surrounding air.
   - Even a great heatsink needs airflow to move the heat energy off it. If there is no airflow, the ambient air provides very little thermal conductance, and the heatsink will effectively become a large thermal capacitor. So, it will certainly delay the onset of thermal performance degradation, but it will also sustain that degradation even when no new thermal energy is being added and take much longer to return to an ambient state.
- In many instances, designers end up chasing their tail solving self-induced issues brought on by adding thermal mass (or any engineering “solution” needlessly added). One such item is EMC performance, especially if the heatsink was grounded. We know of instances where folks added a heatsink and then they failed FCC. Why? It was grounded incorrectly, and then became a noise radiator. Don’t be that designer.
- So, how do you know if you need any thermal solution… item #2:

### 2.	Test your use-cases first!
- You must understand what your system is trying to achieve, and you need to know “what does success look like”. There are many use cases using our Voxl ecosystem and components that do not require any additional thermal mitigation techniques at all. It would be a shame to add more SWaP-C elements (size, weight, power, and cost) for no benefit whatsoever.
- Our Voxl tools have comprehensive logging and monitoring features that will report temperatures, frame rates, RSSI, etc. You must log the important factors relevant for your design goals, and create the overlap of temperature changes to these performance metrics. You need to confirm if you are indeed experiencing thermal mitigation, or some other artifact, such as sub-optimal data paths (having overflows or re-tries due to pushing too much data into a slow data channel, such as 500Hz+ IMU data over I2C, or trying to push 1Gb links into USB2, and so forth), or over-constrained and/or buggy algorithms for your AI processing.
- Get comfortable running tests. Just do it. A 10-20 minute test can open your eyes much more than weeks of design and anaylsis. Work with your SW team to automate and log results you need.

### 3.	Give yourself options in your design.
- Whether you need more thermal mass (in the form of a heatsink) or a fan (preferred), make sure you think through your design and give yourself the option to incorporate these elements later on, after you have tested your use-case and observe performance limitations brought upon by thermal throttling, and try them out one at a time and re-test.
- Plan on a fan, where it can fit, and how to plug into the Voxl board.

### 4.	Airflow, airflow, airflow (4th in this list of info, but this is “the” first way to solve it!) 
- Airflow is the single biggest factor for improved thermal performance, which is why every Voxl platform has a dedicated fan connector, and we sell a very convenient small 25mm x25mm 5V fan on our website that will plug directly into Voxl, Voxl-Flight, Voxl2, and Voxl2-Mini. https://www.modalai.com/collections/accessories/products/voxl-cooling-fan
- If you want to add a heatsink into your design to improve thermal mass, you still should have a fan to blow the heat off of the fins.
- You are making a drone, right? Use the prop-wash!!! Recall step #2 to test your use case? Add into your monitoring the overlap with active flight. You should try to add into your design the ability to deflect some of the drone’s prop-wash into the circuit areas, and you will find a tremendous boost in thermal performance.
   - We have a story about a very experienced thermal team, using the latest and greatest thermal modeling techniques ($100k+ software license add-ons, months of cad development, then months of simulations that took days to converge on each pass, etc.) and after 8-12 months of modeling and physical testing, they found that simply using the drone’s prop wash the most effective solution compared to high-end vapor chambers, heatsinks, or any passive thermal material, by many degrees C. We unfortunaly cannot share that data, but it was an eye opener for many on that project that were convinced otherwise from their decades of experience in the mobile space. It took them over $1M to come to that conclusion. And now we all get to benefit from it. This is why ModalAI will always first push the claim of airflow before using any passive metal brick that WILL negatively impact flight time and your payload capability.
   - We understand that ground-based robots, or drones that may “perch look and listen” potentially in a solar loaded environment, will not benefit from active prop-wash. Therefore, know your use case, test for it, and be clever with ways to avoid that. Maybe reduced performance in that simple “perched” use case is OK? Then, you fly a bit to regain performance and perch back again.
- Re-test and improve on your airflow. If you really think you still need a heatsink or even a heat spreader, then that’s OK. But, make sure you do not create additional issues, such as an EMC violation. Keep your thermal solutions passive, do not make them electrically conductive. 

ModalAI will help ensure you have all the 3D data you need to accurately design your thermal solution. If we do not have a 3D step file for a specific board on our website, please reach out to us on the [Forum](https://forum.modalai.com/) and we will address it.

## EMC

EMC is also a design and integration challenge, but one that can be methodically planned for in advance and solved with a careful set of actions. It is a system level challenge and may require mitigation in all areas of design, including hardware, mechanical, industrial design, and even software or use-case control.

Much of the advice we can offer for EMC mitigation follows closely with the thermal design topic above.
1.	Do not solve a problem you do not have.
2.	Test your use-cases first!
3.	Give yourself options in your design.
4.	And, in the case of EMC, it’s all about proper PCB design and smart wire/cable management.

We are confident ModalAI already handled the PCB design portion on our products by following tried-and-true techniques in the PCBs. This small topic brief will provide several pointers so you can provide the same level of EMC/EMI mitigation fundamentals into your custom designs, wire harnesses, and overall strategy.

1.	The one difference between thermal management and EMI mitigation is that you must do some effort up-front in anticipation of issues, but do not go overboard. Even the most basic strategies can get you far. Many cheaply made electronic products less than $20 can pass FCC, so it’s not an insurmountable goal. Just take it slowly. The biggest thing to manage is unplanned radiators or antennas. If you think about those during the industrial design phase, the rest should go smooth. Do not plan in shielding everything and adding size and weight and assembly time into something you are not sure is needed.

2.	Find your preferred certification lab that offers unofficial pre-screen testing time. You want to take your mock-up prototypes and run them under a similar testing environment with similar software builds to get a sense of where your margins are for the emissions masks, or if there are any mask limit violations so you can focus on what matters. This will be money well worth it. If your normal certification process is about $10-15K, you should be able to get pre-screen testing for a fraction of that.
-	If you have failures, use “sniffer” equipment to find the general area where the noise is coming from and focus on that spot.
-	If you already pass, do not make drastic changes in your cabling or that can result with a failure later.

3.	The options you will need are to potentially fit shielding around certain cables (but not necessary all of them). You will need to think about cables that are either high current (like motor wires) or very high frequency, such as USB3 and MIPI-CSI cables. Most UART/SPI/I2C etc. low-speed cables will not be a problem if you keep simple point-to-point connections cleanly routed.
-	Do not always plan on shielding if you can avoid it. Shielding is a way to stop radiated noise from reaching certain areas but is not the solution for the “source” of the noise. It’s best to always target the source of the noise (i.e.: the aggressor or radiator) so that if you can improve your design there, you will not need shielding.

4.	PCB Design Tricks and Cable Management
-	ModalAI has already followed these PCB design strategies in all our designs, but if you are making custom PCBs, please follow these guidelines (all steps are ways to reduce the risk of creating unplanned radiators):
   - Always have ground planes (4-layer+ boards). Just because you can route a design on 2-layers, does not mean it’s a good idea. If you have any 2-layer PCBs in your system, and you fail any certification, target those first. 
   - Do not route high-speed or high-current signals along the PCB edge. Use Ground floods to fill in the route layers and try to create a full boundary of ground around those layers.
   - Do not “t-route” any signals if you can avoid it. Not only will this help with signal integrity, but it will reduce noise in the PCB. Some architectures, like DDR, are meant for this type of routing and provide guidelines for success. But for general purpose routing, always try to avoid it.
   - Manage your ground and power planes carefully.

        1.	Always make sure every flood has multiple vias in it and you do not create “antenna islands”, such as a ground shape that only has one via on one side. This is a perfect antenna and will radiate all noise within it’s frequency response characteristics.
        2.  Be sure to have close (thin) dielectrics on high-power planes and controlled impedance layers. This will optimize plane capacitance and keep signal inductance low.
- Do not use grounded or metal hardware (screws/washers/spacers). 
   - All ModalAI PCBs have mounting holes that are non-plated and we provide nylon hardware where we can. Do not make your custom hardware have grounded mounting holes or use metal hardware unless you have an entire EMC team that can help guide you through this delicate process. Most likely doing this creates ground loops and antennas, wreaking havoc on your system.
   - Shields for GPS/GNSS, or other areas of highly sensitive circuits, do not need to be grounded. In most cases, just a floating chunk of metal is enough to block noise. If you ground them you risk more problems. Be sure to give your design an option to remove ground connections from shields.
   - If you must use Earth-GND or Chassis-GND in your setup, you will need to work with your EMC/Radiation/Compliance SME (Subject Matter Experts) on ways to implement this at the entire system. Keep in mind, Drones or ground-based robots operate on batteries, and much like mobile phones, do not have a true “earth-ground” reference. Trying to add one in (at DC) can prevent the system from operating properly. In all cases across ModalAI’s hardware descriptions, pin tables, schematics, these app notes, etc., when we refer to “ground” we imply signal return, not Earth-Ground. We will indicate otherwise.
   - Use ground planes in flexes and add EMI coating. Most of our flexes have these. The only ones that may not have them are short runs leading directly into the image sensors, but any longer run and extension flex certainly has the coating. We will provide more info on this in the image sensor design guidelines page. 

- With cables, the most common issue is turning conductive noise that would normally stay local on a PCB or circuit, and then the cable acts as an antenna allowing it to radiate. Cable strategies are hard to change once implemented, so be sure to follow these techniques up-front in order to reduce the impact of changes:
   - Not all cables need to be shielded, but any cable will benefit from twisted pair techniques. Some of our cables we sell have twisted pairs just on the high-speed differential conductors, but it is OK and advisable to twist every signal wire with another cable conductor of its class. 
        1.	i.e.: you can twist UARTs together, or I2C lines together, or Power and Ground, but do not twist a UART with an SPI, or an I2C with a power wire, etc.
        2.	The goal is to have signal edges that occur at the same time provide equal and opposite noise effects, creating destructive interference. That cannot happen if the interfaces are noncoherent. 
        3.	Some mechanical designs benefit from having cables twisted to maintain routing control (so they do not fray or vibrate), so be sure to work with your ME to track these efforts. In this case, you can twist a UART with an I2C, or other, but you will not get any noise reduction benefits. Do not twist power with highly sensitive signals or you can impact performance.
   - Do not lay cables directly above or over any PCB, IC, or power circuit. Make sure that all cables egress from their connector and immediately go away from the electronics or sources of high noise. What can happen in this case is the cable can pick up near-end radiated noise from the IC/High current emanator, and then the cable will re-radiate that noise elsewhere in the system turning it into far-end radiation. If you must lay a cable across a high-noise source, then you may need to plan on shielding that cable in that location or try to place as much of an air gap as possible. Be careful you do not create active ground connections potentially shorting out the PCB/circuit.
