---
layout: default
title: VOXL Inspect Points
parent: Inspect Tools
search_exclude: true
nav_order: 50
permalink: /voxl-inspect-points/
---

# VOXL Inspect Points
{: .no_toc }

`voxl-inspect-points` is a tool to debug point cloud data in MPA. As a command line tool it provides no visualization but does indicate if data is being published along with the number and type of point.

---

## Arguments

```
yocto:/$ voxl-inspect-points -h

Tool to print point cloud data to the screen for inspection.
This will print up to the first 10 points received with each
message. For point clouds read from the TOF sensor, this will
print the 10 points across the middle of the image.

-h, --help              print this help message
-n, --newline           print newline between each pose

typical usage:
/# voxl-inspect-points tof_pc
```


## Example Output

For example, to see if the [Depth from Stereo](/voxl-dfs-server/) is outputting data, run the following command. Note, you should be able to tab out any pipes with the point cloud type.


```
yocto:/$ voxl-inspect-points dfs_point_cloud

timestamp(ms)| # points | # bytes |  Format
     1728549 |     4800 |   57600 | Float XYZ
```


## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-points.c).

