---
layout: default
title: VOXL Inspect GPS
parent: Inspect Tools
search_exclude: true
nav_order: 35
permalink: /voxl-inspect-gps/
---

# VOXL Inspect GPS
{: .no_toc }


This tool subscribes to the `/run/mpa/vvpx4_gps_raw_int` pipe published by [voxl-vision-px4](/voxl-vision-px4/) which provides a copy of all GPS_RAW_INT mavlink messages received from a PX4 flight controller.

Also note that [voxl-portal](/voxl-portal/) subscribes to the same pipe and also shows GPS satellites in the top right corner of the webpage.

---

## Use

```
yocto:/$ voxl-inspect-gps

 dt(ms) |fix type|Sats|Latitude(deg)|Longitude(deg)| Alt(m) |Vel(m/s)|HorErr(m)|AltErr(m)|
  200.2 | 3D FIX |  5 |  32.9007509 | -117.2223741 |   10.9 |   0.02 |  33.962 |  30.965 |
```

## Troubleshooting

If no data is displayed, then either [voxl-vision-px4](/voxl-vision-px4/) is not running or PX4 is not connected or operating. PX4 may also not be connected to a GPS or configured to enable it.


## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-gps.c).
