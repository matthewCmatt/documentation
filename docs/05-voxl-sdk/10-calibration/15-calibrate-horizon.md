---
title: Calibrate Horizon
layout: default
parent: Calibration
has_children: true
nav_order: 15
permalink: /calibrate-horizon/
---


# Calibrate Level Horizon

This calibration process is an in-flight alternative to the level horizon calibration method in QGC. By doing this in flight, airframe asymmetries are taken into account, and you do not need a perfectly level surface with perfectly level landing gear.

We still recommend having QGC open to monitor PX4 and battery health. `voxl-vision-hub` will announce when it has started and completed the calibration process through a mavlink message to QGC, so turn the volume up so you can hear when it is finished.

This procedure should be done in a large enough room to avoid excessive turbulence. Make sure airflow in the room is minimized, including turning off fans and AC units.

When you are ready, take off in position mode and fly/move around for 20-30 seconds to allow VIO to stabilize and for EKF2 to converge on its IMU bias. Then navigate the drone to the middle of the room and leave it hovering.

`voxl-vision-hub` will monitor the roll/pitch values reported by PX4 until the drone remains still enough for 20 seconds to be confident that the measurement is correct. If the still condition is met, it will write new SENS_BOARD_X_OFF & SENS_BOARD_Y_OFF parameters to PX4. The drone may twitch a little in the air depending on how far off it was. `voxl-vision-hub` will then announce through QGC that the calibration is complete.

If you have a particularly wobbly drone or a lot of turbulence, then the stationary condition may not ever be met. You can increase the allowable noise tolerance in `/etc/modalai/voxl-vision-px4.conf` with the field `horizon_cal_tolerance`. Note that increasing this will reduce the accuracy of the calibration.


```
voxl2:/$ voxl-calibrate-px4-horizon

Press ENTER to start the calibration process or Ctrl-C to quit


calibration started
voxl2:/$
```

# TODO: add Video

