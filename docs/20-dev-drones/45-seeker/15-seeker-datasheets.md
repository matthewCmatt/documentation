---
layout: default
title: Seeker Datasheet
parent: Seeker
nav_order: 10
has_children: false
permalink: /seeker-datasheet/
---

# Seeker Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL Seeker is a compact development drone. Use cases are for reconnaissance, mapping and photogrammetry, indoor asset inspection, swarming, GPS denied navigation. See below for specifications.

## High-Level Specs

![Seeker-flowchart](/images/seeker/Seeker-Flowchart.png)
![Seeker-labeled](/images/seeker/seeker-labeled.png)


### Flight Specs

| Component                              | Specification                                                                                                    |
|----------------------------------------|------------------------------------------------------------------------------------------------------------------|
| Take-off Weight                        | 550g                                                                                                             |
| Diagonal Size                          | 270mm motor to motor diagonal <br> 6 inch propellers                                                             |
| Operating Environment                  | Inside - with ModalAI VIO navigation <br> Outside - with GPS or VIO navigation                                   |
| IP Rating                              | None                                                                                                             |
| NDAA Compliant                         | Yes                                                                                                              |
| Blue UAS Framework Compliant           | Yes                                                                                                              |
| Country of Origin                      | USA                                                                                                              |
| Flight Time                            | ~27 minutes                                                                                                      |
| Payload Capacity (Impacts flight time) | 50 - 100g                                                                                                        |
| Motors                                 | QTY. 4 T-motor F2203.5 1500kv Motors                                                                             |
| Propellers                             | QTY. 4 6 inch propellers                                                                                         |
| Frame                                  | 270mm Carbon fiber air frame manufactured from 1/8” carbon panels                                                |
| ESCs                                   | [ModalAI VOXL 4-in-1 ESC V2](https://docs.modalai.com/modal-esc-datasheet/)                                     |
| Battery                                | 11.1V 3S 3000mAh 10C Li-Ion Battery - XT30 <br> 3S SONYVTC6 (recommended) 18650 (triangle orientation packaging) |
| Landing Gear                           | Custom landing gear (front only)                                                                                 |
| Maximum Speed                          | 5m/s per PX4 setting                                                                                             |
| Maximum Height                         | 122m per PX4 setting                                                                                             |
| Geographic Limits System               | [PX4 Geo Fencing](https://docs.px4.io/v1.12/en/flying/geofence.html)                                             |
| GPS                                    | Ublox Neo-M8N module                                                                                             |
| Power Module                           | [ModalAI Power Module v3 - 5V/6A](https://docs.modalai.com/power-module-v3-datasheet/)                           |
| Fail Safe                              | [PX4 Safety Configuration](https://docs.px4.io/v1.12/en/config/safety.html)                                      |
| Battery                                | 4S with XT60 Connector (6S with expanded battery standoffs), 4S weight 336g                                      |
| Operating Temperature                  | +5*C to +40*C                                                                                                    |
| Wind Performance                       | Continuous 20MPH, Gusts over 25MPH                                                                               |

### Processing Specs

| Component              | Specification                                                                                                                                                              |
|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CPU                    | Qualcomm® Snapdragon 821                                                                                                                                                   |
| Sensors                | 1 ICM-42688-P IMUs <br> 2 Barometers (BMP338 and TDK ICP-10100) <br> GPS (Holybro 2nd GPS) <br> Magnetometer Connection (Dronecode compliant) <br> Spare SPI and GPIO port |
| Image Sensors          | Tracking camera OV7251-A36 <br> Time of Flight Module 68FX053B <br> Front OV7251-A50 stereo cameras                                                                        |
| Additional I/O         | 2 GPIO <br> I2C <br> 1 SPI                                                                                                                                                 |
| Operating System       | Linux Yocto Jethro with 3.18 Kernel                                                                                                                                        |
| Open Applications      | Docker <br> ROS <br> PX4 Software                                                                                                                                          |
| ModalAI Software       | [VOXL®-SDK](https://docs.modalai.com/voxl-sdk)                                                                                                                             |
| Video Transmission     | Wi-Fi (2.4GHz or 5GHz) or Cellular (multi-band, carrier dependent)                                                                                                         |
| Developer Connectivity | QGroundControl <br> RViz <br> ADB (Android Debug Bridge)                                                                                                                   |

### Radio Specifications 

| Radio                  | Frequency                             | Power        | Regulatory                                                                                                          |
|------------------------|---------------------------------------|--------------|---------------------------------------------------------------------------------------------------------------------|
| Remote Control         | Spektrum DSMX Receiver (2.4GHz)       |              |                                                                                                                     |
| Wi-Fi                  | 2.4GHz / 5.8GHz                       | 175mW / 83mW | [US FCC ID:PPD-QCNFA324](https://fccid.io/PPD-QCNFA324) [Japan MIC ID: 003-140224](https://gitekidb.com/003-140224) |
| 4G Cellular (Optional) | [4G Modem Datasheet](/4G-LTE-Modems/) |              |                                                                                                                     |
