---
layout: default
title: Connect to Seeker
parent: Seeker User Guide
nav_order: 20
has_children: false
permalink: /seeker-user-guide-adb/
---

# Connect to Seeker
{: .no_toc }

You can connect to Seeker through softap or through ADB. Follow either of these guides to connect to Seeker:

1. TOC
{:toc}

## Connect to Seeker using WiFi

### Connect to VOXL Access Point

From the factory, the vehicle will power up as a WiFi Access Point with a name like `VOXL:XX:XX:XX:XX:XX`).  You can connect to it with a password of `1234567890`.

![voxl-ap-list.png](/images/m500/voxl-ap-list.png)

![voxl-ap-password.png](/images/m500/voxl-ap-password.png)

Once connected to the VOXL AP, you'll get an IP address from VOXL via DHCP, locate the address, in my case it's `192.168.8.51`.

![voxl-ap-ip-address.png](/images/m500/voxl-ap-ip-address.png)

More details can be found at the [VOXL WiFi quickstart page](/wifi-setup/).


### SSH into VOXL

The VOXL's IP address by default is `192.168.8.1`.  SSH into VOXL using the `root` user with password `oelinux123`.

More details available at the [VOXL SSH quickstart page](/ssh-to-voxl/).

![voxl-ssh.png](/images/m500/voxl-ssh.png)

---

## Connect to Seeker with ADB

### Setup ADB
See [here](/setup-adb/)

### Connect to Seeker with ADB

After installing adb, plug a USB cable into Seeker. 

![seeker-usb-gif](/images/seeker/seeker-usb.gif)


Then, run the following:

```bash
adb shell
```

Now you are running a shell inside Seeker's Ubuntu OS!

---

[Next Step: Connect Seeker to QGC](/seeker-user-guide-network/){: .btn .btn-green }
