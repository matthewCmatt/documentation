---
layout: default
title: Qualcomm Flight RB5 Functional Description
parent: Qualcomm Flight RB5 Datasheets
nav_order: 20
has_children: False
permalink: /Qualcomm-Flight-RB5-functional-description/
search_exclude: true
---


# Qualcomm Flight RB5 Reference Drone Functional Description
[Buy Here](https://www.modalai.com/rb5-flight)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---
![RB5](/images/rb5/rb5-hero-final.jpg)

## High-Level Specs

### Flight Specs

| Component                | Specification                                                                          |
|--------------------------|----------------------------------------------------------------------------------------|
| Take-off Weight          | 1235g (899g without battery)                                                           |
| Diagonal Size            | 480mm                                                                                  |
| Flight Time              | > 20 minutes                                                                           |
| Payload Capacity         | 1kg                                                                                    |
| Motors                   | Holybro 2216-880kv Motors                                                              |
| Propellers               | QTY. 4, 10 inch / 254mm                                                                |
| Frame                    | Holybro S500 V2 Frame Kit                                                              |
| ESCs                     | [ModalAI VOXL 4-in-1 ESC V2](https://docs.modalai.com/modal-esc-datasheet/)           |
| Maximum Speed            | 5m/s per PX4 setting                                                                   |
| Maximum Height           | 122m per PX4 setting                                                                   |
| Geographic Limits System | [PX4 Geo Fencing](https://docs.px4.io/v1.12/en/flying/geofence.html)                   |
| GPS                      | Ublox Neo-M8N module                                                                   |
| Power Module             | [ModalAI Power Module v3 - 5V/6A](https://docs.modalai.com/power-module-v3-datasheet/) |
| Fail Safe                | [PX4 Safety Configuration](https://docs.px4.io/v1.12/en/config/safety.html)            |
| Battery                  | 4S with XT60 Connector (6S with expanded battery standoffs), 4S weight 336g            |


### Processing Specs

| Component                  | Specification                                                                                                                                                                                                                                    |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CPU                        | Qualcomm QRB5165                                                                                                                                                                                                                                 |
| Sensors                    | 2 TDK CH-201 ultrasonic rangefinder sensors <br> TDK ICM-42688-P IMU <br> TDK ICP-10100 Barometer <br> GPS/Magnetometer (Holybro)                                                                                                                |
| Image Sensors              | Tracking Sensor - Global Shutter VGA <br> High-resolution 4k30 <br> Front and Rear Stereo - Global Shutter VGA <br> 6x 4-lane MIPI-CSI2 (Up to 7 image sensors)                                                                                  |
| Connections                | USB 3.1 Hub <br> USB-C OTG                                                                                                                                                                                                                       |
| Additional I/O             | 2 GPIO <br> I2C, 4 SPI                                                                                                                                                                                                                           |
| Video Transmission         | Wi-Fi (2.4GHz or 5GHz) or Cellular (multi-band, carrier dependent)                                                                                                                                                                               |
| Expansions                 | ModalAI Legacy B2B connector for backwards compatibility with 4G add-on, Microhard add-on, USB debug board. <br> High speed B2B connector for PCIe2, I2C, SPI, UART, GPIO <br> TDK Chirp Sensor breakout PCBAs for object avoidance capabilities |
| Optional Expansion Add-Ons | 5G modem add-on with USB 3.1 Hub <br> SD Card add-on <br> Gigabit Ethernet add-on with USB 3.1 Hub                                                                                                                                               |
| Open Software Packages     | Ubuntu 18.04 <br> Linux Kernel 4.19 <br> OpenCV <br> TensorFlow Lite <br> ROS 1 / ROS 2 <br> RB5 Flight SDK hosted by ModalAI                                                                                                                    |
| Developer Connectivity     | QGroundControl <br> RViz <br> ADB (Android Debug Bridge)                                                                                                                                                                                         |

### Radio Specifications

| Radio                                                                                      | Frequency                                                                                      | Power        | Regulatory                                       |
|--------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|--------------|--------------------------------------------------|
| Remote Control                                                                             | Spektrum DSMX Receiver (2.4GHz)                                                                |              |                                                  |
| Wi-Fi6 (Wi-Fi 802.11 a/b/g/n/ac/ax)                                                        | 2.4GHz / 5.8GHz                                                                                | 175mW / 83mW | [FCC](https://fccid.io/2AOHHTURBOXC865)          |
| 5G Cellular (Optional) [Quectel RM502Q-AE](https://www.quectel.com/product/5g-rm502q-ae/)) | 5G NR bands worldwide (n1/n2/n3/n5/n7/n8/n12/n20/n25/ n28/n38/n40/n41/n48/n66/n71/n77/n78/n79) |              | [FCC](https://fcc.report/FCC-ID/XMR2020RM502QAE) |


### Drone Dimensions

| Component                 | Specification         |
|---------------------------|-----------------------|
| Motor to Motor            | 334mm x 334mm x 133mm |
| Fully Extended Propellers | 591mm x 591mm x 187mm |


#### 2D Drone Dimension Drawings

![rb5-drone-2D-dims.png](/images/rb5/rb5-drone-2D-dims.png)

### Sub-component Dimensions

| PCB                                             | H x W x D        | Weight | Drawings                                                                                                                                                                                                                             |
|-------------------------------------------------|------------------|--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| QC Flight RB5 Mainboard (M0052)                 | 50 x 84 x 10.9mm | 24.8g  | [2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0052-mechanical-drawing%20v4.pdf) <br> [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0052_RB5_FLIGHT_REVA.step)                 |
| ModalAI 5G Modem Carrier Board (M0067 or M0090) | 67.5 x 36 x 10mm | 12.8g  | [2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0067-mechanical-drawing%20v1.pdf) <br> [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0067_VOXL2_5G_MODEM_DEBUG_REVA_S1_P9.step) |
| Quectel RM502Q-AE                               | 52× 30 × 2.3mm   | 8.7g   | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Quectel_RM500Q-AERM502Q-AE_Hardware_Design_V1.0.pdf)                                                                                               |
| Thundercomm RB5 SOM                             | 45 x 56 x 9mm    | 17.7g  | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Thundercomm-SOM-5165-drawing%20v2.pdf)                                                                                                             |
| Landing Skids                                   |                  |        | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/RB5_landing_gear_skid_v3.STEP)                                                                                                                                |
| Landing Skid Mount                              |                  |        | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/RB5_landing_gear_mount.STEP)                                                                                                                                  |


## System Overview

## High-level Overview

[View in fullsize](/images/rb5/RB5-System-Diagram.png){:target="_blank"}
![RB5.png](/images/rb5/RB5-System-Diagram.png)

## Low-level Block Diagram

[View in fullsize](/images/rb5/rb5-flight-block-diagram.png){:target="_blank"}
![RB5.png](/images/rb5/rb5-flight-block-diagram.png)

## Qualcomm Flight RB5 Core Components

### Labeled Diagrams

[View in fullsize](/images/rb5/rb5-labeled-5.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-5.png)

[View in fullsize](/images/rb5/rb5-labeled-6.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-6.png)

[View in fullsize](/images/rb5/rb5-labeled-3.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-3.png)

[View in fullsize](/images/rb5/rb5-labeled-4.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-4.png)

[View in fullsize](/images/rb5/rb5-labeled-2.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-2.png)

[View in fullsize](/images/rb5/rb5-labeled-1.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/rb5/rb5-labeled-1.png)

### Core Components Part Numbers

#### Reference Drone Part Numbers

| ModalAI Part Number            | Description                                                                           |
|--------------------------------|---------------------------------------------------------------------------------------|
| MRB-D0004-3-F1-B1-C11-M7-T1-K0 | Qualcomm Flight RB5 5G Platform Drone Reference Design and Quectel RM502Q-AE 5G Modem |
| MRB-D0004-3-F1-B1-C11-M0-T1-K0 | Qualcomm Flight RB5 5G Platform Drone Reference Design                                |


#### Sub-component Part Numbers

The reference drone is built around the Qualcomm Flight RB5 Flight Dec, MDK-M0052-1-03 'OEM Kit', which includes:

| ModalAI Part Number                         | Description                         |
|---------------------------------------------|-------------------------------------|
| M10000061                                   | Thundercomm RB5 SOM (QRB5165)       |
| MCCA-00090-1-T                              | Modem Carrier Board                 |
| MCCA-M0052-1-T                              | Qualcomm Flight RB5 Mainboard       |
| MSU-M0014-1-01                              | Tracking Image Sensor               |
| MPS-00005-1                                 | Wall Power Supply                   |
| MDK-M0041-1-B-01                            | APMv3 Power Module                  |
| MSU-M0025-2-01                              | 4k High-resolution RGB Image Sensor |
| [MCBL-00005](/cable-datasheets/#mcbl-00005) | FC-RC-Input                         |
| [MCBL-00029](/cable-datasheets/#mcbl-00029) | QC Flight RB5 to VOXL ESC cables    |
| MSU-00084-1                                 | H-flex cable for 2 image sensors    |
| M10000056                                   | Tracking Image Sensor Mount         |
| MDK-M0049-1-01                              | VOXL ESC                            |



## PX4 Tuning Parameters

Our engineers have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Motor Configuration

The propellers have arrows on them as to which way they should turn. Motor configuration in the image below.

![voxl-m500-motor-configuration.png](/images/m500/voxl-m500-motor-configuration.png)

## Connectivity for Remote Operation

| Connectivity Option | Use Case | Details |
| --- | --- | --- |
| Spektrum R/C | Manual remote control of the vehicle | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/) |
| 5G LTE | Long Range, BVLOS operation | [Setup](https://docs.modalai.com/Qualcomm-Flight-RB5-user-guide-connect-gcs/) |







