---
layout: default
title: Qualcomm Flight RB5 Carrier Board Connectors
parent: Qualcomm Flight RB5 Datasheets
nav_order: 20
has_children: False
permalink: /Qualcomm-Flight-RB5-connectors/
search_exclude: true
---

# Qualcomm Flight RB5 Flight Carrier Board Connectors
{: .no_toc }


## Overview

All single ended signals on B2B connectors J3, J5, J6, J7, and J8 are 1.8V CMOS unless explicitly noted. 
All single ended signals on cable-to-board connectors J10, J11, J12,J18, & J19 are 3.3V CMOS unless explicitly noted.

| Connector | Description         | MPN (Board Side)         | Mating MPN (Board/Cable Side) | Type                                | Signal Feature Summary                                                                                                                                                                     |
|-----------|---------------------|--------------------------|-------------------------------|-------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| J1        | SOM Interface       | SEAF8-50-05.0-L-10-2-K   | SOM                           | High Density 500-pin B2B            | All power and signal interfaces to the RB5 SOM                                                                                                                                             |
| J2        | Fan                 | SM02B-SRSS-TB(LF)(SN)    | SHR-02V-S                     | Cable Header, 2-pin R/A             | 5V DC for FAN + PWM Controlled FAN-Return (GND)                                                                                                                                            |
| J3        | Legacy B2B          | QSH-030-01-L-D-K-TR      | QTH-030-01-L-D-A-K-TR         | B2B Receptacle, 60-pin              | 5V/3.8V/3.3V/1.8V power for plug-in boards, JTAG and Debug Signals, QUP expansion, GPIOs, USB3.1 Gen 2 (USB1)                                                                              |
| J4        | Prime Power In      | 22057045                 | 0050375043                    | Cable Connector, 4-pin R/A          | +5V main DC power in + GND, I2C@5V for power monitors                                                                                                                                      |
| J5        | High Speed B2B      | ADF6-30-03.5-L-4-2-A-TR  | ADM6-30-01.5-L-4-2-A-TR       | B2B Socket, 120-pin                 | More 3.8V/3.3V/1.8V power for plug-in boards, 5V power in for “SOM Mode”, QUP expansion, GPIOs, SDCC (SD Card V3.0), UFS1 (secondary UFS Flash), 2L PCIe Gen 3, AMUX and SPMI PMIC signals |
| J6        | Camera Group 0      | DF40C-60DP-0.4V(51)      | DF40C-60DS-0.4V               | B2B Plug, 60-pin                    | Qty-2 4L MIPI CSI ports, CCI and camera control signals, 8 power rails (from 1.05V up to 5V) for cameras and other sensors, dedicated SPI (QUP) port                                       |
| J7        | Camera Group 1      | DF40C-60DP-0.4V(51)      | DF40C-60DS-0.4V               | B2B Plug, 60-pin                    | Qty-2 4L MIPI CSI ports, CCI and camera control signals, 8 power rails (from 1.05V up to 5V) for cameras and other sensors, dedicated SPI (QUP) port                                       |
| J8        | Camera Group 2      | DF40C-60DP-0.4V(51)      | DF40C-60DS-0.4V               | B2B Plug, 60-pin                    | Qty-2 4L MIPI CSI ports, CCI and camera control signals, 8 power rails (from 1.05V up to 5V) for cameras and other sensors, dedicated SPI (QUP) port                                       |
| J9        | USB-C               | UJ31-CH-3-SMT-TR         | USB Type-C                    | Cable Receptacle, 24-pin R/A        | USB-C with re-driver and display port alternate mode (USB0)                                                                                                                                |
| J10       | SPI Expansion       | SM08B-GHS-TB(LF)(SN)     | GHR-08V-S                     | Cable Header, 8-pin R/A             | SPI@3.3V with 2 CS_N pins, 32kHz CLK_OUT@3.3V                                                                                                                                              |
| J11       | Reserved for Future | SM04B-GHS-TB(LF)(SN)     | GHR-04V-S                     | Cable Header, 4-pin R/A             | Spare I2C not pinned on SOM, reserved for future use. Do not use.                                                                                                                          |
| J12       | R/C UART            | BM04B-GHL-TBT(LF)(SN)(N) | GHR-04V-S                     | Yellow Cable Header, 4-pin Vertical | R/C UART@3.3V, 3.3V_RC switchable voltage                                                                                                                                                  |
| J18       | ESC                 | SM04B-GHS-TB(LF)(SN)     | GHR-04V-S                     | Cable Header, 4-pin R/A             | ESC UART@3.3V, 3.3V reference voltage                                                                                                                                                      |
| J19       | GNSS/MAG            | SM06B-GHS-TB(LF)(SN)     | GHR-06V-S                     | Cable Header, 6-pin R/A             | GNSS UART@3.3V, Magnetometer I2C@3.3V, 5V                                                                                                                                                  |

## Connectors

### J2 Fan

#### J2 Fan Pin-out

| Pin# | Signal | Notes/Usage	|
| --- |	---	| --- |
| 1	| VDC_5V_LOCAL | 5V protected power output * |
| 2	| FAN RETURN (GND) | Return limited to ~400mA |

### J3 Legacy Board to Board Connector

The Legacy Board to Board connector is designed to host VOXL Add-ons such as the LTE Add-on v2 and the Microhard Add-on.

| Connector | MPN |
| --- | --- | 
| Board Connector | QSH-030-01-L-D-K-TR |
| Mating Connector | QTH-030-01-L-D-A-K-TR |

#### J3 Pin-out

| Odd Pin # | Signal/Voltage | Even Pin # | Signal/Voltage |
| --- | --- | --- | --- |
| 1 | DGND | 2 | VDC_5V_LOCAL |
| 3 | GPIO_23_UART7_RXD | 4 | VDC_5V_LOCAL |
| 5 | GPIO_22_UART7_TXD | 6 | VDC_5V_LOCAL |
| 7 | GPIO_52_SPI17_MISO | 8 | USB1_HS_ID_LEGACY (Normally N.C.) |
| 9 | GPIO_53_SPI17_MOSI | 10 | DGND |
| 11 | DGND | 12 | USB1_HS_DM |
| 13 | GPIO_126_I2C9_SCL | 14 | USB1_HS_DP |
| 15 | GPIO_125_I2C9_SDA | 16 | VDC_5V_LOCAL_USB1 |
| 17 | GPIO_55_SPI17_CS | 18 | DGND |
| 19 | GPIO_54_SPI17_SCLK | 20 | USB1_SS_TX_M |
| 21 | DGND | 22 | USB1_SS_TX_P |
| 23 | GPIO_130_I2C10_SCL | 24 | GPIO_20 |
| 25 | GPIO_129_I2C10_SDA | 26 | GPIO_21 |
| 27 | GPIO_35_DBG_UART12_RX | 28 | GPIO_32_QUP12_L0 |
| 29 | GPIO_34_DBG_UART12_TX | 30 | GPIO_33_QUP12_L1 |
| 31 | DGND | 32 | USB1_SS_RX_M |
| 33 | JTAG_SRST_N | 34 | USB1_SS_RX_P |
| 35 | JTAG_TCK | 36 | DGND |
| 37 | JTAG_TDI | 38 | GPIO_131_USB_HUB_RESET |
| 39 | JTAG_TDO | 40 | GPIO_124 |
| 41 | JTAG_TMS | 42 | GPIO_145 |
| 43 | JTAG_TRST_N | 44 | DGND |
| 45 | JTAG_PS_HOLD | 46 | GPIO_90_FAST_BOOT_3 |
| 47 | VREG_S4A_1P8 | 48 | GPIO_76_FAST_BOOT_2 |
| 49 | PM_RESIN_N | 50 | GPIO_47_SPI14_CS2_FAST_BOOT_1 |
| 51 | SDM_RESOUT_N | 52 | GPIO_27_FAST_BOOT_0 |
| 53 | VREG_3P3V_LOCAL | 54 | GPIO_128_WDOG_DIS |
| 55 | KPD_PWR_N | 56 | SDM_FORCE_USB_BOOT |
| 57 | VPH_PWR | 58 | DGND |
| 59 | DGND | 60 | CLK_PMK_PMIC |

### J4 Prime Power-in

#### J4 Prime Power-in Pin-out

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VDCIN_5V	|	DC from Power Module, “unprotected”	|
|	2	|	GND	|	Power Module Return	|
|	3	|	I2C_CLK	|	SSC_QUP_1, 5V signal levels, Pullups on Power Module	|
|	4	|	I2C_SDA	|	SSC_QUP_1, 5V signal levels, Pullups on Power Module	|

### J5 High-speed Board to Board Connector

| Connector | MPN |
| --- | --- | 
| Board Connector | ADF6-30-03.5-L-4-2-A-TR |
| Mating Connector | ADM6-30-01.5-L-4-2-A-TR |


#### J5 Pin-out

| Pin | Signal/Voltage | Pin | Signal/Voltage | Pin | Signal/Voltage | Pin | Signal/Voltage |
| --- | --- | --- | --- | --- | --- | --- | --- | 
| A1 | VDCIN_5V | B1 | VDCIN_5V | C1 | VDCIN_5V | D1 | VDCIN_5V |
| A2 | VDCIN_5V | B2 | VDCIN_5V | C2 | VDCIN_5V | D2 | VDCIN_5V |
| A3 | GND | B3 | GND | C3 | GND | D3 | GND |
| A4 | GND | B4 | GND | C4 | GND | D4 | GND |
| A5 | VREG_3P3V_LOCAL | B5 | GND | C5 | GPIO_119_SPI3_MISO | D5 | GPIO_36_UART13_CTS |
| A6 | VREG_3P3V_LOCAL | B6 | GND | C6 | GPIO_120_SPI3_MOSI | D6 | GPIO_37_UART13_RTS |
| A7 | GND | B7 | GPIO_16_QUP6_L0 | C7 | GPIO_121_SPI3_SCLK | D7 | GPIO_38_UART13_TXD |
| A8 | GPIO_115_I2C2_SDA | B8 | GPIO_17_QUP6_L1 | C8 | GPIO_122_SPI3_CS | D8 | GPIO_39_UART13_RXD |
| A9 | GPIO_116_I2C2_SCL | B9 | GPIO_18_QUP6_L2 | C9 | GPIO_24_I2C8_SDA | D9 | GPIO_8_I2C4_SDA |
| A10 | GPIO_117_QUP2_L2 | B10 | GPIO_19_QUP6_L3 | C10 | GPIO_25_I2C8_SCL | D10 | GPIO_9_I2C4_SCL |
| A11 | GPIO_118_QUP2_L3 | B11 | GPIO_155 | C11 | GND | D11 | PM8250_AMUX1 |
| A12 | SD_UFS_CARD_DET_N | B12 | GPIO_154 | C12 | GPIO_145 (intentional duplicate to Legacy B2B pin 42) | D12 | GND |
| A13 | GND | B13 | GPIO_153 | C13 | GPIO_144 | D13 | PCIE2_REFCLK_M |
| A14 | SDC2_CLK | B14 | GPIO_152 | C14 | GPIO_143 | D14 | PCIE2_REFCLK_P |
| A15 | GND | B15 | GND | C15 | GPIO_142 | D15 | GND |
| A16 | VREG_L9C_2P96 | B16 | GPIO_0_QUP19_L0 | C16 | GPIO_137 | D16 | PCIE2_RX0_M |
| A17 | SDC2_CMD | B17 | GPIO_1_QUP19_L1 | C17 | GND | D17 | PCIE2_RX0_P |
| A18 | SDC2_DATA_0 | B18 | GPIO_2_QUP19_L2 | C18 | GPIO_88 | D18 | GND |
| A19 | SDC2_DATA_1 | B19 | GPIO_3_QUP19_L3 | C19 | GPIO_89 | D19 | PCIE2_RX1_M |
| A20 | SDC2_DATA_2 | B20 | GPIO_56_I2C18_SDA | C20 | GPIO_87_PCIE2_WAKE_N | D20 | PCIE2_RX1_P |
| A21 | SDC2_DATA_3 | B21 | GPIO_57_I2C18_SCL | C21 | GPIO_86 | D21 | GND |
| A22 | GND | B22 | GND | C22 | GPIO_85 | D22 | PCIE2_TX0_M |
| A23 | UFS1_REFCLK | B23 | GPIO_60_QUP11_L0 | C23 | GND | D23 | PCIE2_TX0_P |
| A24 | GND | B24 | GPIO_61_QUP11_L1 | C24 | PMIC_8150L_AMUX1 | D24 | GND |
| A25 | UFS1_TX0_M | B25 | GPIO_62_QUP11_L2 | C25 | GND | D25 | PCIE2_TX1_M |
| A26 | UFS1_TX0_P | B26 | GPIO_63_QUP11_L3 | C26 | GND | D26 | PCIE2_TX1_P |
| A27 | GND | B27 | GND | C27 | GND | D27 | GND |
| A28 | UFS1_RX0_M | B28 | SPMI_CLK | C28 | VPH_PWR_3P8V | D28 | GND |
| A29 | UFS1_RX0_P | B29 | SPMI_DATA | C29 | VPH_PWR_3P8V | D29 | VREG_S4A_1P8 |
| A30 | GND | B30 | GND | C30 | VPH_PWR_3P8V | D30 | VREG_S4A_1P8 |

### J6, J7, and J8 Camera Group [0:2] High-speed Board to Board Connector

| Connector | MPN |
| --- | --- | 
| Board Connector | DF40C-60DP-0.4V(51) |
| Mating Connector | DF40C-60DS-0.4V |


#### J6, J7, and J8 Pin-out (generic group pinout, please work with ModalAI for any mating designs to gauranteee proper operation)

|	Pin#	|	Signal	|
| --- | --- |
|	1	|	GND	|
|	2	|	GND	|
|	3	|	Lower CCI_I2C_SDA	|
|	4	|	DVDD 1.2V	|
|	5	|	Lower CCI_I2C_SCL	|
|	6	|	DOVDD 1.8V	|
|	7	|	GND	|
|	8	|	DVDD 1.05V	|
|	9	|	Lower CSI_CLK_P	|
|	10	|	Lower RST_N	|
|	11	|	Lower CSI_CLK_M	|
|	12	|	Lower MCLK	|
|	13	|	Lower CSI_DAT0_P	|
|	14	|	GND	|
|	15	|	Lower CSI_DAT0_M	|
|	16	|	Lower CCI Timer	|
|	17	|	GND	|
|	18	|	Upper CCI Timer	|
|	19	|	Lower CSI_DATA1_P	|
|	20	|	Upper MCLK	|
|	21	|	Lower CSI_DATA1_M	|
|	22	|	AVDD 2.8V	|
|	23	|	Lower CSI_DATA2_P	|
|	24	|	GND	|
|	25	|	Lower CSI_DATA2_M	|
|	26	|	Upper RST_N, Shared	|
|	27	|	GND	|
|	28	|	Upper CCI_I2C_SDA	|
|	29	|	Lower CSI_DATA3_P	|
|	30	|	Upper CCI_I2C_SCL	|
|	31	|	Lower CSI_DATA3_M	|
|	32	|	Spare MCLK/GPIO	|
|	33	|	GND	|
|	34	|	Group SPI MISO	|
|	35	|	Upper CSI_CLK_P	|
|	36	|	Group SPI MOSI	|
|	37	|	Upper CSI_CLK_M	|
|	38	|	Group SPI SCLK	|
|	39	|	Upper CSI_DATA0_P	|
|	40	|	Group SPI CS_N	|
|	41	|	Upper CSI_DATA0_M	|
|	42	|	VREG_S4A_1P8	|
|	43	|	GND	|
|	44	|	GND	|
|	45	|	Upper CSI_DATA1_P	|
|	46	|	VPH_PWR 3.8V	|
|	47	|	Upper CSI_DATA1_M	|
|	48	|	VPH_PWR 3.8V	|
|	49	|	Upper CSI_DATA2_P	|
|	50	|	GND	|
|	51	|	Upper CSI_DATA2_M	|
|	52	|	3.3V	|
|	53	|	GND	|
|	54	|	GND	|
|	55	|	Upper CSI_DATA3_P	|
|	56	|	5V	|
|	57	|	Upper CSI_DATA3_M	|
|	58	|	5V	|
|	59	|	GND	|
|	60	|	GND	|


### J10 SPI Expansion

#### J10 SPI Expansion Pin-out

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VREG_3P3V_LOCAL	|	3.3V Power Output *	|
|	2	|	MISO (Input)	|	APPS_QUP_14, 3.3V signal levels	|
|	3	|	MOSI (Output)	|	APPS_QUP_14, 3.3V signal levels	|
|	4	|	SCLK (Output)	|	APPS_QUP_14, 3.3V signal levels	|
|	5	|	CS0_N (Output)	|	APPS_QUP_14, 3.3V signal levels	|
|	6	|	CS1_N/GPIO_46 (Output)	|	Second SPI CS_N or GPIO	|
|	7	|	32K_CLK_OUT (Output)	|	32kHz PMIC Sleep CLK, 3.3V signal levels	|
|	8	|	GND	|	GND	|


### J12 R/C UART (Spektrum/DSMX)

#### J12 R/C UART (Spektrum/DSMX) Pin-out

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VREG_3P3V_RCL	|	3.3V Switchable Power Output *	|
|	2	|	RC_UART_TX (Output)	|	APPS_QUP_13, 3.3V signal levels	|
|	3	|	RC_UART_RX (Input)	|	APPS_QUP_13, 3.3V signal levels	|
|	4	|	GND	|	GND	|

### J18 ESC UART

#### J18 ESC UART

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VREG_3P3V_LOCAL	|	3.3V Power Output *	|
|	2	|	ESC_UART_TX (Output)	|	SSC_QUP_2, 3.3V signal levels.	|
|	3	|	ESC_UART_RX (Input)	|	SSC_QUP_2, 3.3V signal levels.	|
|	4	|	GND	|	GND	|

### J19 GNSS/MAG

#### J19 GNSS/MAG

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VDC_5V_LOCAL	|	5V protected Power Output *	|
|	2	|	GNSS UART TX (output)	|	APPS_QUP_18, 3.3V signal levels	|
|	3	|	GNSS UART RX (input)	|	APPS_QUP_18, 3.3V signal levels	|
|	4	|	MAG I2C SCL	|	SSC_QUP_0, 3.3V signal levels, 2.2K Pullup to VREG_3P3V_LOCAL	|
|	5	|	MAG I2C SDA	|	SSC_QUP_0, 3.3V signal levels, 2.2K Pullup to VREG_3P3V_LOCAL	|
|	6	|	GND	|	GND	|



### Power Input/Output Important Note: 
* All power outputs on cable connectors are rated for 1A, however, the system cannot provide 1A simultaneously on all connectors. 
Contact ModalAI for design assistance.

* The difference between VDCIN_5V and VDC_5V_LOCAL is very important. The power module provides VDCIN_5V (raw voltage input) to the platform. On-board is an eFuse that protects the system from accidental wrong-voltage application, droops/brown-outs, or down-stream shorts or overloads. The output of the eFuse is VDC_5V_LOCAL (i.e.: protected output).








