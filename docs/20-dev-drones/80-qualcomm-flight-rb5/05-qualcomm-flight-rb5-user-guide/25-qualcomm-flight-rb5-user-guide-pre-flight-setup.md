---
layout: default
title: RB5 Pre-Flight Setup
parent: Qualcomm Flight RB5 User Guide
nav_order: 25
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-pre-flight-setup/
search_exclude: true
---

# Qualcomm Flight RB5 Pre-Flight Setup
{: .no_toc }

## Using QGroundControl for QGC

We will be using QGroundControl as a means to perform the pre-flight setups and checks as needed.

## Sensor Calibration

Qualcomm Flight RB5 flight sensors are delivered pre-calibrated from the factory. PX4 will warn you if your compass needs to be re-calibrated depending on your location and environmental conditions. If so, or if you'd like re-calibrate all sensors, follow the [px4 sensor calibration precedure](/px4-calibrate-sensors/).

## RC Radio Setup

### Bind Receiver to Transmitter


The RB5 has a Spektrum receiver that must be paired (or "bound") with a transmitter.  Follow these instructions to pair your Spektrum transmitter to RB5. 

<b> Version 1.0 </b>

<p style="color:red"> <b> Early RB5 shipments came with an additional piece of hardware with instructions to enable the receiver to be put in binding mode until a software solution was available. </b> </p>

**If your RB5 has a 5V outlet on the side or if it came with a binding plug (pictured below), see the RB5 quickstart [Here](/Qualcomm-Flight-RB5-quickstart/) for binding instructions.**

<img src="/images/rb5/rb5-bind-old.jpg" alt="old-bind">
<img src="/images/rb5/rb5-bind-old2.jpg" alt="old-bind-2">


---
<b> Version 1.1 </b>

<b> *For later RB5 Shipments, follow these instructions:* </b>


First, make sure your RB5 is powered on.

Next, connect via ADB by plugging the USB-C into RB5's flight deck. 
Then, open up the terminal and type:

```bash
adb shell
```

To use the binding utility, use

```bash
rb5-bind
```

Follow the on-screen prompts. Select your bind type.
```bash
Tool used for binding Spektrum receiver. In order to use, select ONLY 1 bind type.
-d, --debug       enable debug messages
-h, --help        print this help messag
-t, --DSM2_22     3 pulses (DSM2 1024/22ms)
-f, --DSM2_11     5 pulses (DSM2 2048/11ms)
-s, --DSMX_22     7 pulses (DSMX 22ms)
```

Once you select your type, you will see the message:

```bash
An orange light should now be flashing on your receiver.
If so, your receiver is successfully in binding mode
```

Check that the receiver is in binding mode. The light should flash.

![binding-1](/images/rb5/rb5-bind-1.gif)

Once in binding mode, pair your transmitter to the receiver. The exact instructions for pairing depend on your transmitter. Check with your transmitter's manufacturer for up to date information.

![binding-3](/images/rb5/rb5-bind-3.jpg)


Once the transmitter is paired, make sure the receiver light is solid orange.

![binding-2](/images/rb5/rb5-bind-2.gif)

<p style="color:red"> <b> Make sure to power cycle your RB5 before continuing </b> </p>

### Calibrate Radio

Once bound, power cycle the vehicle and restart QGroundControl, otherwise the radio channels will fail to show up.  Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/voxl-sdk/qgc/8-calibrate-radio.png)

### Confirm RC Settings

Every user will want to use different flight modes and different switch assignments, but for getting started with Qualcomm Flight RB5 we suggest starting with something similar to this configuration and working from there.

- "Flap Gyro" switch left of Spektrum Logo
  - Channel 6
    - Up position:     Manual Flight Mode
    - Middle Position: Position Flight Mode
    - Down Position:   Offboard Flight mode

- "Aux2 Gov" switch right of Spektrum Logo
  - Channel 7
    - Up position:     Motor Kill Switch Engaged
    - Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/voxl-sdk/qgc/9-flight-mode-config.png)

If you have a Spektrum DX6e or DX8 radio with a clean acro-mode model you can accomplish the above channel mapping by loading the following config file

[https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params](https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params)

Still confirm the mapping in QGroundControl before flight!

[Next Step: First Flight](/Qualcomm-Flight-RB5-user-guide-first-flight/){: .btn .btn-green }