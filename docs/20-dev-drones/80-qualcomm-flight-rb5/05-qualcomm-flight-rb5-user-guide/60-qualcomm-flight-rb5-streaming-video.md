---
layout: default
title: RB5 Streaming Video
parent: Qualcomm Flight RB5 User Guide
nav_order: 60
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-streaming-video/
search_exclude: true
---

# Qualcomm Flight RB5 Streaming Video
{: .no_toc }

## Streaming Video over Network

[rb5-streamer](https://docs.modalai.com/Qualcomm-Flight-RB5-streamer/#rb5-streamer) is the software package used to stream video. It streams video over IP using RTSP.


## Streaming Video to QGroundControl


[Next Step: External Flight Controllers](/Qualcomm-Flight-RB5-user-guide-external-flight-controller/){: .btn .btn-green }