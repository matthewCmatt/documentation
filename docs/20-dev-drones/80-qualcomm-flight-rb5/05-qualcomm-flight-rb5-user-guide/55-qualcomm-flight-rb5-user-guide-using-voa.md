---
layout: default
title: RB5 Using VOA
parent: Qualcomm Flight RB5 User Guide
nav_order: 55
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-using-voa/
search_exclude: true
---

# Qualcomm Flight RB5 VOA
{: .no_toc }

## Enabling VOA

VOA on the RB5 is disabled by default. The service file for VOA can be started by configuring the voa server:

```bash
rb5-voa-server-configure
```

It will show the following prompt:
```
Would you like to enable the voa service?
This will start voa as a background process
Selecting yes will start the service, no will disable the service

1) yes
2) no
```

Select ```yes``` to enable the service. This will immediately start VOA and enable it on boot

## Using VOA

In order to check if VOA is sending data to PX4, you can check that status of the service file:
```bash
systemctl status rb5-voa-to-px4
```

If the status displays ```active(running)``` then front and rear VOA are running and sending data to PX4.

if the status displayed is ```failed``` then VOA data is not being sent to PX4. It is a known issue that VOA will often fail and the messages won't be sent to PX4. In this case it is best to disable the VOA service files and start it manually.

To restart VOA sending data to PX4, you can simply restart the service file:
```bash
systemctl restart rb5-voa-to-px4
```

To restart front and rear VOA, you can restart their service files with 0 representing front VOA and 1 representing rear VOA:
```bash
systemctl restart rb5-voa-server@0
systemctl restart rb5-voa-server@1
```

## Viewing VOA Data

VOA data can be viewed using the rb5-voa-client command:
```bash
rb5-voa-client
```

**NOTE** A library path was set for this command, if you're command doesn't execute, inside your adb shell run ```ldconfig``` prior.

This will print out the obstacle map in the following format:
```
			Front
 1.1| 1.1| 1.4| 1.3| 1.5| 1.3| 1.5| 1.1| 1.1| 1.3| 1.5|

 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|
 0.0|						   0.0|

 1.3| 3.0| 3.3| 1.5| 1.1| 2.2| 1.1| 1.2| 1.0| 1.1| 1.1|
			Rear


```

The obstacle map shows the distance, in meters, of obstacles around the drone. The top row represents the depth, in 5 degree intervals, of the front 55 degrees of view. As a test you can stand about a meter away from the drone and walk from the left of the drone to the right and see how to depth changes. The bottom row represents the depth, in 5 degree intervals, of the rear 55 degrees of view. The left and right columns represent the depths of obstacles to the left and right of the drone. Those values will currently be set to 0 since left and right obstacle avoidance is disabled.

The detected distances will only be between 1.0m - 8.0m. Any distance outside of that range will display 0.0.


[Next Step: Streaming Video](/Qualcomm-Flight-RB5-user-guide-streaming-video/){: .btn .btn-green }