---
layout: default
title: RB5 External Flight Controller
parent: Qualcomm Flight RB5 User Guide
nav_order: 65
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-external-flight-controller/
search_exclude: true
---

# Qualcomm Flight RB5 External Flight Controller
{: .no_toc }

## Repurpose J12 as a UART

<p style="color:red"> <b> There is a bug in the `1.0.5-M0052-9.1-perf` release preventing this from working.  We are investigating a fix</b></p>

### Summary

Normally, PX4 uses the J12 connector's UART (3.3V TTL) for an RC input.  But, if you want to use an external flight controller you can instead use this UART as a means to communicate using something like MAVLink.

### Disable PX4 from Running on DSP

First, you need to disable PX4 from running on target.

```
systemctl disable rb5-px4-start
```

Reboot the system.

### Create a Cable

This is the J12 pinout.

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VREG_3P3V_RC	|	3.3V Switchable Power Output *	|
|	2	|	RC_UART_TX (Output)	|	APPS_QUP_13, 3.3V signal levels	|
|	3	|	RC_UART_RX (Input)	|	APPS_QUP_13, 3.3V signal levels	|
|	4	|	GND	|	GND	|

### Interface with In Linux

<p style="color:red"> <b> There is a bug in the `1.0.5-M0052-9.1-perf` release preventing this from working.  We are investigating a fix</b></p>

This UART shows up as `/dev/ttyHS1`.  Here's a simple Python example.

```python
import serial

uart_dev='/dev/ttyHS1'

uart = serial.Serial(uart_dev, 115200, xonxoff=False, rtscts=False, dsrdtr=False)
while True:
	bytes = uart.read_all()
	if len(bytes) > 0:
		print(bytes)

```

## Repurpose J19 as a UART

<p style="color:red"> <b> There is a bug in the `1.0.5-M0052-9.1-perf` release preventing this from working.  We are investigating a fix</b></p>

### Summary

Similar to above, on RB5 Flight, J19 has a UART that is typically used for GNSS/PX4.  This can be used similar as above using `/dev/ttyHS2`.

|	Pin#	|	Signal	|	Notes/Usage	|
| --- | --- | --- |
|	1	|	VDC_5V_LOCAL	|	5V protected Power Output *	|
|	2	|	GNSS UART TX (output)	|	APPS_QUP_18, 3.3V signal levels	|
|	3	|	GNSS UART RX (input)	|	APPS_QUP_18, 3.3V signal levels	|
|	4	|	MAG I2C SCL	|	SSC_QUP_0, 3.3V signal levels, 2.2K Pullup to VREG_3P3V_LOCAL	|
|	5	|	MAG I2C SDA	|	SSC_QUP_0, 3.3V signal levels, 2.2K Pullup to VREG_3P3V_LOCAL	|
|	6	|	GND	|	GND	|
