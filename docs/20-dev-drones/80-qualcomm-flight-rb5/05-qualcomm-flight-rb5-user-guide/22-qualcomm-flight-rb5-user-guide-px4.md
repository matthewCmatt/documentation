---
layout: default
title: RB5 PX4
parent: Qualcomm Flight RB5 User Guide
nav_order: 22
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-px4/
search_exclude: true
---

# Qualcomm Flight RB5 PX4
{: .no_toc }

## System Bootup Behavior

PX4 starts automatically after the RB5 has connected to a network and an IP is obtained.

By default, we look for an IP address of `192.168` on the `wlan0` interface.

This is specified in a systemd service file here:

```bash
/etc/systemd/system/rb5-net-check.service
```

You'll notice we speficy an IP and an interface in this file in the `ExecStart` line.  You can adjust as needed:

```bash
[Unit]
Description=rb5-net-check
After=sscrpcd.service
Requires=sscrpcd.service

[Service]
Type=oneshot
ExecStart=/usr/bin/rb5-net-check wlan0 192.168
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

Assuming the network is acquired, PX4 automatically starts.

## Checking PX4 Status

```bash
systemctl status rb5-px4-start
```

If it is not running, it can be enabled to run on boot using the following:

```bash
systemctl enable rb5-px4-start
```

It can also be disabled from running at boot using:

```
systemctl disable rb5-px4-start
```

More information about the services is [located here](/Qualcomm-Flight-RB5-sdk-services/)

## PX4 Startup Configuration

The startup configuration is specified here:

```bash
/etc/modalai/full-m0052.config
```

The service that starts PX4 up on bootup is here:

```bash
/etc/systemd/system/rb5-px4-start.service
```


[Next Step: Connect to Ground Station](/Qualcomm-Flight-RB5-user-guide-connect-gcs/){: .btn .btn-green }