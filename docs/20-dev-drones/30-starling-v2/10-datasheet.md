---
layout: default
title: 2. Datasheet
parent: Starling V2
nav_order: 10
has_children: false
permalink: /starling-v2-datasheet/
---

# Starling V2 Datasheet
{: .no_toc }

## Specifications

| Component       | Specification                                                                          |
|-----------------|----------------------------------------------------------------------------------------|
| Autopilot       | [VOXL2](/voxl2/)                                                                       |
| Take-off Weight | 275g (172g without battery)                                                            |
| Diagonal Size   | 211mm                                                                                  |
| Flight Time     | >30 minutes                                                                            |
| Motors          | 1504                                                                                   |
| Propellers      | 120mm                                                                                  |
| Frame           | 3mm Carbon Fiber                                                                       |
| ESC             | [ModalAI VOXL 4-in-1 ESC V2](https://docs.modalai.com/modal-esc-datasheet/)            |
| GPS             | UBlox M10                                                                              |
| RC Receiver     | 915mhz ELRS                                                                            |
| Power Module    | [ModalAI Power Module v3 - 5V/6A](https://docs.modalai.com/power-module-v3-datasheet/) |
| Battery         | Sony VTC6 3000mah 2S, or any 2S 18650 battery with XT30 connector                      |
| Height          | 83mm                                                                                   |
| Width           | 187mm (Props folded)                                                                   |
| Length          | 142mm (Props folded)                                                                   |


## Hardware Wiring Diagram

[![D0005-V2-compute-wiring](/images/starling-v2/D0005-compute-wiring-d.jpg)](/images/starling-v2/D0005-compute-wiring-d.jpg)


[Diagram draw.io Source](https://gitlab.com/voxl-public/support/drawings/-/blob/master/D0005/D0005-compute-wiring.drawio)

## 3D STEP

[Starling 3D STEP](https://developer.modalai.com/asset/6)

## Add-On Board

The add-on board used on this vehicle has an I2C, UART and GPIOs along with the USB interface.  See [datasheet](/usb2-type-a-breakout-add-on/) for pinouts.
