---
layout: default
title: 1. Hardware Overview
parent: Starling V2
nav_order: 05
has_children: false
permalink: /starling-v2-hardware-overview/
---

# Starling V2 Hardware Overview
{: .no_toc }

This section contains an overview of the hardware components of the Starling V2 Development Drone.

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

For technical details, see the [datasheet](/starling-v2-datasheet/) page.

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

## Hardware Overview Video

{% include youtubePlayer.html id="M9OiMpbEYOg" %}

## Development Kit Contents

Starling v2 is available in a variety of [kits](https://www.modalai.com/products/starling?variant=45182917640496) depending on what you have and need.

Shown here is the `MRB-D0005-4-V2-C6-M22-K3` (not shown is the carrying case).

<img src="/images/starling-v2/MRB-D0005-4-V2-C6-M22-K3.jpg" alt="MRB-D0005-4-V2-C6-M22-K3" width="1280"/>

| Callout | Description                                                    | PNs                        |
|---      |---                                                             |---                         |
|         | Development kit as shown above (includes all below and case)   | MRB-D0005-4-V2-C6-M22-K3   |
| A       | Starling v2 Drone, WiFi                                        | MRB-D0005-4-V2-C6-M22      |
| B       | Starling v2 Propellors (4726FM) (QTY4)                         | M10000302                  |
| C       | Starling v2 Propellor Clips (QTY4)                             | M10000581                  |
| D       | 2S VTC6 batteries XT30 (QTY2)                                  | M10000538                  |
| E       | IFlight Cammando 8 Radio Transmitter, ELRS 915MHz              | M10000563                  |
| F       | Power Supply, 12V 3A, XT60 with XT30 adapter                   | MPS-00005-1, M10000155     |


## Components

<img src="/images/starling-v2/MRB-D0005-4-V2-C6-M22-callouts-a.jpg" alt="MRB-D0005-4-V2-C6-M22-callouts-a" width="1280"/>

| Callout | Description                                                    | PN                         |
|---      |---                                                             |---                         |
| A       | VOXL 2                                                         | MDK-M0054-1                |
| B       | VOXL 4-in-1 ESC                                                | MDK-M0117-1                |
| C       | Barometer Sheild Cap                                           | M10000533                  |
| D       | ToF Image Sensor (PMD)                                         | MDK-M0040                  |
| E       | Tracking Image Sensor (ov7251)                                 | M0014                      |
| F       | Hires Image Sensor (IMX214)                                    | M0025-2                    |
| G       | AC600 WiFi Dongle                                              | AWUS036EACS                |
| H       | GNSS GPS Module & Compass                                      | M10-5883                   |
| I       | 915MHz ELRS Receiver                                           | BetaFPV Nano RX            |
| J       | USB C Connector on VOXL 2(not shown)                           |                            |
| K       | VOXL Power Module                                              | MCCA-M0041-5-B-T           |
| L       | 4726FM Propellor                                               | M10000302                  |
| M       | Motor 1504                                                     |                            |
| N       | XT30 Power Connector                                           |                            |

<br>
[Next: Starling V2 Datasheet](/starling-v2-datasheet/){: .btn .btn-green }
