---
layout: default
title: VOXL m500 Connect Network
parent: M500 User Guides
nav_order: 10
has_children: false
permalink: /voxl-m500-user-guide-connect-network/
youtubeId: aVHBWbwp488
---

# M500 Connect Network
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Connecting qGroundControl over WiFi

For setup and configuration of your M500 you must **Remove the Propellers** for safety.

### Power on VOXL and Flight Core

Now power up the VOXL and Flight Core on the M500. This can be done by connecting a [5V Power Supply](https://www.modalai.com/products/ps-xt60) to the power module's barrel jack. Or you can plug in a 4S battery:

To install a 4S battery, slide into the body as shown.  There is a stop at the front of the vehicle that will prevent the battery from sliding too far forward. The battery should be pressed all the way in until it hits the stop for a consistent center of mass. Power on the vehicle by connecting the battery to the M500's XT60 connector.

![vehicle-battery-2-144-1024.jpg](/images/m500/vehicle-battery-2-144-1024.jpg)


### Connect to VOXL Access Point

From the factory, the vehicle will power up as a WiFi Access Point with a name like `VOXL:XX:XX:XX:XX:XX`).  You can connect to it with a password of `1234567890`.

![voxl-ap-list.png](/images/m500/voxl-ap-list.png)

![voxl-ap-password.png](/images/m500/voxl-ap-password.png)

Once connected to the VOXL AP, you'll get an IP address from VOXL via DHCP, locate the address, in my case it's `192.168.8.51`.

![voxl-ap-ip-address.png](/images/m500/voxl-ap-ip-address.png)

More details can be found at the [VOXL WiFi quickstart page](/wifi-setup/).


### Next Steps
{: .no_toc }

There are roughly two paths you could take:

- Check out PX4 and Fly

[Next Step: Connect to GCS](/voxl-m500-user-guide-px4/){: .btn .btn-green}

- Skip to Machine Vision features to mess around at your desktop

[Next Step: Use VIO](/voxl-m500-user-guide-using-vio/){: .btn .btn-green}
