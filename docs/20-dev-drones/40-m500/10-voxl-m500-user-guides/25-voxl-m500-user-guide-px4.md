---
layout: default
title: VOXL m500 PX4 and GCS
parent: M500 User Guides
nav_order: 15
has_children: false
permalink: /voxl-m500-user-guide-px4/
youtubeId: aVHBWbwp488
---

1. TOC
{:toc}

## Connect m500 to PX4

### SSH into VOXL

The VOXL's IP address by default is `192.168.8.1`.  SSH into VOXL using the `root` user with password `oelinux123`.

More details available at the [VOXL SSH quickstart page](/ssh-to-voxl/).

![voxl-ssh.png](/images/m500/voxl-ssh.png)


### Edit voxl-vision-px4.conf

VOXL needs to know the IP address of your computer running qGroundControl. This is set in the 'qgc_ip' field of the `/etc/modalai/voxl-vision-px4.conf` file on VOXL.  This will inform the `voxl-vision-px4` systemd service to send MAVLink data to our computer.

Run the following command and update the `qgc_ip` field to match your computer's IP.

```bash
yocto:/home/root# vi /etc/modalai/voxl-vision-px4.conf
```

![voxl-vi-cmd-line.png](/images/m500/voxl-vi-cmd-line.png)

Hit `i` to enter edit mode, use arrow keys to navigate to the `qgc_ip` field and edit there. To save, hit `esc` then `:wq` to write and quit.

![voxl-vi-edit.png](/images/m500/voxl-vi-edit.png)

If you put VOXL on your own WiFi or LTE network you will have to do the same procedure.

For more information on this config file and the options available see the [VOXL Vision PX4 manual](/voxl-vision-px4-features/).


### Restart the voxl-vision-px4 Service

To reset the `voxl-vision-px4` service and reload the configuration, run the following command:

```bash
yocto:/home/root# systemctl restart voxl-vision-px4
```

Note this will also reset the Visual Inertial Odometry (VIO) algorithm and reset VIO's origin to wherever the drone is when you reset the service.


## Connect m500 to QGroundControl

### Confirm Connection with QGroundControl

After the previous step is completed, the vehicle should automatically connect to QGroundControl

![voxl-qgc-connected.png](/images/m500/voxl-qgc-connected.png)


## Connecting LTE Modems

For more information on setting up Microhard or LTE modems on your m500, follow the instructions [Here](/modems/)

[Next Step: Pre Flight Setup](/voxl-m500-user-guide-pre-flight-setup/){: .btn .btn-green }
