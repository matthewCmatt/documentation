---
layout: default
title: Sentinel Factory Reset guide
parent: Sentinel User Guide
nav_order: 55
has_children: false
permalink: /sentinel-factory-reset/
youtubeId: 
---
# Sentinel Factory Reset Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }
1. TOC
{:toc}

---


## System Image Flash
If you are having problems with your system, **please** ask questions on the [forum](https://forum.modalai.com/) before reflashing the system image. 

### Download Location
You can [download the system image](https://developer.modalai.com/asset) from our protected downloads page. Although this is usually not necessary since the system image is bundled in our [Platform Releases](/platform-releases/).


### USB Cables

It is recommended to use a USBC to USBA cable.  
We have seen issues with USBC to USBC cables on some host machines.



### Backup Files

{: .alert .danger-alert}
**WARNING:** Backup any special calibration files not saved in `/data/modalai`


- Please backup the following manually prior to updating

```bash
❯ adb pull /etc/modalai .
❯ adb pull /data/misc .
```


### Flash Procedure

It is recommended to install the entire platform release at once (System Image + voxl-suite) as to avoid version mismatches.

Download the latest VOXL2 platform release from [here](https://developer.modalai.com/asset/2) 

- Unzip the download, in this example we'll assume the download name was `voxl2_platform_M.m.b.tar.gz` where M.m.b is the version.

```bash
❯ tar -xzvf voxl2_platform_M.m.b.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
❯ cd voxl2_platform_M.m.b
```

- Now, attach the VOXL 2 via USBC and ensure the unit is powered on
- Make sure that adb see's your by running `adb devices` from the host computer

```bash
❯ adb devices
List of devices attached
f8bb8d44	device
```

  **If no devices show up:** see 'Force VOXL 2 into fastboot' in [VOXL2 System Image](/voxl2-system-image/)

- Check that the `fastboot` command works by running `fastboot devices`

```bash
❯ fastboot devices
```

**If command fails** see `Missing ADB?` below

- Run the following:

```bash
./install.sh
```
### Missing ADB?

Install the Android Debug Bridge (ADB):

```bash
me@mylaptop:~$ sudo apt install android-tools-adb android-tools-fastboot
```
Once the flash has started, you will be prompted to answer a couple questions. 
It is recommended to preserve userdata partitions as opposed to conducting a full wipe in which you would have to recalibrate the cameras and sensors. 
```bash
Would you like to preserve userdata partitions or conduct a full wipe?
1) Preserve(recommended)
2) Wipe
3) Cancel and Exit
#? 1
[INFO] Found all required files
```
If full wipe is conducted view  ```Full-wipe calibration``` below.


## Software Configuration
### Factory Reset MPA
VOXL Boards that were shipped as part of a kit ([M500](/m500/), [Seeker](/seeker/), [VOXL CAM](/voxl-cam/), etc) were configured at the factory with kit-specific MPA configuration starting around June 2021 with the introduction of MPA. In these cases, the kit part number is saved to `/data/modalai/factory_mode.txt` and can be used to put your VOXL MPA services back to their kit-specific factory defaults by running:

```
voxl-configure-mpa --factory-reset
```
Note this file will be wiped along with factory calibration files if you elect to wipe your /data/ partition while flashing a system image (which is not a recommended procedure). 

## Full-wipe calibration
If you conduct a full wipe, all data partitions will be erased, and you will have to calibrate all sensors and cameras and re-configure all softwares. 
### Sensor Calibration
Sentinel flight sensors are delivered pre-calibrated from the factory. If conducting, full wipe, you will need to re-calibrate by following the [px4 sensor calibration procedure](/px4-calibrate-sensors/).
### Camera Calibration
To calibrate cameras, follow the [camera calibration guide](/camera-cal).
## Pre-flight setup
### Connect Network
The following video shows how to connect using a development kit:

{% include youtubePlayer.html id=page.youtubeId %}

Then follow the [pre-flight setup guide](/sentinel-user-guide-pre-flight-setup/) to connect to RC Radio Setup and calibrate radio. 

### Connect to QGC
Ensure QGC IP address is the correct one.
``` bash
cat /etc/modalai/voxl-vision-px4.conf | grep qgc_ip
```
The IP address in the `qgc_ip` field should match the one of your computer.
If it doesn't match, you can use `vi` to modify the file and enter the correct IP address 
```bash 
vi /etc/modalai/voxl-vision-px4.conf
```


## First Flight
Follow  First Flight guide [here](/sentinel-user-guide-first-flight/)


