---
layout: default
title: Sentinel Functional Description
parent: Sentinel Datasheets
nav_order: 20
has_children: False
permalink: /sentinel-functional-description/
---


# Sentinel Drone Functional Description
[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---
![Sentinel](/images/sentinel/sentinel-front-clear.png)

## Sentinel High-Level Specs

### Flight Specs

| Component                | Specification                                                                          |
|--------------------------|----------------------------------------------------------------------------------------|
| Take-off Weight          | 1347g (1011g without battery)                                                           |
| Diagonal Size            | 480mm                                                                                  |
| Flight Time              | > 20 minutes                                                                           |
| Payload Capacity         | 1kg                                                                                    |
| Motors                   | Holybro 2216-880kv Motors                                                              |
| Propellers               | QTY. 4, 10 inch / 254mm                                                                |
| Frame                    | Holybro S500 V2 Frame Kit                                                              |
| ESCs                     | [ModalAI VOXL 4-in-1 ESC V2](https://docs.modalai.com/modal-esc-datasheet/)           |
| Maximum Speed            | 5m/s per PX4 setting                                                                   |
| Maximum Height           | 122m per PX4 setting                                                                   |
| Geographic Limits System | [PX4 Geo Fencing](https://docs.px4.io/v1.12/en/flying/geofence.html)                   |
| GPS                      | Ublox Neo-M8N module                                                                   |
| Power Module             | [ModalAI Power Module v3 - 5V/6A](https://docs.modalai.com/power-module-v3-datasheet/) |
| Fail Safe                | [PX4 Safety Configuration](https://docs.px4.io/v1.12/en/config/safety.html)            |
| Battery                  | Gens Ace 5000mAh, or any 3S battery with XT60 connector                                |

### Processing Specs

| Component                  | Specification                                                                                                                                                                                                                                    |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CPU                        | Qualcomm QRB5165                                                                                                                                                                                                                                 |
| Sensors                    | 2 TDK CH-201 ultrasonic rangefinder sensors <br> TDK ICM-42688-P IMU <br> TDK ICP-10100 Barometer <br> GPS/Magnetometer (Holybro)                                                                                                                |
| Image Sensors              | Tracking Sensor - Global Shutter VGA <br> High-resolution 4k30 <br> Front and Rear Stereo - Global Shutter VGA <br> 6x 4-lane MIPI-CSI2 (Up to 7 image sensors)                                                                                  |
| Connections                | USB 3.1 Hub <br> USB-C OTG                                                                                                                                                                                                                       |
| Additional I/O             | 2 GPIO <br> I2C, 4 SPI                                                                                                                                                                                                                           |
| Video Transmission         | Wi-Fi (2.4GHz or 5GHz) or Cellular (multi-band, carrier dependent)                                                                                                                                                                               |
| Expansions                 | ModalAI Legacy B2B connector for backwards compatibility with 4G add-on, Microhard add-on, USB debug board. <br> High speed B2B connector for PCIe2, I2C, SPI, UART, GPIO <br> TDK Chirp Sensor breakout PCBAs for object avoidance capabilities |
| Optional Expansion Add-Ons | 5G modem add-on with USB 3.1 Hub <br> SD Card add-on <br> Gigabit Ethernet add-on with USB 3.1 Hub                                                                                                                                               |
| Open Software Packages     | Ubuntu 18.04 <br> Linux Kernel 4.19 <br> OpenCV <br> TensorFlow Lite <br> ROS 1 / ROS 2 <br> RB5 Flight SDK hosted by ModalAI                                                                                                                    |
| Developer Connectivity     | QGroundControl <br> RViz <br> ADB (Android Debug Bridge)                                                                                                                                                                                         |

### Radio Specifications

| Radio                                                                                      | Frequency                                                                                      | Power        | Regulatory                                       |
|--------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|--------------|--------------------------------------------------|
| Remote Control                                                                             | Spektrum DSMX Receiver (2.4GHz)                                                                |              |                                                  |
| Wi-Fi6 (Wi-Fi 802.11 a/b/g/n/ac/ax)                                                        | 2.4GHz / 5.8GHz                                                                                | 175mW / 83mW | [FCC](https://fccid.io/2AOHHTURBOXC865)          |
| 5G Cellular (Optional) [Quectel RM502Q-AE](https://www.quectel.com/product/5g-rm502q-ae/)) | 5G NR bands worldwide (n1/n2/n3/n5/n7/n8/n12/n20/n25/ n28/n38/n40/n41/n48/n66/n71/n77/n78/n79) |              | [FCC](https://fcc.report/FCC-ID/XMR2020RM502QAE) |


### Drone Dimensions

| Component                 | Specification         |
|---------------------------|-----------------------|
| Motor to Motor            | 334mm x 334mm x 133mm |
| Fully Extended Propellers | 591mm x 591mm x 187mm |



### Vehicle Components

The following images give an overview of the vehicle components.

### Front
[View in fullsize](/images/sentinel/sentinel-labeled-1.png){:target="_blank"}
![Sentinel-Labeled-1](/images/sentinel/sentinel-labeled-1.png)

### Side
[View in fullsize](/images/sentinel/sentinel-labeled-2.png){:target="_blank"}
![Sentinel-Labeled-2](/images/sentinel/sentinel-labeled-2.png)

### Top
[View in fullsize](/images/sentinel/sentinel-labeled-3.png){:target="_blank"}
![Sentinel-Labeled-3](/images/sentinel/sentinel-labeled-3.png)

### Bottom
[View in fullsize](/images/sentinel/sentinel-labeled-4.png){:target="_blank"}
![Sentinel-Labeled-4](/images/sentinel/sentinel-labeled-4.png)

### Sentinel Mechanical Drawings

#### Sentinel 2D Drone Dimension Drawings

![rb5-drone-2D-dims.png](/images/rb5/rb5-drone-2D-dims.png)

#### Sentinel 3D STEP

[3D STEP Sentinel](https://storage.googleapis.com/modalai_public/modal_drawings/Sentinel_Drone_STEP.zip)

### Sub-component Dimensions

| PCB                                             | H x W x D        | Weight | Drawings                                                                                                                                                                                                                             |
|-------------------------------------------------|------------------|--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| VOXL 2 Autopilot (M0054)                        | 70mm x 36mm      | 16g    | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0054_VOXL2_PVT_SIP_REVA.step)                                                                                                                                |
| ModalAI 5G Modem Carrier Board (M0067 or M0090) | 67.5 x 36 x 10mm | 12.8g  | [2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0067-mechanical-drawing%20v1.pdf) <br> [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0067_VOXL2_5G_MODEM_DEBUG_REVA_S1_P9.step) |
| Quectel RM502Q-AE                               | 52× 30 × 2.3mm   | 8.7g   | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Quectel_RM500Q-AERM502Q-AE_Hardware_Design_V1.0.pdf)                                                                                               |
| Landing Skids                                   |                  |        | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/RB5_landing_gear_skid_v3.STEP)                                                                                                                                |
| Landing Skid Mount                              |                  |        | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/RB5_landing_gear_mount.STEP)                                                                                                                                  |

### Core Components Part Numbers

#### Reference Drone Part Numbers

| ModalAI Part Number    | Description                                           |
|------------------------|-------------------------------------------------------|
| MRB-D0006-4-V1-C11-M3  | Sentinel Reference drone with Microhard Carrier Board |
| MRB-D0006-4-V1-C11-M7  | Sentinel Reference drone with 5G Modem (Quectel)      |
| MRB-D0006-4-V1-C11-M13 | Sentinel Reference drone with 2.4/5ghz WiFi Dongle    |

## Sentinel PX4 Tuning Parameters

Our engineers have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Sentinel Motor Configuration

The propellers have arrows on them as to which way they should turn. Motor configuration in the image below.

![voxl-m500-motor-configuration.png](/images/m500/voxl-m500-motor-configuration.png)

## Sentinel Connectivity for Remote Operation

| Connectivity Option | Use Case                                                         | Details                                                   |
|---------------------|------------------------------------------------------------------|-----------------------------------------------------------|
| Spektrum R/C        | Manual remote control of the vehicle                             | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi                | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/)             |
| 5G LTE              | Long Range, BVLOS operation                                      | [Setup](https://docs.modalai.com/5G-Modem-user-guide/)    |






