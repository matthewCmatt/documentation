---
layout: default
title: Starling V2
nav_order: 30
has_children: true
permalink: /starling-v2/
parent: Dev Drones
summary: Starling V2 is our flagship development drone for both indoor and outdoor flight.
thumbnail: /starling-v2/starling-v2-hero-1.png
buylink: https://www.modalai.com/pages/starling-v2
---

# Starling V2
{: .no_toc }

Starling V2 is our flagship development drone for both indoor and outdoor flight.


<a href="https://www.modalai.com/products/starling?variant=45182917640496" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>



![Starling V2](/images/starling-v2/starling-v2-hero-2.png)

{% include youtubePlayer.html id="aBxCdp3xdi0" %}

### TODO promo video here too
