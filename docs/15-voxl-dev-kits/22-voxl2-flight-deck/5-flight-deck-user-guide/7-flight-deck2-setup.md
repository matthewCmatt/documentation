---
layout: default
title: VOXL 2 Flight Deck Setup
parent: VOXL 2 Flight Deck User Guide
nav_order: 7
has_children: false
permalink: /voxl2-flight-deck-userguide-setup/
---

# VOXL 2 Flight Deck Setup
### Setup Instructions

**GPS/Spektrum**

Connect your GPS/Mag and your Spektrum reciever to **J19 and J10**

The following have been validated, but in general if PX4 supports it, it should work.
- mRo GPS u-Blox Neo-M8N Dual Compass LIS3MDL+ IST8310
- Pixhawk4 2nd GPS Module

<img src="/images/voxl2-flight-deck/flight-deck-gps.jpg" alt="voxl2-flight-deck-connections.png" width="80%">

<br>

Then Connect MCBL-00005 (JST 3 pin) to your Spektrum Receiver (Sold Separately, [Buy Here](https://www.modalai.com/products/spektrum-dsmx-remote-receiver))

<img src="/images/voxl2-flight-deck/flight-deck-spektrum.png" alt="voxl2-flight-deck-connections.png" width="80%">

<hr>

**USB**

Conect a USB type C cable to VOXL 2's **J9** port. 

<img src="/images/voxl2-flight-deck/flight-deck-usb.jpg" alt="voxl2-flight-deck-connections.png" width="80%">

<hr>

**ESC's**

Conect your ESC to **J18**. 

Currently, VOXL 2 only supports UART. If you don't already have an ESC, we recommend using ours: [Buy Here](https://www.modalai.com/products/voxl-esc) 

An additional board will soon be available to support PWM output. For now, only UART is available. 

<img src="/images/voxl2-flight-deck/flight-deck-esc.jpg" alt="voxl2-flight-deck-connections.png" width="80%">

<hr>

**Power**

Connect MCBL-00001 (Molex 4 pin) to **J4**

VOXL 2 Flight Deck comes with a VOXL Power Module. The module requires either a 4S battery with XT60 Connector (6S with expanded battery standoffs), or you can use Wall Power Supply for benchtop development. 
  - Buy Battery [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 
  - Buy Wall Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r) 

<img src="/images/voxl2-flight-deck/flight-deck-power.jpg" alt="voxl2-flight-deck-connections.png" width="80%">

<hr>

[Next: Software](/voxl2-flight-deck-userguide-software){: .btn .btn-green }
