---
layout: default
title: VOXL Flight Deck User Guide
parent: VOXL Flight Deck
nav_order: 5
has_children: false
permalink: /flight-deck-userguide/
---

# VOXL Flight Deck User Guide
{: .no_toc }

This guide walks you through the main connections of the VOXL Flight Deck in effort to assist you getting it setup for a vehicle.

For technical details, see the [datasheet](/voxl-flight-deck-datasheet/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Primary Flight Deck Connections

Below describes the primary connections needed to get the Flight Deck configured on a vehicle and flying.

[Datasheet here](/voxl-flight-deck-platform-datasheet/)

![voxl-flight-deck-connectors](/images/voxl-flight-deck/voxl-flight-deck/voxl-flight-deck-connectors.png)

## Setup

### A. GPS

A standard Dronecode Compliant GPS/Mag connector is available.  The following have been validated, but in general if PX4 supports it, it should work.

See the [pinout here](/voxl-flight-datasheet-connectors/#j1012---external-gps--mag-usart1-i2c1-connector).

- [mRo GPS u-Blox Neo-M8N Dual Compass LIS3MDL+ IST8310](https://store.mrobotics.io/mRo-GPS-u-Blox-Neo-M8N-Dual-Compass-LIS3MDL-IST831-p/mro-gps003-mr.htm)
- [Pixhawk4 2nd GPS Module](https://shop.holybro.com/pixhawk4-2nd-gps-module_p1145.html)

*Note: PX4 requires a magnetometer to initialize.  If you want to fly without a mag and instead use VIO, check out [Flightcore Firmware](/flight-core-firmware/) for a modified version that allows initialization*

![voxl-flight-deck-gps-input](/images/voxl-flight-deck/voxl-flight-deck-gps-input.png)

### B. USB to QGroundControl

The VOXL Flight Deck system provides the ability to connect to QGroundControl (or other MAVLink based Ground Control Stations) over UDP, which allows you to connect wirelessly over LTE, Microhard, Wi-Fi, etc.

Out of the box, the system is configured for this using [voxl-vision-px4](/voxl-vision-px4/)

Sometimes, you need to connect using USB.  You can connect to the **BLUE** connector using [MCBL-00010](/cable-datasheets/#mcbl-00010), a USB cable, and a host computer running QGC.

![voxl-flight-deck-usb-qgc](/images/voxl-flight-deck/voxl-flight-deck-usb-qgc.png)

### C. RC Input

We generally use the Spektrum DX6e or DX8 radio transmitter (e.g. SPM8000/SPMR8000) using [MCBL-00005](/cable-datasheets/#mcbl-00005), but the system can support FrSky as well (cable coming soon). Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work.

See the [pinout here](/voxl-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector).

![voxl-flight-deck-rc-input](/images/voxl-flight-deck/voxl-flight-deck-rc-input.png)

### D. USB to VOXL (adb)

To access VOXL's shell over adb, you'll want to use this USB port and a typical micro-B USB cable.  **Note:** the cable will only consume the micro-B portion of the connector (not the whole USB 3.0 port)

![voxl-flight-deck-usb-adb](/images/voxl-flight-deck/voxl-flight-deck-usb-adb.png)

### E. PX4 MicroSD Card Slot

In order to log and fly missions, you'll need a [MicroSD card](https://dev.px4.io/v1.9.0/en/log/logging.html#sd-cards).

**NOTE:** there's also a MicroSD card slot for VOXL.  This can be used to give more 'disk space', but is not required.

![voxl-flight-deck-px4-sd](/images/voxl-flight-deck/voxl-flight-deck-px4-sd.png)

### F. Power Input

The VOXL Flight Deck kit comes with a VOXL Power Module, which accepts a battery input and outputs a regulated 5VDC/6A, along with an I2C signal for power monitoring via PX4.

![voxl-flight-deck-power-input](/images/voxl-flight-deck/voxl-flight-deck-power-input.png)

### G. PWM/DShot Output

The VOXL Flight/Flight Core supports PWM or DShot output.  See [PWM ESC Calibration](/flight-core-pwm-esc-calibration/) for more information on calibration.

![voxl-flight-deck-pwm-output](/images/voxl-flight-deck/voxl-flight-deck-pwm-output.png)

## Image Sensors

The VOXL Flight Deck comes with pre-calibrated stereo and tracking sensors and a Hi-res FPV image sensor.  These are mounted and read to use.

Here's some helpful links:

- [voxl-vision-px4](/voxl-vision-px4) - a wonderful service, see the link for details ;)
- [RTSP Streaming](/voxl-rtsp) - simple way to stream to QQC and also validate the image sensors

![voxl-flight-deck-image-sensors](/images/voxl-flight-deck/voxl-flight-deck-image-sensors.png)
