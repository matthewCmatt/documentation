---
layout: default
title: VOXL Dev Bundles
parent: VOXL Dev Kits
nav_order: 5
permalink: /voxl-kits/
thumbnail: /voxl/voxl/VOXL-DK-MV-R1.png
buylink: https://www.modalai.com/collections/blue-uas-framework-components/products/voxl
summary: VOXL Dev Bundles
---

# VOXL Dev Bundles
{: .no_toc }


![voxl-dk](/images/voxl/voxl-dk.jpg)

[Available for purchase here](https://www.modalai.com/voxl)

- [MDK-M0006-2-00](https://www.modalai.com/products/voxl) - VOXL Core Board Kit

- [MDK-M0006-2-01](https://www.modalai.com/products/voxl) - VOXL Core Board Kit with Power Module (M0041-1-B), power cable (MCBL-00001), and antennas (MRF-M0006)

- MRF-M0006 - VOXL Wi-Fi Antenna Kit

