---
layout: default
title: 11. VOXL Portal
parent: VOXL Developer Bootcamp
nav_order: 11
permalink: /voxl-portal/
---

# VOXL Portal

The voxl-portal package provides an embedded webserver on VOXL. It enables camera inspection and other debug tools all via web browser interface at your VOXL's IP address.

{% include youtubePlayer.html id="9MC0nBM0BqQ" %}

---

## Accessing VOXL Portal
Ensure `voxl-portal` is running using `voxl2:/$ voxl-inspect-services`. If it's not running, start it with `voxl2:/$ systemctl start voxl-portal`. You can also enable it to run on boot using `voxl2:/$ voxl-configure-portal`.

[WiFi](/wifi-guide/) must be enabled for you to connect to VOXL Portal.

Next, find your VOXL's IP address. On VOXL 2, you can use this command: `voxl2:/$ voxl-my-ip`. On VOXL or VOXL Flight, you must use `ifconfig`.

If your VOXL is in station mode, you can access VOXL Portal by simply pointing your web browser to the IP address of VOXL: 

{: .alert .info-alert}
http://\<VOXL IP>/

If your VOXL is in SoftAP mode, you MUST additionally ensure your host computer is connected to the network SSID that the VOXL is hosting. VOXL Portal will not be able to connect otherwise.

You can see what WiFi mode you are in using this command:
```
voxl2:/$ voxl-wifi getmode
```


## Viewing Cameras

voxl-portal uses the MPA library to dynamically list and display any open camera/image pipes. When the "Cameras" dropdown is selected, any available image output will be displayed, regardless of supported type. 

![voxl-portal-camera-list.png](/images/voxl-sdk/voxl-portal/voxl-portal-camera-list.png)

Select the camera you wish to view, and the image will be displayed as long as the image format is supported.
![voxl-portal-qvio-overlay.png](/images/voxl-sdk/voxl-portal/voxl-portal-qvio-overlay.png)

### Supported image formats
* RAW8
* STEREO_RAW8
* RGB
* YUV420
* YUV422
* NV21
* NV12

### Image Quality
voxl-portal will attempt to adjust to your networks capabilities. When you have a poor network connection, framerate will drop until a consistent framerate can be reached. If you are expecting a higher resolution image than what you are receiving, the cause is likely a poor connection. 

### Multi-View
The Multi-View option is the first under the camera dropdown, and will display a collage of all active and valid image pipes. Each image stream can be turned on or off using the checkbox next to their name. 
 
![voxl-portal-multi-view.png](/images/voxl-sdk/voxl-portal/voxl-portal-multi-view.png)

Keeping too many camera streams open can stress your network and cause quality to reduce. In this case, try to turn off any unecessary camera streams.


---



## Viewing Pointclouds

voxl-portal uses the MPA library to dynamically list and display any open pointcloud pipes. Note that you need either stereo cameras or a ToF sensor to have any pointclouds.

Select any pointcloud in the "Pointclouds" dropdown to view it. All MPA pointcloud types are supported, see [libmodal-pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe/-/blob/master/library/include/modal_pipe_interfaces.h#L418) for the full list.

![voxl-portal-pointcloud-list.png](/images/voxl-sdk/voxl-portal/voxl-portal-pointcloud-list.png)

Note: The pointcloud viewer utilizes WebGl for GPU-accelerated rendering, so running on devices that do not support this may result in other issues.

![voxl-portal-dfs-pointcloud.png](/images/voxl-sdk/voxl-portal/voxl-portal-dfs-pointcloud.png)

The bottom left hand corner of the page includes a button to overlay an image stream on top of the pointcloud grid. Clicking on this tv icon will populate a list above of all available image topics, similar to the camera dropdown. 

![voxl-portal-overlay-stream.png](/images/voxl-sdk/voxl-portal/voxl-portal-overlay-stream.png)

Selecting one will overlay it on the page, where it can be resized via the bottom left corner of the stream and closed using the red x at the bottom left. 

![voxl-portal-overlay-stream-qvio.png](/images/voxl-sdk/voxl-portal/voxl-portal-overlay-stream-qvio.png)



### Tof
voxl-portal has a special viewer for the "Tof" pointcloud topic if your VOXL setup includes a tof camera. This specific pointcloud can be colored by either confidence or reflected light intensity, since those values are included with the tof topic. (Note: Tof Pc only displays Point Cloud data, no confidence or intensity).

<img src="/images/voxl-developer-bootcamp/tof.png" alt="Time of Flight Point Cloud"/>

---


## CPU and IMU Plotting
voxl-portal is able to produce detailed plots with CPU and IMU information. 

{: .alert .simple-alert}
Tip: Hovering above the graph will tell you the the exact x-axis and y-axis values.

### CPU
This tool will plot ten second windows of data, with various CPU, GPU, and memory options. You may choose to plot as few or as many of the listed options as you wish.
<img src="/images/voxl-developer-bootcamp/portal_cpu.png" alt="CPU Plots"/>

### IMU
This tool mimics the CPU plotting tool, but displays gyro and acceleration data.
<img src="/images/voxl-developer-bootcamp/portal-imu.png" alt="IMU Plots"/>

### FFT (Fast Fourier Transform)
This tool helps to measure vibration levels detected by the IMU, and plots angular velocity and acceleration against frequency, computed using FFT. 
<img src="/images/voxl-developer-bootcamp/portal-fft.png" alt=""/>



---


## Flight Info
voxl-portal uses the MPA library and output from `voxl-mavlink-server` to display, in the upper right corner of the portal:
- drone battery level
- number of satellites being used to triangulate GPS position
- flight mode
- arm Status

![voxl-portal-flight-info.png](/images/voxl-sdk/voxl-portal/voxl-portal-flight-info.png)

If these fields are "unknown", you should check that `voxl-mavlink-server` and `voxl-px4` are running.


[VOXL Portal Source Code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-portal)



<br>
[Next: Install QGroundControl](/installing-qgc/){: .btn .btn-green }
