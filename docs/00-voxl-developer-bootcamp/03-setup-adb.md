---
layout: default
title: 3. Setting Up ADB
parent: VOXL Developer Bootcamp
nav_order: 3
permalink: /setting-up-adb/
---

# Setting Up ADB
{: .no_toc }

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The next step after powering on your VOXL is to set up a communication channel between your host computer and your device for things like file transfers and running programs. The following section will be focused on setting up the Android Debug Bridge (ADB) which is a command-line tool which allows you to accomplish this task. All remaining instructions assume the user is running Ubuntu 20.04 or 22.04. 

{: .alert .simple-alert}
**Note:** From this section onwards work will be done in both the user's terminal and in the `adb` shell. When possible, code blocks will start with either `me@mylaptop:~$` or `voxl2:/$` respectively to denote this difference.

## Host Computer Setup
The following section will help you configure ADB on your host machine.


{: .alert .simple-alert}
**Note:** Using USB C to USB type A cables has worked well, we've seen some computers where USB C to USB C cables are not working well. If you have having issues with connections, please try a USB C to USB type A cable.


### ADB Installation


To start, install the Android Debug Bridge (ADB) and fastboot:

#### For Ubuntu 22.04

```bash
me@mylaptop:~$ sudo apt update
me@mylaptop:~$ sudo apt install -y android-tools-adb android-tools-fastboot
```

#### For Ubuntu 20.04

```bash
me@mylaptop:~$ sudo apt update
me@mylaptop:~$ sudo apt install -y adb fastboot
```

<!-- *Tip: To run ADB without root on the host PC, create a file called `/etc/udev/rules.d/51-android.rules` containing this udev rule on the host computer:*  -->

<!-- ```bash
me@mylaptop:~$ sudo su
root@mylaptop:/home/me# echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="05c6", ATTRS{idProduct}=="901d", MODE="0660", GROUP="plugdev", SYMLINK+="voxl%n"' > /etc/udev/rules.d/51-android.rules
root@mylaptop:/home/me# echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="18d1", ATTRS{idProduct}=="d00d", MODE="0660", GROUP="plugdev", SYMLINK+="voxl-fastboot%n"' >> /etc/udev/rules.d/51-android.rules
root@mylaptop:/home/me# udevadm control --reload-rules && udevadm trigger
``` -->

### Basic Interfacing

To check for attached devices, you can use the `adb devices` command:

```bash
me@mylaptop:~$ adb devices
List of devices attached
73a05d48	device
```

To access the ADB Shell, you can use the `adb shell` command:

```bash
me@mylaptop:~$ adb shell
```

To test your setup to this point, you can run a command like `voxl-version` as such:
```bash
voxl2:/$ voxl-version
--------------------------------------------------------------------------------
system-image: 1.6.2-M0054-14.1a-perf-20230612
kernel:       #1 SMP PREEMPT Tue Jun 13 03:27:14 UTC 2023 4.19.125
--------------------------------------------------------------------------------
hw version:   M0054
--------------------------------------------------------------------------------
voxl-suite:   1.0.0
--------------------------------------------------------------------------------
Packages:
Repo:  http://voxl-packages.modalai.com/ qrb5165 SDK-1.0
Last Updated: 2023-03-02 12:58:29
```

To exit the shell you can use the `exit` command:

```bash
me@mylaptop:~$ adb shell
voxl2:/$ echo "I am inside VOXL 2!"
I am inside VOXL 2!
voxl2:/$ exit
exit
me@mylaptop:~$
```

**Note:** If you are using VOXL and not VOXL 2, the `adb shell` command will start an `sh` shell. Generally you want to use a `bash` shell instead, so we recommend starting bash right away. Running `adb shell`  followed by starting `bash` will look like this:

```bash
me@mylaptop:~$ adb shell
voxl:/$
/ # bash
voxl:/$
```

To confirm, you can also look at the `SHELL` variable:

```bash
voxl2:/$ echo $SHELL
/bin/bash
```

With this you should be done and ready to move on to the [WiFi Guide](/wifi-guide/) !



### <span class="block-line"><span><span style="color:#ff0000;">M</span><span style="color:#ff7700;">o</span><span style="color:#ffee00;">d</span><span style="color:#99ff00;">a</span><span style="color:#26ff00;">l</span><span style="color:#00ff51;">A</span><span style="color:#00ffc8;">I&nbsp;</span></span><span><span style="color:#00c3ff;">T</span><span style="color:#004cff;">o</span><span style="color:#2a00ff;">p&nbsp;</span></span><span><span style="color:#9d00ff;">T</span><span style="color:#ff00ea;">i</span><span style="color:#ff0073;">p:</span></span></span> Aliases
For frequent VOXL usage it may be convenient to set up aliases in the `~/.bashrc` file on your host machine. We recommend adding the following aliases:

```bash
alias voxl="adb wait-for-device && adb shell"
export VOXL_IP=<VOXL DEVICE IP ADDRESS HERE>
alias sshh='sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${VOXL_IP}'
```
The former alias allows the user to wait for a device to be ready for connection and then immediately connect when possible. The latter alias allows for a shortening of the `ssh` command to your device. You will learn how to get the IP address for your device in the next section.

To use the `sshh` alias, made sure you have sshpass installed on your host machine: `sudo apt install sshpass`

## Appendix

Listed here for convenience are some other common ADB commands and tips we recommend to the user.

### Run Something Once

This can be done with `adb shell <command>`. If you don't want to start an interactive shell, but just want to run one command on VOXL 2, you can pass the command directly to `adb shell`.

```bash
me@mylaptop:~$ adb shell echo "I am inside VOXL 2"
I am inside VOXL 2
me@mylaptop:~$
```

### Wait for Device

A useful command for creating scripts for host-to-target communications is this one that sits and waits until a device is connected:

```bash
me@mylaptop:~$  adb wait-for-device
```


### Copying Files

You can copy files to/from VOXL 2 (similar to `scp`) using the `adb push` and `adb pull` commands.

For example, to copy `/temp/test.dat` to the VOXL 2 at `/`:

```bash
me@mylaptop:~$ adb push /tmp/test.dat /
/tmp/test.dat: 1 file pushed.
```

To copy it back from the VOXL 2 at `/test.dat` to `/temp`

```bash
me@mylaptop:~$ adb pull /test.dat /tmp
/test.dat: 1 file pulled
```

### Reboot VOXL 2

You can reboot VOXL with a single adb command `adb reboot`. This is often followed by `adb wait-for-device` which returns when VOXL has finished rebooting.

```bash
me@mylaptop:~$  adb reboot
```

<br>
[Next: WiFi Guide](/wifi-setup/){: .btn .btn-green }
