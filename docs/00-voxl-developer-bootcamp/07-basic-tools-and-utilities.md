---
layout: default
title: 7. Basic Tools and Utilities
parent: VOXL Developer Bootcamp
nav_order: 7
permalink: /basic-functionalities/
---

# Basic Tools and Utilities
{: .no_toc }


---

This page demonstrates some basic utilities and tools that you will likely use to interact with VOXL's services. 

Note: For an extensive list of all tools, utilities, and commands available in the VOXL SDK, type `voxl-{TAB}{TAB}` to see all auto-complete options.

---


## voxl-list-pipes

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

```voxl-list-pipes``` is a simple tool to list all of the MPA (Modal Pipe Architecture) pipes currently available in the system. Running the command alone is identical to the command ```ls /run/mpa```, but when giving it the ```-t``` or ```--mode-types``` flag, it will sort the pipes by type, allowing you to easily see which cameras/imus/etc are available in the system.

### Example Output:
```
voxl2:/$ voxl-list-pipes -t
camera_image_metadata_t
    hires
    qvio_overlay
    tof_conf
    tof_depth
    tof_ir
    tof_noise
    tracking

cpu_stats_t
    cpu_monitor

imu_data_t
    imu0
    imu1

mavlink_message_tf
    vvpx4_gps_raw_int
    vvpx4_mavlink_io
    vvpx4_sys_status
    vvpx4_vehicle_gps

point_cloud_metadata_t
    tof_pc

pose_vel_6dof_t
    vvpx4_body_wrt_fixed
    vvpx4_body_wrt_local

qvio_data_t
    qvio_extended

text
    vvpx4_shell

tof_data_t
    tof

vio_data_t
    qvio

voxl2:/$
```
The source code for this can be found [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/bin/voxl-list-pipes)


---

## voxl-inspect-imu

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>


`voxl-inspect-imu` is a tool to check the imu measurements of the imus on voxl. It requires that [voxl-imu-server](/voxl-imu-server/) is running in the background, which can be checked with [voxl-inspect-services](/voxl-inspect-services/).


### Arguments

#### Required
IMU: Which IMU to display data from. Available IMUs can be seen by typing `voxl-inspect-imu {TAB}{TAB}`.

Note: for VOXL 2, you should see available IMU's as `imu_apps`. For VOXL and VOXL Flight, you should see `imu0` and `imu1`

#### Optional

| Parameter    | Description                                                                               | Example                        |
|---           |---                                                                                        |---                             |
|-a --all      | Show all imu data, not scaled down for screen performance (this will print lines at ~1khz)| ```voxl-inspect-imu imu_apps -a``` |
|-h --help     | Print help message                                                                        | ```voxl-inspect-imu --help```  |
|-n --newline  | Print each sample on a new line instead of updating the current output line               | ```voxl-inspect-imu imu_apps -n``` |


### Example Output

```
voxl2:/$ voxl-inspect-imu imu_apps

Acc in m/s^2, gyro in rad/s, temp in C

gravity| accl_x accl_y accl_z| gyro_x gyro_y gyro_z|  Temp |
  9.63 |  0.12  -9.57   1.08 |  0.01   0.02   0.00 | 35.03 | 
```
Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-imu.c).


---

## voxl-inspect-cam

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

Note: only applicable if you have cameras connected to your VOXL.

The utility `voxl-inspect-cam` is a tool to check image metadata coming from MPA services that are publishing camera data. It requires that a service like [voxl-camera-server](/voxl-camera-server/) is running in the background publishing camera, image, or video data. It can also be used to check non-camera images, such as the overlays coming out of [VOXL TFLite Server](/voxl-tflite-server/) or [voxl-qvio-server](/voxl-qvio-server/).

### Arguments

#### Required
Cam: Which image to display data from. Available images can be seen by typing ```voxl-inspect-cam {TAB} {TAB}```. Options that will regularly be available are: `tracking`, `stereo`, `hires`, `tof_depth`, `tof_conf`, `tof_noise`, `tof_ir`, `dfs_disparity`, `qvio_overlay`, `tflite_overlay`.

#### Optional

| Parameter    | Description                                                                      | Example                            |
|---           |---                                                                               |---                                 |
|-h --help     | Print help message                                                               | ```voxl-inspect-cam --help```      |
|-n --newline  | Print each sample on a new line instead of updating the current output line      | ```voxl-inspect-cam tracking -n``` |
|-t --test     | Test mode, simple pass/fail test after two seconds of waiting for a frame        | ```voxl-inspect-cam tracking -t``` |


### Example Output

```
voxl2:/$ voxl-inspect-cam tracking

|size(bytes)| height | width  |exposure(ms)| gain | frame id |latency(ms)|Framerate(hz)| format
|   307200  |    480 |    640 |        3.8 |  100 |   71976  |      18.6 |     30.0    | RAW8
voxl2:/$ 
```
Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-cam.c).


---


## voxl-inspect-battery
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>


This tool subscribes to the `/run/mpa/vvpx4_sys_status` pipe published by `voxl-mavlink-server` which provides a copy of all SYS_STATUS mavlink messages received from a PX4 flight controller. Among other things, this packet contains battery status information.

Also note that [voxl-portal](/voxl-portal/) subscribes to the same pipe and also shows battery percentage in the top right corner of the webpage.


### Use

```bash
voxl2:/$ voxl-inspect-battery

 Voltage |  Charge | Current |
  12.22V |    89%  |   0.49A |
```

### Troubleshooting

If no data is displayed, then either `voxl-px4` or `voxl-mavlink-server` is not running (run [voxl-inspect-services](/voxl-inspect-services/) to check) or PX4 is not connected or operating.

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-battery.c).


---



## voxl-inspect-cpu

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a> 
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a>
<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

`voxl-inspect-cpu` is a tool to check the CPU/GPU utilization and temperature of the various voxl components. It is part of the [VOXL CPU Monitor](/voxl-cpu-monitor/) package and subscribes to the `cpu_monitor` pipe. If you don't get data using this tool, make sure the voxl-cpu-monitor systemd service is running using [voxl-inspect-services](/voxl-inspect-services/). Additionally, to get a breakdown of CPU and memory utilization by process, use the linux command `top` instead.



### Use
```
voxl:/$ voxl-inspect-cpu
```

<img src="/images/voxl-developer-bootcamp/voxl-inspect-cpu.png" alt="voxl-inspect-cpu output"/>

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor/-/blob/master/clients/voxl-inspect-cpu.c).

<br>
[Next: Check Calibration](/check-calibration/){: .btn .btn-green }
