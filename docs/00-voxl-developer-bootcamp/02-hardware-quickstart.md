---
layout: default
title: 2. Hardware Quickstart
parent: VOXL Developer Bootcamp
has_children: true
nav_order: 2
has_toc: false
permalink: /hardware-quickstart/
---

# Hardware Quickstart
{: .no_toc }

## Overview

This section contains hardware quickstarts intended for developer type users and provides the minimum steps needed to get a device powered up and ready to connect to a console.

Once you have finished this section, you can move onto the [Setting Up ADB](/setting-up-adb/) section to get connected to your device.

| Hardware      |                                                                     | Link                                                                       | Compute     |
|---------------|---------------------------------------------------------------------|----------------------------------------------------------------------------|-------------|
| VOXL 2        | <img src="/images/voxl2/m0054-hero-f.png" width="256"/>             | [Quickstart](/voxl-2-hardware-quickstart/){: .btn .btn-green }             | VOXL 2      |
| VOXL 2 Mini   | <img src="/images/voxl2-mini/m0104-hero-bottom.png" width="256"/>   | [Quickstart](/voxl-2-mini-hardware-quickstart/){: .btn .btn-green }        | VOXL 2 Mini |
| Starling v2   | <img src="/images/starling-v2/starling-v2-hero-1.png" width="256"/> | [Quickstart](/starling-v2-hardware-quickstart/){: .btn .btn-green }        | VOXL 2      |
| Sentinel      | <img src="/images/sentinel/sentinel-front-clear.png" width="256">   | [Quickstart](/sentinel-hardware-quickstart/){: .btn .btn-green }           | VOXL 2      |
| VOXL Flight   | <img src="/images/voxl-flight/voxlflighttop.jpg" width="256"/>      | [Quickstart](/voxl-flight-hardware-quickstart/){: .btn .btn-green }        | VOXL Flight |
| VOXL          | <img src="/images/voxl/voxl-whiteb_90.jpg" alt="voxl" width="256">  | [Quickstart](/voxl-hardware-quickstart/){: .btn .btn-green }               | VOXL        |
| M500          | <img src="/images/m500/m500.png" width="256">                       | [Quickstart](/m500-hardware-quickstart/){: .btn .btn-green }               | VOXL Flight |
| Seeker        | <img src="/images/seeker/Seeker-whitebackg.jpg" width="256"/>       | [ Quickstart](/seeker-hardware-quickstart/){: .btn .btn-green }            | VOXL        | 
