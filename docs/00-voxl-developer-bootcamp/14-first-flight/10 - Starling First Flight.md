---
layout: default
title: Starling V2 First Flight
parent: 14. First Flight
nav_order: 10
has_children: false
permalink: /starling-v2-first-flight/
---

# Starling V2 First Flight
{: .no_toc }

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

{% include youtubePlayer.html id="Cpbbye3Z6co" %}


[Next Step: voxl-cross](/voxl-docker-and-cross-installation/){: .btn .btn-green }
