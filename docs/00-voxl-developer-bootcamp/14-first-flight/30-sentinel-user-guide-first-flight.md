---
layout: default
title: Sentinel First Flight
parent: 14. First Flight
nav_order: 30
has_children: false
permalink: /sentinel-user-guide-first-flight/
---

# Sentinel First Flight
{: .no_toc }

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

1. TOC
{:toc}

## Preflight Checks

Disconnect the power supply now and move Sentinel to a safe location where you wish to fly and connect a battery for flight while the Sentinel is on the ground.

Wait for the connection with QGroundControl to be established.

### Attitude Check

Lift the vehicle and move it around, verify that the attitude reported in QGroundControl GUI looks and responds correctly. Try not to cover the tracking camera during this process.

![qgc_attitude_gui.png](/images/voxl-sdk/qgc/qgc_attitude_gui.png)

### Arming Vehicle Without Props

With the propellers **off** arm the vehicle.

- Set killswitch to off ("Aux2 Gov" switch down)
- **Left** stick hold **down** and to the **right**

Validate the motor rotation is as expected and the vehicle responds to throttling up momentarily.

Disarm the vehicle:

- **Left** stick hold **down** and to the **left**

Correct rotation direction of motors:

<img src="/images/voxl-sdk/qgc/QuadRotorX-small.svg" alt="vehicle-overview-props-144-1024.jpg" width="50%">

## Install Propellers

If your motors are blue, follow the first two steps under v1.0. If you motors are black, skip down to v1.1.

<img src="/images/sentinel/sentinel-motors.jpg" alt="sentinel-motors" width="100%">


### v1.0

If your Sentinel has blue motors, follow the directions below to install spacer adapters. 

1.  This version requires 1mm spacer adapters to be added to the propellors. 
2.  Grab the spacer and pop it into the propellors as shown.  

 *Make sure the adapters have not fallen off your propellors, and check the motors for adapters that may have fallen from previous flights* 


<img src="/images/sentinel/sentinel-prop-spacer.jpg" alt="sentinel-prop-spacer" width="70%">

<img src="/images/sentinel/sentinel-prop-spacer2.png" alt="sentinel-prop-spacer2" width="70%">


### v1.1
If your Sentinel has black motors, start here. 

3. Unscrew the bell caps and remove the washers. 

Two of the propellors labeled 'R' are designed to turn clockwise and correspond to motors 3 and 4 in the diagram above. Check your props, and slide them onto the motors according to the diagram. 

<img src="/images/sentinel/sentinel-right-prop.jpg" alt="sentinel-prop-spacer2" width="70%">

<img src="/images/sentinel/sentinel-prop-1.jpg" alt="sentinel-prop-spacer" width="70%">

---

4. Slide the washers back on as shown, then screw the bell caps on.

<img src="/images/sentinel/sentinel-prop-2.jpg" alt="sentinel-prop-spacer" width="70%">

<img src="/images/sentinel/sentinel-prop-3.jpg" alt="sentinel-prop-spacer" width="70%">

 ---

5. Tighten by sliding a tool through the holes. Make sure to hold the motors, and not the props while tightening. 

<img src="/images/sentinel/sentinel-prop-4.jpg" alt="sentinel-prop-spacer" width="70%">


### Safety Check
Before flying, make sure to conduct a thorough safety check: 
<ul>
<li>Make sure all connectors and cabling is secure and not loose </li>
<li>Make sure none of the wires are cut, frayed, or damaged </li>
<li>If you have any connectivity add-ons, make sure the antennas are secure and are not loose </li>
<li>Make sure the SD and SIM cards have not popped out </li>
<li>Make sure the reciever is properly plugged in </li>
<li>Make sure the cameras are free of any grease, dust, or debris </li>
<li>Make sure the battery you are using is fully  charged </li>
<li>Make sure the props are on correctly and that they are securly attached </li>
</ul>


### First Flight (Position Mode)

You should be comfortable flying before proceeding! 

We recommend your first flight be in position hold (vio) mode outside with a GPS lock. 
For more information on flying in position mode, click the green button below for a vio overview.

Once confortable with position mode, now safely fly in manual mode! No instructions here, this is an advanced mode and you should know what you are doing if you're flying!


[Next Step: voxl-cross](/voxl-docker-and-cross-installation/){: .btn .btn-green }
