---
layout: default
title: 5. Inspect and Configure SKU
parent: VOXL Developer Bootcamp
nav_order: 5
permalink: /sku/
---

# Inspect and Configure SKU
{: .no_toc }

Your VOXL or VOXL development kit ships with a SKU number written to a persistent data partition at `/data/modalai/sku.txt` which it used to inform various [Modal Pipe Architecture](/mpa/) tools what sort of product the VOXL is installed in. Most importantly, `voxl-configure-mpa` reads this SKU to decide which services to enable and how they should be configured.

Generally speaking, you should not need to change the SKU unless you are transferring VOXL to a different product or changing which cameras it is connected to. However, it is useful to know what it is and what it means.

## Table of Contents

1. TOC
{:toc}

---



## Inspect SKU

To inspect the SKU of your device, first access the device with `adb shell`. Then use the `voxl-inspect-sku` command to get a printout like this:

```bash
voxl2:/$ voxl-inspect-sku

family code:   MCCA-M0054 (voxl2-board-only)
compute board: 4 (voxl2)
hw version:    1
cam config:    0
SKU:           MCCA-M0054-C0
```

Similarly, the printout for a Starling V2 drone would look like this:

```bash
voxl2:/$ voxl-inspect-sku

family code:   MRB-D0005 (starling)
compute board: 4 (voxl2)
hw version:    2
cam config:    6
SKU:           MRB-D0005-4-V2-C6
```

## Configure SKU

The voxl-configure-sku tool should be used to change the SKU that's saved to disk. It will ensure the new `/data/modalai/sku.txt` is formatted correctly.

You can run the following help command to see the full list of arguments and options. However most of these arguments are for production use or CI test scripts.

```bash
voxl2:/$ voxl-configure-sku --help
```

If you are changing the SKU assigned to your VOXL, we suggest using the wizard which will present you options and ask several questions while constructing the desired SKU numer.

```bash
voxl2:/$ voxl-configure-sku --wizard
```

Running voxl-configure-sku will not do anything other than updating the `sku.txt` file on disk. To actually reconfigure all of the services on VOXL see the [voxl-configure-mpa](/voxl-configure-mpa/) tool.

## Understanding the SKU Number

ModalAI developemnt kit SKU numbers following the following structure:

```bash
{family code}-{board code}-V{hw revision}-C{camera config}
```

The `voxl-inspect-sku` tool above parses this and prints out the human readable version. To see tables of the valid family codes, board codes, and camera configs, you can use these tools:

```bash
voxl2:/$ voxl-print-family-table
voxl2:/$ voxl-print-board-table
voxl2:/$ print-camera-configs
```

These are the same functions that are called to construct the help text and wizard options for voxl-configure-sku.


## Source Code

The source code for the above tools can be found at [https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-configurator](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-configurator).

<br>
[Next: The Modal Pipe Architecture](/mpa/){: .btn .btn-green }
