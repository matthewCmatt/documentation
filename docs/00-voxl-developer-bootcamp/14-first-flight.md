---
layout: default
title: 14. First Flight
parent: VOXL Developer Bootcamp
nav_order: 14
permalink: /first-flight/
has_children: true
has_toc: false
---

# First Flight
{: .no_toc }


Now it's finally time for your first flight! Each development drone is a little different so please select from the below list.


[Starling V2 First Flight](/starling-v2-first-flight/){: .btn .btn-green }


[Sentinel First Flight](/sentinel-user-guide-first-flight/){: .btn .btn-green }


## Skip This Step

If you are using a VOXL Development Kit instead of one of the above Development Drones, you can skip to the next step:

[Next Step: voxl-cross](/voxl-docker-and-cross-installation/){: .btn .btn-green }
