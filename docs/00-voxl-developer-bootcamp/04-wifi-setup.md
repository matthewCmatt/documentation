---
layout: default
title: 4. WiFi Setup
parent: VOXL Developer Bootcamp
nav_order: 4
permalink: /wifi-setup/
has_children: true
has_toc: false
---

# WiFi Setup
{: .no_toc }


While ADB is a great way to communicate with your device, having a wired USB connection may not always be ideal. All VOXL-based devices support WiFi as a way to wirelessly connect between a host machine and your device. The following pages will outline the process to implement a wireless connection.


## QRB5165 Platforms

* If your device is based on the newer `qrb5165` processor ([VOXL 2](/voxl-2/), [VOXL 2 Mini](/voxl2-mini/), [Sentinel](/sentinel/), [Starling v2](/starling-v2/)), click here: 

[VOXL 2 WiFi Setup](/voxl-2-wifi-setup/){: .btn .btn-green }


## APQ8096 Platforms

* If your device is based on the older `apq8096` processor ([VOXL](/voxl/), [VOXL Flight](/voxl-flight/), [M500](/m500/), [Seeker](/seeker/)), click here:

[VOXL WiFi Setup](/voxl-wifi-setup/){: .btn .btn-green }

