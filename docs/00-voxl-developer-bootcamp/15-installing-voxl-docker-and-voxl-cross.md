---
layout: default
title: 15. Setup voxl-cross
parent: VOXL Developer Bootcamp
nav_order: 15
permalink: /voxl-docker-and-cross-installation/
---

# How to setup voxl-docker and voxl-cross
{: .no_toc }

---
## Background
VOXL Docker is a bash script that helps launch docker images with the right permissions and with the local directory mounted in the docker to easily build VOXL applications.

`voxl-cross` is a ubuntu-based docker image containing arm 32- and 64-bit cross-compilers for VOXL. It includes the opkg package manager which allows voxl packages to be installed in the docker image for easy dependency satisfaction when compiling. It is not the only, but is the most commonly used docker image for compiling VOXL applications.

---


## Installing VOXL Docker

### Prerequisite: Install Docker CE

1) Official instructions are here: https://docs.docker.com/install/linux/docker-ce/ubuntu/

However, they are summarized at time of writing as follows. we can't guarantee these commands are current whereas the above link should be current.

```bash
# install instructions from https://docs.docker.com/install/linux/docker-ce/ubuntu/
me@mylaptop:~/$ sudo apt remove docker docker-engine docker.io containerd runc
me@mylaptop:~/$ sudo apt update
me@mylaptop:~/$ sudo apt install -y apt-transport-https ca-certificates curl
me@mylaptop:~/$ curl -fsSL https://get.docker.com -o get-docker.sh
me@mylaptop:~/$ sudo sh ./get-docker.sh
me@mylaptop:~/$ sudo apt install docker-compose
me@mylaptop:~/$ 
```

2) Add current user to docker group to allow usage of docker commands without root privileges. You will need to restart your OS after running this command for the group permissions to be applied.

```bash
me@mylaptop:~/$ sudo usermod -a -G docker $USER
me@mylaptop:~/$ sudo reboot
```

### Install the voxl-docker Script

To build any VOXL project using VOXL Docker, you will need the dockerfiles and scripts in the voxl-docker respository. Install as below:

```bash
me@mylaptop:~/$ mkdir -p ~/git
me@mylaptop:~/$ cd ~/git
me@mylaptop:~/$ git clone https://gitlab.com/voxl-public/voxl-docker
me@mylaptop:~/$ cd voxl-docker
me@mylaptop:~/$ ./install-voxl-docker-script.sh
```


### Testing your Installation

To test your installation, try running the following commands. It may also help to read through the help menu to understand the the voxl-docker script's arguments.

```bash
me@mylaptop:~/$ voxl-docker -h

This is primarily used for running the voxl-emulator image for compiling ARM
apps-proc code for VOXL and the voxl-hexagon docker image for cross-compiling
hexagon SDSP programs. This can also launch any other installed docker image
with the -i argument.

By default this mounts the current working directory as the home directory
inside the docker for easy compilation of whichever project you are currently
working in. The directory that gets mounted inside the docker can be manually
specified with the -d argument.

The voxl-hexagon image starts with the username "user" with UID and GID 1000
which should match the first user on your desktop to avoid permissions issues.

The voxl-emulator image starts, by default, with the same username, UID, and GID
inside the docker as the user that launched it.

Since the voxl-emulator image is designed to emulate the userspace environment
that runs onboard the VOXL itself, you may wish to run as the root user inside
the voxl-emulator docker image to test certain behaviors as the root user.
This more closely mimics the on-target environment as the VOXL image runs as
root by default. Enter this mode with the -p option.

You can also specify the entrypoint for the docker image launch. By default this
is set to /bin/bash but can be user-configured with the -e option. This is most
likely used to pass the docker a command to execute before exiting automatically.
For example, to build the librc_math project in one command:

~/git/librc_math$ voxl-docker -i voxl-emulator -e "/bin/bash build.sh"


ARGUMENTS:
  -h:      : Print this help message
  -d <name>: The name of the directory to mount as ~/ inside the docker
  -i <name>: Docker image to run, usually voxl-emulator or voxl-hexagon
  -p       : for voxl-emulator image ONLY, runs as root user inside docker
  -l       : list installed docker images
  -e       : set the entrypoint for the docker image launch
```

To list available docker images (note: you should not see any images until you install them later):
```bash
me@mylaptop:~/$ voxl-docker -l
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
voxl-hexagon        latest              637d89d3a530        19 minutes ago      5.28GB
voxl-emulator       latest              0e15518b8f95        1  months ago       1.26GB
```

---

## Installing voxl-cross

### Install Pre-built voxl-cross

First, download the pre-built voxl-cross [voxl-cross_V2.5.tar.gz](https://developer.modalai.com/asset/download/120) from https://developer.modalai.com/.

Then run the following commands to load, tag as latest, and run (using voxl-docker) the docker image.

```
me@mylaptop:~/$ docker load -i voxl-cross_V2.5.tar.gz
me@mylaptop:~/$ docker tag voxl-cross:V2.5 voxl-cross:latest
me@mylaptop:~/$ voxl-docker -i voxl-cross
```

### Build voxl-cross

If you would like to build the voxl-cross docker image yourself instead of using the pre-built version, you can find instructions for this at the project's [README](https://gitlab.com/voxl-public/support/voxl-docker).


#### To learn to use voxl-cross to build and deploy applications, move onto the next page.




<br>
[Next: Building voxl-cross-template](/voxl-cross-template/){: .btn .btn-green }
