---
layout: default
title: 2.2 VOXL 2 Mini
parent: 2. Hardware Quickstart
has_children: false
nav_order: 11
permalink: /voxl-2-mini-hardware-quickstart/
---

# VOXL 2 Mini Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**NOTE**:  *Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.*

For technical details, see the [datasheet](/voxl2-mini-datasheet/).

<!--
## Quickstart Video

<div class="video-container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
-->

## How to Setup Hardware

### How to Power On and Prepare to Connect

#### Using VOXL ESC Mini

<img src="/images/voxl2-mini/m0104-developer-bootcamp-power-esc-usb.png" width="1280"/>

- Connect power cable to VOXL 2 Mini `J1`
- The other side of power cable should come soldered to the VOXL Mini ESC
- Connect USBC cable to VOXL 2 Mini `J9`
- Connect other side of USBC cable (recommended USB type A) to host computer
- Connect the ESC XT60 connector to battery or wall wart power supply

#### Using a VOXL Power Module

<img src="/images/voxl2-mini/m0104-developer-bootcamp-power-apm-usb.png" width="1280"/>

- Connect power cable to VOXL 2 Mini `J1`
- Connect other side of power cable to Power Module
- Connect USBC cable to VOXL 2 Mini `J9`
- Connect other side of USBC cable (recommended USB type A) to host computer
- Connect Power Module to the wall wart power supply
- Plug wall wart power supply into mains supply

### How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
