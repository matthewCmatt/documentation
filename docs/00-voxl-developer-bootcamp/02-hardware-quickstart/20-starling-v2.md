---
layout: default
title: 2.3 Starling V2
parent: 2. Hardware Quickstart
nav_order: 20
has_children: false
permalink: /starling-v2-hardware-quickstart/
---

# Starling v2 Hardware Quickstart
{: .no_toc }

This section provides the minimum steps needed to get a device powered up and ready to connect to a console.

More details can be found on the Starling V2 [Hardware Overview](/starling-v2-hardware-overview/) and [Datasheet](/starling-v2-datasheet/) pages, but are not necessary to complete the bootcamp.

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*



## How to Power On and Prepare to Connect

### 1. Ensure propellor clips are installed:

<img src="/images/starling-v2/MRB-D0005-prop-clips.jpg" alt="MRB-D0005-prop-clips" width="1280"/>


### 2. Connect USB-C Cable

- Connect USB-C cable to VOXL 2
- Connect other side of USB-C cable (recommmended USB type A) to host computer for later use

<img src="/images/starling-v2/MRB-D0005-4-V2-C6-M22-callouts-rear.jpg" alt="MRB-D0005-4-V2-C6-M22-callouts-rear" width="1280"/>

### 3. Power ON

- Connect the battery or wall power supply to the XT30 connector (labeled `Power` above)
  - *NOTE: do not arm the vehicle while using wall power*

The development drone will bootup and play a jingle via the ESC.

## How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
