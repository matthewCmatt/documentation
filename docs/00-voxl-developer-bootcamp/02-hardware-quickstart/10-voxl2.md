---
layout: default
title: 2.1 VOXL 2
parent: 2. Hardware Quickstart
has_children: false
nav_order: 10
permalink: /voxl-2-hardware-quickstart/
---

# VOXL 2 Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**NOTE**:  *Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.*

<!--
## Quickstart Video

<div class="video-container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
-->

For technical details, see the [datasheet](/voxl2-datasheet/).

## How to Setup Hardware

### How to Power On and Prepare to Connect

<br>
**NOTE**:  Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.

<img src="/images/voxl2/M0054-developer-bootcamp-power-usb.png" width="1280"/>

- Connect power cable to VOXL 2 `J4`
- Connect other side of power cable to Power Module
- Connect USBC cable to VOXL 2 `J9`
- Connect other side of USBC cable (recommmended USB type A) to host computer
- Connect Power Module to the wall wart power supply
- Plug wall wart power supply into mains supply

Three LEDs should illuminate GREEN ([LED details found here](/voxl2-leds)).

### How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

<br>
[Next Step: Setting Up ADB](/setting-up-adb/){: .btn .btn-green }
