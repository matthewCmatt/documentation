---
layout: default
title: VOXL Flight Kits
parent: VOXL Flight Datasheets
nav_order: 40
permalink: /voxl-flight-kits/
---

# VOXL Flight Kits
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Flight Development Kits

![voxl-flight-dk](/images/voxl-flight/voxl-flight-dk.jpg)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/voxl-flight)

Kits available:

- [MDK-M0019-2-00](https://www.modalai.com/products/voxl-flight?variant=31707275362355) - VOXL Flight Board Only
- [MDK-M0019-2-01](https://www.modalai.com/products/voxl-flight?variant=31636287094835) - VOXL FLight Power Module v3 (M0041-1-B), cables (MCK-M0019) and antennas (MRF-M0019)

- MRF-M0019-1 - Wi-Fi antennas
- MCK-M0019-1 - VOXL Flight Cable Kit PWM breakout (M0022), and cables (USB-to-JST MCBL-00010, RC input MCBL-00005)
