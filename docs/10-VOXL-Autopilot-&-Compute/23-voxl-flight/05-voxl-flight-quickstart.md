---
layout: default
title: VOXL Flight Quickstart
nav_order: 5
parent: VOXL Flight
has_children: false
permalink: /voxl-flight-quickstart/
has_toc: false
youtubeId: Kjw7X6WKDUw
---

# VOXL Flight Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxl-flight-dk](/images/voxl-flight/voxl-flight-dk.jpg)

## Quickstart Video

{% include youtubePlayer.html id=page.youtubeId %}

## Helpful Links

- [VOXL Flight Support Forum](https://forum.modalai.com/category/8/voxl-flight)
- [User Guide - VOXL Features](/voxl-quickstart-requirements/)
- [User Guide - Flight Core Features](/flight-core-getting-started/)
- [Data Sheet](/voxl-flight-datasheet/)
- [PWM ESC Calibration](/flight-core-pwm-esc-calibration/)

## Connectors - Top

![VOXL-Flight Top](/images/voxl-flight/voxl-flight/voxl-flight-overlay-top-144-2.png)

*Note: 1000 Series connectors accessible from the STM32/PX4*

| Connector | Summary | Used By |
| --- | --- | --- |
| J2  | Hires 4k Image Sensor (CSI0) | Snapdragon - Linux |
| J3  | Stereo Image Sensor (CSI1) | Snapdragon - Linux |
| J6  | Cooling Fan Connector | Snapdragon - Linux |
| J7  | BLSP6 (GPIO) and BLSP9 (UART) | Snapdragon - Linux |
| J13  | Expansion B2B | Snapdragon - Linux |
| J14  | Integrated GNSS Antenna Connection | Snapdragon - Linux |
| J1001  | Programming and Debug/UART3 | STM32 - PX4 |
| J1002  | UART ESC, UART2/TELEM3 | STM32 - PX4 |
| J1003  | PPM RC In | STM32 - PX4 |
| J1004  | RC Input, Spektrum/SBus/UART6  | STM32 - PX4 |
| J1006  | USB 2.0 Connector (PX4/QGroundControl only) | STM32 - PX4 |
| J1007  | 8-Channel PWM / 4-Channel DShot Output | STM32 - PX4 |
| J1008  | CAN Bus | STM32 - PX4 |
| J1009  | I2C3, UART4 | STM32 - PX4 |
| J1010  | Telemetry (TELEM1) | STM32 - PX4 |
| J1011  | I2C2, Safety Button Input | STM32 - PX4 |
| J1012  | External GPS & Mag, UART1, I2C1 | STM32 - PX4 |
| J1013  | Power Input, I2C3 | STM32 - PX4 (powers whole  system) |

## Connectors - Bottom

![VOXL-Flight Bottom](/images/voxl-flight/voxl-flight/voxl-flight-overlay-bottom-144.png)

*Note: 1000 Series connectors accessible from the STM32/PX4*

| Connector | Summary | Used By |
| --- | --- | --- |
| J4  | Tracking/Optic Flow Image Sensor (CSI2) | Snapdragon - Linux |
| J8  | USB 3.0 OTG | Snapdragon - Linux, **adb** |
| J10  | BLSP7 UART and I2C off-board | Snapdragon - Linux |
| J11  | BLSP12 UART and I2C off-board  | Snapdragon - Linux |
| VOXL microSD  |  | Snapdragon - Linux |
| PX4 microSD  | 32Gb Max | STM32 - PX4 |
| Wi-Fi Antennas | Included | Snapdragon - Linux |
