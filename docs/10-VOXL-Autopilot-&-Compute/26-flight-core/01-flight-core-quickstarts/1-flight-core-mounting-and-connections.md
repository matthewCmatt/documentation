---
layout: default
title: Flight Core Mounting and Connections
parent: Flight Core Quickstarts
nav_order: 1
permalink: /flight-core-mounting-and-connections/
---

# Flight Core Mounting and Connections
{: .no_toc }

## Summary

Before configuring the Flight Core's software, we suggested mounting it to your airframe and make sure the airframe is physically ready to fly. You should connect at least the following items to proceed through this getting-started manual.

- Motors/ESCs
- Flight Core Power Module
- RC Receiver (e.g. Spektrum Satellite)
- Wireless Telemetry Link, either:
  - VOXL running voxl-vision-px4
  - or SiK 915mhz point-to-point telemetry link

If you wish to use a GPS/magnetometer you should attach it now as we will reach the calibration step shortly.

If you intend to use a VOXL as the primary wireless telemetry connection, you should make sure it's software is up to date has [voxl-vision-px4 installed and configured](/voxl-vision-px4-installation/) with the IP address of your ground control computer already set.

## Connections

The [Flight Core Connections](/flight-core-connections/) are described here, which will show you how to connect everything in detailed documentation.


[Next: Flight Core Firmware](/update-flight-core-firmware/){: .btn .btn-green }
