---
layout: default
title: Flight Core Firmware
parent: Flight Core User Guides
nav_order: 98
has_children: false
permalink: /flight-core-firmware/
---

# Flight Core Firmware - PX4 Flight Controller Firmware
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

The PX4 firmware discussed here applies to both Flight Core and VOXL Flight. We are currently shipping a minor fork of PX4 v1.14.0

ArduPilot / ArduCopter support is progressing through the ArduPilot community, more details below.

Note, [Flight Core v2](/flight-core-v2/) firmware can be found [here](flight-core-v2-firmware)


## PX4 v1.14

Starting with PX4 1.14, our flight core and VOXL2-SDSP PX4 firmwares and now built from the same repo. The flight Core V1 firmware can be found here: [http://voxl-packages.modalai.com/dists/fc-v1/](http://voxl-packages.modalai.com/dists/fc-v1/)

The SDK-1.1 folder contains the PX4 firmware that's tested with SDK-1.1 running on VOXL.

Make sure you upgrade your flight core firmware with the file included in the voxl_SDK_1.1.0.tar.gz SDK installer. The SDK instaLL script will only flash VOXL, you will beed to follow the instructions below to flash the flight core.

## PX4 v1.11

PX4 v1.11.3 has been tested and is included in Flight Core and VOXL Flight production releases starting 2022-01-27 using a slightly modified fork as `v1.11.3-0.2.3`.

### Firmware Files - PX4 Stable Release

With PX4 v1.11, you can update Flight Core or VOXL Flight using **QGroundControl** by selecting the current stable release.

### Firmware Files - ModalAI Builds

We're constantly testing new stuff, and we've taken v1.11 and added some tweaks (see changelog below).  You can get the ModalAI v1.11 files here:

- [Update using QGroundControl](https://storage.googleapis.com/modalai_public/flightcore-releases/v1.11.3-0.2.3-modalai_fc-v1_default.px4) ([commit](https://github.com/modalai/px4-firmware/commit/f40863b2a72a3cf1c8d85b364a25a5dff3e87898))

- [Update using Binary (not common, e.g. STLink)](https://storage.googleapis.com/modalai_public/flightcore-releases/v1.11.3-0.2.3-modalai_fc-v1.bin)  ([commit](https://github.com/modalai/px4-firmware/commit/f40863b2a72a3cf1c8d85b364a25a5dff3e87898))

### Update Procedure

The firmware update procedure [can be found here](/update-flight-core-firmware/)

### v1.11 Change Log

The goal is to have PX4 mainline support for Flight Core, although occasionally fixes or enhancements are introduced and released by ModalAI faster than PX4 releases.

ModalAI's v1.11 development branch is found [here](https://github.com/modalai/px4-firmware/tree/modalai-1.11)

| Version | Type | Change Description  | Commit | Status |
|---      |---   |---                  |---     |---     |
| [FCv1 - v1.11.3-0.2.3](https://storage.googleapis.com/modalai_public/flightcore-releases/v1.11.3-0.2.3-modalai_fc-v1_default.px4) | ModalAI Enhancement | - Add alternate GPS LED i2c address <br>- Add support for: <br>- GPS-to-VIO fusion<br> - Automatic Data Fusion Selection<br> - VIO airspaces <br> - FCv2 Board Support added <br> - Mainline Cherry Pick: icm42688p: properly disable anti-aliasing and notch filter [info here](https://github.com/modalai/px4-firmware/commit/ff53393d0082eb3b7fce66be68d0ceea710adb63)| [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.2.3) | Released |
| [v1.11.3-0.0.5](https://storage.googleapis.com/modalai_public/flightcore-releases/1.11.3-0.0.5.modalai_fc-v1_default.px4) | ModalAI Enhancement | - Integrate v1.11.3 patches from PX4 mainline | [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.0.5) | Released |
| v1.11.2-0.0.4 | ModalAI Enhancement | - Add support for MDK-M0051-1-00 test device <br> - Add modalai system command for factory testing | [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.0.4) |  |
| v1.11.2-0.0.3 | ModalAI Enhancement | - [Make ICM42688 default](https://github.com/modalai/px4-firmware/commit/9b5bad6e19cf806d5e822a77bf348e392f126876) <br> - [Add support for M0051](https://github.com/modalai/px4-firmware/commit/6e33449f4d5b172f0deb90191758f6bf8d5d629b)  | [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.0.3) |  |
| v1.11.2-0.0.2 | ModalAI Enhancement | - [Allow EKF to init without VIO](https://github.com/modalai/px4-firmware/commit/a317030af9b33b32078e756e05e5a49213b1a49c) | [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.0.2) |  |
| v1.11.2-0.0.1 | ModalAI Enhancement | - [UART ESC Support](https://github.com/modalai/px4-firmware/commit/1dfa9feba729a243d60bbaa8fa9f8fcb226404ac) <br> - [EKF init w/o mag](https://github.com/modalai/px4-firmware/commit/65c59f1af7b3d077b186fecb088ba554efd178d9)  | [Tag](https://github.com/modalai/px4-firmware/releases/tag/v1.11.3-0.0.1) |  |

<br/>


## PX4 v1.10

When using PX4 v1.10, it is recommended that you use the [ModalAI v1.10 build](https://github.com/modalai/px4-firmware/tree/modalai-1.10), which fixes some bugs present in the PX4 v1.10 release affecting this hardware (see changelog below for details).

### Firmware Files

You can get the v1.10 files here:

- [Update using QGroundControl](https://storage.googleapis.com/flight-core-firmware-1-10/latest/modalai_fc-v1_default.px4)(git hash: 782a54bfd6ec2bd0a82cd43ce8c5dc9cd3de6ff0)
- [Update using Binary (not common, e.g. SEGGER)](https://storage.googleapis.com/flight-core-firmware-1-10/latest/modalai_fc-v1.bin)(git hash: 782a54bfd6ec2bd0a82cd43ce8c5dc9cd3de6ff0)

### Update Procedure

The firmware update procedure [can be found here](/update-flight-core-firmware/)

### v1.10 Change Log

The goal is to have PX4 mainline support for Flight Core, although occasionally fixes or enhancements are introduced and released by ModalAI faster than PX4 releases.

*NOTE: in v1.10, to find the version, run `ver all` from nsh and find the fw-hash*


| Version (fw-hash) | Type         | Change Description                                                   | PR / Commit        | Status |
|--- |---           |---                                                                   |---                 | ---    |
| Latest, [782a54bf](https://github.com/modalai/px4-firmware/commit/782a54bfd6ec2bd0a82cd43ce8c5dc9cd3de6ff0)  | Enchancement | Add support for VOXL PM v3 (INA231)                                  | [782a54bf](https://github.com/modalai/px4-firmware/commit/782a54bfd6ec2bd0a82cd43ce8c5dc9cd3de6ff0) | [Merged into Master](https://github.com/PX4/Firmware/pull/15577), likely in v1.12 or v1.11 maintenance |
| | Bug Fix      | Fix for timer config bug resulting in chan5 output being overwritten | [65c2b4d7](https://github.com/modalai/px4-firmware/commit/65c2b4d76c51a078b760cdff94d8ff18bd2347ca) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Enchancement | Support velocity odometry with MAV_FRAM_LOCAL_FRD                    | [1cbbfffc](https://github.com/modalai/px4-firmware/commit/1cbbfffc91a5a520b9b9bb189e82c79b57b0ab18) | stale - no longer required by voxl-vision-px4 |
| | Enchancement | Allow EKF2 to intialize without mag if VIO valid                     | [2d7ff3c6](https://github.com/modalai/px4-firmware/commit/2d7ff3c6ac517162f96ee8764b6cf3b9458662ec) | **Not mainlined, PR rejected** |
| | Enchancement | HW detection of Flight Core vs VOXL Flight                           | [589c4a46](https://github.com/modalai/px4-firmware/commit/589c4a4657c45a1a69cb5af4b0d9c95c6c6c5315) and [0f6ffc53](https://github.com/modalai/px4-firmware/commit/0f6ffc53b9000f8f276dbbbef2a2d10fe5849d11) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Bug Fix      | Fixes issue with Flight Core HW rev detection                        | [589c4a46](https://github.com/modalai/px4-firmware/commit/589c4a4657c45a1a69cb5af4b0d9c95c6c6c5315) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Bug Fix      | Fix for SD card writing failures                                     | [ea1e5b72](https://github.com/modalai/px4-firmware/commit/ea1e5b72be5c76bf14b7c1145ad16f7a811502ff) and [dbd065ab](https://github.com/modalai/px4-firmware/commit/dbd065abb38f1bfe3a2dc3e548a98f68d514437d) and [fd0a00f9](https://github.com/modalai/px4-firmware/commit/fd0a00f92af24409e6c262899107c3a2d1a4e5c6) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Bug Fix      | Fix for timer config bug resulting in chan5 output being overwritten | [65c2b4d7](https://github.com/modalai/px4-firmware/commit/65c2b4d76c51a078b760cdff94d8ff18bd2347ca) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Enchancmeent | Add Support for VOXL-Flight                                          | [5ffab958](https://github.com/modalai/px4-firmware/commit/5ffab958d3dffcbfab68ca341fc6089a54b35e70) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Bug Fix      | rotate default orientation 180° yaw                                  | [dc816a5c](https://github.com/modalai/px4-firmware/commit/dc816a5c8ad5f1bff861a0ae709028905137f1d5) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
| | Bug Fix      | Various stack size and defconfig bug fixes                           | [a37c028a](https://github.com/modalai/px4-firmware/commit/a37c028a49e5b300b808b358fd53bec33fff081f) and [5dd4c0dc](https://github.com/modalai/px4-firmware/commit/5dd4c0dc0d09b318287528fdca3c3d18a364f90b) and [ff994390](https://github.com/modalai/px4-firmware/commit/ff99439084aab39dda6892047e2c3bd16dfdb20a) | Fixed in ModalAI v1.10, Mainlined in v1.11 |
|  | **PX4 Release**  |**PX4 v1.10.0 production release**                                       | [4f6faac2](https://github.com/modalai/px4-firmware/commit/4f6faac2c86daa14417b33bfbf9c4e44bf17492a) | |


## How to Build the Firwmare

This example uses the PX4 mainline repository.

```bash
git clone https://github.com/PX4/Firmware.git
cd Firmware
make modalai_fc-v1
```

## Flashing Using SEGGER JLink

You can use `JFlash` with a `.jlink` file with command like so, where `deadbeef.bin` is the name of the binary file:

```bash
loadbin modalai_fc-v1.bin,0x08008000
```

## ArduPilot

ModalAI's Flight Core is integrated into ArduPilot by the great ArduPilot community. The engineers at ModalAI do not have experience with ArduPilot, so can not offer significant support. It is recommended to find support through the normal ArduPilot channels ([boards](https://discuss.ardupilot.org/) & [Discord](https://ardupilot.org/dev/docs/ardupilot-discord-server.html)). Hopefully ModalAI will gain experience with ArduPilot in the future.

ArduPilot pull request for ModalAI Flight Core v1 base hardware support [Github](https://github.com/ArduPilot/ardupilot/pull/16255/)

ArduPilot pull request for VOXL-compatible Visual Inertial Odometry support [Github](https://github.com/ArduPilot/ardupilot/pull/19563)


[Next: Flight Core Bootloader](/flight-core-bootloader/){: .btn .btn-green }
