---
layout: default
title: VOXL 2 Kernel Build
parent: VOXL 2 System Image
nav_order: 25
permalink:  /voxl2-kernel-build-guide/
---

# VOXL 2 Kernel Build Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL 2 kernel comes from here: https://git.codelinaro.org/clo/la/kernel/msm-4.19

We started with the RB5 development kit and slowly updated the device tree and kernel to work on VOXL2 and VOXL2 Mini.

The aim of this guide is to show you how to build a new kernel for VOXL 2 and VOXL 2 mini and modify it.

## Build Components

We're trying to adhere to bitbake standards, but likely have some inconsistencies with our recipes...  Feel free to offer suggestions if things are bugging you or submit PRs!

| Component      | Repo |
| -----------    |---   |
| qrb5165-kernel-build-docker | [repo](https://gitlab.com/voxl-public/system-image-build/qrb5165-kernel-build-docker/-/tree/qrb5165-ubun1.0-14.1a)|
| qrb5165-kernel | [repo](https://gitlab.com/voxl-public/system-image-build/qrb5165-kernel/-/tree/qrb5165-ubun1.0-14.1a) |
| meta-voxl2     | [repo](https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tree/qrb5165-ubun1.0-14.1a) |
| meta-voxl2-bsp | [repo](https://gitlab.com/voxl-public/system-image-build/meta-voxl2-bsp/-/tree/qrb5165-ubun1.0-14.1a) |

## Setup Guide

### Requirements

- git
- Docker
- the more cores the better if you want to build quickly...

### Build Docker

We use "build dockers" all over the place to easily share build environments and use in our CI pipelines.

This step is optional, with the alternative being you look at the `Dockerfile` for the requirements for the build machine.

```bash
git clone -b qrb5165-ubun1.0-14.1a https://gitlab.com/voxl-public/system-image-build/qrb5165-kernel-build-docker
cd qrb5165-kernel-build-docker

./docker-build-image.sh

...

Step 28/28 : RUN export PATH=$PATH:/usr/local/go/bin
 ---> Running in 865ac9837af2
Removing intermediate container 865ac9837af2
 ---> 83b19199b7b5
Successfully built 83b19199b7b5
Successfully tagged qrb5165-kernel-build:qrb5165-ubun1.0-14.1a
```

### Run Build Docker

Now that we have the docker image created (use `docker image ls` to see), we can run an instance of this using a helper script:

```bash
./docker-run-image.sh

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

user@f3c06dd0167d:~$

```

Now you are in the chroot'd environment, and you don't need to muddy your system with tools.

## Build Guide

### First Time Use (Part 1: Sync)

We have to synchronize with the https://git.codelinaro.org/clo as well as the various ModalAI open source components:

{: .alert .warning-alert}
**WARNING:** This sync command takes some time (10 minutes?) before progressing...

```bash
./qrb5165-sync.sh

...

repo has been initialized in /home/user/build_mount/lu.um.1.2.1/apps_proc
```

Now we have the source code needed.

### First Time Use (Part 1: Patch)

Apply patches found in the `patches` directory.

This is done to pull in VOXL 2 customizations and fix errors that are outside the scope of the product but needed to complete the build (e.g. build system fixes).

To apply the patches, run the following:

```bash
./qrb5165-patch.sh
```

### Build

Now, you can build for VOXL 2, which is the `M0054` machine.  If building for RB5 Flight, you will use the `M0052` machine.

```bash
./qrb5165-build.sh -m M0054
```

### Artifacts

The kernel image is `qti-ubuntu-robotics-image-m0054-boot.img` and lives inside the build docker at:

```
/home/user/build_mount/lu.um.1.2.1/apps_proc/build-qti-distro-ubuntu-fullstack-debug/tmp-glibc/deploy/images/m0054/qti-ubuntu-robotics-image-m0054-boot.img
```

After you exit the build docker, it will be in your current directory in `workspace`, for example:

```bash
-la workspace/lu.um.1.2.1/apps_proc/build-qti-distro-ubuntu-fullstack-debug/tmp-glibc/deploy/images/m0054/
total 414500
drwxr-xr-x 2 modalai modalai      4096 May  3 15:18 .
drwxr-xr-x 3 modalai modalai      4096 May  3 15:09 ..
-rw-r--r-- 2 modalai modalai  21387346 May  3 15:09 Image.gz
-rw-r--r-- 1 modalai modalai  21393408 May  3 15:18 qti-ubuntu-robotics-image-m0054-boot.img
-rw-r--r-- 2 modalai modalai   5859250 May  3 15:09 System.map
-rw-r--r-- 2 modalai modalai 375915296 May  3 15:09 vmlinux
```

## Flashing Guide

### Test

You can boot the new kernel without flashing it.

First, check out the current version:

```bash
adb shell uname -a
Linux qrb5165-rb5 4.19.125 #1 SMP PREEMPT Fri Apr 15 16:54:55 UTC 2022 aarch64 aarch64 aarch64 GNU/Linux
```

Now, run the following:

```bash
adb reboot bootloader
fastboot flash boot_a qti-ubuntu-robotics-image-m0054-boot.img
fastboot flash boot_b qti-ubuntu-robotics-image-m0054-boot.img
fastboot reboot
```

Now, check the new version:

```bash
adb shell uname -a
Linux qrb5165-rb5 4.19.125 #1 SMP PREEMPT Tue May 3 22:06:09 UTC 2022 aarch64 aarch64 aarch64 GNU/Linux
```

### Flash

If testing worked above, you can now flash it:

```bash
adb reboot bootloader
fastboot flash boot qti-ubuntu-robotics-image-m0054-boot.img
adb wait-for-device
```

## Developer Topics

### How to Rebuild

You don't need to re-sync and patch, just run and build.  For example, starting back on the host machine (outside docker):

```bash
./docker-run-image.sh
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

user@11dbde7bdab7:~$
user@11dbde7bdab7:~$ ls build_mount/lu.um.1.2.1/apps_proc/poky/

bitbake        meta-openembedded            meta-qti-audio   meta-qti-core     meta-qti-ml        meta-qti-sepolicy  meta-selftest  meta-voxl2-bsp     qti-conf         README.qemu
documentation  meta-poky                    meta-qti-bsp     meta-qti-data     meta-qti-qmmf      meta-qti-ss-mgr    meta-selinux   meta-webkit        README.hardware  scripts
LICENSE        meta-qti-adk                 meta-qti-bt      meta-qti-display  meta-qti-robotics  meta-qti-ubuntu    meta-skeleton  meta-yocto-bsp     README.LSB
meta           meta-qti-aosphal-adaptation  meta-qti-camera  meta-qti-gst      meta-qti-security  meta-qti-video     meta-voxl2     oe-init-build-env  README.poky
```

Shown above, all the source is still there.

You can edit files, and the build again!

```
./qrb5165-build.sh
```

### How to Enable In Tree Driver (Example)

See: https://gitlab.com/voxl-public/system-image-build/meta-voxl2-bsp/-/tree/qrb5165-ubun1.0-14.1a/recipes-kernel/linux-msm

For example, for M0054 (VOXL 2):

Enable `smsc95xx` using `CONFIG_USB_NET_SMSC95XX`

In:

```bash
apps_proc/poky/meta-voxl2-bsp/recipes-kernel/linux-msm/files/m005x.cfg
```

We can enable by adding `CONFIG_USB_NET_SMSC95XX=y` to the `m005x.cfg` file.

Now, you can build again:

```bash
./qrb5165-build.sh -m M0054
```

Note: to rebuild, clean out the existing build:

```
./qrb5165-clean.sh

# this time, for M0104, as example
./qrb5165-build.sh -m M0104
```

Flash following procedures above.  We can check for the flag before the update:

```bash
voxl2:/$ zcat /proc/config.gz | grep CONFIG_USB_NET_SMSC95XX
# CONFIG_USB_NET_SMSC95XX is not set
```

And then after updating:

```bash
voxl2:/$ zcat /proc/config.gz | grep CONFIG_USB_NET_SMSC95XX
CONFIG_USB_NET_SMSC95XX=y
```
