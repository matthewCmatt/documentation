---
layout: default
title: VOXL 2 External Flight Controller
parent: VOXL 2 User Guides
nav_order: 15
permalink:  /voxl2-external-flight-controller/
---

# VOXL 2 Connecting an External Flight Controller
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

Although VOXL 2 has the capability to run PX4 in a real time DSP, some use cases call for an external flight controller.

You can communicate between a flight controller TELEM port and a VOXL 2 UART port.  For example, Flight Core has a default telemetry port on J5 with a MavLINK instance running at 57600 baud.

<img src="/images/voxl2/m0054-ext-fc-real.jpg" alt="m0054-ext-fc-real"/>

## VOXL-SDK Integration

**Available starting in SDK 1.1**

The [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) library can be used to interface with the UARTs.  Using the Applications Process UARTs, you can follow standard Linux paradigms.  Using the DSP UARTs, you need to use the `libqrb5165-io` library.

If you are interfacing via MAVLink, it's recommended to use the [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) project, which depends on `libqrb5165-io` and shows usage.

## UART Devices

### Application Processor UARTs

As of System Image 1.6, there are [4 applications processor UARTs exposed](/voxl2-linux-user-guide/#uarts) on VOXL 2, which can be used using standard Linux techniques.

#### VOXL 2

| UART            | Connector               | Add-On / Adapter | libqrb5165-io ID/enum |
|------           |-------------------------|----------------- |------- |
| `/dev/ttyHS0`   | [J8](/voxl2-connectors/), camera group 2      | [M0076](https://www.modalai.com/products/mdk-m0076-1) with hand soldering to test points | `0`, `APPS_HS0_UART` |
| `/dev/ttyHS1`   | [J3](/voxl2-connectors/), Board to Board      | [M0125](/voxl2-usb3-uart-add-on/) / [Purchase Here](https://www.modalai.com/products/usb-3-uart-expansion-adapter?variant=40740708876339) | `1`, `APPS_HS1_UART` |
| `/dev/ttyHS2`   | [J5](/voxl2-connectors/), HS Board to Board   | [M0090](/5G-Modem-datasheet/) / [Purchase Here](https://www.modalai.com/products/m0090?variant=40594936463411) | `2`, `APPS_HS2_UART` |
| `/dev/ttyHS3`   | [J5](/voxl2-connectors/), HS Board to Board   | M0130 - Internal Use | `3`, `APPS_HS3_UART` |

#### VOXL 2 Mini

On VOXL 2 Mini, there's [1vapplications processor UART exposed](/voxl2-mini-linux-user-guide/)

| UART            | Connector               | libqrb5165-io ID/enum |
|------           |-------------------------|------- |
| `/dev/ttyHS0`   | [J10](/voxl2-mini-connectors/), pins 4/5 (TX/RX) | `0`, `APPS_HS0_UART` |

### DSP UARTs (via Apps Proc)

**Available starting in SDK 1.1**

**NOTE1: at this time, only one of these QUPX UARTs can be used at a time** <br>

**NOTE2: if running `voxl-px4`, these DSP UARTs aren't available** <br>

Additionally, through the [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) project, you can access the DSP UARTs from the applications processor as well, the the board-to-cable interfaces (no add on board required!).

| UART            | Connector               | libqrb5165-io ID/enum |
|------           |-------------------------|---- |
| `QUP2`          | VOXL2 [J18](/voxl2-connectors/), VOXL2 Mini [J19](/voxl2-connectors/) pin 7/8, ESC Port  |  `12`, `SLPI_QUP2_UART` |
| `QUP6`          | [J19](/voxl2-connectors/) pin 2/3, GNSS Port  | `16`, `SLPI_QUP6_UART` |
| `QUP7`          | [J19](/voxl2-connectors/) pin 10/11, RC IO Port  | `17`, `SLPI_QUP7_UART` |

<img src="/images/voxl2/m0054-ext-fc-dsp.jpg" alt="m0054-ext-fc-dsp.jpg"/>

#### DSP UART to Flight Core v2

<img src="/images/voxl2/M0054-ext-fc-m0087.jpg" alt="M0054-ext-fc-m0087.jpg"/>

## Add-On Boards

### Using USB/UART Addon Board (M0125)

In the following example, we'll add the USB3/UART Add-On Board (M0125) to VOXL 2 (M0054) which gives us a 4 pin JST GH connector with a UART availabe at `/dev/ttyHS1` from the applications processor.

Using `MCBL-00067-1`, connect from M0125's J3 to Flight Core's (v1/v2) (M0018/M0087) J5, TELEM1/TELEM2 (respectively).

<img src="/images/voxl2/m0054-ext-fc-m0125.png" alt="m0054-ext-fc-m0125.png"/>

| M0125 J3  | M0018 J5 |
|------     |---       |
| 1         |          |
| 2 RX      | 2 TX     |
| 3 TX      | 3 RX     |
| 4 GND     | 6 GND    |

### Using M.2 5G Modem Addon Board (M0090)

The VOXL2 (`M0054`) can have the M.2 5G Modem Board Addon attached, which exposes a UART on `M0090 - J9` that is accessible in VOXL2's application processor as `/dev/ttyHS2`.

Using `MCBL-00066-1`, connect from M0090's J9 to Flight Core's (v1/v2) (M0018/M0087) J5, TELEM1/TELEM2 (respectively).

<img src="/images/voxl2/m0054-ext-fc-m0090.png" alt="m0054-ext-fc-m0090.png"/>

| M0090 J9  | M0018 J5 |
|------     |---       |
| 1         |          |
| 2 TX      | 3 RX     |
| 3 RX      | 2 TX     |
| 4         |          |
| 5         |          |
| 6         | 6 GND    |

### Using J8 Camera Connector

You will need to use a level shifter likely, as the logic is 1.8V here.  VOXL2's camera group 3 connector can be used with M0076 interposer which has test points you can solder to.

<img src="/images/voxl2/m0054-ext-fc-m0076.jpg" alt="m0054-ext-fc-m0076.jpg"/>

- UART_TX - TP7
- UART_RX - TP8
- VREF 1P8 - TP1
- DGND - TP11

## VOXL SDK Software Setup

For information on MAVlink routing in VOXL-SDK please see [here](/mavlink/).

#### autopilot_uart_bus Values

`voxl-mavlink-server` ships in the VOXL SDK, with a config file located at `/etc/modalai/voxl-mavlink-server.conf`.  For the `px4_uart_bus` field, these are current options:


| ENUM             | Device / Name           | autopilot_uart_bus value |
|------------------|-------------------------|--------------------|
| `APPS_HS0_UART`  | `/dev/ttyHS0`           | "autopilot_uart_bus": 0  |
| `APPS_HS1_UART`  | `/dev/ttyHS1`           | "autopilot_uart_bus": 1  |
| `APPS_HS2_UART`  | `/dev/ttyHS2`           | "autopilot_uart_bus": 2  |
| `APPS_HS3_UART`  | `/dev/ttyHS3`           | "autopilot_uart_bus": 3  |
| `SLPI_QUP2_UART` | `QUP2`                  | "autopilot_uart_bus": 12 |
| `SLPI_QUP6_UART` | `QUP6`                  | "autopilot_uart_bus": 16 |
| `SLPI_QUP7_UART` | `QUP7`                  | "autopilot_uart_bus": 17 |

#### autopilot_uart_bus Values

For `autopilot_uart_bus`, configure this the same as the MAVLink instance on Flight Core.  In this example, we are using `921600`.

For `en_external_uart_ap`, set to `true`.

For apps proc, standard termios rates up to `4000000`.

For DSP, a bit more custom rates:

```
9600, 38400, 57600, 115200, 230400, 250000, 420000, 460800, 921600,  1843200, 2000000
```

### External Flight Controller PX4 SW Setup

If using Flight Core, you would likely need to connect over the USB interface first to configure the external flight controller.

For this example to work out of the box, make sure to set  `SER_TELX_BAUD` to the default baud rate of  **921600** if you've changed it.

- Connect to QGroundControl over USB and open the parameters.
  - On Flight Core v1 (M0018)
    - J1 is mapped to `TELEM2` in PX4 (/dev/ttyS4), use `SER_TEL2_BAUD`
    - J4 is mapped to `TELEM3` in PX4 (/dev/ttyS4), use `SER_TEL3_BAUD`.  This is used by ModalAI ESCs by default.
    - J5 is mapped to `TELEM1` in PX4 (/dev/ttyHS6), use `SER_TEL1_BAUD`
  - On Flight Core v2 (M0087)
    - J1 is mapped to `TELEM1` in PX4 (/dev/ttyHS6), use `SER_TEL1_BAUD`
    - J5 is mapped to `TELEM2` in PX4 (/dev/ttyHS4), use `SER_TEL2_BAUD`.  This is used by ModalAI ESCs by default.

Check the status of MAVLink instances by opening the MAVLink Console in QGC and running `mavlink status`:

On Flight Core v1, you should see something like this if using `921600` baud:

```bash
instance #1:
    mavlink chan: #1
    type:        GENERIC LINK OR RADIO
    flow control: ON
    rates:
      tx: 81.8 B/s
      txerr: 0.0 B/s
      tx rate mult: 0.682
      tx rate max: 1200 B/s
      rx: 20.9 B/s
      rx loss: 0.0%
      received from sysid: 0 compid: 197: 43, lost: 0, last 343 ms ago
    FTP enabled: YES, TX enabled: YES
    mode: Normal
    MAVLink version: 2
    transport protocol: serial (/dev/ttyS6 @921600)
```

Above is showing you that the Flight Core is communicating with VOXL2 successfully, note `tx: 81.8 B/s` and `rx: 20.9 B/s` rates indicate bidirectional traffic.

## pymavlink Software Based Example

{: .alert .warning-alert}
**WARNING:** This clashes with the example above, so ensure to disable the `voxl-mavlink-server` if you configured the VOXL SDK.

The `SER_TEL1_BAUD`/`SER_TEL1_BAUD` baud rate should be set to `57600` for thi example (for Flight Core v1/v2 respectively).

<img src="/images/voxl2/m0054-ext-fc-py.png" alt="m0054-ext-fc-py"/>

First, connect VOXL 2 to the internet so we can use `apt` to install `pymavlink`.  You can connect an ethernet or WiFi dongle to the USB3 port.

Conect to the VOXL 2 over ADB:

```
adb shell
```

Now on target:

```
apt update
sudo apt-get install python3-pip
sudo apt-get install libxml2-dev libxslt-dev python-dev
sudo pip3 install pymavlink pyserial
```

Get the `mavlink_shell.py` tool from PX4:

```
cd /home
wget https://raw.githubusercontent.com/PX4/PX4-Autopilot/main/Tools/mavlink_shell.py
```

Now, run the program and interact with the Flight Core.

```
python3 mavlink_shell.py /dev/ttyHS1
Connecting to MAVLINK...

NuttShell (NSH)

nsh> ver all

HW arch: MODALAI_FC_V1
HW type: V106
HW version: 0x00000000
HW revision: 0x00000006
FW git-hash: 7008425b466eba94e35304e502454208d578ba56
FW version: 1.11.3 0 (17498880)
FW git-branch: fcio-v2
OS: NuttX
OS version: Release 8.2.0 (134349055)
OS git-hash: ec20f2e6c5cc35b2b9bbe942dea55eabb81297b6
Build datetime: May 10 2022 18:38:31
Build uri: localhost
Toolchain: GNU GCC, 9.3.1 20200408 (release)
PX4GUID: 0002000000003930353534385109003a002e
MCU: STM32F76xxx, rev. Z
```
