---
layout: default
title: VOXL 2 Unbricking
parent: VOXL 2 User Guides
nav_order: 30
permalink:  /voxl2-unbricking/
---

# Unbricking your VOXL 2
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

If you have corrupted your VOXL's filesystem (if ADB is unable to detect your VOXL, this is a common sign), follow this guide.

{: .alert .warning-alert}
This process will perform a factory reset. All data on the VOXL will be wiped.

### How does it work?

We use Qualcomm's tool for unbricking Snapdragon devices called QDL.

Snapdragon based devices contain a special mode of operation, called Emergency Download Mode (EDL). In this mode, the device identifies itself as `05c6:901d Qualcomm, Inc.` via. USB and can communicate with a PC host.

EDL mode itself implements the Qualcomm Sahara protocol, which accepts an OEM-digitally-signed programmer over USB. The programmer implements the Firehose protocol which allows the host PC to send commands to write into the onboard storage (UFS).

An open source tool (for Linux) that implements the Qualcomm Sahara and Firehose protocols has been developed by Linaro, and can be used to program (or unbrick) Snapdragon based devices.

## Usage

### Prerequisites

#### Ubuntu Host

A couple different libraries are required to build the QDL program, install them using the following:

```
$ sudo apt-get install libxml2-dev libudev-dev
```

Some Linux distributions come with `ModemManager`, a tool for configuring Mobile Broadband. This program, if installed, will interfere with the QDL flashing. One option is to remove the program with the following:

```
$ sudo apt remove --purge modemmanager
```

Alternatively, if you need ModemManager, you can temporarily stop it from running with

```
$ sudo systemctl stop ModemManager
```

and restart it with:

```
$ sudo systemctl start ModemManager
```

GCC is also needed. This can be installed with
```
$ sudo apt install gcc
```

#### OSX Host

Install `libusb`:

```
brew install libusb
brew link libusb
```

### Build

Clone the QDL repository using the following:

```
$ git clone https://git.codelinaro.org/linaro/qcomlt/qdl.git
```

Move into the project directory:

```
$ cd qdl
```

Build and install program using:

```
$ make
$ make install
```

This will install the `qdl` program into `/usr/local/bin`

Test the install by running the following and verifying the output:

```
$ qdl

qdl [--debug] [--storage <emmc|nand|ufs>] [--finalize-provisioning] [--include <PATH>] <prog.mbn> [<program> <patch> ...]
```

### Image Download

Download the latest image release from the [ModalAI protected downloads page](https://downloads.modalai.com)

Latest: [VOXL 2 QDL Image 14.1a_1.1.2](https://developer.modalai.com/asset/download/104)

Unzip the downloaded release:

```
$ unzip r14.1a_1.1.2-M0054-14.1a-FLAT-V3.zip
```

Move into the `ufs` sub-directory of the unzipped release:  

```
$ cd 1.1.2-M0054-14.1a-FLAT-V3/ufs
```

## Flash

### Hardware preparation

In order to force your VOXL 2 into EDL mode, slide the `SW2` switch into the `ON` position, see below:

<img src="/images/voxl2/m0054-edl.png" alt="m0054-edl" width="640"/>

Now plug in power and USB-C cable from VOXL 2 to host PC.

### Initiate flash

While in the `ufs` sub-directory of the image release, run the following `qdl` command in order to flash the device:

```
$ sudo qdl --debug --storage ufs --include . prog_firehose_ddr.elf rawprogram_unsparse0.xml rawprogram1.xml rawprogram2.xml rawprogram3.xml rawprogram4.xml rawprogram5.xml patch0.xml patch1.xml patch2.xml patch3.xml patch4.xml patch5.xml
```

All of the required `*.xml` files as well as the directory path (in this case we use `.` to represent the current directory location) need to be specified in order for `qdl` to find all of the required flash data.

The `qdl` flash will take 10-15 minutes to complete (may be faster or slower depending on hardware and USB cable).

Once completed, you should see an output similar to the following exerpt: 

```
<response value="ACK" rawmode="false" /></data>
partition 1 is now bootable
FIREHOSE WRITE: <?xml version="1.0"?>
<data><power value="reset"/></data>

FIREHOSE READ: <?xml version="1.0" encoding="UTF-8" ?>
<data>
<log value="INFO: Calling handler for power" /></data>
LOG: INFO: Calling handler for power
FIREHOSE READ: <?xml version="1.0" encoding="UTF-8" ?>
<data>
<response value="ACK" rawmode="false" /></data>
```

Remove power and USB from the VOXL 2, slide `SW2` switch back to the `OFF` position.

You can now plug in power and, after a short time, USB (first boot after QDL takes slightly longer than following boots) ADB will be available.

Verify that the flash was successful by checking the installed image version:

```
# adb onto VOXL 2
$ adb shell

# check version
$ root@qrb5165-rb5:/# cat /etc/version
1.1.2-M0054-14.1a-FLAT-V3
```

This version should match the image you just flashed.


DEBUGGING NOTE: If you are unable to ADB onto the device but the device is being detected correctly by the host PC as seen below:

```
$ lsusb

Bus 001 Device 022: ID 05c6:901d Qualcomm, Inc. KONA-QRD _SN:21217FEB
```

restarting the ADB server may fix the issue:

```
$ adb kill-server

$ adb start-server
```

You should now be able to ADB onto your device successfully.

## Next Steps

Now that your VOXL 2 has been reset to the factory image, you will need to reinstall the SDK (including the System Image and VOXL Suite) to get your VOXL back to it's original, out-of-the-box state. Without the SDK installed, your VOXL is unable to run the tools, utilities, and services that ModalAI has developed.

To do this, follow [this guide](/upgrading-sdk-version/).