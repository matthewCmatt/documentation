---
layout: default
title: VOXL 2 Power
parent: VOXL 2 User Guides
nav_order: 1
permalink:  /voxl2-power/
youtubeiId1: lisiTWOa7lM
---

# VOXL 2 Power
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware

### J4 - Main 5VDC Input with I2C 

The main power input is `J4` and is typically paired with a [VOXL Power Module v3](/power-module-v3-datasheet/), which provides the up to 6A momentary inrush current and provides two I2C based power monitoring ICs, providing PX4 the battery status.

The recommended cable is `MCBL-00001` for this interface.

<img src="/images/voxl2/m0054-user-guides-power.png" alt="m0054-user-guides-power.png" width="1280"/>

M0054 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

## Software

### Checking Battery Status

With the VOXL PM attached, you check the battery using `px4-listener battery_status` command after getting a shell to VOXL 2.

Here's an example from a 3S battery:

```
px4-listener battery_status

TOPIC: battery_status
 battery_status_s
	timestamp: 1080175258  (0.059704 seconds ago)
	voltage_v: 12.6662
	voltage_filtered_v: 12.6659
	current_a: 0.1758
	current_filtered_a: 0.1791
	average_current_a: 0.0000
	discharged_mah: 51.6742
	remaining: 1.0000
	scale: 1.0000
	temperature: nan
	cell_count: 3
	voltage_cell_v: [4.1200, 4.3200, 4.2200, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]
	max_cell_voltage_delta: 0.0000
	capacity: 0
	cycle_count: 0
	run_time_to_empty: 0
	average_time_to_empty: 0
	serial_number: 0
	manufacture_date: 0
	state_of_health: 0
	max_error: 0
	interface_error: 0
	connected: True
	source: 0
	priority: 0
	id: 1
	is_powering_off: False
	warning: 0
```

### voxlpm PX4 Driver

The [voxlpm](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/power_monitor/voxlpm) PX4 driver monitors both battery and companion computer regulated voltage.

The driver runs in PX4 on the DSP, and interfaces through SSC QUP2.

You can't get to this I2C bus through Ubuntu at this time.


## Related Video

{% include youtubePlayer.html id=page.youtubeiId1 %}
