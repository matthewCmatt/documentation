---
layout: default
title: VOXL 2 Wifi Dongle User Guide
parent: VOXL 2 User Guides
nav_order: 9
permalink:  /voxl2-wifidongle-user-guide/
youtubeId: aVHBWbwp488
---

# VOXL 2 Wifi Dongle User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

VOXL 2 by itself doesn't have Wi-Fi or a LAN port to keep the core size and weight low, but it's easy to overcome.

- Add-on board for networking:
  - [USB Expansion Board](https://www.modalai.com/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot) (M0017) + JST-to-USB cable (MCBL-00009-1) + [Ethernet](/voxl2-connecting-quickstart/) or [Wi-Fi dongle](/voxl2-wifidongle-user-guide/)
  - 5G Modem Add-on (M0090-3-01) + your own VPN (or email devops @ modalai.com if interested in possible beta programs)
  - [Microhard Modem Add-On](https://www.modalai.com/products/voxl-microhard-modem-usb-hub) (M0048-1)

The `M0078-2` USB Debug add-on and `M0090` or PCIe/5G Modem Carrier add-on board provides access to a USB 2.0 hub via a 4-pin and JST connector when used with [4-pin JST to USBA Female Cable](/cable-datasheets/#mcbl-00009).

This allows you to plug in a Wi-Fi dongle over USB.

## Supported Wi-Fi Dongles

The following have been tested in an office environment and work on system image 1.1.2 and newer.

| Dongle                          | Chipset            | NDAA '20 Section 848 Compliant | Radio & FCC Reporrt                                          | Link                                                                                                                                                                     |                                                                                                                                                                   |
|---------------------------------|--------------------|--------------------------------|--------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Alfa AWUS036ACS 802.11ac AC600  | Realtek RTL8812AU  | Yes (COO Taiwan)               | Frequency Range: 5180.0-5240.0, 5745.0-5825.0, 2412.0-2462.0 | [Link](https://fcc.report/FCC-ID/2AB878811)                                                                                                                              | [Product](https://www.alfa.com.tw/products/awus036acs) [Amazon](https://www.amazon.com/Network-AWUS036ACS-Wide-Coverage-Dual-Band-High-Sensitivity/dp/B0752CTSGD) |
| Alfa AWUS036EAC 802.11ac AC1200 | Realtek RTL8812AU  | Yes (COO Taiwan)               |                                                              | [Product](https://www.alfa.com.tw/products/awus036eac?variant=36473966035016) [Amazon](https://www.amazon.com/Alfa-Long-Range-AWUS036EAC-Wireless-Adapter/dp/B00WC4C3X4) |                                                                                                                                                                   |
| TP-link TL-WN725N N150          | Realtek RTL8818EUS | No (COO China)                 |                                                              | [Product](https://www.tp-link.com/us/home-networking/adapter/tl-wn725n/) [Link](https://www.amazon.com/wifi-adapter-usb-pc-network/dp/B008IFXQFU/)                       |                                                                                                                                                                   |


## Connect Wifi Dongle

If you are using a VOXL USB expansion and emergency fastboot board, connect the JST to USB adapter onto:
- USB2 Host on M0017
- J3 on M0078

Then, plug your Wifi Dongle into the USB adapter.

First, ADB onto your device if you haven't already done so - this is by connecting the USB-C adapter on the voxl2 onto the computer doing the editing:

```bash
adb shell
```

Then follow the [VOXL Developer Bootcam WiFi Guide](/voxl-2-wifi-guide/).

## Related Video

{% include youtubePlayer.html id=page.youtubeId %}
