---
layout: default
title: VOXL 2 ESC Configs
parent: VOXL 2 User Guides
nav_order: 3
permalink:  /voxl2-esc-configs/
---

# VOXL 2 ESC Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

We are working to expand the capabilities of VOXL 2 every day. The built-in PX4 flight controller enables industry leading SWAP for an autonomous flight controller, but not every interface and flight controller are supported yet. This page provides an overview of available connectivity. If this connectivity is insufficient for your application, VOXL 2 is a world-class companion computer for autonomous navigation and AI when paired with an [external flight controller](/voxl2-external-flight-controller/).

## ESC Options for Built-in Flight Controller

| Interface | Protocol                                                                                                              | Instructions             | Max Number of Channels | Example Hardware         |
|-----------|-----------------------------------------------------------------------------------------------------------------------|--------------------------|------------------------|--------------------------|
| UART      | [Github](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/uart_esc/modalai_esc)                      | Below                    | 4                      | [Modal ESC](/modal-esc/) |
| PWM       | PX4IO                                                                                                                 | [VOXL 2 I/O](/voxl2-io/) | 8                      |                          |
| DSHOT     |                                                                                                                       | Not yet supported        |                        |                          |

## UART ESC Option

### Hardware Setup  

#### Communications

For bi-direction UART communications, connect VOXL 2's `J18` connector to the VOXL ESC's (M0049 or M0117) `J2` using [MCBL-00029](/cable-datasheets/#mcbl-00029) or building your own following this.

<img src="/images/voxl2/m0054-user-guides-esc.png" alt="m0054-user-guides-esc.png" width="1280"/>

M0049/M0117 J2:
- Connector on board : Hirose DF13A-6P-1.25H
- Mating connector : DF13-6S-1.25C

M0054 J18:
- Connector on board : SM04B-GHS-TB(LF)(SN)
- Mating connector : GHR-04V-S

#### VBAT Power and Motors

Use standard installation processes following the [datasheet](https://docs.modalai.com/modal-esc-datasheet/) pinouts.

#### Tuning

Most often, you will want to tune your ESC for your motor/prop setup.  Tools to help facilitate this are [here](https://docs.modalai.com/modal-esc-v2-manual/#voxl-esc-tools).  The outcome of the tuning process are parameters that you flash to the ESC using tools aforementioned.

### Software Setup

#### Checking Status

The `px4-qshell modalai_esc` command can be used to access the driver which has debug info and tools.

The `px4-listener esc_status` can be used to check status of published topics.

####  ModalAI ESC PX4 Driver

The PX4 parameters that are used by the system are [here](https://docs.modalai.com/modal-esc-px4-user-guide/#px4-params)

For example, if you need to change the motor ordering or min/max RPMs, these are params you will want to tweak.

The driver source is [here](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/uart_esc/modalai_esc) for reference.

When PX4 runs on VOXL2, [this is where the driver is started](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/rb5-flight/voxl-px4.config#L124)

#### How to Spin Motors

At this time, the motor test functionality offered via QGroundControl and MAVLink isn't routed into the ESC driver.

To do a spin test, please see [here](https://docs.modalai.com/voxl-px4-developer-guide/#spin-motors)
