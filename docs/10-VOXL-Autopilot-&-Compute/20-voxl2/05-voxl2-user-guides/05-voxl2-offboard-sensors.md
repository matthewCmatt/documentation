---
layout: default
title: VOXL 2 Offboard Sensors
parent: VOXL 2 User Guides
nav_order: 5
permalink:  /voxl2-guides-onboard-offboard-sensors/
---

# VOXL 2 Offboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## External GPS (Mag / GNSS)

### Hardware

Tested hardware:

- Holybro M9N GPS
- Matek PN ???

<img src="/images/voxl2/m0054-user-guides-gps.png" alt="m0054-user-guides-gps.png" width="1280"/>

| Pin # | Signal Name     | Notes                               |
|-------|-----------------|-------------------------------------|
| 1     | VDC_5V_LOCAL    | GNSS/Mag power *                    |
| 2     | GNSS TX 3P3V    | slpi_proc, SSC_QUP6                 |
| 3     | GNSS RX 3P3V    | slpi_proc, SSC_QUP6                 |
| 4     | MAG SCL 3P3V    | slpi_proc, SSC_QUP0                 |
| 5     | MAG SDA 3P3V    | slpi_proc, SSC_QUP0                 |
| 6     | GND             |                                     |
| (others omitted) |  |  | 

### Software

#### Checking Sensor Status

You can check GPS and mag status using `px4-listener sensor_gps` and `px4-listener sensor_mag`

```
px4-listener sensor_gps

TOPIC: sensor_gps
 sensor_gps_s
	timestamp: 291194815  (0.136990 seconds ago)
	time_utc_usec: 0
	lat: 0
	lon: 0
	alt: -17000
	alt_ellipsoid: 0
	s_variance_m_s: 1000.0001
	c_variance_rad: 3.1416
	eph: 4294967.5000
	epv: 3761869.7500
	hdop: 99.9900
	vdop: 99.9900
	noise_per_ms: 103
	jamming_indicator: 6
	vel_m_s: 0.0000
	vel_n_m_s: 0.0000
	vel_e_m_s: 0.0000
	vel_d_m_s: 0.0000
	cog_rad: 0.0000
	timestamp_time_relative: 0
	heading: nan
	heading_offset: 0.0000
	fix_type: 0
	jamming_state: 0
	vel_ned_valid: False
	satellites_used: 0
```

```
px4-listener sensor_mag

TOPIC: sensor_mag
 sensor_mag_s
	timestamp: 331057756  (0.032405 seconds ago)
	timestamp_sample: 331057149  (607 us before timestamp)
	device_id: 396809 (Type: 0x06, I2C:1 (0x0E))
	x: 0.0765
	y: 0.1121
	z: 0.3333
	temperature: nan
	error_count: 0
	is_external: True
```

#### PX4 Driver and Setup

See GPS driver [here](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/gps).

The default driver is Holybro and is set [here](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/rb5-flight/voxl-px4#L13)

For Matek, see [here](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/rb5-flight/voxl-px4#L22)