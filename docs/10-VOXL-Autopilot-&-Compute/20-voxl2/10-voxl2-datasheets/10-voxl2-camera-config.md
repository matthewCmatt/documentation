---
layout: default
title: VOXL 2 Camera Configs
parent: VOXL 2 Datasheets
nav_order: 10
permalink:  /voxl2-camera-configs/
youtubeiId2: J7iYorzm5rs
---

# VOXL 2 Camera Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL 2 has 3 camera groups (shown below), where each group has:

- QTY-2 full 4 lane MIPI CSI ports
- CCI and camera control signals
- 8 power rails (from 1.05V up to 5V) for cameras and other sensors
- Dedicated I/O bus (one of UART, SPI, i2c hard-coded depending on group)

<img src="/images/voxl2/m0054-iamge-sensor-groups-v2.png" alt="m0054-image-sensors-groups">

## Available Configurations

The camera configurations are purpose built via a combination of hardware and software to support more flexible options. Each configuration needs to be developed as its own effort, and then supported as a part of the [system image](/voxl2-system-image/) and [voxl-camera-server](/voxl-camera-server/). The software needs to be configured using [voxl-configure-cameras](/voxl-camera-server/), and the image sensors must be connected to the port that matches the configurations documented on this page.

The configurations are not mix and match. One may have luck with different combinations, but that does not mean it will work in the future.

### SDK 1.0

- SDK 1.0.0
- System Image 1.6.2

Please see a new [voxl2-image-sensors](/voxl2-image-sensors/) page if you are on SDK 1.0+ and looking for additional setup information.

#### 1.6.2 Supported Sensor Hardware IDs

| HW Sensor ID | Sensor Type              | Description | Interposer    |
|:-------------|:-------------------------|:------------|:--------------|
| 0            | OV7251 [M0015](/M0015/)  | Stereo      | M0010 + M0076 |
| 0            | OV9782 [M0113](/M0113/)  | Stereo      | M0010 + M0076 |
| 0            | PMD TOF [M0040](/M0040/) | ToF         | M0040 + M0076 |

| HW Sensor ID | Sensor Type              | Description | Interposer    |
|:-------------|:-------------------------|:------------|:--------------|
| 1            | OV7251 [M0015](/M0015/)  | Stereo      | M0010 + M0076 |
| 1            | OV9782 [M0113](/M0113/)  | Stereo      | M0010 + M0076 |
| 1            | PMD TOF [M0040](/M0040/) | ToF         | M0040 + M0076 |

| HW Sensor ID | Sensor Type                   | Description | Interposer      |
|:-------------|:------------------------------|:------------|:----------------|
| 2            | OV7251 [M0014](/M0014/)       | Tracking    | M0076, M0084 JL |
| 2            | ov9782                        | Tracking    | M0076, M0084 JL |
| 2            | PMD TOF [M0040](/M0040/)      | Depth       | M0076, M0084 JL |
| 2            | IMX678 [M0061-2](/M0061/)     | Hires       | M0076, M0084 JL |
| 2            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0076, M0084 JL |
| 2            | IMX214 [M0025-2](/M0025/)     | Hires       | M0076, M0084 JL |

| HW Sensor ID | Sensor Type                   | Description | Interposer |
|:-------------|:------------------------------|:------------|:-----------|
| 3            | IMX678 [M0061-2](/M0061/)     | Hires       | M0084 JU   |
| 3            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0084 JU   |
| 3            | IMX214 [M0025-2](/M0025/)     | Hires       | M0084 JU   |
| 3            | PMD TOF [M0040](/M0040/)      | ToF         | M0084 JU   |

| HW Sensor ID | Sensor Type                   | Description | Interposer      |
|:-------------|:------------------------------|:------------|:----------------|
| 4            | OV7251 [M0015](/M0015/)       | Stereo      | M0010 + M0076   |
| 4            | OV9782 [M0113](/M0113/)       | Stereo      | M0010 + M0076   |
| 4            | PMD TOF [M0040](/M0040/)      | ToF         | M0040 + M0076   |
| 4            | IMX678 [M0061-2](/M0061/)     | Hires       | M0076, M0084 JL |
| 4            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0076, M0084 JL |
| 4            | IMX214 [M0025-2](/M0025/)     | Hires       | M0076, M0084 JL |

| HW Sensor ID | Sensor Type               | Description              | Interposer      |
|:-------------|:--------------------------|:-------------------------|:----------------|
| 5            | OV7251 [M0015](/M0015/)   | Stereo                   | M0010 + M0076   |
| 5            | OV9782 [M0113](/M0113/)   | Stereo                   | M0010 + M0076   |
| 5            | PMD TOF [M0040](/M0040/)  | ToF                      | NA              |
| 5            | IMX678 [M0061-2](/M0061/) | Hires (alt address 0x20) | M0076, M0084 JL |

#### 1.6.2 Supported Sensor Settings

| Sensor                        | Sensor Type         | Resolution @ FPS                                                                                        |
|:-------------------------------|:---------------------|:---------------------------------------------------------------------------------------------------------|
| IMX678 [M0061-2](/M0061/)     | Hires               | 3840x2160 @ 30 <br> 1920x1080 @ 30                                                                      |
| IMX412/577 [M0061-1](/M0061/) | Hires               | 1920x1080 @ 30                                                                                          |
| IMX214 [M0025-2](/M0025/)     | Hires               | 4096 x 2160 @ 30 <br> 3840x2160 @ 30 <br> 2048x1536 @ 30 <br> 1920 x 1080 @ 30 <br> 1280 x 720 @ 30 <br> 1024 x 768 @ 30 <br> 640 x 480 @ 30 |
| PMD TOF [M0040](/M0040/)      | ToF                 | 224 x 1557 @ 5 <br> 224x172 @ 5                                                                                    |
| OV7251 [M0014](/M0014/)       | Tracking Config     | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Left  | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Right | 640x480 @ 30                                                                                            |
| ov9782                        | Tracking Config     | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Left  | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Right | 1280x800 @ 30                                                                                           |


### Platform Release 0.9.5

- SDK 0.9.5
- System Image 1.5.5

#### Changes

- Added IMX678 support 1920x1080@30, 3840x2160@30
- Enabled more sensor options (see table below)

#### 1.5.5 Supported Sensor Hardware IDs

| HW Sensor ID | Sensor Type              | Description | Interposer    |
|:-------------|:-------------------------|:------------|:--------------|
| 0            | OV7251 [M0015](/M0015/)  | Stereo      | M0010 + M0076 |
| 0            | OV9782 [M0113](/M0113/)  | Stereo      | M0010 + M0076 |
| 0            | PMD TOF [M0040](/M0040/) | ToF         | M0040 + M0076 |

| HW Sensor ID | Sensor Type              | Description | Interposer    |
|:-------------|:-------------------------|:------------|:--------------|
| 1            | OV7251 [M0015](/M0015/)  | Stereo      | M0010 + M0076 |
| 1            | OV9782 [M0113](/M0113/)  | Stereo      | M0010 + M0076 |
| 1            | PMD TOF [M0040](/M0040/) | ToF         | NA            |

| HW Sensor ID | Sensor Type                   | Description | Interposer      |
|:-------------|:------------------------------|:------------|:--------------|
| 2            | OV7251 [M0014](/M0014/)       | Tracking    | M0076, M0084 JL |
| 2            | ov9782                        | Tracking    | M0076, M0084 JL |
| 2            | PMD TOF [M0040](/M0040/)      | Depth       | M0076, M0084 JL |
| 2            | IMX678 [M0061-2](/M0061/)     | Hires       | M0076, M0084 JL |
| 2            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0076, M0084 JL |
| 2            | IMX214 [M0025-2](/M0025/)     | Hires       | M0076, M0084 JL |

| HW Sensor ID | Sensor Type                   | Description | Interposer     |
|:-------------|:------------------------------|:------------|:--------------|
| 3            | IMX678 [M0061-2](/M0061/)     | Hires       | M0084 JU |
| 3            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0084 JU |
| 3            | IMX214 [M0025-2](/M0025/)     | Hires       | M0084 JU |
| 3            | PMD TOF [M0040](/M0040/)      | ToF         | M0084 JU |

| HW Sensor ID | Sensor Type                   | Description | Interposer      |
|:-------------|:------------------------------|:------------|:----------------|
| 4            | OV7251 [M0015](/M0015/)       | Stereo      | M0010 + M0076   |
| 4            | OV9782 [M0113](/M0113/)       | Stereo      | M0010 + M0076   |
| 4            | PMD TOF [M0040](/M0040/)      | ToF         | M0040 + M0076   |
| 4            | IMX678 [M0061-2](/M0061/)     | Hires       | M0076, M0084 JL |
| 4            | IMX577/412 [M0061-1](/M0061/) | Hires       | M0076, M0084 JL |
| 4            | IMX214 [M0025-2](/M0025/)     | Hires       | M0076, M0084 JL |

| HW Sensor ID | Sensor Type               | Description              | Interposer      |
|:-------------|:--------------------------|:-------------------------|:----------------|
| 5            | OV7251 [M0015](/M0015/)   | Stereo                   | M0010 + M0076   |
| 5            | OV9782 [M0113](/M0113/)   | Stereo                   | M0010 + M0076   |
| 5            | PMD TOF [M0040](/M0040/)  | ToF                      | NA              |
| 5            | IMX678 [M0061-2](/M0061/) | Hires (alt address 0x20) | M0076, M0084 JL |


#### 1.5.5 Supported Sensor Settings

Some sensors may list additional capabilities when probed from software.  Below are what we've tested against, other settings may have unexpected results.

| Sensor                        | Sensor Type         | Resolution @ FPS                                                                                        |
|:-------------------------------|:---------------------|:---------------------------------------------------------------------------------------------------------|
| IMX678 [M0061-2](/M0061/)     | Hires               | 3840x2160 @ 30 <br> 1920x1080 @ 30                                                                      |
| IMX412/577 [M0061-1](/M0061/) | Hires               | 1920x1080 @ 30                                                                                          |
| IMX214 [M0025-2](/M0025/)     | Hires               | 4096 x 2160 @ 30 <br> 3840x2160 @ 30 <br> 2048x1536 @ 30 <br> 1920 x 1080 @ 30 <br> 1280 x 720 @ 30 <br> 1024 x 768 @ 30 <br> 640 x 480 @ 30 |
| PMD TOF [M0040](/M0040/)      | ToF                 | 224 x 1557 @ 5                                                                                     |
| OV7251 [M0014](/M0014/)       | Tracking Config     | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Left  | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Right | 640x480 @ 30                                                                                            |
| ov9782                        | Tracking Config     | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Left  | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Right | 1280x800 @ 30                                                                                           |


### Platform Release 0.9

- SDK 0.9
- System Image 1.4.1

#### 1.4.1 Supported Sensor Hardware IDs

| HW Sensor ID | Sensor Type             | Description |
|:--------------|:-------------------------|:-------------|
| 0            | OV7251 [M0015](/M0015/) | Stereo      |
| 0            | OV9782 [M0113](/M0113/) | Stereo      |
| 0            | PMD TOF [M0040](/M0040/)| ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 1            | OV7251 [M0015](/M0015/)  | Stereo      |
| 1            | OV9782 [M0113](/M0113/)  | Stereo      |
| 1            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 2            | OV7251 [M0014](/M0014/)  | Tracking      |
| 2            | ov9782                   | Stereo      |
| 2            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type                   | Description |
|:--------------|:-------------------------------|:-------------|
| 3            | IMX214 [M0025-2](/M0025/)     | Hires      |
| 3            | IMX577/412 [M0061-1](/M0061/) | Hires      |
| 3            | PMD TOF [M0040](/M0040/)      | ToF     |


| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 4            | OV7251 [M0015](/M0015/)  | CV,BW,      |
| 4            | OV9782 [M0113](/M0113/)  | Stereo      |
| 4            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 5            | OV7251 [M0015](/M0015/)  | Stereo      |
| 5            | OV9782 [M0113](/M0113/)  | Stereo      |
| 5            | PMD TOF [M0040](/M0040/) | ToF     |

#### 1.4.1 Supported Sensor Settings

| Sensor                        | Sensor Type         | Resolution @ FPS                                                                                        |
|:-------------------------------|:---------------------|:---------------------------------------------------------------------------------------------------------|
| IMX214 [M0025-2](/M0025/)     | Hires               | 4096 x 2160 @ 30 <br> 3840x2160 @ 30 <br> 2048x1536 @ 30 <br> 1920 x 1080 @ 30 <br> 1280 x 720 @ 30 <br> 1024 x 768 @ 30 <br> 640 x 480 @ 30 |
| IMX412/577 [M0061-1](/M0061/) | Hires Config        | 1920x1080 @ 30                                                                                          |
| PMD TOF [M0040](/M0040/)      | ToF                 | 224 x 1557 @ 5                                                                                          |
| OV7251 [M0014](/M0014/)       | Tracking Config     | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Left  | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Right | 640x480 @ 30                                                                                            |
| ov9782                        | Tracking Config     | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Left  | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Right | 1280x800 @ 30                                                                                           |

## Quickstart Video

The following video demonstrates setting up the image sensors for the "C11" VOXL 2 development kit:

{% include youtubePlayer.html id=page.youtubeiId2 %}

## Image Sensor Configurations - SDK 1.0

Please see a new [voxl2-image-sensors](/voxl2-image-sensors/) page if you are on SDK 1.0+ and looking for additional setup information.

### C-01: Front Stereo and Tracking

<img src="/images/voxl2/M0054-tracking-stereo.jpg" alt="M0054-tracking-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/)            | Fsync in0    | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/)            | Fsync out0   | Front stereo |
| 2 (J7L)    | OV7251 [M0014](/M0014/)            |              | Tracking     |


### C-02: Tracking Only

<img src="/images/voxl2/M0054-tracking.jpg" alt="M0054-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 2 (J7L)    | OV7251 [M0014](/M0014/)|           | Tracking     |

### C-03: Front Stereo, Hires, and Tracking

<img src="/images/voxl2/M0054-imx214-tracking-stereo.jpg" alt="M0054-imx214-tracking-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/)|Fsync in0  | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/)|Fsync out0 | Front stereo |
| 2 (J7L)    | OV7251 [M0014](/M0014/)|           | Tracking     |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C-04: Hires and Tracking

<img src="/images/voxl2/M0054-imx214-tracking.jpg" alt="M0054-imx214-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 2 (J7L)    | OV7251 [M0014](/M0014/) |          | Tracking     |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C-05: ToF and Tracking

<img src="/images/voxl2/M0054-pmdtof-tracking.jpg" alt="M0054-pmdtof-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/) |         | ToF          |
| 2 (J7L)    | OV7251 [M0014](/M0014/) |          | Tracking     |

### C-06: ToF, Hires, and Tracking (D0005 Starling)

<img src="/images/voxl2/M0054-pmdtof-imx214-tracking.jpg" alt="M0054-pmdtof-imx214-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/) |         | ToF          |
| 2 (J7L)    | imx214, imx412      |              | Hires        |
| 3 (J7U)    | OV7251 [M0014](/M0014/) |          | Tracking     |

### C-08: Hires Only

<img src="/images/voxl2/M0054-imx214.jpg" alt="M0054-imx214" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C-09: ToF Only

<img src="/images/voxl2/M0054-pmdtof.jpg" alt="M0054-pmdtof" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/)|          | ToF          |

### C-10: Front Stereo Only (ov7251)

<img src="/images/voxl2/M0054-stereo.jpg" alt="M0054-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/) |Fsync in0 | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/) |Fsync out0| Front stereo |

### C-11: Front Stereo, Rear Stereo, Hires, and Tracking (D0006 Sentinel)

<img src="/images/voxl2/M0054-D0006-sentinel.jpg" alt="M0054-D0006-sentinel" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/) |Fsync in0 | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/) |Fsync out0| Front stereo |
| 2 (J7L)    | OV7251 [M0014](/M0014/) |          | Tracking     |
| 3 (J7U)    | imx214, imx412      |              | Hires        |
| 4 (J8L)    | OV7251 [M0015](/M0015/) |Fsync in1 | Rear Stereo  |
| 5 (J8L)    | OV7251 [M0015](/M0015/) |Fsync out1| Rear Stereo  |


### C-12: Front Stereo Only (ov9782)

<img src="/images/voxl2/M0054-ov9782-stereo.jpg" alt="M0054-ov9782-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV9782, M0113       | Fsync in0    | Front stereo |
| 1 (J6L)    | OV9782, M0113       | Fsync out0   | Front stereo |

### C-13: Front Stereo (ov9782) and Hires

<img src="/images/voxl2/M0054-imx214-ov9782-stereo.jpg" alt="M0054-imx412-ov9782-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV9782, M0113       | Fsync in0    | Front stereo |
| 1 (J6L)    | OV9782, M0113       | Fsync out0   | Front stereo |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C-14: ToF, Hires, and Tracking

<img src="/images/voxl2/M0054-pmdtof-imx214-tracking.jpg" alt="M0054-pmdtof-imx214-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/)|          | ToF          |
| 2 (J7L)    | imx214, imx412      |              | Hires        |
| 3 (J7U)    | OV7251 [M0014](/M0014/) |          | Tracking     |

### C-15: Stereo flip (ov9782)

<img src="/images/voxl2/M0054-ov9782-stereo.jpg" alt="M0054-ov9782-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV9782, M0113       |              | Stereo       |
| 1 (J6L)    | OV9782, M0113       |              | Stereo       |

### C-16: ToF, Hires, and 2 Trackings

<img src="/images/voxl2/M0054-imx214-tracking-pmdtof.jpg" alt="M0054-imx214-tracking-pmdtof" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/)|          | ToF          |
| 2 (J7L)    | imx214, imx412      |              | Hires        |
| 3 (J7U)    | OV7251 [M0014](/M0014/) |          | Tracking     |
| 3 (J8L)    | OV7251 [M0014](/M0014/) |          | Tracking 2   |

### C-17: ToF, Tracking, Stereo(0v9782), and Hires(imx412)

<img src="/images/voxl2/M0054-pmdtof-imx412-tracking-tracking2.jpg" alt="M0054-pmdtof-imx412,tracking-tracking2" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD ToF [M0040](/M0040/)|          | ToF          |
| 2 (J7L)    | imx412 [M0061](/M0061/) |          | Hires        |
| 3 (J7U)    | OV7251 [M0014](/M0014/) |          | Tracking     |
| 4 (J8L)    | OV9782, M0113       |              | Stereo       |
| 5 (J8L)    | OV9782, M0113       |              | Stereo       |

## Image Sensor Configurations - SDK 0.9.5

### C3 - Front Stereo, Hires, and Tracking

<img src="/images/voxl2/m0054-front-stereo-hires-tracking.jpg" alt="m0054-front-stereo-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/) |Fsync in0 | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/) |Fsync out0| Front stereo |
| 2 (J7L)    | ov7251              |              | Tracking     |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C4 - Tracking and Hires Only

<img src="/images/voxl2/m0054-hires-tracking.jpg" alt="m0054-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 2 (J7L)    | ov7251              |              | Tracking     |
| 3 (J7U)    | imx214, imx412      |              | Hires        |

### C6 - Hires + ToF + Tracking

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

<img src="/images/voxl2/M0054-pmdtof-imx214-tracking.jpg" alt="M0054-pmdtof-imx214-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | PMD TOF [M0040](/M0040/) |              | Depth 0      |
| 2 (J7L)    | imx214, imx412      |              | Hires        |
| 3 (J7U)    | ov7251              |              | Tracking     |


### C10 - Front Stereo Only

<img src="/images/voxl2/m0054-front-stereo.jpg" alt="m0054-front-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/) |Fsync in0 | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/) |Fsync out0| Front stereo |

### C11 - Front Stereo, Rear Stereo, Hires, and Tracking

<img src="/images/voxl2/m0054-dual-stereo-hires-tracking.jpg" alt="m0054-dual-stereo-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0 (J6L)    | OV7251 [M0015](/M0015/)             | Fsync in0    | Front stereo |
| 1 (J6L)    | OV7251 [M0015](/M0015/)             | Fsync out0   | Front stereo |
| 2 (J7L)    | ov7251              |               | Tracking     |
| 3 (J7U)    | imx214, imx412      |               | Hires        |
| 4 (J8L)    | OV7251 [M0015](/M0015/)             | Fsync in1    | Rear stereo  |
| 5 (J8L)    | OV7251 [M0015](/M0015/)             | Fsync out1   | Rear stereo  |

### C8 - Hires Only

Available starting with [Platform Release 0.9.5, sys img 1.5.3](/voxl2-system-image/#changelog).

[M0061-2](/M0061/) based IMX678, on M0054 J8 (no other camera location for IMX678 is supported in 1.5.3, must be J8).

Shown here, the [M0076](/M0076/) interposer plugs into M0054 J8.  The [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module.

<img src="/images/voxl2/m0054-imx678-m0061-2.JPG" alt="m0054-imx678-m0061-2" width="1280"/>

[M0025-2](/M0025/) based IMX214, on M0054 J8.

<img src="/images/voxl2/m0054-imx214-m0025-2.JPG" alt="m0054-imx214-m0025-2" width="1280"/>


**SW Setup**

```bash
voxl2:/$ voxl-configure-cameras 8
```

### C9 - Time of Flight (ToF) Only

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

<img src="/images/voxl2/m0054-single-tof-m0076.jpg" alt="m0054-single-tof-m0076" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | PMD TOF [M0040](/M0040/)             |              | Depth 0      |

**SW Setup**

```bash
voxl2:/$ voxl-configure-cameras 9
```

### CX - Two Time of Flights (ToF) 

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

Using the [M0076-1](https://www.modalai.com/products/mdk-m0076-1) interposers on `J6` and `J8` as examples:

<img src="/images/voxl2/m0054-dual-tof-m0076.jpg" alt="m0054-dual-tof-m0076" width="1280"/>

Using the [M0084-1](https://www.modalai.com/products/m0084-dual-camera-adapter) dual camera adapter ("y-flex") on `J8` as example:

<img src="/images/voxl2/m0054-dual-tof-m0084.jpg" alt="m0054-dual-tof-m0084" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | PMD TOF [M0040](/M0040/)             |              | Depth 0      |
| 1          | PMD TOF [M0040](/M0040/)             |              | Depth 1      |

**SW Setup**

NA

## Current Limitations

- As a means to allow coexistence of OV7251 sensors on `CCI3`, we are not resetting sensors when they normally should per Qualcomm, to prevent the sensors losing a runtime address swap.  This modification is in the kernel, dmesg shows `MODALAI HACK` where it's happening.
- Sensor ID 1 and Sensor ID 5 share a reset line (gpio100), no specific known defects but an area to watch out.
- Sensor indicies assume all six sensors are in place and will shift when not fully populated.
- All camera AVDD rails in this configuration are left on always (2P8VDC) as they're shared

<img src="/images/voxl2/m0054-camera-config-v0.png" alt="m0054-camera" width="1280"/>
