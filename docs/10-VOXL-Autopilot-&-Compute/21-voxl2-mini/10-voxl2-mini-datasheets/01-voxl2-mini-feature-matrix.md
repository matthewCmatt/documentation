---
layout: default
title: VOXL 2 Mini Feature Matrix
parent: VOXL 2 Mini Datasheets
nav_order: 1
permalink:  /voxl2-mini-feature-matrix/
---

# VOXL 2 Mini Feature Matrix
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

## Specifications

| Feature                                                                       | VOXL 2 Mini                                                      | VOXL 2                                                           |
|-------------------------------------------------------------------------------|------------------------------------------------------------------|------------------------------------------------------------------|
| Dimensions                                                                    | 42mm x 42mm                                                      | 70mm x 36mm                                                      |
| Weight                                                                        | 11g                                                              | 16g                                                              |
| MIPI Image Sensors                                                            | [4 Concurrent](/voxl2-mini-image-sensors/)                       | [6 Concurrent](/voxl2-camera-configs/)                           |
| CPU                                                                           | QRB5165 <br>8 cores up to 3.091GHz <br>8GB LPDDR5<br>128GB Flash | QRB5165 <br>8 cores up to 3.091GHz <br>8GB LPDDR5<br>128GB Flash |
| OS                                                                            | Ubuntu 18.04 - Linux Kernel v4.19                                | Ubuntu 18.04 - Linux Kernel v4.19                                |
| GPU                                                                           | Adreno 650 GPU – 1024 ALU                                        | Adreno 650 GPU – 1024 ALU                                        |
| NPU                                                                           | 15 TOPS                                                          | 15 TOPS                                                          |
| Flight Controller Embedded                                                    | Yes (Sensors DSP)                                                | Yes (Sensors DSP)                                                |
| Built in WiFi                                                                 | No                                                               | No                                                               |
| Add-on Connectivity                                                           | via USB3 port, WiFi, 5G, 4G/LTE, Microhard                       | Via Add-on board, WiFi, 5G, 4G/LTE, Microhard                    |
| Video Encoding                                                                | 8K30 h.264/h.265 108MP still images                              | 8K30 h.264/h.265 108MP still images                              |
| VOXL SDK: GPS-denied navigation, SLAM, obstacle avoidance, object recognition | Yes                                                              | Yes                                                              |
| ROS                                                                           | ROS 1 & 2                                                        | ROS 1 & 2                                                        |
| QGroundControl                                                                | Yes                                                              | Yes                                                              |
| ATAK                                                                          | Yes                                                              | Yes                                                              |
| NDAA ’20 Section 848 Compliant                                                | Yes, Assembled in USA                                            | Yes, Assembled in USA                                            |
| PMD TOF                                                                       | Yes                                                              | Yes                                                              |
| FLIR Boson                                                                    | USB                                                              | USB                                                              |
| FLIR Lepton                                                                   | USB, SPI in development                                          | USB, SPI in development                                          |

## Platform Release

VOXL 2 Mini requires Platform Release 1.0 and newer.  See [Platform Releases](/platform-release/).

For additional details regarding compatibility see the following resources:
* [VOXL 2 RC Configs](/voxl2-mini-rc-configs/)
* [VOXL 2 ESC Configs](/voxl2-mini-esc-configs/)

