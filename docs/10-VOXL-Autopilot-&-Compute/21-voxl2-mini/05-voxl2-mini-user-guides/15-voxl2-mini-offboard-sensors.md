---
layout: default
title: VOXL 2 Mini Offboard Sensors
parent: VOXL 2 Mini User Guides
nav_order: 15
permalink:  /voxl2-mini-offboard-sensors/
---

# VOXL 2 Mini Offboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---
## GPS (GNSS/Mag)

An external GPS (GNSS/Mag) can be connected to VOXL 2's J19 connector.

<img src="/images/voxl2-mini/m0104-user-guides-external-sensors-gps.jpg" alt="m0104-user-guides-external-sensors-gps.jpg" width="1280"/>


| Name / Designator            | Description         | Interface                      |
|---                           |---                  |---                             |
| GNSS UART / exposed via J19  | PX4 GNSS            | SSC_QUP6, 2W UART, SLPI (sDSP) | 
| Mag I2C / exposed via J19    | PX4 Mag             | SSC_QUP0, I2C, SLPI (sDSP)     |

**J19:** Full pinouts for all the connects are [available here](/voxl2-connectors/).

| Pin # | Signal Name     | Notes                               |
|-------|-----------------|-------------------------------------|
| 1     | VDC_5V_LOCAL    | GNSS/Mag power *                    |
| 2     | GNSS TX 3P3V    | slpi_proc, SSC_QUP7                 |
| 3     | GNSS RX 3P3V    | slpi_proc, SSC_QUP7                 |
| 4     | MAG SCL 3P3V    | slpi_proc, SSC_QUP1                 |
| 5     | MAG SDA 3P3V    | slpi_proc, SSC_QUP1                 |
| 6     | GND             |                                     |
| 7     |    | Not used for GNSS/Mag |
| 8     |    | Not used for GNSS/Mag |
| 9     |    | Not used for GNSS/Mag |
| 10    |    | Not used for GNSS/Mag |
| 11    |    | Not used for GNSS/Mag |
| 12    |    | Not used for GNSS/Mag |

## Power Monitoring

### VOXL Mini ESC

The VOXL ESC Mini provides power monitoring through the [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) driver.

The same UART interface used to control the ESC provides feedback including the battery voltage being regulated by the VOXL Mini ESC.

<img src="/images/voxl2-mini/m0104-m0129.jpg" alt="m0104-m0129" width="1280"/>

### VOXL Power Module 

<img src="/images/voxl2-mini/m0104-user-guides-external-sensors-pm.jpg" alt="m0104-user-guides-external-sensors-pm" width="1280"/>

Using the VOXL Power Module provides the ability to monitor battery voltage and regulated 3.8VDC voltage from the power module.

| Name / Designator           | Description    | Interface                                | Driver |
|---                          |---             |---                                       |---     |
| PX4 Power Monitoring (J1)   | QTY2 INA231    | SSC_QUP2, I2C, SLPI (sDSP), Addrs: 0x44h and 0x45h | [voxlpm](https://dev.px4.io/master/en/middleware/modules_driver.html#voxlpm) |


**J1:** Full pinouts for all the connects are [available here](/voxl2-mini-connectors/).

| Pin# | Signal   | Notes/Usage                                            |
|------|----------|--------------------------------------------------------|
| 1    | VDCIN_3P8V | DC from Power Module, “unprotected”                  |
| 2    | GND      | Power Module Return                                    |
| 3    | I2C_CLK  | SSC_QUP_1, 3.8V signal levels, Pullups on Power Module |
| 4    | I2C_SDA  | SSC_QUP_1, 3.8V signal levels, Pullups on Power Module |

Full pinouts for all the connects are [available here](/voxl2-mini-connectors/).

## External Interfaces

The following interfaces are available from the Applications Processor and Sensors DSP.

<img src="/images/voxl2-mini/m0104-offboard-sensors-linux.jpg" alt="m0014-offboard-sensors-linux"/>

| Name / Designator         | Description         | Interface        |
|---                        |---                  |---               |
| SPI0 / exposed via J6     | camera group 0 SPI  | /dev/spidev0.0   | 
| SPI14 / exposed via J10   | external SPI        | /dev/spidev14.0  |

Full pinouts for all the connects are [available here](/voxl2-mini-connectors/).
