---
layout: default
title: VOXL 2 Mini Shell Access
parent: VOXL 2 Mini User Guides
nav_order: 5
permalink: /voxl2-mini-shell-access/
---

# VOXL 2 Mini Shell Access
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

There are two primary ways to access the VOXL 2 Mini shell:

|--- |--- |
| J9 | using the USBC connection, you can connect to a host computer running Android Debug Bridge |
| J3 | using the USB3 interface, you can connect ethernet/wireless adapters and connect via SSH |

<img src="/images/voxl2-mini/m0104-user-guides-usb.jpg" alt="m0104-user-guides-usb" width="1280"/>

## ADB

See [setup ADB page](/setup-adb/) if you have not setup ADB on your host PC.

We recommend using a USBC to USBA flexible type cable to prevent too much torque on the J9 connector.

<img src="/images/voxl2-mini/m0104-j9-usbc-to-usba.jpg" alt="m0104-j9-usbc-to-usba" width="1280"/>

Here is an example screen shot that shows:
- running the `adb devices` command to list connected VOXL 2 Mini device to the host PC
- running the `adb shell` command to open a terminal running on VOXL 2 Mini

<img src="/images/voxl2-mini/m0104-adb-terminal.jpg" alt="m0104-adb-terminal" width="640"/>

## SSH

To setup the hardware, see the [VOXL 2 Mini USB Connection](/voxl2-mini-usb-connections/) page.

First you need to use the ADB method to get to a shell ;)

If using wireless, run the `voxl-wifi` utility to setup either in station mode or SoftAP mode. 

If using wired ethernet, set up a static IP address.

Once this is done, you can disconnect the adb connection and add the network connection.