---
layout: default
title: VOXL 2 Mini Power
parent: VOXL 2 Mini User Guides
nav_order: 1
permalink: /voxl2-mini-power/
---

# VOXL 2 Mini Power
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware Options

### VOXL Power Module

The main power input is `J1` and can be paired with a [VOXL Power Module](/power-module-v3-datasheet/), which provides the up to 6A momentary inrush current and provides two I2C based power monitoring ICs, providing PX4 the battery status.

Note, for VOXL 2 Mini, the `MDK-M0041-4` (note the `-4`) version is required, that has a 3.8VDC output instead of 5VDC.

<img src="/images/voxl2-mini/m0104-user-guides-power.jpg" alt="m0104-user-guides-power.jpg" width="1280"/>

M0104 J1:
- Connector on board: Molex 2059-72-0041
- Mating connector: 2059792041

<img src="/images/voxl2-mini/m0104-j1-m0041.jpg" alt="m0104-j1-m0041" width="1280"/>

### VOXL Mini ESC - 4-in-1

The main power input is `J1` and the 3.8V and GND pins can be paried with the output of the VOXL Mini ESC's regulated outpout.

The battery status information is sent over UART to the [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) driver through J19 instead of the I2C bus on J1.

For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL Mini ESC's (M0129) `J1` using [MCBL-TODO](/cable-datasheets/#mcbl-TODO) or building your own following this.

<img src="/images/voxl2-mini/m0104-m0129.jpg" alt="m0104-m0129" width="1280"/>

## LEDs

The `D1` LED illuminates green when the 3.3V regulator is reporting "good".

The `D2` LED illuminates green when the 5.0V USB bus regulator is reporting "good".

More info about the other LEDs can be located [here](/voxl2-mini-leds/)

## Software

### Checking Battery Status

With the VOXL PM attached, you check the battery using `px4-listener battery_status` command after getting a shell to VOXL 2.

Here's an example from a 2S battery:

```
px4-listener battery_status

TOPIC: battery_status
 battery_status_s
	timestamp: 1080175258  (0.059704 seconds ago)
	voltage_v: 7.2224
	voltage_filtered_v: 7.2224
	current_a: 0.1758
	current_filtered_a: 0.1791
	average_current_a: 0.0000
	discharged_mah: 51.6742
	remaining: 1.0000
	scale: 1.0000
	temperature: nan
	cell_count: 2
	voltage_cell_v: [3.6662, 3.6662, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]
	max_cell_voltage_delta: 0.0000
	capacity: 0
	cycle_count: 0
	run_time_to_empty: 0
	average_time_to_empty: 0
	serial_number: 0
	manufacture_date: 0
	state_of_health: 0
	max_error: 0
	interface_error: 0
	connected: True
	source: 0
	priority: 0
	id: 1
	is_powering_off: False
	warning: 0
```

### voxlpm PX4 Driver

The [voxlpm](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/power_monitor/voxlpm) PX4 driver monitors both battery and companion computer regulated voltage.

The driver runs in PX4 on the DSP, and interfaces through SSC QUP2.

You can't get to this I2C bus through Ubuntu at this time.

### modal_io PX4 Driver

The [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) PX4 driver monitors both battery via the VOXL Mini ESC.

The driver runs in PX4 on the DSP, and interfaces through SSC QUP2.
