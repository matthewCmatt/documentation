---
layout: default
title: VOXL 2 Mini ESC Configs
parent: VOXL 2 Mini User Guides
nav_order: 11
permalink: /voxl2-mini-esc-configs/
---

# VOXL 2 Mini ESC Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

We are working to expand the capabilities of VOXL 2 every day. The built-in PX4 flight controller enables industry leading SWAP for an autonomous flight controller, but not every interface and flight controller are supported yet. This page provides an overview of available connectivity. If this connectivity is insufficient for your application, VOXL 2 is a world-class companion computer for autonomous navigation and AI when paired with an [external flight controller](/voxl2-external-flight-controller/).

## ESC Options for Built-in Flight Controller

| Protocol  | Driver                                                                                    | Instructions             | Max Number of Channels | Example Hardware           |
|-----------|-----------------------------------------------------------------------------------------  |--------------------------|------------------------|--------------------------  |
| UART      | [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) | Below                    | 4                      | [VOXL ESC](/modal-esc/) |
| UART      | [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) | Below                    | 4                      | VOXL Mini ESC             |
| PWM       | modal_io based solution coming soon                                                       | Not yet supported        | 8                      |                            |
| DSHOT     | modal_io based solution coming soon                                                       | Not yet supported        | 8                      |                            |

## VOXL ESC - 4-in-1

### Hardware Setup  

<<<<<<< HEAD
For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL ESC's (M0049 or M0117) `J2` using [MCBL-TODO](/cable-datasheets/#mcbl-TODO) or building your own following this.
=======
For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL ESC's (M0049 or M0117) `J2` using [MCBL-00081](https://docs.modalai.com/cable-datasheets/#mcbl-00081) or building your own following this.
>>>>>>> documentation/master

<img src="/images/voxl2-mini/m0104-user-guides-esc.jpg" alt="m0104-user-guides-esc.jpg" width="1280"/>

M0049/M0117 J2:
- Connector on board : Hirose DF13A-6P-1.25H
- Mating connector : DF13-6S-1.25C

M0104 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

#### VBAT Power and Motors

Use standard installation processes following the [datasheet](https://docs.modalai.com/modal-esc-datasheet/) pinouts.

### Software Setup

#### Checking Status

The `px4-qshell modal_io status` command can be used to access the driver which has debug info and tools.

The `px4-listener esc_status` can be used to check status of published topics.

####  PX4 Driver

The PX4 parameters that are used by the system are [here](https://docs.modalai.com/modal-esc-px4-user-guide/#px4-params)

For example, if you need to change the motor ordering or min/max RPMs, these are params you will want to tweak.

The driver source is [is here](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) for reference.

When PX4 runs on VOXL2, [this is where the driver is started](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/voxl2/target/voxl-px4-start#L94)

#### How to Spin Motors

- Use the `actuator_test` command, for example `actuator_test set -m 1 -v 0.05 -t 10`, which spins PX4 motor 1 for 10 seconds
- Use the motor test feature in QGC.

## VOXL Mini ESC - 4-in-1

### Hardware Setup  

<<<<<<< HEAD
For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL Mini ESC's (M0129) `J1` using [MCBL-TODO](/cable-datasheets/#mcbl-TODO) or building your own following this.
=======
For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL Mini ESC's (M0129) `J1` using [MCBL-00082](https://docs.modalai.com/cable-datasheets/#mcbl-00082) or building your own following this.
>>>>>>> documentation/master

<img src="/images/voxl2-mini/m0104-m0129.jpg" alt="m0104-m0129" width="1280"/>

M0129 J1:
- Connector - 4 Position JST GH, Vertical, BM04B-GHS-TBT
- Mating Connector - JST GHR-04V-S

M0104 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S