---
layout: default
title: VOXL 2 Image Sensors
parent: VOXL 2 Mini User Guides
nav_order: 7
permalink:  /voxl2-mini-image-sensors/
---

# VOXL 2 Mini Image Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


# Overview

## Summary

In general, the process for selecting image sensors and interposers are use case specific, but there are some common use cases that the VOXL ecosystem supports out of the box.

The following describe two configurations that are available as development kits.

Lower on this page, general information about the interposers and image sensors will be listed that provide customization options.

## Please, Don't Hot Swap!

When playing with the hardware, please ensure you unplug VOXL 2 Mini from the power supply and USB when changing image sensors.  If you have the system powered on, you can damage things!

## Stereo Flex not Currently Supported

The [stereo sensor flex](/M0010) is not currently supported in the VOXL 2 Mini camera configurations.

# Development Kit Configurations

## MDK-M0104-1-C4 - Hires, Tracking

### Hardware Setup

| M0104 Connector | Interposer | Sensor     | HW ID |
|---              |---         |---         |---    |
| J6              |          |            |        |
| -               |          |            |        |
| J7              | M0135 JL   | Tracking ([M0014](/m0024/) ) | 2 |
| -               | M0135 JU   | Hires ([M0025-2](m0025/) )   | 3 |

<img src="/images/voxl2-mini/MDK-M0104-1-C4-a.jpg" alt="MDK-M0104-C4-a" width="1280"/>

<img src="/images/voxl2-mini/MDK-M0104-1-C4-b.jpg" alt="MDK-M0104-C4-b" width="1280"/>

### Software Setup

Run the following command on target to configure.

```
camera-server-config-helper hires:imx214:0 tracking0:ov7251:1
```

To check the camera streams, run `voxl-inspect-cam -a`:

<img src="/images/voxl2-mini/MDK-M0104-1-C4-inspect-cam.jpg" alt="MDK-M0104-C4-inspect-cam" width="1280"/>


## MDK-M0104-1-C6 - ToF, Hires, Tracking

### Hardware Setup

| M0104 Connector | Interposer | Sensor     | HW ID |
|---              |---         |---         |---    |
| J6              | M0076      | ToF([M0040](/m0040/) ) | 0 |
| -               |            |            |        |
| J7              | M0135 JL   | Tracking ([M0014](/m0024/) ) | 2 |
| -               | M0135 JU   | Hires ([M0025-2](m0025/) )   | 3 |

<img src="/images/voxl2-mini/MDK-M0104-1-C6-a.jpg" alt="MDK-M0104-1-C6-a" width="1280"/>

<img src="/images/voxl2-mini/MDK-M0104-1-C6-b.jpg" alt="MDK-M0104-1-C6-b" width="1280"/>

### Software Setup

Run the following command on target to configure.

```
camera-server-config-helper tof:pmd-tof:0 hires:imx214:1 tracking0:ov7251:2
```

You can check the image sensor status using `voxl-inspect-cam -a`:

<img src="/images/voxl2-mini/MDK-M0104-1-C6-inspect-cam.jpg" alt="MDK-M0104-C6-inspect-cam" width="1280"/>

# Hardware Information

## Camera Configuration Summary

There are four 4-lane CSI and four CCI interfaces exposed on two connectors, J6 and J7.

- J6 - CCI0/CSI0 and CCI1/CSI1, representing HW IDs 0/1 respectively
- J7 - CCI2/CSI2 and CCI3/CSI3, representing HW IDs 2/3 respectively

<img src="/images/voxl2-mini/m0104-image-sensors-config.jpg" alt="m0104-image-sensors-config" width="1280"/>

Custom flexes can be made to interface to J6/J7, or the following interposers can be used.

## Interposers

The VOXL 2 Mini (board ID `M0104`) ecosystem consists of "interposers" to provide image sensor connections to the board.

These interposers route the image sensor contral and data, and also interfaces like SPI/UART/I2C/GPIO in some cases.  The focus of this document is just image sensor connections.

### M0135 - Dual Sensor Interposer

- QTY 2 Sensors
- [M0135 Datasheet](/m0135)
- M0104 connectors: J6 or J7

<img src="/images/voxl2-mini/m0104-m0135-overview-0.jpg" alt="m0104-m0135-overview-0" width="1280"/>

<img src="/images/voxl2-mini/m0104-m0135-overview-1.jpg" alt="m0104-m0135-overview-1" width="1280"/>

### M0076 - Single Sensor Interposer

- QTY 1 Sensor
- [M0076 Datasheet](/m0076)
- M0104 connectors: J6 or J7

<img src="/images/voxl2-mini/m0104-m0076-overview-0.jpg" alt="m0104-m0076-overview-0" width="1280"/>

<img src="/images/voxl2-mini/m0104-m0076-overview-1.jpg" alt="m0104-m0076-overview-1" width="1280"/>

### M0084 - Dual Sensor Flex

- QTY 2 Sensors
- [M0084 Datasheet](/m0084)
- M0104 connectors: J6 or J7

<img src="/images/voxl2-mini/m0104-m0084-overview-0.jpg" alt="m0104-m0084-overview-0" width="1280"/>

<img src="/images/voxl2-mini/m0104-m0084-overview-1.jpg" alt="m0104-m0084-overview-1" width="1280"/>

## Image Sensors

### Tracking

- Limitations: total 4 concurrent Tracking Sensors at once on VOXL 2 Mini

| Image Sensor Type | Part Number     | VOXL 2 Mini Connector   | Interposer | Interpose Connector | Sensor HW ID |
|-------------------|-------------    |-----------------------  |------------|---  |--- |
| Tracking          | [M0014](/M0014) | M0104 J6                | M0135      | JL  | 0  |
| Tracking          | [M0014](/M0014) | M0104 J6                | M0135      | JU  | 1  |
| Tracking          | [M0014](/M0014) | M0104 J7                | M0135      | JL  | 2  |
| Tracking          | [M0014](/M0014) | M0104 J7                | M0135      | JU  | 3  |
| Tracking          | [M0014](/M0014) | M0104 J6                | M0076      | J1  | 1  |
| Tracking          | [M0014](/M0014) | M0104 J7                | M0076      | J1  | 2  |
| Tracking          | [M0014](/M0014) | M0104 J6                | M0084      | JL  | 0  |
| Tracking          | [M0014](/M0014) | M0104 J6                | M0084      | JU  | 1  |
| Tracking          | [M0014](/M0014) | M0104 J7                | M0084      | JL  | 2  |
| Tracking          | [M0014](/M0014) | M0104 J7                | M0084      | JU  | 3  |

### ToF

- Limitations: **total 2 concurrent ToF Sensors at once on VOXL 2 Mini**

| Image Sensor Type | Part Number     | VOXL 2 Mini Connector   | Interposer | Interpose Connector | Sensor HW ID |
|-------------------|-------------    |-----------------------  |------------|---  |--- |
| ToF               | [M0040](/M0040) | M0104 J6                | M0135      | JL  | 0  |
| ToF               | [M0040](/M0040) | M0104 J6                | M0135      | JU  | 1  |
| ToF               | [M0040](/M0040) | M0104 J7                | M0135      | JL  | 2  |
| ToF               | [M0040](/M0040) | M0104 J7                | M0135      | JU  | 3  |
| ToF               | [M0040](/M0040) | M0104 J6                | M0076      | J1  | 1  |
| ToF               | [M0040](/M0040) | M0104 J7                | M0076      | J1  | 2  |
| ToF               | [M0040](/M0040) | M0104 J6                | M0084      | JL  | 0  |
| ToF               | [M0040](/M0040) | M0104 J6                | M0084      | JU  | 1  |
| ToF               | [M0040](/M0040) | M0104 J7                | M0084      | JL  | 2  |
| ToF               | [M0040](/M0040) | M0104 J7                | M0084      | JU  | 3  |

### Hires

- Limitations: **total 2 concurrent Hires Sensors at once on VOXL 2 Mini**

| Image Sensor Type | Part Number                               | VOXL 2 Mini Connector   | Interposer | Interpose Connector | Sensor HW ID |
|-------------------|-------------                              |-----------------------  |------------|---  |--- |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J6                | M0135      | JL  | 0  |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J6                | M0135      | JU  | 1  |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J7                | M0135      | JL  | 2  |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J7                | M0135      | JU  | 3  |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J6                | M0076      | J1  | 1  |
| ToF               | [M0025](/M0025), [M0061](/M0061) + flex   | M0104 J7                | M0076      | J1  | 2  |
| ToF               | [M0025](/M0025), [M0061](/M0061)          | M0104 J6                | M0084      | JL  | 0  |
| ToF               | [M0025](/M0025), [M0061](/M0061)          | M0104 J6                | M0084      | JU  | 1  |
| ToF               | [M0025](/M0025), [M0061](/M0061)          | M0104 J7                | M0084      | JL  | 2  |
| ToF               | [M0025](/M0025), [M0061](/M0061)          | M0104 J7                | M0084      | JU  | 3  |


## Beta Configurations

The following are not supported by the `voxl-configure-mpa` setup and are considered beta level.

### Dual ToF, Dual Tracking

#### Hardware Setup

<img src="/images/voxl2-mini/m0104-dual-tof-dual-tracking.jpg" alt="m0104-dual-tof-dual-tracking" width="1280"/>

#### Software Setup

```
camera-server-config-helper tof0:pmd-tof:0 tracking0:ov7251:1 tof2:pmd-tof:2 tracking1:ov7251:2
```

### Quad Tracking

NOTE: QVIO currently only supports a single input.

#### Hardware Setup

<img src="/images/voxl2-mini/m0104-quad-tracking.jpg" alt="m0104-quad-tracking" width="1280"/>

#### Software Setup

```
camera-server-config-helper tracking0:ov7251:0 tracking1:ov7251:1 tracking2:ov7251:2 tracking3:ov7251:3
```