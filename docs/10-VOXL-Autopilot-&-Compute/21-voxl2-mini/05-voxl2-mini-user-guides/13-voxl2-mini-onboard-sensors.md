---
layout: default
title: VOXL 2 Mini Onboard Sensors
parent: VOXL 2 Mini User Guides
nav_order: 13
permalink:  /voxl2-mini-onboard-sensors/
---

# VOXL 2 Mini Onboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

Below describes the VOXL 2 Mini onboard sensors.

<img src="/images/voxl2-mini/m0104-datasheets-onboard.jpg" alt="m0104-datasheets-onboard" width="1280"/>

## IMUs

Note the XYZ axis as drawn respresent the IMU data as reported by the voxl-imu-server MPA service, not the physical orientation of the IMU on the PCB. This aligns with the FRD reference frame when mounted on a drone in the typical orientation such as on the Sentinel reference drone.

![voxl2-mini-imu-locations](/images/voxl2-mini/m0104-2d-imu-diagram.png)

IMU Orientation Image Downloadable File Link [Here](https://storage.googleapis.com/modalai_public/modal_drawings/M0104_VOXL2_MINI_2D_PUBLIC_DWG_03-30-23.PDF)

| Name / Designator           | Description    | Interface                   | MPA Pipe          |
|---                          |---             |---                          |---                |
| PX4 IMU / IMU0 / U16        | TDK ICM42688p  | SSC_QUP5, SPI, SLPI (sDSP)  | /run/mpa/imu_px4  |
| Apps Proc IMU / IMU1 / U17  | TDK ICM42688p  | /dev/spidev3.0, SPI         | /run/mpa/imu_apps |

## Barometers

| Name / Designator           | Description    | Interface                                |
|---                          |---             |---                                       |
| PX4 Baro 0 / BARO1 / U19    | TDK ICP-10100  | SSC_QUP4, I2C, SLPI (sDSP), Addr: 0x63h  |