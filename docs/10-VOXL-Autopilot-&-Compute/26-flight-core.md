---
layout: default3
title: Flight Core
nav_order: 26
has_children: true
permalink: /flight-core/
parent: VOXL Autopilot & Compute
summary: Flight Core is a PX4 Drone Flight Controller - Assembled in the USA. Flight Core can be paired with VOXL for obstacle avoidance and indoor or outdoor GPS-denied navigation. 
thumbnail: /flight-core/flight-core.png
buylink: https://www.modalai.com/collections/blue-uas-framework-components/products/flight-core
---

# Flight Core
{: .no_toc }

Flight Core is a PX4 and ArduPilot [Blue UAS Framework](https://www.modalai.com/pages/blue-uas-framework) Flight Controller - Assembled in the USA. Flight Core can be paired with [VOXL](/voxl/) or [VOXL 2](/voxl-2/) for obstacle avoidance and indoor or outdoor GPS-denied navigation. Flight Core can also be used independently as a standalone, high-performance, secure flight controller. Flight Core is a part of the [Blue UAS Framework](https://www.modalai.com/pages/blue-uas-framework) and is NDAA '20 Section 848 compliant.


<a href="https://www.modalai.com/collections/blue-uas-framework-components/products/flight-core" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/10/flight-core" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

![flight-core-img](/images/flight-core/flight-core.jpg)


