---
layout: default
title:  V2 LTE Pro Modem Dongle Datasheet
parent: 4G LTE Modems
nav_order: 5
permalink: /lte-modem-v2-pro-datasheet/
---

# LTE Modem v2 Pro Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview
 
TODO: graphic

## Development Kits

| PN           | Description |
|---           |---          |
| MDK-M0130-00 | board only |

## Dimensions

### 3D Drawings

TODO: 3D step

[3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/TODO.step)

### 2D Drawings

Xmm x Xmm x Xmm

<img src="/images/todo.png"/>

## Features

| Feature          | Details                                                                                                                                                         |
|:-----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Weight           | X g                                                                                                                                                              |

[Top](#table-of-contents)

## Block Diagram

<br>

![M0130-block-diagram.jpg](/images/modems/M0130-block-diagram.jpg)

## Connector Callouts

<img src="/images/modems/M0130-datasheet-callouts.png" alt="m0130-connectors" width="1280"/>

<hr>

### J5 - apps_proc UART7 and I2C9

| Connector | MPN |
| --- | --- | 
| Board Connector | SM06B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VREG_3P3V_LOCAL       |                                               |
| 2          | UART_TX_3P3V          | `/dev/ttyHS1`                                 |
| 3          | UART_RX_3P3V          | `/dev/ttyHS1`                                 |
| 4          | I2C_SDA_3P3V          | `/dev/i2c-1`                                  |
| 5          | I2C_SCL_3P3V          | `/dev/i2c-1`                                  | 
| 6          | GND                   |                                               | 

<hr>

### J8 - apps_proc SPI11, UART13 I2C2 and GPIO

| Connector | MPN |
| --- | --- | 
| Board Connector | SM06B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VREG_3P3V_LOCAL       |                                               |
| 2          | SPI_MISO_3P3V         | `/dev/spidev11.0`                             |
| 3          | SPI_MOSI_3P3V         | `/dev/spidev11.0`                             |
| 4          | SPI_SCLK_3P3V         | `/dev/spidev11.0`                             |
| 5          | SPI_CS_3P3V           | `/dev/spidev11.0`                             |
| 6          | GPIO_152              |                                               |
| 7          | GPIO_88               | input only ini SPI mode                       |
| 8          | I2C_SDA_3P3V          | `/dev/i2c-0`                                  |
| 9          | I2C_SCL_3P3V          | `/dev/i2c-0`                                  |
| 10         | UART_TX_3P3V          | `/dev/ttyHS3`                                 |
| 11         | UART_RX_3P3V          | `/dev/ttyHS3`                                 |
| 12         | GND                   |                                               |

<hr>

### J9 - apps_proc UART19 and I2C10

| Connector | MPN |
| --- | --- | 
| Board Connector | SM06B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VREG_3P3V_LOCAL       |                                               |
| 2          | UART_TX_3P3V          | `/dev/ttyHS2`                                 |
| 3          | UART_RX_3P3V          | `/dev/ttyHS2`                                 |
| 4          | I2C_SDA_3P3V          | `/dev/i2c-2`                                  |
| 5          | I2C_SCL_3P3V          | `/dev/i2c-2`                                  | 
| 6          | GND                   |                                               | 

### J10 - apps_proc USB2 (1A limit, port2)

| Connector | MPN |
| --- | --- | 
| Board Connector | SM04B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VDC_5V_USB1_PORT2     | D4 LED                                        |
| 2          | USB1_CON_D_N          |                                               |
| 3          | USB1_CON_D_P          |                                               |
| 4          | GND                   |                                               |

### J11 - apps_proc USB3 (2A limit, port3)

| Connector | MPN |
| --- | --- | 
| Board Connector | SM10B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VDC_5V_USB1_PORT3     | D16 LED                                       |
| 2          | USB1_HS_HUB3_CON_D_N  |                                               |
| 3          | USB1_HS_HUB3_CON_D_P  |                                               |
| 4          | GND                   |                                               |
| 5          | USB1_HS_HUB3_SS_RX_N  |                                               |
| 6          | USB1_HS_HUB3_SS_RX_P  |                                               |
| 7          | GND                   |                                               |
| 8          | USB1_HS_HUB3_SS_TX_N  |                                               |
| 9          | USB1_HS_HUB3_SS_TX_P  |                                               |
| 10         | GND                   |                                               |

### J12 - apps_proc USB2 (1A limit, port4)

| Connector | MPN |
| --- | --- | 
| Board Connector | SM04B-GHS-TB(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | VDC_5V_USB1_PORT4     | D16 LED                                       |
| 2          | USB1_CON_D_N          |                                               |
| 3          | USB1_CON_D_P          |                                               |
| 4          | GND                   |                                               |