---
layout: default
title: VOXL Developer Bootcamp
nav_order: 04
has_children: true
permalink: /voxl-developer-bootcamp/
---

# VOXL Developer Bootcamp
{: .no_toc }

---
Welcome to the VOXL Developer Bootcamp! We're glad you're joining the community. This section is designed to guide you through all the core features of your VOXL hardware and SDK that you will likely need when developing for your custom applications.

### We highly recommend you to complete the bootcamp before attempting your first flight!
It's quick, helpful, and later sections may refer back to the bootcamp.

Estimated time to complete: 2 hours
