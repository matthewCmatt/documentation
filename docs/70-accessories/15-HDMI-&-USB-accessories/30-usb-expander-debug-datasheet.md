---
layout: default
title:  USB Expander and Debug Board Datasheet
parent: HDMI and USB accessories
grand_parent: Accessories
nav_order: 30
has_children: false
permalink: /usb-expander-and-debug-datasheet/
---

# USB Expander and Debug Board Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Board Connections

![voxl-usb-expander-and-debug.png](/images/other-products/hdmi-usb-accessories/voxl-usb-expander-and-debug-2.png)

## Specification

| Specicifcation | Value |
| --- | --- |
| Weight | 5g |
| Debug Console Connector | Micro USB, Female |
| Debug Console Mating Connector | Micro USB, Male |
| USB2 Host Connector | 4 Position JST GH, Vertical, BM04B-GHS-TBT |
| USB2 Host Mating Connector | JST GHR-04V-S |
| J1 Connector | Samtec Inc, MPN: QTH-030-01-F-D-K |
| J1 Mating Connector | Samtec Inc., MPN: QSH-030-01-L-D-A-K |

## USB2 Host Pinout

Pin 1 is marked in the `Board Connections` graphic above.

| Pin | Value |
| --- | --- |
| 1 | VBUS |
| 2 | USB_M |
| 3 | USB_P |
| 4 | GND |

## User Manual

Information on usage is found in the [user manual](/usb-expander-and-debug-manual/)
