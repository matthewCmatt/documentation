---
layout: default3
title: Power Modules
parent: Accessories
nav_order: 5
has_children: true
permalink: /power-modules/
thumbnail: /other-products/hdmi-usb-accessories/pow-mod.png
buylink: https://www.modalai.com/collections/accessories
summary: ModalAI's range of high performance 5V power adapters for drones (sUAS, UAVs) and other robotic use cases
---

# Power Modules 
{: .no_toc }

<a href="https://www.modalai.com/collections/accessories" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/14/power-modules" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


ModalAI offers high performance 5V power adapters for drones (sUAS, UAVs) and other robotic use cases. These power modules support voltage and current monitoring for estimating battery consumption and remaining charge.

The v3 power monitor supports 2S-6S batteries, but does not support hot swapping.

The v2 power monitor supports 2S-5S batteries and hot-swapping batteries with a wall power supply.

<img src="/images/other-products/hdmi-usb-accessories/pow-mod.jpg" alt="hdmi-expansion" width="60%">


{:toc}
