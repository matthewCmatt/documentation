---
layout: default3
title: Image Sensors
parent: Accessories
nav_order: 10
has_children: true
permalink: /image-sensors/
thumbnail: /other-products/image-sensors/thumbnail.png
buylink: https://www.modalai.com/collections/cameras
summary: ModalAI's range of tracking, Hi-Res, Time of Flight, and Stereo image sensors
---

# Image Sensors
{: .no_toc }

Documentation for ModalAI's range of tracking, Hi-Res, Time of Flight, and Stereo image sensors. 

<a href="https://www.modalai.com/collections/cameras" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/15/image-sensors" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![microhard](/images/other-products/image-sensors/imagesensors.png)

{:toc}

## Image Sensor Catalog

| NUMBER    | DESCRIPTION                                 | WEIGHT | 3D Model                                                                                            | BUY   | DATASHEET |
|-----------|---------------------------------------------|--------|-----------------------------------------------------------------------------------------------------|--:-:--|--:--------|
| M0014     | OV7251 167° Tracking  Module 9cm Flex       | 0.5g   | [STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0014%20Tracking%20Camera.STEP) |       | [Datasheet](/M0014/) |
| M0015     | OV7251 Stereo Sensor Module                 |        |                                                                                                     |       | [Datasheet](/M0015/) |
| M0024-1   | IMX214 4k Sensor Module (M8 lens 107°)      | 3.78g  |                                                                                                     |       | [Datasheet](/M0024/) |
| M0024-2   | IMX214 4k Sensor Module (M8 lens 146°)      | 2.95g  |                                                                                                     |       | [Datasheet](/M0024/) |
| M0025     | IMX214 4k Sensor Module 8.5mm x 8.5mm       |        |                                                                                                     |       | [Datasheet](/M0025/) |
| M0026     | IMX377 4k Sensor Module (M12 lens)          |        |                                                                                                     |       | [Datasheet](/M0026/) |
| M0040     | PMD TOF Module                              | 3g     |                                                                                                     |       | [Datasheet](/M0040/) |
| M0061-1   | IMX412 Framos VOXL Interposer               |        |                                                                                                     |       | [Datasheet](/M0061/) |
| M0061-2   | IMX678 Framos VOXL Interposer               |        |                                                                                                     |       | [Datasheet](/M0061/) |
| M0072     | OV7251 167° Tracking Module Molex Connector | 0.4g   |                                                                                                     |       | [Datasheet](/M0072/) |
| M0073     | OV9782 RGB Global Shutter with IR Filter    |        |                                                                                                     |       |           |
| M0107     | IMX412 RGB Rolling Shutter with IR Filter   | 7.5g   | [STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0107-m12.STEP)                 |       |           | 
| M0113     | OV9782 RGB Global Shutter without IR Filter |        |                                                                                                     |       |           | 
| M0143     | IMX678 RGB Rolling Shutter                  | 7.97g  | [STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0143-IMX678.STEP)              |       |           |


## Extending MIPI Image Sensor Cable Lenghts

It is strongly recommended to not connect multiple flex cables back-to-back to increase length beyond what ModalAI has shipped as a valid and supported configuration. The risk here includes:
* Incorrect connector orientation risk resulting in sensor or Voxl failures (including power to ground shorts)
* Adding mating cycles to connectors that have limited life span
* Reducing reliability due to increased interconnect points
* Increasing the length or creating a configuration beyond the data link limits ModalAI has already proven and supports 

If your application needs an extended length for your image sensor, please [contact ModalAI](https://modalai.com/contact) and we can explore a custom flex/cable hardware and software solution that will work for you.
