---
layout: default
title: Image Sensor Hardware Design and Integration Guide
parent: Image Sensor Flex Cables and Adapters
parent: Accessories
nav_order: 20
has_children: false
permalink: /image-sensor-design-guide/
---

# Image Sensor Hardware Design and Integration Guide
{: .no_toc }

This page will host a variety of hardware topics relevant to Hardware Engineers that are designing image sensor components relating to our VOXL ecosystem.
Image sensor flex cables, adapter boards, and modules are hard to get right. There are many topics that must be well understood in order to have success the first time, and every time.

The range of applications covered here will include, but are not limited to:
- MIPI CSI fundamental aspects
- VOXL based MIPI CSI Architectures
  - VOXL/VOXL-Flight vs VOXL 2 vs VOXL 2 Mini
- Common Image Sensor specifications
- ModalAI's new Computer Vision SW Framework definitions (only applicable to VOXL 2+, not VOXL)
- Schematic Conventions & the "do's and don'ts" of electrical design for MIPI CSI
- Flexes vs Rigid PCB design and the tradeoffs why/when each is selected
  - Key things to know when working with your Mechanical Engineering team
  - bend radius, clearances, pin 1's
- Complete Schematic Examples (RB5 flight Flexes and VOXL-Cam rigid PCB)
- HighSpeed Routing Constraints
  - Impedance Control, Length Limits, Via Structures and routing tips
- Manufacturing Tips and Tricks for Flexes and Rigid PCBs
  - Layer Stackups
  - EMI coating
  - Ground planes, solid vs hatching
- Testing

and lastly, if this all becomes too overwhelming:

- How to contract ModalAI to do your flexes for you
  - What info is needed
  - Our typical process and turn-around times

## Application Note Outline



### MIPI CSI Overview

PENDING, TBD, etc etc... this will all take some time. Please check back often. 

### VOXL MIPI CSI Architectures

If there are certain topics you want to see first on that list above, please reach out on the [Forum](https://forum.modalai.com/) and add the tag "Image Sensor Hardware Design and Integration Guide" in your topic so we can prioritize our responses
