---
layout: default
title: M0135 VOXL 2 Dual Image Sensor Adapter
parent: Image Sensor Flex Cables and Adapters
nav_order: 109
has_children: false
permalink: /m0135/
---

# M0135 VOXL 2 and VOXL 2 Mini Dual Image Sensor Adapter
{: .no_toc }

## Specification

| Specicifcation                  | Value                                     |
|---------------------------------|-------------------------------------------|
| Length                          |                                           |
| Width                           |                                           |
| VOXL2-side Connector, J1        | DF40C-60DS-0.4V                           |
| Stereo Module Connectors, J2 and J3 |                                   |


Description TO DO

### Pin Out VOXL2-side, J1

| Camera Group | Connector Pin # | Group Function      | VOXL2 Specific Net Name  |
|--------------|-----------------|---------------------|--------------------------|
| 0:J6         | 1               | GND                 | DGND                     |
| 0:J6         | 2               | GND                 | DGND                     |
| 0:J6         | 3               | Lower CCI_I2C_SDA   | CCI_I2C0_SDA             |
| 0:J6         | 4               | DVDD 1.2V           | PM8009_VREG_L2_DVDD_1P2  |
| 0:J6         | 5               | Lower CCI_I2C_SCL   | CCI_I2C0_SCL             |
| 0:J6         | 6               | DOVDD 1.8V          | PM8009_VREG_L7_DOVDD_1P8 |
| 0:J6         | 7               | GND                 | DGND                     |
| 0:J6         | 8               | DVDD 1.05V          | PM8009_VREG_L1_DVDD_1P05 |
| 0:J6         | 9               | Lower CSI_CLK_P     | CSI0_CLK_CON_P           |
| 0:J6         | 10              | Lower RST_N         | GPIO_93_CAM0_RST_N       |
| 0:J6         | 11              | Lower CSI_CLK_M     | CSI0_CLK_CON_M           |
| 0:J6         | 12              | Lower MCLK          | CAM_MCLK0_CON            |
| 0:J6         | 13              | Lower CSI_DAT0_P    | CSI0_LANE0_CON_P         |
| 0:J6         | 14              | GND                 | DGND                     |
| 0:J6         | 15              | Lower CSI_DAT0_M    | CSI0_LANE0_CON_N         |
| 0:J6         | 16              | Lower CCI Timer     | GPIO_110_CCI_TIMER1      |
| 0:J6         | 17              | GND                 | DGND                     |
| 0:J6         | 18              | Upper CCI Timer     | GPIO_113_CCI_TIMER4      |
| 0:J6         | 19              | Lower CSI_DATA1_P   | CSI0_LANE1_CON_P         |
| 0:J6         | 20              | Upper MCLK          | CAM_MCLK1_CON            |
| 0:J6         | 21              | Lower CSI_DATA1_M   | CSI0_LANE1_CON_N         |
| 0:J6         | 22              | AVDD 2.8V           | PM8009_VREG_L5_AVDD_2P8  |
| 0:J6         | 23              | Lower CSI_DATA2_P   | CSI0_LANE2_CON_P         |
| 0:J6         | 24              | GND                 | DGND                     |
| 0:J6         | 25              | Lower CSI_DATA2_M   | CSI0_LANE2_CON_N         |
| 0:J6         | 26              | Upper RST_N, Shared | GPIO_109_CAM3_RST_N      |
| 0:J6         | 27              | GND                 | DGND                     |
| 0:J6         | 28              | Upper CCI_I2C_SDA   | CCI_I2C1_SDA             |
| 0:J6         | 29              | Lower CSI_DATA3_P   | CSI0_LANE3_CON_P         |
| 0:J6         | 30              | Upper CCI_I2C_SCL   | CCI_I2C1_SCL             |
| 0:J6         | 31              | Lower CSI_DATA3_M   | CSI0_LANE3_CON_N         |
| 0:J6         | 32              | Spare MCLK/GPIO     | MCLK6_G0_CON             |
| 0:J6         | 33              | GND                 | DGND                     |
| 0:J6         | 34              | Group SPI MISO      | GPIO_28_CAM0_SPI0_MISO   |
| 0:J6         | 35              | Upper CSI_CLK_P     | CSI1_CLK_CON_P           |
| 0:J6         | 36              | Group SPI MOSI      | GPIO_29_CAM0_SPI0_MOSI   |
| 0:J6         | 37              | Upper CSI_CLK_M     | CSI1_CLK_CON_N           |
| 0:J6         | 38              | Group SPI SCLK      | GPIO_30_CAM0_SPI0_CLK    |
| 0:J6         | 39              | Upper CSI_DATA0_P   | CSI1_LANE0_CON_P         |
| 0:J6         | 40              | Group SPI CS_N      | GPIO_31_CAM0_SPI0_CS     |
| 0:J6         | 41              | Upper CSI_DATA0_M   | CSI1_LANE0_CON_N         |
| 0:J6         | 42              | VREG_S4A_1P8        | VREG_S4A_1P8             |
| 0:J6         | 43              | GND                 | DGND                     |
| 0:J6         | 44              | GND                 | DGND                     |
| 0:J6         | 45              | Upper CSI_DATA1_P   | CSI1_LANE1_CON_P         |
| 0:J6         | 46              | VPH_PWR 3.8V        | VPH_PWR                  |
| 0:J6         | 47              | Upper CSI_DATA1_M   | CSI1_LANE1_CON_N         |
| 0:J6         | 48              | VPH_PWR 3.8V        | VPH_PWR                  |
| 0:J6         | 49              | Upper CSI_DATA2_P   | CSI1_LANE2_CON_P         |
| 0:J6         | 50              | GND                 | DGND                     |
| 0:J6         | 51              | Upper CSI_DATA2_M   | CSI1_LANE2_CON_N         |
| 0:J6         | 52              | 3.3V                | VREG_3P3V_LOCAL          |
| 0:J6         | 53              | GND                 | DGND                     |
| 0:J6         | 54              | GND                 | DGND                     |
| 0:J6         | 55              | Upper CSI_DATA3_P   | CSI1_LANE3_CON_P         |
| 0:J6         | 56              | 5V                  | VDC_5V_LOCAL             |
| 0:J6         | 57              | Upper CSI_DATA3_M   | CSI1_LANE3_CON_N         |
| 0:J6         | 58              | 5V                  | VDC_5V_LOCAL             |
| 0:J6         | 59              | GND                 | DGND                     |
| 0:J6         | 60              | GND                 | DGND                     |
| 1:J7         | 1               | GND                 | DGND                     |
| 1:J7         | 2               | GND                 | DGND                     |
| 1:J7         | 3               | Lower CCI_I2C_SDA   | CCI_I2C2_SDA             |
| 1:J7         | 4               | DVDD 1.2V           | PM8009_VREG_L2_DVDD_1P2  |
| 1:J7         | 5               | Lower CCI_I2C_SCL   | CCI_I2C2_SCL             |
| 1:J7         | 6               | DOVDD 1.8V          | PM8009_VREG_L7_DOVDD_1P8 |
| 1:J7         | 7               | GND                 | DGND                     |
| 1:J7         | 8               | DVDD 1.05V          | PM8009_VREG_L1_DVDD_1P05 |
| 1:J7         | 9               | Lower CSI_CLK_P     | CSI2_CLK_CON_P           |
| 1:J7         | 10              | Lower RST_N         | GPIO_92_CAM1_RST_N       |
| 1:J7         | 11              | Lower CSI_CLK_M     | CSI2_CLK_CON_M           |
| 1:J7         | 12              | Lower MCLK          | CAM_MCLK2_CON            |
| 1:J7         | 13              | Lower CSI_DAT0_P    | CSI2_LANE0_CON_P         |
| 1:J7         | 14              | GND                 | DGND                     |
| 1:J7         | 15              | Lower CSI_DAT0_M    | CSI2_LANE0_CON_N         |
| 1:J7         | 16              | Lower CCI Timer     | GPIO_111_CCI_TIMER2      |
| 1:J7         | 17              | GND                 | DGND                     |
| 1:J7         | 18              | Upper CCI Timer     | GPIO_114_CCI_ASYNC_IN    |
| 1:J7         | 19              | Lower CSI_DATA1_P   | CSI2_LANE1_CON_P         |
| 1:J7         | 20              | Upper MCLK          | CAM_MCLK3_CON            |
| 1:J7         | 21              | Lower CSI_DATA1_M   | CSI2_LANE1_CON_N         |
| 1:J7         | 22              | AVDD 2.8V           | PM8009_VREG_L6_AVDD_2P8  |
| 1:J7         | 23              | Lower CSI_DATA2_P   | CSI2_LANE2_CON_P         |
| 1:J7         | 24              | GND                 | DGND                     |
| 1:J7         | 25              | Lower CSI_DATA2_M   | CSI2_LANE2_CON_N         |
| 1:J7         | 26              | Upper RST_N, Shared | GPIO_109_CAM3_RST_N      |
| 1:J7         | 27              | GND                 | DGND                     |
| 1:J7         | 28              | Upper CCI_I2C_SDA   | CCI_I2C3_SDA             |
| 1:J7         | 29              | Lower CSI_DATA3_P   | CSI2_LANE3_CON_P         |
| 1:J7         | 30              | Upper CCI_I2C_SCL   | CCI_I2C3_SCL             |
| 1:J7         | 31              | Lower CSI_DATA3_M   | CSI2_LANE3_CON_N         |
| 1:J7         | 32              | Spare MCLK/GPIO     | MCLK6_G1_CON             |
| 1:J7         | 33              | GND                 | DGND                     |
| 1:J7         | 34              | Group SPI MISO      | GPIO_4_CAM1_SPI1_MISO    |
| 1:J7         | 35              | Upper CSI_CLK_P     | CSI3_CLK_CON_P           |
| 1:J7         | 36              | Group SPI MOSI      | GPIO_5_CAM1_SPI1_MOSI    |
| 1:J7         | 37              | Upper CSI_CLK_M     | CSI3_CLK_CON_N           |
| 1:J7         | 38              | Group SPI SCLK      | GPIO_6_CAM1_SPI1_CLK     |
| 1:J7         | 39              | Upper CSI_DATA0_P   | CSI3_LANE0_CON_P         |
| 1:J7         | 40              | Group SPI CS_N      | GPIO_7_CAM1_SPI1_CS      |
| 1:J7         | 41              | Upper CSI_DATA0_M   | CSI3_LANE0_CON_N         |
| 1:J7         | 42              | VREG_S4A_1P8        | VREG_S4A_1P8             |
| 1:J7         | 43              | GND                 | DGND                     |
| 1:J7         | 44              | GND                 | DGND                     |
| 1:J7         | 45              | Upper CSI_DATA1_P   | CSI3_LANE1_CON_P         |
| 1:J7         | 46              | VPH_PWR 3.8V        | VPH_PWR                  |
| 1:J7         | 47              | Upper CSI_DATA1_M   | CSI3_LANE1_CON_N         |
| 1:J7         | 48              | VPH_PWR 3.8V        | VPH_PWR                  |
| 1:J7         | 49              | Upper CSI_DATA2_P   | CSI3_LANE2_CON_P         |
| 1:J7         | 50              | GND                 | DGND                     |
| 1:J7         | 51              | Upper CSI_DATA2_M   | CSI3_LANE2_CON_N         |
| 1:J7         | 52              | 3.3V                | VREG_3P3V_LOCAL          |
| 1:J7         | 53              | GND                 | DGND                     |
| 1:J7         | 54              | GND                 | DGND                     |
| 1:J7         | 55              | Upper CSI_DATA3_P   | CSI3_LANE3_CON_P         |
| 1:J7         | 56              | 5V                  | VDC_5V_LOCAL             |
| 1:J7         | 57              | Upper CSI_DATA3_M   | CSI3_LANE3_CON_N         |
| 1:J7         | 58              | 5V                  | VDC_5V_LOCAL             |
| 1:J7         | 59              | GND                 | DGND                     |
| 1:J7         | 60              | GND                 | DGND                     |
| 2:J8         | 1               | GND                 | DGND                     |
| 2:J8         | 2               | GND                 | DGND                     |
| 2:J8         | 3               | Lower CCI_I2C_SDA   | CCI_I2C1_SDA             |
| 2:J8         | 4               | DVDD 1.2V           | PM8009_VREG_L2_DVDD_1P2  |
| 2:J8         | 5               | Lower CCI_I2C_SCL   | CCI_I2C1_SCL             |
| 2:J8         | 6               | DOVDD 1.8V          | PM8009_VREG_L7_DOVDD_1P8 |
| 2:J8         | 7               | GND                 | DGND                     |
| 2:J8         | 8               | DVDD 1.05V          | PM8009_VREG_L1_DVDD_1P05 |
| 2:J8         | 9               | Lower CSI_CLK_P     | CSI4_CLK_CON_P           |
| 2:J8         | 10              | Lower RST_N         | GPIO_78_CAM2_RST_N       |
| 2:J8         | 11              | Lower CSI_CLK_M     | CSI4_CLK_CON_M           |
| 2:J8         | 12              | Lower MCLK          | CAM_MCLK4_CON            |
| 2:J8         | 13              | Lower CSI_DAT0_P    | CSI4_LANE0_CON_P         |
| 2:J8         | 14              | GND                 | DGND                     |
| 2:J8         | 15              | Lower CSI_DAT0_M    | CSI4_LANE0_CON_N         |
| 2:J8         | 16              | Lower CCI Timer     | GPIO_112_CCI_TIMER3      |
| 2:J8         | 17              | GND                 | DGND                     |
| 2:J8         | 18              | Upper CCI Timer     | GPIO_111_CCI_TIMER2      |
| 2:J8         | 19              | Lower CSI_DATA1_P   | CSI4_LANE1_CON_P         |
| 2:J8         | 20              | Upper MCLK          | CAM_MCLK5_CON            |
| 2:J8         | 21              | Lower CSI_DATA1_M   | CSI4_LANE1_CON_N         |
| 2:J8         | 22              | AVDD 2.8V           | PM8009_VREG_L6_AVDD_2P8  |
| 2:J8         | 23              | Lower CSI_DATA2_P   | CSI4_LANE2_CON_P         |
| 2:J8         | 24              | GND                 | DGND                     |
| 2:J8         | 25              | Lower CSI_DATA2_M   | CSI4_LANE2_CON_N         |
| 2:J8         | 26              | Upper RST_N, Shared | GPIO_109_CAM3_RST_N      |
| 2:J8         | 27              | GND                 | DGND                     |
| 2:J8         | 28              | Upper CCI_I2C_SDA   | CCI_I2C3_SDA             |
| 2:J8         | 29              | Lower CSI_DATA3_P   | CSI4_LANE3_CON_P         |
| 2:J8         | 30              | Upper CCI_I2C_SCL   | CCI_I2C3_SCL             |
| 2:J8         | 31              | Lower CSI_DATA3_M   | CSI4_LANE3_CON_N         |
| 2:J8         | 32              | Spare MCLK/GPIO     | MCLK6_G2_CON             |
| 2:J8         | 33              | GND                 | DGND                     |
| 2:J8         | 34              | Group SPI MISO      | GPIO_12_CAM1_SPI5_MISO   |
| 2:J8         | 35              | Upper CSI_CLK_P     | CSI5_CLK_CON_P           |
| 2:J8         | 36              | Group SPI MOSI      | GPIO_13_CAM1_SPI5_MOSI   |
| 2:J8         | 37              | Upper CSI_CLK_M     | CSI5_CLK_CON_N           |
| 2:J8         | 38              | Group SPI SCLK      | GPIO_14_CAM1_SPI5_CLK    |
| 2:J8         | 39              | Upper CSI_DATA0_P   | CSI5_LANE0_CON_P         |
| 2:J8         | 40              | Group SPI CS_N      | GPIO_15_CAM1_SPI5_CS     |
| 2:J8         | 41              | Upper CSI_DATA0_M   | CSI5_LANE0_CON_N         |
| 2:J8         | 42              | VREG_S4A_1P8        | VREG_S4A_1P8             |
| 2:J8         | 43              | GND                 | DGND                     |
| 2:J8         | 44              | GND                 | DGND                     |
| 2:J8         | 45              | Upper CSI_DATA1_P   | CSI5_LANE1_CON_P         |
| 2:J8         | 46              | VPH_PWR 3.8V        | VPH_PWR                  |
| 2:J8         | 47              | Upper CSI_DATA1_M   | CSI5_LANE1_CON_N         |
| 2:J8         | 48              | VPH_PWR 3.8V        | VPH_PWR                  |
| 2:J8         | 49              | Upper CSI_DATA2_P   | CSI5_LANE2_CON_P         |
| 2:J8         | 50              | GND                 | DGND                     |
| 2:J8         | 51              | Upper CSI_DATA2_M   | CSI5_LANE2_CON_N         |
| 2:J8         | 52              | 3.3V                | VREG_3P3V_LOCAL          |
| 2:J8         | 53              | GND                 | DGND                     |
| 2:J8         | 54              | GND                 | DGND                     |
| 2:J8         | 55              | Upper CSI_DATA3_P   | CSI5_LANE3_CON_P         |
| 2:J8         | 56              | 5V                  | VDC_5V_LOCAL             |
| 2:J8         | 57              | Upper CSI_DATA3_M   | CSI5_LANE3_CON_N         |
| 2:J8         | 58              | 5V                  | VDC_5V_LOCAL             |
| 2:J8         | 59              | GND                 | DGND                     |
| 2:J8         | 60              | GND                 | DGND                     |




### Pin Out Mating-side, J2 (TBD CSI Port)

| Pin | Net                   | Upper CCI Notes/Usage                          |
|-----|-----------------------|------------------------------------------------|
| 1   | DGND                  |                                                |
| 2   | DGND                  |                                                |
| 3   | AFVDD                 | Normally Unused, 2.8V                          |
| 4   | CAM_RST_STBY_N        | Normally Unused, power down signal             |
| 5   | CCI_I2C_SDA0          | CCI I2C Bus, SDA                               |
| 6   | 1P8_VDDIO             | 1.8V VDDIO (LDO)                               |
| 7   | CCI_I2C_SCL0          | CCI I2C Bus, SCL                               |
| 8   | VREG_DVDD             | 1.05V (default) or 1.2V DVDD LDO               |
| 9   | CAM_RST0_N            | Sensor Reset Control, 0                        |
| 10  | CAM_MCLKx_BUFF        | MCLK, Buffered from Voxl2, 1.8V                |
| 11  | DGND                  |                                                |
| 12  | DGND                  |                                                |
| 13  | MIPI_CSI_CLK_CONN_P   | MIPI CSI High Speed Diff Pair, CLK_P           |
| 14  | CAM_FLASH             | Sensor Sideband Control, Normally Unused       |
| 15  | MIPI_CSI_CLK_CONN_M   | MIPI CSI High Speed Diff Pair, CLK_M           |
| 16  | CAM_SYNC_0            | Sensor Sync Signal, Shorted to SYNC_1          |
| 17  | MIPI_CSI_LANE0_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 0_P   |
| 18  | CAM_MCLK1             | Second MCLK, Buffered from Voxl, 1.8V.         |
| 19  | MIPI_CSI_LANE0_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 0_M   |
| 20  | CAM_AVDD_2P8          | Sensor 2.8V AVDD LDO                           |
| 21  | DGND                  |                                                |
| 22  | DGND                  |                                                |
| 23  | MIPI_CSI_LANE1_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 1_P   |
| 24  | CAM_RST1_N            | Sensor Reset Control, 1                        |
| 25  | MIPI_CSI_LANE1_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 1_M   |
| 26  | CAM_SYNC_1            | Sensor Sync Signal, Shorted to SYNC_0          |
| 27  | MIPI_CSI_LANE2_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 2_P   |
| 28  | CCI_I2C_SDA1          | Second CCI I2C Bus, SDA                        |
| 29  | MIPI_CSI_LANE2_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 2_M   |
| 30  | CCI_I2C_SCL1          | Second CCI I2C Bus, SCL                        |
| 31  | DGND                  |                                                |
| 32  | DGND                  |                                                |
| 33  | MIPI_CSI_LANE3_CONN_P | MIPI CSI High Speed Diff Pair, Data Lane 3_P   |
| 34  | VPH_PWR_3P8V          | 3.8V Primary "Phone" Power (mimics nominal 1S) |
| 35  | MIPI_CSI_LANE3_CONN_M | MIPI CSI High Speed Diff Pair, Data Lane 3_M   |
| 36  | DGND                  |                                                |



### Pin Out Mating-side, J3 (TBD CSI Port)

| Pin | Net                   | TBD   CCI Notes/Usage                          |
|-----|-----------------------|------------------------------------------------|
| 1   |                       |                                                |

## Technical Drawings

### 3D STEP File

[3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0135_simple.STEP)

### 2D Diagrams

