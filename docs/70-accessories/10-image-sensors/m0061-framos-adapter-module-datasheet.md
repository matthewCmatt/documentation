---
layout: default
title: M0061 Framos Adapter Module Datasheet
parent: Image Sensors
nav_order: 61
has_children: false
permalink: /M0061/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### MDK-M0061-1 m12 IMX412 120° FOV ([Buy Here](https://www.modalai.com/producs/M0061-1))

| Specicifcation | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX412 [Datasheet](https://www.sony-semicon.co.jp/products/common/pdf/IMX412-AACK_Flyer03.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 7.857 mm (Type 1/2.3) 12.3 Mega-pixel                                                          |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | 27629F-16MAS-CM                                                                                |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | TBD                                                                                            |
| IR Filter      | TBD                                                                                            |

### MDK-M0061-2 m12 IMX678 120° FOV ([Buy Here](https://www.modalai.com/producs/M0061-2))

| Specicifcation | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX678 [Datasheet](https://www.framos.com/wp-content/uploads/FSM-IMX678_V1A_Datasheet_v1.0d_Brief.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 8.3 Mpx / 3856 x 2180 px                                                                       |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | SL-HD3125BMP                                                                                   |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | TBD                                                                                            |
| IR Filter      | TBD                                                                                            |

## Mechanical Drawings (3D Step)

[M0061 Adapter Board for Framos Module](https://storage.googleapis.com/modalai_public/modal_drawings/M0061_CCA_3D.stp)

## Module Connector Schematic for J2

![voxl-schematic-for-camera-module-to-connect-to-J2.png](../../images/other-products/image-sensors/voxl-schematic-for-camera-module-to-connect-to-J2.png)

## VOXL 2 Integration

### IMX678 based M0062-2

For [Platform Release 0.9.5, sys img 1.5.5](/voxl2-system-image/#changelog).

#### Options for J8

<img src="/images/voxl2/m0054-imx678-m0061-2.JPG" alt="m0054-imx678-m0061-2" width="1280"/>

- [M0076](/M0076/) interposer plugs into M0054 J8
- [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

<img src="/images/voxl2/m0054-imx678-m0061-2-c.JPG" alt="m0054-imx678-m0061-2-c" width="1280"/>

- [M0076](/M0076/) interposer plugs into M0054 J8
- [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

#### Options for J7

<img src="/images/voxl2/m0054-imx678-m0061-2-b.JPG" alt="m0054-imx678-m0061-2-b" width="1280"/>

- [M0084](/M0084/) flex plugs into M0054 J7
- [M0084](/M0084/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

### Dual Hires Example

<img src="/images/voxl2/m0054-imx412-imx678.JPG" alt="m0054-imx412-imx678" width="1280"/>


- IMX412 (M0061-1) connected to M0084 flex, plugged into M0054 J7
- IMX678 (M0062-2) connected to M0036 flex connected to M0076 interposer plugged into M0054 J8


More information [here](/voxl2-camera-configs/)
