---
layout: default
title: M0025 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 25
has_children: false
permalink: /M0025/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### M0025-1 8.5cm IMX214 100° FOV ([Buy Here](https://www.modalai.com/M0025))

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | ~100°                                                                                               |
| TV Distortion  |                                                                                                     |
| Weight         | <1g                                                                                                 |


### M0025-2 8.5cm IMX214 81° FOV 

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | 81.3° x 69.0° x 54°                                                                                 |
| TV Distortion  |                                                                                                     |
| Weight         | <1g                                                                                                 |

## Pin Out

![Hi_Res_Pin1.JPG](/images/other-products/image-sensors/Hi_Res_Pin1.JPG)

![Hi_Res_Location.JPG](/images/other-products/image-sensors/Hi_Res_Location.JPG)

## Samples of Hires sensor on Sentinel.
### Indoor
![hires_in.jpg](/images/other-products/image-sensors/Samples/hires_in.jpg)
{:target="_blank"}

### Outdoor
![hires_out.jpg](/images/other-products/image-sensors/Samples/hires_out.jpg)
{:target="_blank"}