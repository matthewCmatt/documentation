---
layout: default
title: M0113 OV9782 Module Datasheet
parent: Image Sensors
nav_order: 26
has_children: false
permalink: /M0113/
---

# VOXL RGB Global Shutter

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification


### M0113-1 OV9782 121.3° FOV ([Buy Here](https://www.modalai.com/M0113))

| Specicifcation | Value            |
|----------------|------------------|
| Sensor         | OV9782           |
| Shutter        | Global Shutter   |
| Resolution     | 1280x800         |
| Framerate      |                  |
| Lens Size      |                  |
| Focusing Range |                  |
| Focal Length   |                  |
| F Number       |                  |
| Fov(DxHxV)     | 121.3°           |
| TV Distortion  |                  |
| Weight         |                  |
| IR Filter      | No IR-cut Filter |

## 2D Drawing

![m0113](/images/other-products/image-sensors/m0113-2d.jpg)