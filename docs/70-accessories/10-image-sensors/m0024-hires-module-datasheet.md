---
layout: default
title: M0024 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 24
has_children: false
permalink: /M0024/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### M0024-1 10cm IMX214 107° FOV  ([Buy Here](https://www.modalai.com/msu-m0024-1))

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   | 3.33mm                                                                                              |
| F Number       | 2.75                                                                                                |
| Fov(DxHxV)     | 107°                                                                                                |
| TV Distortion  | < 6%                                                                                                |
| Weight         | 3.78g                                                                                               |

#### M0024-1 Drawings

[2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0024_2D_11-01-21.pdf)

### M0024-2 10cm IMX214 146° FOV  ([Buy Here](https://www.modalai.com/msu-m0024-1))

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      |                                                                                                     |
| Focusing Range |                                                                                                     |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | 146°                                                                                                |
| TV Distortion  |                                                                                                     |
| Weight         | 2.95g                                                                                               |

#### M0024-2 Drawings



## Module Connector Schematic for J2

![voxl-schematic-for-camera-module-to-connect-to-J2.png](../../images/other-products/image-sensors/voxl-schematic-for-camera-module-to-connect-to-J2.png)

