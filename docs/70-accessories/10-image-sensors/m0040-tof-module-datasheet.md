---
layout: default
title: M0040 Time of Flight Module Datasheet
parent: Image Sensors
nav_order: 40
has_children: false
permalink: /M0040/
---

# VOXL Time of Flight (ToF) Sensor Datasheet
{: .no_toc }

## Specification

The PMD Time of Flight sensor produces high-fidelity depth mapping indoors up to 6m. On the VOXL platform this sensor is mutually exclusive to the stereo cameras, meaning the stereo cameras need to be replaced with the TOF Add-on.

### Requirements

* VOXL (APQ8096) Requires VOXL System Image 2.3 or greater.
* VOXL 2 (QRB5165) Requires SDK 0.9 or greater

### Details

| Specicifcation    | Value                                                                                                                                                                    |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Part Number       | MSU-M0040-1-01                                                                                                                                                           |
| Technology        | [PMD](https://www.pmdtec.com/)                                                                                                                                           |
| Rate              | 5 - 45FPS in configurable option modes for distance / accuracy / framerate                                                                                               |
| Exposure Time     | 4.8 ms typ. @ 45 fps / 30 ms typ. @ 5 fps                                                                                                                                |
| Resolution        | 224 x 171 (38k) px                                                                                                                                                       |
| FOV (H x W)       | 85.1° 106.5° (measured by experiemnt)                                                                                                                                    |
| Range             | 4 - 6m                                                                                                                                                                   |
| Depth Resolution  | <= 1% of distance (0.5 – 4m @ 5fps) <= 2% of distance (0.1 – 1m @ 45fps)                                                                                                 |
| Time Sync         | No physical pin, but the frame timestamp is measured with 50ns precision on a single clock. All of the sensors on the VOXL platform are timestamped for computer vision. |
| Power Consumption | <2W                                                                                                                                                                      |
| Weight            | 3g                                                                                                                                                                       |
| Dimensions        | 24mm x 10.6mm                                                                                                                                                            |
| Eye Safe          | Yes                                                                                                                                                                      |


## 2D/3D Drawings of A65 Module

* 2D Envelope Dimensions: 24mm x 10.6mm
* 3D Model (STEP): [MCAM-00005-TOF-A65.STEP](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.STEP)
* 3D Model (SolidWorks 2020): [MCAM-00005-TOF-A65.SLDPRT](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.SLDPRT)

## Pin-out

![TOF_Pin1s.JPG](/images/other-products/image-sensors/TOF_Pin1s.JPG)

![TOF_Pin_to_VOXL.JPG](/images/other-products/image-sensors/TOF_Pin_to_VOXL.JPG)

![TOF_Location.JPG](/images/other-products/image-sensors/TOF_Location.JPG)

### Pin-out J1 MFPC-M0040

| VOXL Board Connector | MFPC-M0040 J1 Mating Connector |
| ---        | ---                       |
| Panasonic, [MPN: AXT336124](https://www3.panasonic.biz/ac/ae/control/connector/base-fpc/p4s/index.jsp?c=move) | [Panasonic MPN: AXT436124](https://www3.panasonic.biz/ac/ae/control/connector/base-fpc/p4s/index.jsp?c=move) |

Pin-out:
| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | VREG_L17A_2P8 (AFVDD)  | 4     | CAM0_STANDBY_N         |
| 5     | CCI_I2C_SDA0           | 6     | VREG_LVS1A_1P8 (DOVDD) |
| 7     | CCI_I2C_SCL0           | 8     | VREG_L3A_1P1 (DVDD)    |
| 9     | CAM0_RST0_N            | 10    | CAM_MCLK0_BUFF         |
| 11    | GND                    | 12    | GND                    |
| 13    | MIPI_CSI0_CLK_CONN_P   | 14    | CAM_FLASH              |
| 15    | MIPI_CSI0_CLK_CONN_M   | 16    | CAM_SYNC_0             |
| 17    | MIPI_CSI0_LANE0_CONN_P | 18    | CAM0_MCLK3             |
| 19    | MIPI_CSI0_LANE0_CONN_M | 20    | VREG_L22A_2P8 (AVDD)   |
| 21    | GND                    | 22    | GND                    |
| 23    | MIPI_CSI0_LANE1_CONN_P | 24    | CAM_RST1_N             |
| 25    | MIPI_CSI0_LANE1_CONN_M | 26    | CAM_SYNC_1             |
| 27    | MIPI_CSI0_LANE2_CONN_P | 28    | CCI_I2C_SDA1           |
| 29    | MIPI_CSI0_LANE2_CONN_M | 30    | CCI_I2C_SCL1           |
| 31    | GND                    | 32    | GND                    |
| 33    | MIPI_CSI0_LANE3_CONN_P | 34    | VPH_PWR                |
| 35    | MIPI_CSI0_LANE3_CONN_M | 36    | GND                    |


## 2D Drawings of M0040 FPCA

* 2D Dimensions of Flex M0040:

![M0040_2D](/images/other-products/flex-cables/m0040-2d.png)

## Example Code

The best approach to access TOF data on VOXL is to write an MPA client that listens to the voxl-camera-server pipe data. An example of that is [here](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros/-/blob/master/src/interfaces/tof_interface.cpp).

### Lower-level Examples

[HAL3 TOF Point Cloud Publishing to MPA Pipe in voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server/-/blob/master/src/api_interface/hal3/hal3_camera_mgr_tof.cpp)

