---
layout: default3
title: VOXL ESCs
nav_order: 75
has_children: true
parent: Accessories
permalink: /voxl-escs/
thumbnail: /modal-esc/esc.png
buylink: https://www.modalai.com/products/voxl-esc
summary: Documentation for ModalAI's high-performance, closed-loop Electronic Speed Controllers (ESCs) that use a digital interface (UART, i2c in future).
---

# ModalAI Electronic Speed Controller (ESC)
{: .no_toc }

ModalAI's Electronic Speed Controllers (ESCs) are high-performing, closed-loop speed controllers that use a digital interface (UART, i2c in future).

<a href="https://www.modalai.com/products/voxl-esc" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/13/escs" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

## Brief Overview
Brushless Electronic Speed Controllers (**ESCs**) are devices that consist of hardware
and software for controlling three-phase brushless DC (**BLDC**) motors. ESCs communicate
with the flight controller, which instructs the ESCs how fast the motor should spin.

ModalAI's ESCs implement the following advanced features:
* Full integration with ModalAI VOXL and Flight Core PCBs
* Bi-directional UART communication with checksum; status and fault monitoring
* Real-time status and health reporting at high update rate (100Hz+ each)
* Closed-loop RPM control for best flight performance
* LED control from flight controller via UART

## Feature Comparison

<table>
<tr>
  <td><center>M0049<br><img src="/images/modal-esc/m0049/m0049_top_bottom.jpg" width="300"></center></td>
  <td><center>M0117<br><img src="/images/modal-esc/m0117/m0117_top_bottom.jpg" width="300"></center></td>
  <td><center>M0134<br><img src="/images/modal-esc/m0134/m0134_top_bottom.jpg" width="300"></center></td>
</tr>
</table>

| Feature                                                                                                 | ModalAI 4-in-1 ESC (M0049)                  | ModalAI 4-in-1 ESC (M0117)                  | ModalAI 4-in-1 ESC (M0134)                  |
|---------------------------------------------------------------------------------------------------------|---------------------------------------------|---------------------------------------------|---------------------------------------------|
| Nominal Input Voltage                                                                                   | 6.0V-16.8V (2-4S Lipo)                      | 6.0V-16.8V (2-4S Lipo)                      | 6.0V-16.8V (2-4S Lipo)                      |
|                                                                                                         |                                             |                                             | 6.0V-25.2V (2-6S Lipo) (M0134-6)            |
| Aux Power Output                                                                                        | 1x 4.5V, 1x 5.0V (adj) 600mA each           | 1x 5.0V (adjustable) 600mA                  | 1x 5.0V (adjustable) 600mA                  |
| Max Continuous Current Per Motor                                                                        | 20A (thermally limited)                     | 20A (thermally limited)                     | 20A (thermally limited)                     |
| Max Burst Current Per Motor                                                                             | 40-50A (requires airflow and heat-spreader) | 40-50A (requires airflow and heat-spreader) | 40-50A (requires airflow and heat-spreader) |
| MCU                                                                                                     | STM32F051K86                                | STM32F051C6U6                               | STM32F051K86                                |
| MOSFET Driver                                                                                           | MP6530                                      | MP6530 (M0117-1)                            | MP6530 (M0134-1)                            |
|                                                                                                         |                                             | MP6531A (M0117-3)                           | MP6531A (M0134-3, M0134-6)                  |
| MOSFETs                                                                                                 | AON7528 (N)                                 | AON7528 (N)                                 | AON7528 (N)                                 |
| Individual Current Sensing                                                                              | 4x 1mOhm + INA186                           | 4x 0.5mOhm + INA186                         | 4x 0.5mOhm + INA186                         |
| ESD signal protection                                                                                   | ✅                                          | ✅                                          | ✅                                          |
| Temperature Sensing                                                                                     | ✅                                          | ✅                                          | ✅                                          |
| On-board Status LEDs                                                                                    | ✅                                          | ✅                                          | ✅                                          |
| External LEDs                                                                                           | Neopixel LEDs                               | N/A                                         | Neopixel LEDs                               |
| Secure Bootloader                                                                                       | Yes (AES256)                                | Yes (AES256)                                | Yes (AES256)                                |
| PWM Switching Frequency                                                                                 | 48, 24 Khz                                  | 48, 24 Khz                                  | 48, 24 Khz                                  |
| Maximum RPM (6 pole pairs)                                                                              | 50K+                                        | 50K+                                        | 50K+                                        |
| PWM control input                                                                                       | ✅                                          | ✅                                          | ✅                                          |
| Active Freewheeling                                                                                     | ✅                                          | ✅                                          | ✅                                          |
| Disable Regenerative Braking                                                                            | ❌ Always enabled                           | ❌ Always enabled (M0117-1)                 | ❌ Always enabled (M0134-1)                 |
|                                                                                                         |                                             | ✅ (M0117-3)                                | ✅ (M0134-3, M0134-6)                       |
| Tone Generation                                                                                         | ✅                                          | ✅                                          | ✅                                          |
| Closed-loop RPM Control                                                                                 | Yes (10Khz)                                 | Yes (10Khz)                                 | Yes (10Khz)                                 |
| Number of UART ports                                                                                    | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  |
| [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) UART Protocol | ✅                                          | ✅                                          | ✅                                          |
| Weight without wires (g)                                                                                | 9.5                                         | 9.5                                         | 9.5                                         |
| Board Dimensions (mm)                                                                                   | 40.5 x 40.5                                 | 40.5 x 40.5                                 | 40.5 x 40.5                                 |
| Mounting Hole Size, Pattern (mm)                                                                        | 3.05, 31.0 x 33.0                           | 3.05, 31.0 x 33.0                           | 3.05, 31.0 x 33.0                           |
| Notes                                                                                                   | EOL                                         |                                             |                                             |

## Mechanical Drawings

| PCB     | 3D STEP                                                                                                              |
|---------|----------------------------------------------------------------------------------------------------------------------|
| M0049-1 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0049_ESC_4_IN_1_REVB_CAM_FINAL_20200610.stp) |
| M0117-1 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0117_ESC_4_IN_1_AD_REVA(-1_MAIN).step)       |
| M0134-1 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0134_ESC_4_IN_1_32_REVA.step)       |
