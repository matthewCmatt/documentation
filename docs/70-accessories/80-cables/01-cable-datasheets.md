---
layout: default
title: Cable Datasheets
parent: Cables
grand_parent: Accessories
nav_order: 1
has_children: false
permalink: /cable-datasheets/
thumbnail: /other-products/cables/mcbl-6.png
buylink: https://www.modalai.com/collections/cables
summary: Datasheets for ModalAI's range of cables. 
---

# Cable Datasheets
{: .no_toc }

ModalAI has various cables used by its systems.  Below captures the cables by part number and provides pinouts and other relevant information.
The [Cable User Guide page](https://docs.modalai.com/cable-userguides/) has information on finding the right cable for your system and how to make your own cables with specific examples.

Note when voltages are listed as "Source" that side is an Output voltage for use by a "Load" or device on the other side. Be sure to check that two sources are not shorted together without first verifying from ModalAI if they are from the same prime source.
Weights provided in grams are subject to +/-0.25 gram tolerances.

## Table of contents
{: .no_toc .text-delta }

### Cable Catalog

| Cable  (Click to jump to section)               | Category                 | Description                                                       | Purchase                                                   |
|-------------------------------------------------|--------------------------|-------------------------------------------------------------------|------------------------------------------------------------|
| [MCK-M0018-1](/cable-datasheets/#mck-m0018-1)   | Cable Kit                | Flight Core Cable Kit                                             | [Purchase](https://modalai.com/products/mck-m0018-1)       |
| [MCK-M0087-2](/cable-datasheets/#mck-m0087-2)   | Cable Kit                | Flight Core V2 Cable Kit                                          | [Purchase](https://modalai.com/products/mck-m0087-2)       |
| [MCBL-00001](/cable-datasheets/#mcbl-00001)     | Power                    | VOXL Flight and VOXL/VOXL 2 to Power Module Cable                 | [Purchase](https://modalai.com/products/mcbl-00001)        |
| [MCBL-00003](/cable-datasheets/#mcbl-00003)     | Power                    | Flight Core to Power Module Cable                                 | [Purchase](https://modalai.com/products/mcbl-00003)        |
| [MCBL-00004](/cable-datasheets/#mcbl-00004)     | Signal-PWM               | PWM Output Cable                                                  | [Purchase](https://modalai.com/products/mcbl-00004)        |
| [MCBL-00005](/cable-datasheets/#mcbl-00005)     | Signal-UART/RC+PWR       | RC Input Cable (Spektrum)                                         | [Purchase](https://modalai.com/products/mcbl-00005)        |
| ~~[MCBL-00006](/cable-datasheets/#mcbl-00006)~~ | USB                      | (END OF LIFE) 4-pin JST GH to USBA Cable                          |                                                            |
| [MCBL-00007](/cable-datasheets/#mcbl-00007)     | Signal-UART              | VOXL to Flight Core/VOXL ESC Serial Cable                         | [Purchase](https://modalai.com/products/mcbl-00007)        |
| [MCBL-00008](/cable-datasheets/#mcbl-00008)     | Signal-UART              | VOXL to Flight Controller TELEM port                              | [Purchase](https://modalai.com/products/mcbl-00008)        |
| [MCBL-00009](/cable-datasheets/#mcbl-00009)     | USB                      | 4-pin JST GH to USBA Female Cable                                 | [Purchase](https://modalai.com/products/mcbl-00009)        |
| [MCBL-00010](/cable-datasheets/#mcbl-00010)     | USB                      | 4-pin JST GH to micro USB Female Cable                            | [Purchase](https://modalai.com/products/mcbl-00010)        |
| [MCBL-00011](/cable-datasheets/#mcbl-00011)     | Power                    | VOXL-PM-Y for Flight Deck R0/R1 Cable                             | [Purchase](https://modalai.com/products/mcbl-00011)        |
| [MCBL-00013](/cable-datasheets/#mcbl-00013)     | Signal-UART              | VOXL Flight to VOXL ESC Cable (cross over)                        |                                                            |
| [MCBL-00014](/cable-datasheets/#mcbl-00014)     | Signal-UART              | Flight Core to VOXL ESC Cable (cross over)                        | [Purchase](https://modalai.com/products/mcbl-00014)        |
| [MCBL-00015](/cable-datasheets/#mcbl-00015)     | Signal-Multi             | 4pin-JST-GH-to-4pin-JST-GH cable                                  | [Purchase](https://modalai.com/products/mcbl-00015)        |
| [MCBL-00016](/cable-datasheets/#mcbl-00016)     | Signal-Multi             | 6pin-JST-GH-to-pigtail break out cable                            | [Purchase](https://modalai.com/products/mcbl-00016)        |
| [MCBL-00017](/cable-datasheets/#mcbl-00017)     | Power                    | 5V Stand Alone Dongle Cable, v1                                   |                                                            |
| [MCBL-00018](/cable-datasheets/#mcbl-00018)     | Signal-UART/RC+PWR       | RC Input Cable, Servo (S.Bus, FrSky)                              | [Purchase](https://modalai.com/products/mcbl-00018)        |
| ~~[MCBL-00019](/cable-datasheets/#mcbl-00019)~~ | USB                      | (OBSOLETE) USB3 Host Mode, USB3.0 A Female Plug to Micro-B plug   |                                                            |
| [MCBL-00020](/cable-datasheets/#mcbl-00020)     | Signal-Multi             | USB Cable, 4-pin Molex Picoblade to 4-pin JST-GH, ArduCam         |                                                            |
| [MCBL-00021](/cable-datasheets/#mcbl-00021)     | Signal-UART/RC+PWR       | RC Input Cable, FrSky R-XSR to Flight Core / VOXL Flight          |                                                            |
| [MCBL-00022](/cable-datasheets/#mcbl-00022)     | USB                      | USB3 10-pin JST cable                                             | [Purchase](https://modalai.com/products/mcbl-00022)        |
| ~~[MCBL-00023]()~~                              | Signal-Multi             | (END OF LIFE) M0051 2x15 JST Consolidated Pigtail Cable           |                                                            |
| [MCBL-00024](/cable-datasheets/#mcbl-00024)     | Power                    | Power Module to Stand Alone Modem Cable                           | [Purchase](https://modalai.com/products/mcbl-00024)        |
| [MCBL-00025](/cable-datasheets/#mcbl-00025)     | Signal-UART              | LTE v2 to Flight Core Telemetry                                   |                                                            |
| [MCBL-00028](/cable-datasheets/#mcbl-00028)     | Signal-UART/GNSS+I2C+PWR | Seeker GPS cable                                                  |                                                            |
| [MCBL-00029](/cable-datasheets/#mcbl-00029)     | Signal-UART              | VOXL 2 or QC Flight RB5 to ESC cable /  VOXL 2 to ESC             | [Purchase](https://modalai.com/products/mcbl-00029)        |
| ~~[MCBL-00030](/cable-datasheets/#mcbl-00030)~~ | Power-ESC                | ESC motor cables (End-Of-Life/Last Time Buys)                     |                                                            |
| [MCBL-00031](/cable-datasheets/#mcbl-00031)     | Signal-Multi             | 6 pin-JST-GH-to 6-pin-JST-GH passthrough, Breakout                | [Purchase](https://modalai.com/products/mcbl-00031)        |
| [MCBL-00041](/cable-datasheets/#mcbl-00041)     | USB                      | 4-pin JST GH to USBA Female Cable                                 | [Purchase](https://modalai.com/products/mcbl-00041)        |
| [MCBL-00048](/cable-datasheets/#mcbl-00048)     | Signal-UART/GNSS+I2C+PWR | Flight Core v2 DroneCode Compliant 6-pin JST to 6-pin SHR         |                                                            |
| [MCBL-00061](/cable-datasheets/#mcbl-00061)     | Signal-UART/RC+PWR       | VOXL 2 to VOXL 2 I/O                                              | [Purchase](https://modalai.com/products/mcbl-00061)        |
| [MCBL-00062](/cable-datasheets/#mcbl-00062)     | Power                    | Flight Core v2 Power Cable                                        | [Purchase](https://modalai.com/products/mcbl-00062)        |
| [MCBL-00063](/cable-datasheets/#mcbl-00063)     | Signal-UART              | Flight Core v2 UART ESC Cable                                     |                                                            |
| [MCBL-00064](/cable-datasheets/#mcbl-00064)     | Signal-UART/RC+PWR       | VOXL 2 IO RC Input Cable, Servo (S.Bus, FrSky)                    | [Purchase](https://modalai.com/products/mcbl-00064)        |
| [MCBL-00065](/cable-datasheets/#mcbl-00065)     | Signal-UART/RC+PWR       | VOXL 2 IO RC Input Cable, FrSky S.BUS R-XSR                       |                                                            |
| [MCBL-00066](/cable-datasheets/#mcbl-00066)     | Signal-UART              | VOXL 2 Add-on UART 6-pin JST to FlightCore 6-pin JST (cross over) | [Purchase](https://modalai.com/products/mcbl-00066)        |
| [MCBL-00067](/cable-datasheets/#mcbl-00067)     | Signal-UART              | VOXL 2 Add-on UART 4-pin JST to FlightCore 6-pin JST (straight)   | [Purchase](https://modalai.com/products/mcbl-00067)        |
| [MCBL-00068](/cable-datasheets/#mcbl-00068)     | USB                      | VOXL 2 4-pin JST USB to Doodle Labs Helix                         | [Purchase](https://modalai.com/products/mcbl-00068)        |
| [MCBL-00069](/cable-datasheets/#mcbl-00069)     | Signal-UART              | VOXL 2 ESC 4-pin JST UART to Custom TMotor F55A ESC               | [Purchase](https://modalai.com/products/mcbl-00069)        |
| [MCBL-00070](/cable-datasheets/#mcbl-00070)     | Signal-Multi             | VOXL 2 12-Pin JST GH Breakout Cable                               |                                                            |
| [MCBL-00071](/cable-datasheets/#mcbl-00071)     | Signal-Multi/USB         | 4-Pin JST GH Breakout Cable                                       |                                                            |
| [MCBL-00072](/cable-datasheets/#mcbl-00072)     | USB                      | VOXL 2 10-pin JST USB to Boson RHP-BOS-VPC3-IF                    | [Purchase](https://modalai.com/products/mcbl-00072)        |
| [MCBL-00076](/cable-datasheets/#mcbl-00076)     | Signal-UART/GNSS+RC+PWR  | Starling/Sentinel GPS + RC UART cable                             |                                                            |
| [MCBL-00078](/cable-datasheets/#mcbl-00078)     | Power                    | XT30U-M to Pigtail 150mm                                          |                                                            |
| [MCBL-00080](/cable-datasheets/#mcbl-00080)     | USB                      | VOXL 2 10-pin JST USB3 to 4-pin JST USB2                          | [Purchase](https://modalai.com/products/mcbl-00080)        |
| [MCBL-00081](/cable-datasheets/#mcbl-00081)     | Signal-UART              | VOXL 2 Mini 12-pin JST to 6-pin DF13 ESC UART                     |                                                            |
| [MCBL-00082](/cable-datasheets/#mcbl-00082)     | Signal-UART              | VOXL 2 Mini 12-pin JST to 4-pin JST ESC UART                      |                                                            |
| [MCBL-00083](/cable-datasheets/#mcbl-00083)     | Signal-Multi             | VOXL 2 8-Pin JST GH Breakout Cable                                |                                                            |
| [MCBL-00085](/cable-datasheets/#mcbl-00085)     | USB+PWR                  | VOXL 2 10-Pin JST USB to Doodle Labs RM-2025-62M3 Cable           |                                                            |
| [MCBL-00086](/cable-datasheets/#mcbl-00086)     | Signal-UART/GNSS+RC+PWR  | Sentinel Holybro (Pixhawk) GPS + RC UART Cable                    |                                                            |
| [MCBL-00090](/cable-datasheets/#mcbl-00090)     | USB-to-UART              | ModalAI USB to Serial Debug UART Cable                            |                                                            |
| [MCBL-00211](/cable-datasheets/#mcbl-00211)     | Power                    | VOXL-PM-Y for FCv2 Flight Deck Cable                              |                                                            |
| [MCBL-00218](/cable-datasheets/#mcbl-00218)     | Signal-UART/RC+PWR       | RC Input Cable, Servo (S.Bus, FrSky), Flight Core V2              | [Purchase](https://modalai.com/products/mcbl-00218)        |
| [MCBL-00221](/cable-datasheets/#mcbl-00221)     | Signal-UART/RC+PWR       | RC Input Cable, FrSky R-XSR to Flight Corev2                      |                                                            |




### Related Accessories

| Accesories                                   | Description                                           |
|---                                           |---                                                    |
| [MCCA-M0022](https://modalai.com/collections/accessories/products/flight-core-pwm-output-cable-and-break-out-board))  | PWM Breakout PCB                                      |

---

## MCK-M0018-1

![MCK-M0018-1](/images/other-products/cables/FC-CBL-R1-1.jpg)

Description:

- Flight Core Cable Kit

Contains:

- PWM Break Out Board ([MCCA-M0022](https://modalai.com/collections/accessories/products/flight-core-pwm-output-cable-and-break-out-board))
- [MCBL-00004](/cable-datasheets/#mcbl-00004) - PWM breakout cable
- [MCBL-00005](/cable-datasheets/#mcbl-00005) - RC Input Cable
- [MCBL-00010](/cable-datasheets/#mcbl-00010) - JST to microUSB female (MCBL-00006, not provided, shown in image)
- [MCBL-00007](/cable-datasheets/#mcbl-00007) - VOXL to Flight Core Serial Cable (provides MAVLink connection over UART between VOXL and Flight Core)

Buy:

- [Available on the ModalAI shop](https://modalai.com/products/mck-m0018-1)

---

## MCK-M0087-2

![MCK-M0087-2]()

Description:

- Flight Core V2 Cable Kit

Contains:

- PWM Break Out Board ([MCCA-M0022](https://modalai.com/collections/accessories/products/flight-core-pwm-output-cable-and-break-out-board))
- [MCBL-00004](/cable-datasheets/#mcbl-00004) - PWM breakout cable
- [MCBL-00005](/cable-datasheets/#mcbl-00005) - RC Input Cable
- [MCBL-00008](/cable-datasheets/#mcbl-00008) - VOXL to Flight Controller TELEM port
- [MCBL-00010](/cable-datasheets/#mcbl-00010) - JST to microUSB female
- [MCBL-00016](/cable-datasheets/#mcbl-00016) - 6pin-JST-GH-to-pigtail break out cable
- [MCBL-00062](/cable-datasheets/#mcbl-00062) - Flight Core v2 Power Cable
- [MCBL-00063](/cable-datasheets/#mcbl-00063) - Flight Core v2 UART ESC Cable


Buy:

- [Available on the ModalAI shop](https://modalai.com/products/mck-m0087-2)

---

## MCBL-00001

![MCBL-00001](/images/other-products/cables/mcbl-1.jpg)

Description:

- VOXL Flight and VOXL to Power Module Cable
- Note there are three variants, -1, -3, and -4. 
- - "-1" is nominally for sale and part of our power module kits and larger drones, 125mm.
- -  MCBL-00001-1: VOXL Flight and VOXL/VOXL 2 to Power Module Cable
- - "-3" and "-4" are shorter lengths (40mm and 70mm respectively) embedded in complete drone kits, but can be made available for purchase upon request
- -  MCBL-00001-3: VOXL Flight and VOXL/VOXL 2 to Power Module Cable, 40mm
- -  MCBL-00001-4: VOXL Flight and VOXL/VOXL 2 to Power Module Cable, 70mm

Where used:

- [VOXL Flight J1013](/VOXL-flight-datasheet-connectors/#j1013---5v-dc-power-input-i2c3-to-power-cable-apm/) to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)
- [VOXL J10](/VOXL-datasheet-connectors/#j1-5v-dc-in-i2c-to-power-cable-apm/) to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)
- Included in several drone platforms, various lengths



Details:

| Component   | Details |
|---          |--- |
| Connector A | Molex, 0050375043 |
| Connector B | Molex, 0050375043 |
| Length      | 125mm (-1 variant), 40mm (-3 variant), 70mm (-4 variant) |
| Insulator   | Silicon |
| Color       | Black |
| Gauge       | 20 AWG |
| Weight       | 4.5 grams (-1 variant) |

Pinout:
Note this cable is symetric and can be used in either direction where the power module is the voltage source and the VOXL/VOXLFlight/VOXL 2 are the loads for pin 1. 

| A   |          | B  |             |
|---  |---       |--- |---          |
| 1   | 5V DC    | 1  | 5V DC       |
| 2   | GND      | 2  | GND         |
| 3   | I2C3_SCL | 3  | PM_SCL      |
| 4   | I2C3_SDA | 4  | PM_SDA      |

Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00001-1%20Drawing%20v3.pdf), [See -3 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00001-3%20Drawing%20v2.pdf), and [See -4 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00001-4%20Drawing%20v2.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00003

![MCBL-00003](/images/other-products/cables/mcbl-3.jpg)

Description:

- Flight Core to Power Module Cable

Where Used:

- [Flight Core J6](/flight-core-datasheet-connectors/#j6---VOXL-power-management-input--expansion/)

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-06V-S |
| Connector B | Molex, 0050375043 |
| Length      | 120mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 5V DC  (Load)      | 1  | 5V DC (Source)      |
| 2   | -            | -  |             |
| 3   | -            | -  |             |
| 4   | EXP_I2C_SCL  | 3  | PM_SCL      |
| 5   | EXP_I2C_SDA  | 4  | PM_SDA      |
| 6   | GND          | 2  | GND         |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00003%20Drawing%20v7.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00004

![MCBL-00004](/images/other-products/cables/mcbl-4.jpg)

Description:

- PWM Output Cable

Where Used:

- [VOXL Flight J1007](/VOXL-flight-datasheet-connectors/#j1007---8-channel-pwmdshot-output-connector) or [Flight Core J7](/flight-core-datasheet-connectors/#j7---8-channel-pwm-output-connector) to PWM Break Out Board ([MCCA-M0022](/cable-datasheets/#mcca-M0022))

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-10V-S |
| Connector B | JST, GHR-10V-S |
| Length      | 120mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:
Note this cable is symetric and can be used in either direction with the FlightCore side providing 5VDC voltage as the Source for reference/monitor circuits only (cannot power a servo/BLDC).

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 5V DC        | 1  | 5V DC       |
| 2   | PWM_CH0      | 2  | PWM_CH0     |
| 3   | PWM_CH1      | 3  | PWM_CH1     |
| 4   | PWM_CH2      | 4  | PWM_CH2     |
| 5   | PWM_CH3      | 5  | PWM_CH3     |
| 6   | PWM_CH4      | 6  | PWM_CH4     |
| 7   | PWM_CH5      | 7  | PWM_CH5     |
| 8   | PWM_CH6      | 8  | PWM_CH6     |
| 9   | PWM_CH7      | 9  | PWM_CH7     |
| 10  | GND          | 10 | GND         |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00004%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.


---

## MCBL-00005

![MCBL-00005](/images/other-products/cables/mcbl-5.jpg)

Description:

- RC Input Cable (Spektrum)

Where Used:

- [VOXL Flight J1004](/VOXL-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector/) or [Flight Core J12](/flight-core-datasheet-connectors/#j12---rc-input--usart6-connector) to RC Receiver (e.g. Spektrum)

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | JST, ZHR-3 |
| Length      | 150mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 3.3VDC  (Source)     | 1  | 3.3VDC   (Load)    |
| 2   | USART6_TX    |-  | NC     |
| 3   | SPEKTRUM RX (3.3V), SBus RX (3.3V), USART6_RX      | 3  | SPEKTRUM TX (3.3V), SBus TX (3.3V)     |
| 4   | GND      | 2  | GND    |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00005%20Drawing%20v7.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.


---

## MCBL-00006 - END OF LIFE

![MCBL-00006](/images/other-products/cables/mcbl-6.jpg)

Description:

- 4-pin JST to USBA Male/Plug Cable
  - Used for connecting USB Hosts such as a PC (Type-A connector side) to USB Peripherals (JST side) 
  - Flight Core (PX4) to QGroundControl (MAVLink)
  - Microhard Standalone Modem to host computer

Where Used:

- [VOXL Flight J1006](/VOXL-flight-datasheet-connectors/#j1006---usb-connector/) or [Flight Core J3](/flight-core-datasheet-connectors/#j3---usb-connector/) to Host Computer with QGroundControl
- [Microhard USB Carrier Board USB2 Host port](/microhard-usb-carrier/) to Host Computer

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | USB Type A Male/Plug |
| Length      | 1m |
| Sheilded    | Yes |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | VBUS (Load)     | 1  | VBUS (Source)    |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| 4   | GND      | 4  | GND    |

---

## MCBL-00007

![MCBL-00007](/images/other-products/cables/mcbl-7.jpg)

Description:

- VOXL to Flight Core Serial Cable (provides MAVLink connection over UART between VOXL and Flight Core)
- VOXL to VOXL ESC Cable (straight through)
- No Power Connection, Signals + GND connections only

Where Used:

- [Flight Core J1](/flight-core-datasheet-connectors/#j1---VOXL-communications-interface-connector/) to [VOXL J12](/VOXL-datasheet-connectors/#j12-blsp5-off-board-uart-esc/)
- [VOXL J12](/VOXL-datasheet-connectors/#j12-blsp5-off-board-uart-esc/) to VOXL ESC (J2 on VOXL ESC V2/V3, M0027/M0049/M0117/M0134)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (Flight Core) |
| Connector B | DF13-6S-1.25C (VOXL) |
| Length      | 90mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | FC RX       | 2  | VOXL TX     |
| 3   | FC TX       | 3  | VOXL RX     |
| 4   | -        | 4  | -      |
| 5   | GND      | 5  | GND    |
| 6   | -        | 6  | -      |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00007%20Drawing%20v7.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00008

![MCBL-00008](/images/other-products/cables/mcbl-8.jpg)

Description:

- VOXL to Flight Controller TELEM port (Dronecode Compliant)
- No Power Connection, Signals + GND connections only

Where Used:

- [VOXL J12](/VOXL-datasheet-connectors/#j12-blsp5-off-board-uart-esc/) to Flight Controller TELEM, such as [Flight Core J5](/flight-core-datasheet-connectors/#j5---telemetry-connector/) or other Dronecode Compliant TELEM ports

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL) |
| Connector B | JST, GHR-06V-S (Flight Core) |
| Length      | 90mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | -        | -  |        |
| 2   | VOXL TX       | 3  | FC RX     |
| 3   | VOXL RX       | 2  | FC TX     |
| 4   | -        | -  | -      |
| 5   | GND      | 6  | GND    |
| 6   | -        |    | -      |


---

## MCBL-00009

![MCBL-00009](/images/other-products/cables/mcbl-9.jpg)

Description:

- 4-pin JST to USBA Female/Receptacle Cable

Where Used:

- Used for connecting ModalAI USB Hosts (JST side, examples below) to USB Peripherals (Type A side)
- [LTE Modem and USB Hub Addon J16 and J17](/lte-modem-and-usb-add-on-datasheet/) to USB peripheral device
- [USB Expander and Debug Board](/usb-expander-and-debug-datasheet/) to USB peripheral device
- [Microhard and USB Hub Add-on USB J16 and J17](/microhard-add-on-datasheet/) to USB peripheral device

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S) |
| Connector B | USB 2.0 Type A Female/Receptacle |
| Length      | ~60mm |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | VBUS (Source side)      | 1  | VBUS (Load side)   |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| 4   | GND      | 4  | GND    |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00009-1%20Drawing%20v15.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00010

![MCBL-00010](/images/other-products/cables/mcbl-10.jpg)

Description:

- 4-pin JST to micro USB Female/Receptacle Cable
  - Used for connecting USB Hosts such as a PC (Micro USB connector side) to ModalAI USB Peripherals (JST side). 
    - Note: Micro USB side requires a standard Type-A to MicroUSB cable/adapter depending on your system setups
  - Flight Core (PX4) to micro USB cable to QGroundControl (MAVLink)
  - Microhard Standalone Modem to micro USB cable to host computer

Where Used:

- [VOXL Flight J1006](/VOXL-flight-datasheet-connectors/#j1006---usb-connector/) or [Flight Core J3](/flight-core-datasheet-connectors/#j3---usb-connector/) to micro USB cable to Host Computer with QGroundControl
- [Microhard USB Carrier Board USB2 Host port](/microhard-usb-carrier/) to micro USB cable to Host Computer

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | Micro USB 2.0 Female |
| Length      | ~60mm |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | VBUS (Load)      | 1  | VBUS (Source)    |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| -   |          | 4  | ID (unused)    |
| 4   | GND      | 5  | GND    |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00010-1%20Drawing%20v9.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00011-1

![MCBL-00011-1](/images/other-products/cables/mcbl-11.jpg)

Description:

- VOXL-PM-Y for Flight Deck R0/R1 Cable
- Powers both VOXL and FlightCore from one Power Module, with the I2C bus for power monitoring going to FlightCore

Where Used:

- VOXL-m500-R1
- [VOXL J1](/VOXL-datasheet-connectors/#j1-5v-dc-in-i2c-to-power-cable-apm/) + [Flight Core J6](/flight-core-datasheet-connectors/#j6---VOXL-power-management-input--expansion/)to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)

Details:

| Component     | Details |
|---            |--- |
| Connector A   | JST, GHR-06V-S (Flight Core side, 4 wires)|
| Connector B   | Molex, 0050375043 (Power Module side, 4 wires) |
| Connector C   | Molex, 0050375043 (VOXL side, 2 wires) |
| Length        | 120mm |
| Insulator     | PVC (flexible) |
| Color         | Black |
| Gauge         | 22AWG (B-to-C)/ 26AWG (B-to-A) |

Pinout:

| C   | VOXL     |  B | VOXL PM |  A | Flight Core |
|---  |---       |--- |---      |--- |---          |
| 1   | 5V DC (Load)   | 1  | 5V DC (Source)  | 1  | 5V DC (Load)      |
| 2   | GND      | 2  | GND     | 6  | GND         |
| -   |          | 3  | SCL     | 4  | EXP_I2C_SCL |
| -   |          | 4  | SDA     | 5  | EXP_I2C_SDA |

---

## MCBL-00013

![MCBL-00013](/images/other-products/cables/mcbl-13.jpg)

Description:

- VOXL Flight to VOXL ESC V2/V3 Cable (cross over)
- This is a UART RX/TX cross-over version of MCBL-00007 for Hirose DF13-6P
- No Power Connection, Signals + GND connections only

Where Used:

- [VOXL Flight J1002](/VOXL-flight-datasheet-connectors/#j1002---uart-esc-uart2telem3-interface-connector/) to VOXL ESC (J2 on VOXL ESC V2/V3, M0027/M0049/M0117/M0134)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL Flight) |
| Connector B | DF13-6S-1.25C (ESC) |
| Length      | 100mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   | VOXL Flight | B  | VOXL ESC V2/V3 |
|---  |---          |--- |---       |
| 1   | -           | -  |          |
| 2   | RX          | 3  | TX       |
| 3   | TX          | 2  | RX       |
| 4   | -           | -  | -        |
| 5   | GND         | 5  | GND      |
| 6   | -           | -  | -        |

---

## MCBL-00014

![MCBL-00014](/images/other-products/cables/mcbl-14.jpg)

Description:

- Flight Core to VOXL ESC Cable (cross over)
- This is just like MCBL-00013 but one of the 6-pin DF13's is replaced by an 8-pin for FlightCore J4
- No Power Connection, Signals + GND connections only

Where Used:

- Flight Core J4 to VOXL ESC (J2 on VOXL ESC V2/V3, M0027/M0049/M0117/M0134)
- Connecting ESC's' to VOXL CAM Flight Core (on Seeker Drone, for example)



Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-8S-1.25C (Flight Core) |
| Connector B | DF13-6S-1.25C (ESC) |
| Length      | 100mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   | Flight Core | B  | VOXL ESC V2/V3 |
|---  |---          |--- |---       |
| 1   | -           | -  |                |
| 2   | RX          | 3  | TX       |
| 3   | TX          | 2  | RX       |
| 4   | -           | -  | -        |
| 5   | GND         | 5  | GND      |
| 6   | -           | -  | -        |
| 7   | -           |
| 8   | -           |

---

## MCBL-00015

![MCBL-00015](/images/other-products/cables/mcbl-15.jpg)

Description:

- 4pin-JST-to-4pin-JST cable (straight through 1:1)

Where Used:

- VOXL USB Add-On boards to Stand Alone Modem Boards, ESC UART from Voxl2 to Mini-ESC (M0129)
- Conforms to all ModalAI 4-pin JST USB2.0 formats, either Host (VBUS, Pin1, is a source) or Peripheral (VBUS, Pin1, is a load)
- Great item to have for making your own cables!!
- Note there are two variants, -1 and -2. 
- - "-1" is nominally for sale, 100mm.
- - "-2" is a longer length, 150mm, embedded in complete drone kits, but can be made available for purchase upon request

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | JST, GHR-04V-S |
| Length      | 100mm (-1 variant), 150mm (-2 variant) |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight      | 1.1 grams (-1 variant), 1.4 grams (-2 variant) |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | Pin1     | 1  | Pin1   |
| 2   | Pin2     | 2  | Pin2   |
| 3   | Pin3     | 3  | Pin3   |
| 4   | Pin4     | 4  | Pin4   |

Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00015-1%20Drawing%20v2.pdf), [See -2 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00015-2%20Drawing%20v2.pdf) Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00016

![MCBL-00016](/images/other-products/cables/mcbl-16.jpg)

Description:

- 6pin-JST-to-pigtail break out cable

Where Used:

- M0048 ICM42688 IMU Breakout Board
- Any ModalAI 6-pin JST where soldering or custom harnesses are needed
- Note: pigtail side is tinned for ~10mm making it easy to install JST SSHL-002T-P0.2 pins if needed
- Great item to have for making your own cables!!

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-06V-S) |
| Connector B | None |
| Length      | 100mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight       | 1.4 grams |

Pinout:

| A   |          |    |        |
|---  |---       |--- |---     |
| 1   |          | 1  | (pigtail) |
| 2   |          | 2  | (pigtail) |
| 3   |          | 3  | (pigtail) |
| 4   |          | 4  | (pigtail) |
| 5   |          | 5  | (pigtail) |
| 6   |          | 6  | (pigtail) |

---

## MCBL-00017

![MCBL-00017](/images/other-products/cables/mcbl-17.jpg)

Description:

- Power Supply (not supplied) to Stand Alone Modem Dongle
- Connector mates to the Red JST J8 on Microhard boards, or J3 on Sierra LTE V2 modem boards
- Tinned leads on the other side allow soldering or converting to pins as needed by the customer.
- NOTE: Cut the wire length to 150mm or less if anticipating needing the full 4A capable of these designs (high power radio modes + all USB connectors used). Refer to the Where Used links below for power consumption estimates.

Where Used:

- [MDK-M0030-1-02 J3](/lte-modem-v2-dongle-datasheet/#j3---5vdc-power-in)
- [MDK-M0030-1-01 J3](/lte-modem-v2-dongle-datasheet/#j3---5vdc-power-in)
- [MA-SA-2 J3](/microhard-usb-carrier/)

Details:

| Component   | Details |
|---          |--- |
| Connector A | [SFHR-02V-R](https://www.digikey.com/en/products/detail/jst-sales-america-inc/SFHR-02V-R/2328483) |
| Connector B | None/Tinned ~10mm |
| Length      | 300 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black |
| Gauge       | 22 AWG |
| Pre-crimped | [ALEALEA22K102](https://www.digikey.com/en/products/detail/jst-sales-america-inc/ALEALEA22K102/9923160) |

Pinout:

| A   | Wire        | B  |Modem |
|---  |---          |--- |---       |
| 1   | RED (Source)        | 1  | VDCIN_5V (Load) |
| 2   | BLACK       | 2  | GND      |

---

## MCBL-00018

![MCBL-00018](/images/other-products/cables/mcbl-18.jpg)

Description:

- RC Input (S.Bus, FrSky)
- Picks up Power on Red+Black, and Receive Signal on Orange from FlightCore or VOXLFlight to a R/C receiver 3-pin 0.1" hobby format
- -  ModalAI conforms to the notion of Pin 1 = Signal, Pin 2 = 5V, & Pin 3 = GND for the 3-pin hobby format. Please notify us if you see anything here to the contrary.
- Note: This is only intended for remote receivers and cannot power high current servos or BLDCs

Where Used:

- 4-pin JST (connector B): [VOXL Flight J1004](/VOXL-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector/) or [Flight Core J12](/flight-core-datasheet-connectors/#j12---rc-input--usart6-connector) and 3-pin JST (connector A): [VOXL Flight J1003](/VOXL-flight-datasheet-connectors/#j1003---ppm-rc-in) or [Flight Core J9](/flight-core-datasheet-connectors/#j9---ppm-rc-in) to  3-pin Harwin M20 Receptacle (connector C) to RC Receiver (e.g. S.Bus/FrSky)
- Note: This is not for Spektrum, which requires +3.3V. For Spektrum, please see MCBL-00005

| Component   | Details |
|---          |--- |
| Connector A | JST GHR-03V-S, for +5V Power/GND  |
| Connector B | JST GHR-04V-S, for UART_RX signal, +3.3V levels |
| Connector C | Harwin M20-1060300, CONN RCPT HSG 3POS 2.54mm (or similar) |
| Length      | 150 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Orange |
| Gauge       | 26 AWG |

Pinout:

| A   | Power       | C  | RC Receiver    | B  | Signal   |
|---  |---          |--- |---       |--- |---       |
| 1   | RED (5V, Source)    | 2  | 5V DC (Load)   |    |          |
| 2   |             | 1  | S.Bus/DSMX (Orange)    | 3  | S.Bus/DSMX (Orange)    |
| 3   | BLACK (GND) | 3  | GND      |    |          |
|     |             |    |          |    |          |

---

## MCBL-00020

![MCBL-00020](/images/other-products/cables/mcbl-20.jpg)

Description:

- USB Cable, Arducam-4 pin Molex Picoblade to 4 -pin JST-GH

Where Used:

- VOXL Add-ons with USB hub to ArduCam USB Camera

Details:

| Component   | Details |
|---          |--- |
| Connector A | 4POS 1.25mm PICOBLADE |
| Connector B | 4POS 1.25mm JST |
| Length      | 100mm |
| Insulator   | Silicon |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | V_BUS    | 1  | V_BUS  |
| 2   | DATA_M   | 2  | DATA_M |
| 3   | DATA_P   | 3  | DATA_P |
| 4   | GND      | 4  | GND    |

[See Schematic Here](/images/other-products/cables/MCBL-00020-1-v5.pdf)

---

## MCBL-00021

![MCBL-00021-1](/images/other-products/cables/mcbl-21-1.jpg)

Description:

- RC Input (FrSky R-XSR)
- Picks up Power on Red+Black, and Receive Signal on Yellow from FlightCore or VOXLFlight to FrSky R-XSR format R/C receiver 5-pin Picoblade format
- Note: This is just like MCBL-00018 but instead of the 3-pin hobby format receptacle, it is pinned for the FrSky R-XSR 5-position Picoblade

Where Used:

- 4-pin JST (connector B): [VOXL Flight J1004](/VOXL-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector/) or [Flight Core J12](/flight-core-datasheet-connectors/#j12---rc-input--usart6-connector) and 3-pin JST (connector A): [VOXL Flight J1003](/VOXL-flight-datasheet-connectors/#j1003---ppm-rc-in) or [Flight Core J9](/flight-core-datasheet-connectors/#j9---ppm-rc-in) to  5-pin Molex Picoblade (connector C) to FrSky R-XSR Receiver

| Component   | Details |
|---          |--- |
| Connector A | JST GHR-03V-S, for +5V Power/GND  |
| Connector B | JST GHR-04V-S, for UART_RX signal, +3.3V levels |
| Connector C | Molex 0510210500, 5-position Picoblade housing 1.25mm |
| Length      | 140 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Yellow |
| Gauge       | 26 AWG |

Pinout:

![R-XSR-Pinout](/images/other-products/cables/mcbl-21-1-pinout.png)

| A   | Power       | C  | RC Receiver    | B  | Signal   |
|---  |---          |--- |---       |--- |---       |
| 1   | RED (5V, Source)    | 4  | 5V DC (Load)   |    |          |
| 2   |             | 2  | S.Bus (Yellow)    | 3  | S.Bus (Yellow)    |
| 3   | BLACK (GND) | 5  | GND      |    |          |
|     |             |    |          |    |          |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00021-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00022

![MCBL-00022](/images/other-products/cables/mcbl-00022.jpg)

Description:

- USB3 10-pin JST cable to USB3-Type A Receptacle
- Note: This is not qualified for 10Gbps speeds. This is a proto-type design suitable for most application development. Customers are advised to source their own shielded and speed-qualified cable for high-volume needs.

Where Used:

- Any ModalAI CCA USB downstream port (host only) with the 10-pin JST ModalAI USB3 format (examples are M0062, M0067, M0090, M0104, M0130).

Details:

| Component   | Details |
|---          |--- |
| Connector A | 10POS 1.25mm JST, GHR-10V-S |
| Connector B | CONN RCPT USB3.0 TYPEA, CNC Tech 1003-006-01200    |
| Length      | 80mm |
| Insulator   | Silicon |
| Color       | Black, White/Green TP |
| Gauge       | 26AWG VBUS/GND, 28AWG Twisted Pair Signals |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00022-2%20Drawing%20v20.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00024

![MCBL-00024](/images/other-products/cables/mcbl-24.jpg)

Description:

- Power Module to Stand Alone Modem Cable

Where Used:

- Used to power a standalone modem board (Sierra M0030-2/M0130-2, or Microhard M0048-2/M0059-2, or 5G M0090-2) directly from the ModalAI Power Module V3

Details:

| Component     | Details |
|---            |--- |
| Connector A   | Molex, 0050375043 (Power Module side, 2 wires) |
| Connector B   | SFHR-02V-R (Modem side, 2 wires) |
| Length        | 100mm |
| Insulator     | PVC (flexible) |
| Color         | Black/Red |
| Gauge         | 26AWG |

Note: We are in process of manufacturing a 22AWG version of this cable for maximum current use cases. Contact us for more info and stay tuned for updates.

Pinout:

| A   | POWER MODULE     |  B | Modem Side | 
|---  |---       |--- |---      |
| 1   | 5V DC (Source)   | 1  | 5V DC (Load)  |
| 2   | GND      | 2  | GND     | 
| 3   |   -       |   |      | 
| 4   |    -      |   |      | 

[See Drawing Here](/images/other-products/cables/MCBL-00024-1-v5.pdf)

---

## MCBL-00025

![MCBL-00025](/images/other-products/cables/mcbl-25.jpg)

Description:

- LTE v2 to Flight Core (DroneCode Compliant) Telemetry
- No Power Connection, Signals + GND connections only


Where Used:

- The M0030 Sierra 4G/LTE Modem has a spare UART port on J9 (matches the pinout of J2 as shown [here](https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/))
- This connector provides a 3.3V UART link directly to the Modem, which can be connected to FlightCore J5 (UART7) to create a 4G Telemetry or general purpose data link.
- Note: Software support of this feature may still be in BETA form, please contact ModalAI for support
- Flightcore side pinouts also match other DroneCode compliant connectors J6 (expansion UART4) and J10 (GNSS UART1) ports
- The 4-pin side on the 4G LTE Modem also matches many other Serial Debug Ports on ModalAI HW and can be used as a general purpose UART converter from the 4-wire eSH format to 6-wire eGH format (DroneCode Compliant) 

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-06V-S, FlightCore Side, +3.3V levels  |
| Connector B | JST 1.0mm SHR-06V-S, 4G Modem Side, +3.3V levels |
| Length      | 100mm |
| Insulator   | Silicon |
| Color       | black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | -    | 1  | -  |
| 2   | TX (FlightCore Output/Transmit)  | 2  | RX (Modem Input/Receive) |
| 3   | RX (FlightCore Input/Receive)  | 3  | TX (Modem Output/Transmit) |
| 4   | -      | 4  | GND    |
| 5   | -      | -  | -    |
| 6   | GND      | -  | -    |


[See Drawing Here](/images/other-products/cables/MCBL-00025-1-v5.pdf)

---


## MCBL-00028

![MCBL-00028-2](/images/other-products/cables/mcbl-28-2.jpg)

Description:

- Seeker GPS Cable

Where Used:

- Connects HolyBro GPS/Magnetometer unit to VOXL-CAM on Seeker drone using FlightCore J10 (6-pin)
- This cable provides power + UART + I2C


Details:

| Component   | Details |
|---          |--- |
| Connector A | 10 POS 1.0mm JST, GNSS Side |
| Connector B | 6 POS 1.25mm JST, FlightCore J10 side |
| Length      | 230mm |
| Insulator   | Silicon |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | 5VDC (Load)   | 1  | 5VDC (Source) |
| 2   | RX (GNSS Input)  | 2  | TX (FlightCore Output) |
| 3   | TX (GNSS Output)  | 3  | RX (FlightCore Input)|
| 4   | SCL      | 4  | SCL (FlightCore Host Side)    |
| 5   | SDA      | 5  | SDA (FlightCore Host Side)    |
| 6   | -      | -  | -    |
| 7   | -      | -  | -    |
| 8   | -      | -  | -    |
| 9   | -      | -  | -    |
| 10   | GND      | 6  | GND    |

[See Drawing Here]()
- updated drawing coming soon!
---

## MCBL-00029

![MCBL-00029](/images/other-products/cables/mcbl-29.jpg)

Description:

- RB5 Flight (M0052) or VOXL 2 (M0054) to VOXL ESC V2/V3 Cable
- This is just like MCBL-00007 but one side is now the new 4-pin JST GH format for ESC instead of DF13 on VOXL
- No Power Connection, Signals + GND connections only
- Note there are two variants, -1, and -2. 
- - "-1" is nominally for sale and part of our larger drones at 200mm.
- - "-2" is shorter at 70mm and will be embedded into the new Starling drone kit, but can be made available for purchase upon request

Where Used:

- Qualcomm Flight RB5 Mainboard ESC Port (J18) to VOXL ESC V2/V3 J2 (M0027/M0049/M0117/M0134)
- VOXL 2 ESC Port (J18) to VOXL ESC V2/V3 J2 (M0027/M0049/M0117/M0134)
- 200mm length for long wire M500 frame installations
- 70mm for smaller drone installations

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL ESC), +3.3V levels |
| Connector B | JST 1.25mm GHR-04V-S, RB5 Flight/VOXL 2 Side, +3.3V levels |
| Length      | 200mm (-1 variant), 70mm (-2 variant)|
| Insulator   | Silicon (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |  VOXL ESC V2/V3        |  B | RB5 Flight / VOXL 2       |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | ESC RX       | 2  | VOXL 2/RB5-Flight TX     |
| 3   | ESC TX       | 3  | VOXL 2/RB5-Flight RX     |
| 4   | -        | -  | -      |
| 5   | GND      | 4  | GND    |
| 6   | -        | -  | -      |

Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00029-1%20Drawing%20v4.pdf), [See -2 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00029-2%20Drawing%20v3.pdf) Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00030

![MCBL-00030-3](/images/other-products/cables/mcbl-30-3.jpg)

Description:

- **End-Of-Life:** Being Replaced by MCBL-00040-2 with 3.5mm bullet terminal system
- 2mm Bullet Connector to tinned lead for ESC soldering and easy motor mating
- RB5 Flight ESC to motor cables (legacy usage)
- Note: We have various lengths for this cable
- - MCBL-00030-2 = 250mm (as per drawing link below, but Obsolete, use other lengths), 
- - MCBL-00030-3 = 265mm
- - MCBL-00030-4 = 210mm

Details:

| Component   | Details |
|---          |--- |
| Connector A | Female Bullet Wire,2mm, 18-22AWG |
| Length      | 250mm |
| Insulator   | Silicon |
| Gauge       | 20 AWG |


[See Drawing Here](/images/other-products/cables/MCBL-00030-2-v1.pdf)

---

## MCBL-00031

![MCBL-00031-1](/images/other-products/cables/mcbl-31-1.jpg)

Description:

- 6 pin-JST-GH-to 6-pin-JST-GH passthrough (1:1/Straight-through wiring)
- Note there are two variants, -1, and -2. 
- - -1, 100mm,  is nominally for sale and the -2 is a longer version at 150mm.


Where Used:

- Connects Qualcomm Flight RB5 Flight Deck Modem and Chirp Sensors
- Connects Voxl2/Voxl2 Mini and FlightCoreV2 Dronecode Compliant ports to Dronecode Compliant GNSS modules
- Chirp Breakout to Debug or Modem Carrier Board
- Any place where a 6-pin JST GH is used for debugging, external sensors, etc. 
- Great item to have for making your own cables!!

Details:

| Component   | Details                                        |
|-------------|------------------------------------------------|
| Connector A | 6POS 1.25mm JST                                |
| Connector B | 6POS 1.25mm JST                                |
| Length      | 100mm (-1 variant), 150mm (-2 variant)         |
| Insulator   | PVC (flexible) (some old units may be silicone)|
| Color       | Black                                          |
| Gauge       | 26 AWG                                         |
| Weight      | 1.7 grams (-1 variant), 2.4 grams (-2 variant) |

Pinout:

| A |          | B |         |
|---|----------|---|---------|
| 1 |          | 1 |         |
| 2 |          | 2 |         |
| 3 |          | 3 |         |
| 4 |          | 4 |         |
| 5 |          | 5 |         |
| 6 |          | 6 |         |


Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00031-1%20Drawing%20v3.pdf), [See -2 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00031-2%20Drawing%20v2.pdf).
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00041

![MCBL-00041](/images/other-products/cables/mcbl-41.jpg)

Description:

- 4-pin JST to USBA Female/Receptacle Cable
- 150mm (6") version of MCBL-00009

Where Used:

- Used for connecting ModalAI USB Hosts (JST side, examples below) to USB Peripherals (Type A side)
- [LTE Modem and USB Hub Addon J16 and J17](/lte-modem-and-usb-add-on-datasheet/) to USB peripheral device
- [USB Expander and Debug Board](/usb-expander-and-debug-datasheet/) to USB peripheral device
- [Microhard and USB Hub Add-on USB J16 and J17](/microhard-add-on-datasheet/) to USB peripheral device
- This cable is often included in many kits, but can be made available to purchase seperately if you need it. Please use our [contact page](https://www.modalai.com/pages/contact-us) to reach out with inquiries.

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S) |
| Connector B | USB 2.0 Type A Female/Receptacle |
| Length      | ~150mm |
| Weight       | 10.9 grams |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | VBUS (Source side)      | 1  | VBUS (Load side)   |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| 4   | GND      | 4  | GND    |

---

## MCBL-00048

![MCBL-00048-1](/images/other-products/cables/mcbl-00048-1.jpg)

Description:

- Connects Matek GPS/Magnetometer unit to ModalAI DroneCode compliant ports, such as J10 on FlightCore V2
- This cable provides power + UART + I2C


Where Used:

- ModalAI FlightCore (v1 or V2) J10 (or other Dronecode compliant ports) to COTS Matek GNSS module, or others, with 6-pin JST SHR format (be sure to check GNSS module vendor specs as they change often)


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-06V-S, FCv2 side, +3.3V signal levels |
| Connector B | JST 1.0mm SHR-06V-S-B, Module side, +3.3V signal levels |
| Length      | 150mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight      | 2.2 grams |

Pinout:

| A   |  FlightCore V2        |  B | GNSS Module       |
|---  |----------------       |--- |----------------   |
| 1   | 5V (Source)           | 6  | 5V (Load)         |
| 2   | UART_TX               | 4  | UART_RX           |
| 3   | UART_RX               | 3  | UART_TX           |
| 4   | MAG_I2C_SCL           | 1  | SCL               |
| 5   | MAG_I2C_SDA           | 2  | SDA               |
| 6   | GND                   | 5  | GND               |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00048-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00061

![MCBL-00061-1](/images/other-products/cables/mcbl-00061-1.jpg)

Description:

- Cable providing the primary connection used to connect VOXL 2 to VOXL 2 IO, which provides power and UART based communications

Where Used:

- Connects VOXL 2 J19 to VOXL 2 I/O J4

Details:

| Component   | Details   |
|-------------|-----------|
| Connector A | GHR-12V-S |
| Connector B | GHR-04V-S |
| Length      | 150mm     |
| Insulator   | Silicone  |
| Color       | Black     |
| Gauge       | 26AWG     |


Pinout:

| A  |          | B |            |
|----|----------|---|------------|
| 1  | NC       |   |            |
| 2  | NC       |   |            |
| 3  | NC       |   |            |
| 4  | NC       |   |            |
| 5  | NC       |   |            |
| 6  | NC       |   |            |
| 7  | NC       |   |            |
| 8  | NC       |   |            |
| 9  | 3P3V     | 1 | 3P3V_IO    |
| 10 | VOXL 2 TX | 2 | VOXL 2IO RX |
| 11 | VOXL 2 RX | 3 | VOXL 2IO TX |
| 12 | GND      | 4 | GND        |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00061-1%20Drawing%20v5.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00062

![MCBL-00062](/images/other-products/cables/MCBL-00062.png)


Description:

- Cable connecting the VOXL Power Module v3 to the Flight Core v2, providing power and I2C connections
- This is basically an MCBL-00001 (or MCBL-0003) but the LOAD side is now a 4-pin JST GH with a 1:1 pinout
- Note there are two variants, -1, and -2. 
- - -1, 150mm,  is nominally for sale and the -2 is a shorter version at 75mm inlcuded in some drones (for sale upon request)

Where Used:

![MCBL-00062-1](/images/other-products/cables/mcbl-00062-1.png)

- Connects ModalAI Power Module V3 output connector to FlightCoreV2 J13
- Note, J13 is intended to be RED to help indicate Power. Due to supply chain challenges, some boards might have the standard white color connector for J13.

Details:

| Component   | Details                               |
|---          |-------------------------------------- |
| Connector A | JST, GHR-04V-S                        |
| Connector B | Molex, 0050375043                     |
| Length      | 150mm (-1 variant), 75mm (-2 variant) |
| Insulator   | Silicone (flexible)                   |
| Color       | Black                                 |
| Gauge       | 26 AWG                                |
| Weight      | 1.9 grams (-1), 1.3 grams (-2)        |

Pinout:

| A   | FCv2 Side         |  B | Power Module V3 Side  |
|---  |---                |--- |---                    |
| 1   | 5V DC  (Load)     | 1  | 5V DC (Source)        |
| 2   | GND               | 2  | GND                   |
| 3   | I2C_SCL           | 3  | PM_SCL                |
| 4   | I2C_SDA           | 4  | PM_SDA                |


Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00062-1%20Drawing%20v3.pdf), [See -2 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00062-2%20Drawing%20v2.pdf).
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00063

![MCBL-00063](/images/other-products/cables/MCBL-00063.jpg)

Description:

- Cable providing the primary connection used to connect Flight Core v2 to the ModalAI UART ESC.
- The cable is effectively a 6-pin JST version of the 4-pin JST used on MCBL-00029 (instead of a VOXL 2, use a FCv2 with Dronecode format).
- Note there are two variants, -1 and -2 with the only difference in length. -1 = 200mm, -2 = 90mm


Where Used:

- ModalAI ESC (M0049/M0117/M0134) J2 to FlightCoreV2 J5 (starting 1.13.2-0.1.2)

![MCBL-00063-1](/images/flight-core-v2/m0087-modalai-esc-v2.png)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL ESC), +3.3V levels |
| Connector B | JST 1.25mm GHR-06V-S, FCv2 side, +3.3V levels |
| Length      | (-1 variant) 200mm, (-2 variant) 90mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight      | (-1 variant) 1.4 grams, (-2 variant) 0.9 grams |

Pinout:

| A   |  VOXL ESC V2/V3        |  B | FlightCore V2       |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | ESC RX       | 2  | FCv2 TX     |
| 3   | ESC TX       | 3  | FCv2 RX     |
| 4   | -        | -  | -      |
| 5   | GND      | 6  | GND   |
| 6   | -        | -  | -      |


Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00063-1%20Drawing%20v3.pdf) & [See -2 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00063-2%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00064


![MCBL-00064](/images/other-products/cables/MCBL-00064.png)

Description:

- RC Input (S.Bus, FrSky)
- Picks up Power on Red+Black, and Receive Signal on Orange from VOXL 2 I/O Board (M0065) to a R/C receiver 3-pin 0.1" hobby format
- Note: This is only intended for S.BUS (or +5V) remote receivers and cannot power high current servos or BLDCs
- Note: For Spektrum Receivers with +3.3V power supply needs, please see MCBL-00005
- This is just like MCBL-00018 but instead of needing two JST GH connectors, it has been consolidated into one 4-pin

Where Used:

![MCBL-00064-1](/images/other-products/cables/mcbl-00064-1.png)

- 4-pin JST (connector B): VOXL 2 I/O (M0065) J3 (should be black in color, supply chain permitting) to  3-pin Harwin M20 Receptacle (connector A) to RC Receiver (e.g. S.BUS/FrSky)

| Component   | Details |
|---          |--- |
| Connector B | JST GHR-04V-S, +5V Power/GND, UART_RX signal, +3.3V levels  |
| Connector A | Harwin M20-1060300, CONN RCPT HSG 3POS 2.54mm (or similar) |
| Length      | 150 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Orange (might be all black)|
| Gauge       | 26 AWG |
| Weight       | 1.5 grams |

Pinout:

| A   | RC Receiver       | B  | VOXL 2 IO (M0065)    |
|---  |---          |--- |---       |
| 1   | Orange, S.Bus Signal (Output)    | 3  | S.Bus signal (Input)   |
| 2   | Red, +5V DC (Load)            | 1  | +5V DC (Source)    |
| 3   | BLACK, GND | 4  | GND      |


[See Drawing Here]()

---

## MCBL-00065

![MCBL-00065-1](/images/other-products/cables/mcbl-00065-1.png)


Description:

- RC Input (S.Bus, FrSky)
- Picks up Power on Red+Black, and Receive Signal on Orange from VOXL 2 I/O Board (M0065) to an FrSky R-XSR receiver 5-pin Molex Picoblade format
- Note: This is only intended for S.BUS (or +5V) remote receivers and cannot power high current servos or BLDCs
- This is just like MCBL-00021-1 but for VOXL 2 IO instead of FlightCore and therefore consolidated into one 4-pin JST

Where Used:

- 4-pin JST (connector B): VOXL 2 I/O (M0065) J3 (should be black in color, supply chain permitting) to  5-pin molex Picoblade on FrSky R-SSR RC Receiver

| Component   | Details |
|---          |--- |
| Connector B | JST GHR-04V-S, +5V Power/GND, UART_RX signal, +3.3V levels  |
| Connector A | Molex 0510210500, 5-position Picoblade housing 1.25mm |
| Length      | 150 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Orange (might be all black)|
| Gauge       | 26 AWG |

Pinout:

![R-XSR-Pinout](/images/other-products/cables/mcbl-21-1-pinout.png)

MCBL-00021-1 shown but the same Picoblade pinout is used on MCBL-00065 as shown

| A   | RC Receiver       | B  | VOXL 2 IO (M0065)    |
|---  |---          |--- |---       |
| 2   | Orange, S.Bus Signal (Output)    | 3  | S.Bus signal (Input)   |
| 4   | Red, +5V DC (Load)            | 1  | +5V DC (Source)    |
| 5   | BLACK, GND | 4  | GND      |


[See Drawing Here]()

---

## MCBL-00066

![MCBL-00066](/images/other-products/cables/MCBL-00066.png)

Description:

- Cable providing the primary connection used to connect Flight Core (V1 or v2) to the ModalAI 5G Modem (M0090)
- Compatible with other plug-in boards with the 6-pin JST format as listed here.
- The cable is effectively a 6-pin JST version of the 4-pin JST used on MCBL-00067.


Where Used:

- ModalAI VOXL 2 5G Plug In Board (M0090) J9 to FlightCore V1/V2 J5 (or other DroneCode compliant UART ports on the designs, Software dependant)
- This can also be used on other ModalAI DroneCode compliant UART ports (6-pin JST GH), but Software mapping would require changes by the user.


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-06V-S, VOXL 2 side, +3.3V levels |
| Connector B | JST 1.25mm GHR-06V-S, FCv2 side, +3.3V levels |
| Length      | 250mm |
| Insulator   | Silicon (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight       | 1.9 grams |

Pinout:

| A   |  VOXL 2 J9        |  B | FlightCore V2 J5      |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | UART TX (Output)       | 3  | FCv2 UART RX (Input)     |
| 3   | UART RX (Input)       | 2  | FCv2 UART TX (Output)     |
| 4   | -        | -  | -      |
| 5   | -      | -  |     |
| 6   | GND        | 6  | GND      |


[See Drawing Here]()

---

## MCBL-00067

![MCBL-00067-1](/images/other-products/cables/mcbl-00067-1.png)

Description:

- Cable providing the primary connection used to connect Flight Core (V1 or v2) to the ModalAI USB and UART Breakout Board (M0125)
- Compatable with other plug-in boards with the 6-pin JST format as listed here.
- The cable is effectively a 4-pin JST version of the 6-pin JST used on MCBL-00066.


Where Used:

- ModalAI VOXL 2 USB and UART Breakout Board (M0125) J3 to FlightCore V1/V2 J5 (or other DroneCode compliant UART ports on the designs, Software dependant)
- This can also be used on other ModalAI DroneCode compliant UART ports (6-pin JST GH), but Software mapping would require changes by the user.
- Note: M0125 4-Pin JST UART port has RX on pin 2 and TX on pin 3, this is not common across our other HW designs (it was done intentionally, so be careful)


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-04V-S, VOXL 2 side, +3.3V levels |
| Connector B | JST 1.25mm GHR-06V-S, FCv2 side, +3.3V levels |
| Length      | 250mm |
| Insulator   | Silicon (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight       | 1.9 grams |

Pinout:

| A   |  M0125 UART Breakout J3        |  B | FlightCore V2 J5      |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | UART RX (Input)       | 2  | FCv2 UART TX (Output)     |
| 3   | UART TX (Output)       | 3  | FCv2 UART RX (Input)     |
| 4   | GND        | 6  | GND      |


[See Drawing Here]()

---

## MCBL-00068

![MCBL-00068-1](/images/other-products/cables/mcbl-00068-1.jpg)

Description:

- Cable providing a 4-pin JST USB connection from a VOXL 2 Plug-in Board (M0090, M0130, etc) to a Doodle Labs Helix Smart Radio Primary connector
- Compatable with any ModalAI 4-pin JST USB connectors on various designs
- Note: Standalone versions of our USB boards require a stable high-power source that can deliver 5W to the Helix
- Note: Doodle Labs Pinout notations in their documents has an incorrect pin1-pin15 callout. They are aware of it and will eventually fix it. We show an image below to help users understand this as our drawings use the proper JST based notations for pin 1


Where Used:

- VOXL 2 + M0090 (5G modem)
- VOXL 2 + M0130 (4G Modem, coming soon!!)
- VOXL 2 + M0078 USB Debug Board


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-04V-S, VOXL 2 side |
| Connector B | JST 1.25mm GHR-15V-S, Doodle Labs Helix side |
| Length      | 150mm |
| Insulator   | PVC (flexible)|
| Color       | Black |
| Gauge       | 26AWG (Power/GND), 28AWG USB Data |
| Weight       | 2.6 grams |

Pinout:

| A   |  VOXL 2 Side       |  B | Doodle Labs Helix 15-pin Side      |
|---  |---       |--- |---     |
| 1   | VBUS (Source)        | 1 & 2  | VCC/VBUS (Load), 2-way splice       |
| 2   | USB_D-       | 14  | D-     |
| 3   | USB_D+       | 15  | D+     |
| 4   | GND        | 3 & 4 & 13  | GND, 3-way splice      |

![MCBL-00068-pinout](/images/other-products/cables/mcbl-00068-pinout-note.png)


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00068-1%20Drawing%20v7.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00069

![MCBL-00069-1](/images/other-products/cables/mcbl-00069-1.jpg)

Description:

- Cable providing the VOXL 2 ESC UART 4-pin JST to a TMotor F55A ESC 
- Note: The TMotor F55A ESC has custom ModalAI firmware and hardware mods on it, thus allowing operation as noted here for the UART lines and the 4 MCUs. If you did not buy the TMotor ESC through ModalAI, this will not work


Where Used:

- VOXL 2 + TMotor F55A ESC

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-04V-S, VOXL 2 side, +3.3V signal levels |
| Connector B | JST 1.0mm SHR-10V-S-B, F55A ESC, +3.3V signal levels |
| Length      | 200mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 28 AWG |
| Weight       | 1.1 grams |

Pinout:

| A   |  VOXL 2 Side       |  B | TMotor Connector Side      |
|---  |---       |--- |---     |
| 1   | -        | -  |       |
| 2   | UART_TX (Output of V2)       | 1  | ESC UART RX (Input to ESC)     |
| 3   | UART_RX (Input to V2)       | 3  | ESC UART TX (Output of ESC)     |
| 4   | GND        | 10  | GND     |

This image shows the standard pinout for reference, but we use the signals as follows due to custom mods:
![MCBL-00069-pinout](/images/other-products/cables/mcbl-00069-pinout-guide.png)

- TELEM Data output set as ESC UART Input
- ESC[1:4] set as ESC UART Outputs

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00069%20Drawing%20v2.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00070

![MCBL-00070-1](/images/other-products/cables/mcbl-00070-1.jpg)

Description:

- Cable providing a full JST GH 12-pin breakout


Where Used:

- VOXL 2 J19, M0130 4G Modem J8
- Any ModalAI 12-pin JST where soldering or custom harnesses are needed
- Note: pigtail side is tinned for ~10mm making it easy to install JST SSHL-002T-P0.2 pins if needed
- Great item to have for making your own cables!!


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-12V-S |
| Connector B | None: Pigtails |
| Length      | 100mm |
| Insulator   | PVC |
| Color       | Black |
| Gauge       | 26 AWG |
| Weight       | 3.0 grams |

Pinout:

| A   |          |    |        |
|---  |---       |--- |---     |
| 1   |          | 1  | (pigtail) |
| 2   |          | 2  | (pigtail) |
| 3   |          | 3  | (pigtail) |
| 4   |          | 4  | (pigtail) |
| 5   |          | 5  | (pigtail) |
| 6   |          | 6  | (pigtail) |
| 7   |          | 7  | (pigtail) |
| 8   |          | 8  | (pigtail) |
| 9   |          | 9  | (pigtail) |
| 10   |          | 10  | (pigtail) |
| 11   |          | 11 | (pigtail) |
| 12   |          | 12  | (pigtail) |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00070-1%20Drawing%20v6.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00071

![MCBL-00071-1](/images/other-products/cables/mcbl-00071-1.jpg)

Description:

- Cable providing a full JST GH 4-pin breakout, but with Pins 2&3 as twisted pair, suited for all of ModalAI's USB2 ports


Where Used:

- VOXL or VOXL 2 Plug-In boards
- Microhard Plug-ins (M0048), Sierra 4G Plug-ins (M0030, M0130), 5G Plug-ins (M0090)
- Any ModalAI 4-pin JST where soldering or custom harnesses are needed
- Note: pigtail side is tinned for ~10mm making it easy to install JST SSHL-002T-P0.2 pins if needed
- Great item to have for making your own cables!!


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-04V-S |
| Connector B | None: Pigtails |
| Length      | 150mm |
| Insulator   | PVC |
| Color       | Black, White/Green TP |
| Gauge       | 26 AWG, 28AWG Twisted Pair (Pins 2&3) |
| Weight       | 1.3 grams |

Pinout:

| A   |          |    |        |
|---  |---       |--- |---     |
| 1   |          | 1  | (pigtail) |
| 2   |          | 2  | (pigtail, TP) |
| 3   |          | 3  | (pigtail, TP) |
| 4   |          | 4  | (pigtail) |



Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00071-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00072

![MCBL-00072-1](/images/other-products/cables/mcbl-00072-1.jpg)

Description:

- VOXL 2 10-pin JST USB to Boson RHP-BOS-VPC3-IF 6-JST
- Cable providing a direct conection from ModalAI's USB3 ports (M0090, M0130) to a FLIR Boson that uses the RHP-BOS-VPC3-IF "backpack"
- Note: Only picks up the USB2 bus and not the SuperSpeed signals, so bandwidth would be limited to the 480Mbps USB2 standard

Where Used:

- VOXL 2 Modem or USB3 expansion boards
- Sierra 4G Plug-ins (M0130), 5G Plug-ins (M0090)


Details:

| Component   | Details |
|---          |--- |
| Connector A | 10 POS 1.25mm JST, GHR-10V-S, VOXL 2 USB3 10-pin |
| Connector B | 6 POS 1.00mm JST, SHR-06V-S-B, Backpack side |
| Length      | 70mm |
| Insulator   | PVC |
| Color       | Black (VBUS/GND), White/Green Twisted Pair USB_D+/D- |
| Gauge       | Black 26 AWG, White/Green TP 28AWG |
| Weight       | 0.9 grams |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | VBUS (Source)   | 1  | 5VDC (Load) |
| 2   | USB_D-   | 6  | Boson D- |
| 3   | USB_D+  | 5  | Boson D+|
| 4   | GND      | 2  | GND    |
| 5   | -      | -  | -    |
| 6   | -      | -  | -    |
| 7   | -      | -  | -    |
| 8   | -      | -  | -    |
| 9   | -      | -  | -    |
| 10   | -      | -  |-    |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00072-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00076

![MCBL-00076-1](/images/other-products/cables/mcbl-00076-1.jpg)

Description:

- Connects Voxl2/Voxl2-Mini J19 in the Starling or Sentinel Drone Platform to Matek GPS/Magnetometer M10 unit (DroneCode compliant) and pigtails for soldering to a R/C unit UART+PWR
- This cable provides power (GNSS+RC) + UART (GNSS) + I2C (MAG) + UART (R/C)
- Note there are two variants, -1, and -3. 
- - "-1" is included in the Starling platform for shrter cable length installations.
- - "-3" is longer intended for the Sentinel platform installations.

Where Used:

- ModalAI Starling Drone, with either Voxl2 or Voxl2-Mini
- ModalAI Sentinel Drone, with either Voxl2 or Voxl2-Mini


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-12V-S, Voxl2 side, +3.3V signal levels |
| Connector B | JST 1.25mm GHR-06V-S, Module side, +3.3V signal levels |
| Connector C | Pigtails for soldering to R/C unit, +3.3V signal levels |
| Length      | 100mm (CONA-CONB), 45mm pigtails (-1) |
|             | 250mm (CONA-CONB), 155mm pigtails (-3) |
| Insulator   | Silicone |
| Color       | Black, Red, White, Yellow |
| Gauge       | 28 AWG |
| Weight      | 1.7 grams (-1), 3.4 grams (-3)  |

Pinout:

| A   |  Voxl2/ Voxl2-Mini    |  B | GNSS Module       |  C  | Pigtails                |
|---  |----------------       |--- |----------------   | --- | -----------------       |
| 1   | 5V (Source)           | 1  | 5V (Load)         | PG1 | R/C 5V (Red, Load)      |
| 2   | UART_TX               | 2  | UART_RX           |     |                         |
| 3   | UART_RX               | 3  | UART_TX           |     |                         |
| 4   | MAG_I2C_SCL           | 4  | SCL               |     |                         |
| 5   | MAG_I2C_SDA           | 5  | SDA               |     |                         |
| 6   | GND                   | 6  | GND               |     |                         |
| 10  | UART_TX               |    |                   | PG2 | R/C_RX (White)          |
| 11  | UART_RX               |    |                   | PG3 | R/C_TX (Yellow          |
| 12  | GND                   |    |                   | PG4 | R/C GND (Black)         |


Pinout, Part Number, & Additional Specs: [See -1 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00076-1%20Drawing%20v4.pdf) and [See -3 Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00076-3%20Drawing%20v5.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00078

![MCBL-00078](/images/other-products/cables/mcbl-00078-1.jpg)

Description:

- Multi-Purpose XT30 Male to Pigtail Leads, 150mm
- Easily identifiable RED (+) and GREEN (-) cables
- Robust adhesive lined heatshrink for long-lasting protection of pin solder joints


Where used:

- Included in Starling ESC for system power from battery
- Useful for ESC side or target side electronics when using an XT30-F based battery


Details:

| Component   | Details |
|---          |--- |
| Connector A | AMASS, XT30U-M |
| Connector B | Pigtails, stripped and tinned 3.5mm |
| Length      | 150mm (-1 variant) |
| Insulator   | Silicon |
| Color       | Red (+) and Green (-)  |
| Gauge       | 18 AWG |
| Weight      | 5.4 grams (-1 variant) |



| A   |          | B  |             |
|---  |---       |--- |---          |
| +   | +        | +  | RED         |
| -   | -        | -  | GREEN       |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00078-1%20Drawing%20v5.pdf).
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of these drawings.

---

## MCBL-00080

![MCBL-00080](/images/other-products/cables/mcbl-00080-1.jpg)

Description:

- USB3 10-pin JST cable to USB2 4-Pin JST GH plug

Where Used:

- Any ModalAI CCA USB downstream port (host only) with the 10-pin JST ModalAI USB3 format (examples are M0062, M0067, M0090, M0104, M0130).
- Ideal for connecting standalone modules (Microhard, LTE) to the Voxl2 Mini platform (standalone dongles still require independant power from MCBL-00017-1)

Details:

| Component   | Details |
|---          |--- |
| Connector A | 10POS 1.25mm JST, GHR-10V-S |
| Connector B | 4POS 1.25mm JST, GHR-04V-S    |
| Length      | 100mm |
| Insulator   | PVC |
| Color       | Black, White/Green TP |
| Gauge       | 26AWG VBUS/GND, 28AWG Twisted Pair Signals |
| Weight      | 1.2 grams |


Pinout:

| A  |               | B |               |
|----|---------------|---|---------------|
| 1  | VBUS (Source) | 1 | VBUS  (Load)  |
| 2  | USB_D-        | 2 | D-            |
| 3  | USB_D+        | 3 | D+            |
| 4  | GND           | 4 | GND           |
| 5  | NC            |   |               |
| 6  | NC            |   |               |
| 7  | NC            |   |               |
| 8  | NC            |   |               |
| 9  | NC            |   |               |
| 10 | NC            |   |               |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00080-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00081

![MCBL-00081-1](/images/other-products/cables/mcbl-00081-1.jpg)

Description:

- Cable providing the ESC UART connection between Voxl2 Mini and ModalAI ESC V2 (M0049, M0117, M0134).

Where Used:

- Connects VOXL 2 Mini J19 to ModalAI ESC V2 J2

Details:

| Component   | Details       |
|-------------|---------------|
| Connector A | GHR-12V-S     |
| Connector B | DF13-6S-1.25C |
| Length      | 90mm          |
| Insulator   | PVC (flexible)|
| Color       | Black         |
| Gauge       | 26AWG         |
| Weight      | 0.9 grams     |


Pinout:

| A  |          | B |            |
|----|----------|---|------------|
| 1  | NC       |   |            |
| 2  | NC       |   |            |
| 3  | NC       |   |            |
| 4  | NC       |   |            |
| 5  | NC       |   |            |
| 6  | NC       |   |            |
| 7  | UART_TX  | 2 | ESC_RX     |
| 8  | UART_RX  | 3 | ESC_TX     |
| 9  | NC       |   |            |
| 10 | NC       |   |            |
| 11 | NC       |   |            |
| 12 | GND      | 5 | GND        |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00081-1%20Drawing%20v2.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00082

![MCBL-00082-1](/images/other-products/cables/mcbl-00082-1.jpg)

Description:

- Cable providing the ESC UART connection between Voxl2 Mini and ModalAI Mini ESC (M0129).

Where Used:

- Connects VOXL 2 Mini J19 to ModalAI Mini ESC J1

Details:

| Component   | Details       |
|-------------|---------------|
| Connector A | GHR-12V-S     |
| Connector B | GHR-04V-S     |
| Length      | 90mm          |
| Insulator   | PVC (flexible)|
| Color       | Black         |
| Gauge       | 26AWG         |
| Weight      | 0.9 grams     |


Pinout:

| A  |          | B |            |
|----|----------|---|------------|
| 1  | NC       |   |            |
| 2  | NC       |   |            |
| 3  | NC       |   |            |
| 4  | NC       |   |            |
| 5  | NC       |   |            |
| 6  | NC       |   |            |
| 7  | UART_TX  | 2 | ESC_RX     |
| 8  | UART_RX  | 3 | ESC_TX     |
| 9  | NC       |   |            |
| 10 | NC       |   |            |
| 11 | NC       |   |            |
| 12 | GND      | 4 | GND        |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00082-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00083

![MCBL-00083-1](/images/other-products/cables/mcbl-00083-1.jpg)

Description:

- Cable providing a full JST GH 8-pin breakout


Where Used:

- VOXL 2 & VOXL 2 Mini J10
- Any ModalAI 8-pin JST where soldering or custom harnesses are needed
- Note: pigtail side is tinned for ~10mm making it easy to install JST SSHL-002T-P0.2 pins if needed
- Great item to have for making your own cables!!


Details:

| Component   | Details              |
|---          |---                   |
| Connector A | JST 1.25mm GHR-08V-S |
| Connector B | None: Pigtails       |
| Length      | 100mm + 10mm pigtails |
| Insulator   | PVC                  |
| Color       | Black                |
| Gauge       | 26 AWG               |
| Weight      | 2.1 grams            |

Pinout:

| A   |          |    |           |
|---  |---       |--- |---        |
| 1   |          | 1  | (pigtail) |
| 2   |          | 2  | (pigtail) |
| 3   |          | 3  | (pigtail) |
| 4   |          | 4  | (pigtail) |
| 5   |          | 5  | (pigtail) |
| 6   |          | 6  | (pigtail) |
| 7   |          | 7  | (pigtail) |
| 8   |          | 8  | (pigtail) |



Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00083-1%20Drawing%20v4.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.


## MCBL-00085

![MCBL-00085-1](/images/other-products/cables/mcbl-00085-1.jpg)

Description:

- Cable providing a 10-pin JST USB + Power connection from a VOXL 2 Plug-in Board (Sierra 4G Modem, M0130) to a Doodle Labs Helix RM-2025-62M3 Smart Radio Port 1 (Data) and Port 4 (Power) connectors
- Compatable with any ModalAI 10-pin JST USB connectors on various designs,
- Note: Only M0130 (4G Sierra Modem for Voxl2) at this time can supply the 2A needed on VBUS for the 10W max power use case reported in the device spec by Doodle Labs. Other 10-pin format designs (M0062, M0090, M0144) will work but range may be limited.


Where Used:

- VOXL 2 + M0090 (5G modem, <10W capable VBUS)
- VOXL 2 + M0130 (4G Modem, 10W capable VBUS)
- VOXL 2 + M0078 USB Debug Board (configurable VBUS)


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-10V-S, VOXL 2 Plug-In Board side |
| Connector B | JST 1.25mm GHR-06V-S, Doodle Labs Helix side, Port 4, Power |
| Connector c | JST 1.25mm GHR-04V-S, Doodle Labs Helix side, Port 1, USB Data |
| Length      | 150mm |
| Insulator   | PVC |
| Color       | Black (Power + GND), White/Green TP (USB)|
| Gauge       | 26AWG (Power/GND), 28AWG USB Data |
| Weight      | 3.7 grams |

Pinout:

| A   | VOXL2 Host (M0130, Source)   |  B    | Helix Port4 (Power, Load)    |  C | Helix Port1 (USB Data)  |
|---  |---                           |---    |---                           |--- |---                      |
| 1   | 5V VBUS (Source)             | 1,2,3 | 5V DC (Load)                 | -  | -                       |
| 2   | D-                           | -     | -                            | 2  | D-                      |
| 3   | D+                           | -     | -                            | 3  | D+                      |
| 4   | GND                          | 4     | GND                          | 4  | GND                     |
| 7   | GND                          | 5     | GND                          | -  | -                       |
| 10  | GND                          | 6     | GND                          | -  | -                       |

Image Courtesy of Doodle Labs minioem-v0323.pdf (miniOEM Mesh Rider Radio – Connector
Descriptions (2023 Update))
![MCBL-00085-pinout](/images/other-products/cables/mcbl-00085-connection-guide.jpg)


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00085-1%20Drawing%20v5.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00086

![MCBL-00086](/images/other-products/cables/mcbl-00086.jpg)


Description:

- Connects Voxl2 J19 in the Sentinel Drone Platform to Holybro GPS/Magnetometer unit (Not DroneCode compliant) and pigtails for soldering to a R/C unit UART+PWR
- This cable provides power (GNSS+RC) + UART (GNSS) + I2C (MAG) + UART (R/C)
- The case lid is opened, supplied cable is removed, and  this cable CON-B plugs in directly to the CCA inside and the case lid is re-installed.


Where Used:

- ModalAI Sentinel Drone, with Voxl2 and Holybro (Pixhawk) GNSS Module


Details:

| Component   | Details |
|---          |--- |
| Connector A | JST 1.25mm GHR-12V-S, Voxl2 side, +3.3V signal levels |
| Connector B | JST 1.0mm SHR-10V-S-B, Module side, +3.3V signal levels |
| Connector C | Pigtails for soldering to R/C unit, +3.3V signal levels |
| Length      | 250mm (CONA-CONB), 150mm pigtails |
| Insulator   | Silicone |
| Color       | Black, Red, White, Yellow |
| Gauge       | 28 AWG |
| Weight      | 3.3 grams  |

Pinout:

| A   |  Voxl2/ Voxl2-Mini    |  B | GNSS Module       |  C  | Pigtails                |
|---  |----------------       |--- |----------------   | --- | -----------------       |
| 1   | 5V (Source)           | 1  | 5V (Load)         | PG1 | R/C 5V (Red, Load)      |
| 2   | UART_TX               | 2  | UART_RX           |     |                         |
| 3   | UART_RX               | 3  | UART_TX           |     |                         |
| 4   | MAG_I2C_SCL           | 4  | SCL               |     |                         |
| 5   | MAG_I2C_SDA           | 5  | SDA               |     |                         |
| 6   | GND                   | 10 | GND               |     |                         |
| 10  | UART_TX               |    |                   | PG2 | R/C_RX (White)          |
| 11  | UART_RX               |    |                   | PG3 | R/C_TX (Yellow          |
| 12  | GND                   |    |                   | PG4 | R/C GND (Black)         |


Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00086-1%20Drawing.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00090

![MCBL-00090](/images/other-products/cables/mcbl-00090.jpg)


Description:

- Voxl Ecosystem Format Serial Debug Console Cable to Host PC USB Port
- This cable converts ModalAI "standard" 4-pin JST SRSS Debug UART port to USB using embedded FTDI adapter


Where Used:

- All over ModalAI's Ecosystem of Debug boards and plug-in boards, such as:
- - M0144 B-Quad (J6), M0130 New 4G Modem (J6, DNI'd), M0104 VOXL 2 MINI (J4), M0048 Microhard Modem (J2), M0030 4G Modem V2 (J2) 
- Serial Debug Port Settings at Host (115200 "8N1"):
- - 115,200 Baud
- - 8 bits
- - No Parity/Flow Control
- - 1 stop bit


Details:

| Component   | Details |
|---          |--- |
| Connector A | USB STD-A Plug |
| Connector B | JST 1.0mm SHR-04V-S, +3.3V signal levels |
| Length      | 1 Meter |
| Insulator   | PVC |
| Color       | Black |
| Gauge       | 28 AWG |
| Weight      | 27.4 grams  |

Pinout:

| A   |  PC/USB               |  B | Target Board with Serial Debug Port               |
|---  |----------------       |--- |--------------------------------------------       |
| 1   | VBUS                  | 1  |                                                   |
| 2   | D-                    | 2  | UART_RX, 3.3V Signal Levels                       |
| 3   | D+                    | 3  | UART_TX, 3.3V Signal Levels                       |
| 4   | DGND                  | 4  | DGND                                              |



---


## MCBL-00211

![MCBL-00211-1](/images/other-products/cables/mcbl-00211-1.jpg)

Description:

- VOXL-PM-Y for FCv2 Flight Deck
- Powers both VOXL and FlightCoreV2 from one Power Module, with the I2C bus for power monitoring going to FlightCoreV2

Where Used:

- VOXL-m500
- [VOXL J1](/VOXL-datasheet-connectors/#j1-5v-dc-in-i2c-to-power-cable-apm/) + [Flight Core V2 J13](/flight-core-v2-datasheets-connectors/#j13---power-input-and-VOXL-pm-monitoring/)to [VOXL PM v3](/power-module-datasheets/)


Details:

| Component     | Details |
|---            |--- |
| Connector A   | Molex, 0050375043 (Power Module side, 4 wires)|
| Connector B   | Molex, 0050375043 (VOXL side, 2 wires) |
| Connector C   | JST, GHR-04V-S (Flight Core side, 4 wires)  |
| Length        | 120mm |
| Insulator     | PVC (flexible) |
| Color         | Black |
| Gauge         | 20AWG (A-to-B)/ 26AWG (A-to-C) |
| Weight        | 4.0 grams                   |

Pinout:

| A   | VOXL PM        |  B | VOXL         |  C | Flight Core V2  |
|---  |---             |--- |---           |--- |---              |
| 1   | 5V DC (Source) | 1  | 5V DC (Load) | 1  | 5V DC (Load)    |
| 2   | GND            | 2  | GND          | 2  | GND             |
| 3   | SCL            | -  | -            | 3  | APM_I2C3_SCL_5V |
| 4   | SDA            | -  | -            | 4  | APM_I2C3_SDA_5V |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00211-1%20Drawing%20v3.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---

## MCBL-00218

![MCBL-00218](/images/other-products/cables/mcbl-00218-1.jpg)

Description:

- RC Input (S.Bus, FrSky)
- Picks up Power on Red+Black, and Receive Signal on Orange from FlightCoreV2 to a R/C receiver 3-pin 0.1" hobby format
- -  ModalAI conforms to the notion of Pin 1 = Signal, Pin 2 = 5V, & Pin 3 = GND for the 3-pin hobby format. Please notify us if you see anything here to the contrary.
- Note: This is only intended for R/C receivers and cannot power high current servos or BLDCs

Where Used:

- Power 4-pin JST (connector B): [Flight Core V2 J8](https://docs.modalai.com/flight-core-v2-datasheets-connectors/#j8---can-bus-connector) and R/C signal 4-pin JST (connector C): [Flight Core J12](https://docs.modalai.com/flight-core-v2-datasheets-connectors/#j12---rc-input) to  3-pin Harwin M20 Receptacle (connector A) to RC Receiver (e.g. S.Bus/FrSky)
- Note: This is not for Spektrum, which requires +3.3V. For Spektrum, please see MCBL-00005

| Component   | Details |
|---          |--- |
| Connector A | Harwin M20-1060300, CONN RCPT HSG 3POS 2.54mm  |
| Connector B | JST GHR-04V-S, for +5V Power/GND |
| Connector C | JST GHR-04V-S, for UART_RX signal, +3.3V levels |
| Length      | 150 mm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Orange |
| Gauge       | 26 AWG |
| Weight      | 1.7 grams      |

Pinout:

| A   | RC Receiver         | B  | Power            | C  | R/C Signal          |
|---  |---                  |--- |---               |--- |---                  |
| 1   | S.Bus/DSMX (Orange) |    |                  |  3 | S.Bus/DSMX (Orange) |
| 2   | 5V DC (Load)        | 1  | RED (5V, Source) |    |                     |
| 3   | BLACK (GND)         | 4  | GND              |    |                     |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00218-1%20Drawing%20v2.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.

---


## MCBL-00221

![MCBL-00221-1](/images/other-products/cables/mcbl-00221-1.jpg)

Description:

- RC Input (FrSky R-XSR)
- Picks up Power on Red+Black (from CANBUS port), and Receive Signal on Yellow (R/C Port) from FlightCoreV2 to FrSky R-XSR format R/C receiver 5-pin Picoblade format
- Note: This is just like MCBL-00021 but for FlightCoreV2 instead of FCv1

Where Used:

- 4-pin JST (connector B, yellow cable): [Flight Core V2 J12](https://docs.modalai.com/flight-core-v2-datasheets-connectors/#j12---rc-input) and 4-pin JST (connector A, Red+Black cables): [Flight Core V2 J8](https://docs.modalai.com/flight-core-v2-datasheets-connectors/#j8---can-bus-connector) to  5-pin Molex Picoblade (connector C) to FrSky R-XSR Receiver
- Note if CONA and CONB are installed in the wrong/swapped ports on FCv2, damage should not occur due to the FrSKy receiving 3.3V instead of 5V, but the UART TX of the FrSky might be over-driven by the CANBUS transciever damaging your receiver. Please install connectors carefully.

| Component   | Details |
|---          |--- |
| Connector A | JST GHR-04V-S, for +5V Power/GND  |
| Connector B | JST GHR-04V-S, for UART_RX signal, +3.3V levels |
| Connector C | Molex 0510210500, 5-position Picoblade housing 1.25mm |
| Length      | 140 mm |
| Insulator   | Silicone |
| Color       | Red/Black/Yellow |
| Gauge       | 26 AWG |
| Weight       | 1.3 grams |

Pinout:

![R-XSR-Pinout](/images/other-products/cables/mcbl-21-1-pinout.png)

| A   | Power             | C  | RC Receiver    | B  | Signal            |
|---  |---                |--- |---             |--- |---                |
| 1   | RED (5V, Source)  | 4  | 5V DC (Load)   |    |                   |
| 2   |                   | 2  | S.Bus (Yellow) | 3  | S.Bus (Yellow)    |
| 3   |                   |    |                |    |                   |
| 4   | BLACK (GND)       | 5  | GND            |    |                   |

Pinout, Part Number, & Additional Specs: [See Drawing Here](https://storage.googleapis.com/modalai_public/modal_drawings/MCBL-00221-1%20Drawing%20v4.pdf)
Contact ModalAI on the [Forum](https://forum.modalai.com/) for .DXF or .DWG formats of this drawing.
