---
layout: default
title: VOXL Mini 4-in-1 ESC Datasheet
parent: VOXL ESCs
nav_order: 1
permalink: /voxl-mini-esc-datasheet/
---

# VOXL Mini 4-in-1 ESC Datasheet
{: .no_toc }

---

## Hardware Overview

This Mini ESC is targeted for aerial vehicles under 750g. For vehicles up to 1500g, please use the [VOXL 4-in-1 ESC](/modal-esc-datasheet/).

![m0129-labels.jpg](/images/modal-esc/m0129/m0129-labels.jpg)

## Dimensions

[M0129 VOXL Mini 4-in-1 ESC 3D CAD](https://storage.googleapis.com/modalai_public/modal_drawings/M0129_ESC_4_IN_1_MICRO_20230222_FINAL.stp)
![m0129-dimensions.jpg](/images/modal-esc/m0129/m0129-dimensions.png)


## Specifications

|                        | Details                                                                                         |
|------------------------|-------------------------------------------------------------------------------------------------|
| Power Input            | 2-4S Lipo (6-18V)                                                                               |
|                        |                                                                                                 |
| Power Output Regulated | 3.8V @ 5A                                                                                       |
| Performance            |                                                                                                 |
|                        |                                                                                                 |
| Features               | Open-loop control (set desired % power)                                                         |
|                        | Closed-loop RPM control (set desired RPM), used in PX4 driver                                   |
|                        | Regenerative braking                                                                            |
|                        | Smooth sinusoidal spin-up                                                                       |
|                        | Tone generation using motors                                                                    |
|                        | Real-time RPM, temperature, voltage, current feedback via UART                                  |
| Communications         | Supported by VOXL 2, VOXL 2 Mini, VOXL Flight, VOXL and Flight Core                             |
|                        | Dual Bi-directional UART up to 2Mbit/s (3.3VDC logic-level)                                     |
|                        |                                                                                                 |
| Connectors             | 4-pin JST GH for UART communication, solder pads for 3.8V output                                |
|                        |                                                                                                 |
|                        |                                                                                                 |
|                        |                                                                                                 |
| Hardware               | MCU : STM32F051K86 @ 48Mhz, 64KB Flash                                                          |
|                        | Mosfet Driver : MP6531                                                                          |
|                        | Mosfets	: CSD17575Q3 (N-channel)                                                               |
|                        | Current Sensing : 0.5mOhm + INA186 (total current only)                                         |
|                        | ESD Protection : Yes (on UART and PWM I/O)                                                      |
|                        | Temperature Sensing : Yes                                                                       |
|                        | On-board Status LEDs : Yes                                                                      |
|                        | Weight (no wires) : 5.87g                                                                       |
|                        | Motor Connectors: N/A (solder pads)                                                             |
| PX4 Integration        | Supported in PX4 1.12 and higher                                                                |
|                        | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources              | [Manual](/modal-esc-v2-manual/)                                                                 |
|                        | [PX4 Integration User Guide](/modal-esc-px4-user-guide/)                                        |
